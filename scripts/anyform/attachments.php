<?php

if (!defined('BASEPATH')) {
	die('Access denied!');
}
/** @var \Nette\DI\Container $container */
$container = $config->getContainer();

/** @var \Nette\Http\Request $httpRequest */
$httpRequest = $container->getByType('Nette\Http\Request');

$id = $httpRequest->getPost('id');


if(!$id) {
	die('Skript neposlal žádná data.');
}

/** @var \Kdyby\Doctrine\EntityManager $em */
$em = $container->getByType('Kdyby\Doctrine\EntityManager');

$entity = $em->find(\App\Model\Entities\ReportForm::getClassName(), $id);
if(!$entity) {
	die('Neplatné ID záznamu');
}

$path = $entity->attachments;
if(!$path) {
	die("Hlášení neobsahuje přílohy.");
}
$fullPath = $container->parameters['wwwDir'].'/'.$path;

if(!file_exists($fullPath) || !is_dir($fullPath)) {
	die('Neplatná data záznamu');
}

$zipname = \Nette\Utils\Strings::webalize('report-'.$id.'-'.date("Y-m-d").($entity->name ? '-'.$entity->name.($entity->surname ? '-'.$entity->surname : '') : '').'.zip', '.');

$tmpFile = $container->parameters['tempDir'].'/zip/'.\Nette\Utils\Random::generate();
@mkdir(dirname($tmpFile), 0777, TRUE);

HZip::zipDir($fullPath, $tmpFile);

if(!file_exists($tmpFile) || !filesize($tmpFile)) {
	die ("Hlášení neobsahuje přílohy.");
}

header("Content-Type: application/zip");
header("Content-Disposition: attachment; filename=$zipname");
header("Content-Length: " . filesize($tmpFile));

readfile($tmpFile);

unlink($tmpFile);
die();


class HZip
{
	/**
	 * Add files and sub-directories in a folder to zip file.
	 * @param string $folder
	 * @param ZipArchive $zipFile
	 * @param int $exclusiveLength Number of text to be exclusived from the file path.
	 */
	private static function folderToZip($folder, &$zipFile, $exclusiveLength) {
		$handle = opendir($folder);
		while (false !== $f = readdir($handle)) {
			if ($f != '.' && $f != '..') {
				$filePath = "$folder/$f";
				// Remove prefix from file path before add to zip.
				$localPath = substr($filePath, $exclusiveLength);
				if (is_file($filePath)) {
					$zipFile->addFile($filePath, $localPath);
				} elseif (is_dir($filePath)) {
					// Add sub-directory.
					$zipFile->addEmptyDir($localPath);
					self::folderToZip($filePath, $zipFile, $exclusiveLength);
				}
			}
		}
		closedir($handle);
	}

	/**
	 * Zip a folder (include itself).
	 * Usage:
	 *   HZip::zipDir('/path/to/sourceDir', '/path/to/out.zip');
	 *
	 * @param string $sourcePath Path of directory to be zip.
	 * @param string $outZipPath Path of output zip file.
	 */
	public static function zipDir($sourcePath, $outZipPath)
	{
		$z = new ZipArchive();
		$z->open($outZipPath, ZIPARCHIVE::CREATE);
		self::folderToZip($sourcePath, $z, strlen("$sourcePath/"));
		$z->close();
	}
}
 