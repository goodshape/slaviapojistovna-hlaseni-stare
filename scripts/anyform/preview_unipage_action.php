<?php

if (!defined('BASEPATH')) {
	die('Access denied!');
}
$container = $config->getContainer();

/** @var \Nette\Http\Request $httpRequest */
$httpRequest = $container->getByType('Nette\Http\Request');

$id = $httpRequest->getPost('id');

$data = $httpRequest->getPost('data');
$locale = $httpRequest->getPost('language_id', 'cs');

if(!$id || !$data) {
	if($data) {
		die ('Náhled nelze zobrazit pro nový záznam.');
	}

	die('Skript neposlal žádná data.');
}



$xml = simplexml_load_string($data);
$contentXml = simplexml_load_string($xml->panel->record->source->content);
$nameXml = simplexml_load_string($xml->panel->record->source->name);
$content = get_localized($contentXml, $locale);
$name = get_localized($nameXml, $locale);

/** @var \Nette\Application\Application $application */
$application = $container->getByType('Nette\Application\Application');


$params = [
	'action' => 'default',
	'locale' => $locale,
	'id' => $id,
];

$request = new Nette\Application\Request(
	'SimplePage',
	'GET',
	$params,
	$httpRequest->getPost(),
	$httpRequest->getFiles(),
	array(Nette\Application\Request::SECURED => $httpRequest->isSecured())
);

$application->onPresenter[] = function($application, $presenter) {
	$presenter->autoCanonicalize = FALSE;
};

$application->onResponse[] = function($application, $response) use ($content, $name) {
	if($response instanceof \Nette\Application\Responses\TextResponse) {
		$template = $response->getSource();
		$page = $template->page;
		$page->content = $content;
		$page->name = $name;
	}
};
try {
	$application->onStartup($this); //prevents weird errors
	$application->processRequest($request);
} catch (\Nette\Application\BadRequestException $e) {
	die ('Náhled  nelze zobrazit pro neexistující nebo neaktivní stránky.');
}

function get_localized(SimpleXMLElement $root, $locale)
{
	foreach($root->node as $item) {
		if(((string) $item->language_id) === $locale) {
			return (string) $item->text;
		}
	}
	return (string) $root->node->text;
}
