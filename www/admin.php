<?php

$config = require_once __DIR__ .'/../anyshape/bootstrap.php';

$class = safeStr(url(1));
$method = safeStr(url(2));

// neni-li zadana zadna trida, presmerujeme na klienta
if (empty($class)) {
    redirect(BASEURL . "anyshape/client/");
}

// neni-li nadefinovana dana trida, error - nahrani resi autoloading
if (!class_exists(ucfirst($class))) {
    if(!class_exists($class)) {
        error('unknown_class');
    }
}
else {
    $class = ucfirst($class);
}



// neni-li vytvorena jeji instance, tak ji vytvorim
if (!isset($$class)) {
    $$class = new $class($config);
}

// existuje-li zadana metoda, tak ji zavolam
if (method_exists($$class, $method)) {
    $$class->$method();
} else {
    error('unknown_method');
}