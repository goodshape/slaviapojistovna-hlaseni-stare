/**
 * Checkbrowser for interactive guided
 *
 * WARNING - changed callbacks names!
 * wrapped to closure!
 */
(function(window) {
// Define browser strings
    var firefoxString = "Firefox";
    var chromeString = "Chrome";
    var ieString = "MSIE";
    var operaString = "OPR";
    var safariString = "Safari";

// Define supported versions, value ( supported >=), null - unsupported
// Windows
    var windowsFirefoxVersion = 16;
    var windowsChromeVersion = 23;
    var windowsOperaVersion = 17;
    var windowsIeVersion = 9;

// Ubuntu
    var ubuntuFirefoxVersion = 29;
    var ubuntuChromeVersion = 39;

// OS X
    var osxSafariVersion = 8;
    var osxFirefoxVersion = 33;
    var osxChromeVersion = 39;
    var osxOperaVersion = 26;


// Define mininal resolution
    var minWidth = 1024;
    var minHeight = 768;


// Main function call success or error
    window.supportConference = function () {

        // Check OS (mobile platform filter)
        if (checkSystem()) {

            // Get browser
            var browser = getBrowser();
            if (browser !== null) {

                // Get browser version
                var browserVersion = getBrowserVersion(browser);

                // Check supported version
                if (checkSupportedBrowserVersion(browser, browserVersion)) {

                    // Check flash plugin
                    if (checkFlashInstalled) {

                        // Check minimal resolution
                        if (checkMinimalResolution()) {

                            interactive_guide_success();
                            return;
                        }
                    }
                }
            }
        }

        interactive_guide_error();
    };


    var checkSystem = function () {

        var mobile = ['iphone', 'ipad', 'android', 'blackberry', 'nokia', 'opera mini', 'windows mobile', 'windows phone', 'iemobile'];
        for (var i in mobile) {
            if (navigator.userAgent.toLowerCase().indexOf(mobile[i].toLowerCase()) > 0) {
                return false;
            }
        }

        return true;
    };

    var getBrowser = function () {

        // IE / Firefox
        if (navigator.vendor === "") {

            if (navigator.userAgent.search("Firefox") !== -1) {

                return firefoxString;

            } else {

                return ieString;
            }

        } else if (navigator.vendor.search("Google") !== -1) {

            // Chrome
            return chromeString;
        } else if (navigator.userAgent.search(operaString) !== -1) {

            // Opera
            return operaString;
        } else if (navigator.userAgent.search(safariString) !== -1) {

            // Safari
            return safariString;
        } else {
            return null;
        }
    };

    var getBrowserVersion = function (browser) {


        if (browser === firefoxString) {

            var pattern = /(Firefox\/)([0-9]{1,2})/g;
            var string = navigator.userAgent;
            var result = string.match(pattern);

            if (result.length > 0) {

                var version = result[0].replace('Firefox/', '');
                return version;
            } else {

                return null;
            }
        } else if (browser === chromeString) {

            var pattern = /(Chrome\/)([0-9]{1,2})/g;
            var string = navigator.userAgent;
            var result = string.match(pattern);

            if (result.length > 0) {

                var version = result[0].replace('Chrome/', '');
                return version;
            } else {

                return null;
            }
        } else if (browser === ieString) {

            var pattern = /(rv:)([0-9]{1,2})/g;
            var string = navigator.userAgent;
            var result = string.match(pattern);

            if (result.length > 0) {

                var version = result[0].replace('rv:', '');
                return version;
            } else {

                return null;
            }

        } else if (browser === operaString) {

            var pattern = /(OPR\/)([0-9]{1,2})/g;
            var string = navigator.userAgent;
            var result = string.match(pattern);

            if (result.length > 0) {

                var version = result[0].replace('OPR/', '');
                return version;
            } else {

                return null;
            }

        } else if (browser === safariString) {

            var pattern = /(Version\/)([0-9]{1,2})/g;
            var string = navigator.userAgent;
            var result = string.match(pattern);

            if (result.length > 0) {

                var version = result[0].replace('Version/', '');
                return version;
            } else {

                return null;
            }

        } else {

            return null;
        }
    };

    var checkSupportedBrowserVersion = function (browser, version) {

        // Windows
        if (navigator.platform.indexOf("Win") != -1) {

            if ((browser === firefoxString && windowsFirefoxVersion !== null) && version >= windowsFirefoxVersion) {

                return true;
            } else if ((browser === chromeString && windowsChromeVersion !== null) && version >= windowsChromeVersion) {

                return true;
            } else if ((browser === ieString && windowsIeVersion !== null) && version >= windowsIeVersion) {

                return true;
            } else if ((browser === operaString && windowsOperaVersion !== null) && version >= windowsOperaVersion) {

                return true;
            } else {

                return false;
            }
        } else if (navigator.platform.indexOf("Linux") != -1) {

            // Ubuntu

            if ((browser === firefoxString && ubuntuFirefoxVersion !== null) && version >= ubuntuFirefoxVersion) {

                return true;
            } else if ((browser === chromeString && ubuntuChromeVersion !== null) && version >= ubuntuChromeVersion) {

                return true;
            } else {

                return false;
            }
        } else if (navigator.platform.indexOf("Mac") != -1) {

            // OS X

            if ((browser === firefoxString && osxFirefoxVersion !== null) && version >= osxFirefoxVersion) {

                return true;
            } else if ((browser === chromeString && osxChromeVersion !== null) && version >= osxChromeVersion) {

                return true;
            } else if ((browser === operaString && osxOperaVersion !== null) && version >= osxOperaVersion) {

                return true;
            } else if ((browser === safariString && osxSafariVersion !== null) && version >= osxSafariVersion) {

                return true;
            } else {

                return false;
            }
        }

    };

    var checkFlashInstalled = function () {

        if (typeof navigator.mimeTypes['application/x-shockwave-flash'] != 'undefined') {
            return true;
        } else {
            return false;
        }
    }

    var checkMinimalResolution = function () {

        var width = screen.width;
        var height = screen.height;

        if (width >= minWidth && height >= minHeight) {

            return true;
        } else {

            return false;
        }

    };



})(this);