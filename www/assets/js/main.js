
(function($){
	var ie = (function(){
	var undef,
	    v = 3,
	    div = document.createElement('div'),
	    all = div.getElementsByTagName('i');

    while (
        div.innerHTML = '<!--[if gt IE ' + (++v) + ']><i></i><![endif]-->',
        all[0]
    );

    return v > 4 ? v : false;
	
	}());
	var lmenu = false;
	$.fn.contactCatcher = function(options){
		var options = $.extend({
				toggle : '.contact-catcher__toggle',
				openClass : 'contact-catcher--open',
				speed : 1000,
			},options),
		self = $(this);
		method = ({
			init:function(){
				console.log("INIT catcher")
				toggle = self.find(options.toggle)
				toggle.on("click touch",function(){
					console.log("INIT catcher")
					if(self.data("open") === "true"){
						method.close()
					}else{
						method.open()
					}
				})
				$(window).on("scroll",function(){
					if($(window).scrollTop() > 1000 && !self.data("open")){
						console.log("should opens")
						method.open()
					}
				})
			},
			open:function(){
				self.data("open","true")
				self.stop().animate({
					"top" : $(window).height() - self.outerHeight()
				},options.speed)
				self.addClass(options.openClass)
			},
			close:function(){
				self.data("open","false")
				self.stop().animate({
					"top" : $(window).height()
				},options.speed)
				self.removeClass(options.openClass)					
			}
		})
		method.init();
	}
	$.fn.expander = function(options){
		var options = $.extend({
				wrapper : '.tab-box',
			},options),
			$this = $(this),
			$wrapper = $this.parents(options.wrapper),
			$tabs = $(options.tabs),
			method = ({
			init:function(){
				$this.on('click touch', function(event) {
					fromtop = getScrollOffsets().y;
					tabstop = $wrapper.offset().top;
				//console.log(fromtop + " und " + tabstop)
	            	if(fromtop>tabstop){
	            	//console.log("scrollujeme")
						$('html, body').animate({
					        scrollTop: tabstop
					    }, 500);	
	            	}
					event.preventDefault();
					$(this).parents(options.wrapper).addClass('mega');
					//$(this).css({'width':''}).css({'width':$(this).outerWidth()})
					$(this).parent().addClass('current');
				});
			},
			closeme:function(){
				$wrapper.find('.close').each(function(index, el) {					
					$(this).on('click touch', function(event) {
						//console.log('ok');
						event.preventDefault();
						$wrapper.removeClass('mega');
						$this.parent().removeClass('current');
					});
				});
			}
		})
		method.init();
		method.closeme();
	}
	$.fn.gStabs = function(options){
		var options = $.extend({
			nav : '.gs-tabnav',
			tabs: '.tab-box',
			navItem : '.tab-nav',
			},options),
			$this = $(this),
			tabs = new Object({}),
			$nav = $(options.nav),
			$tabs = $(options.tabs),
			$navItem = $nav.find(options.navItem);
		tabs = ({
			init:function(){
				var tabheight;
				var resize = function (){
					tabheight = 0;
					$tabs.css('height', '');
					$tabs.each(function(index, el) {
						if(tabheight<$(this).height()){
							tabheight = $(this).height();
						}
					});
					$tabs.height(tabheight);
					tabheight = tabheight + $nav.height();
					if($nav.find('.open').length > 0){
						$this.css({
							'height':tabheight
						})
					}
					
				}
				$(window).resize(function () { 
	                resize();
	                $this.css('height', '');
	                //console.log('th:'+tabheight)
	                if($nav.find('.open').length > 0){
						$this.css({
							'height':tabheight
						})
					}
	            });
	            resize();
				$navItem.on('click touch', function(event) {

					event.preventDefault();
					$this.animate({
						'height':tabheight
					},500)
					$navItem.each( function(index, val) {
						 $(this).removeClass('open')
					});
					$(this).addClass('open')
					var item = $(this).data('for');
					tabs.tabSwitch(item);
				});
			},
			tabSwitch:function(item){
				$tabs.each( function(index, val) {
					$(this).removeClass('open');
				});
				$this.find(options.tabs+"[data-name='"+item+"']").addClass('open')
			}
		})
		tabs.init();
	}
	$.fn.gsDropdown = function(options){
		var options = $.extend({
				topbool:false
			},options);
		$(this).each(function(){
			var	$this = $(this),
				topbool = options.topbool,
				target = $this.find('.dropdown-target'),
				trigger = $this.find('.dropdown-trigger'),
				triggerclick = $this.find('.dropdown-trigger-click'),
				trleft,left,top,trwidth,tawidth,offsets,tOut;
			$this.css({'position':'relative'})

			var resize = function (){
				target.css({'position':'absolute', "visibility": "hidden", "display": "block"});
				trleft = trigger.position().left;
				top = trigger.outerHeight();
				tawidth = target.outerWidth();
				trwidth = trigger.outerWidth();
				left = trleft-((tawidth - trwidth)/2)
				//console.log(left+' '+top)
				if(topbool){
					target.css({ 
						"visibility": "visible",
						"display": "none",
						"left" : left,
						"bottom" : top,
						"z-index":999,
					});
				}else{
					target.css({ 
						"visibility": "visible",
						"display": "none",
						"left" : left,
						"top" : top,
						"z-index":999,

					});
				}
			}
			resize();
			$(window).resize(function () { 
				resize(); 
				//console.log('resized')
			 });
			$(".report-holder").on('click touch', function(event) {
				event.preventDefault();
				/* Act on the event */
			});
			//console.log('no touch')
				trigger.on('mouseenter', function(event) {
					event.preventDefault();
					target.stop().slideDown(500)
				//console.log('trigger in')
				});	
				triggerclick.on('click touch', function(event) {
					event.preventDefault();
					target.stop().slideDown(500)
				//console.log('trigger in')
				});	

				$this.on('mouseleave', function(event) {
					event.preventDefault();
					target.stop().slideUp(500);
				//console.log('this out')
				});	
				target.on('mouseover mouseenter', function(event) {
					event.preventDefault();
				//console.log('target in')
					clearTimeout(tOut)
					target.stop().slideDown(500);
				});	
				target.on('mouseleave', function(event) {
					event.preventDefault();
				//console.log('target out')
					target.stop().slideUp(500);
				});
				trigger.on('click touchend', function(event) {
					event.preventDefault();
					target.stop().slideToggle(500)
				//console.log('trigger click')
				});

		})
	}
	$.fn.gsParaSlide = function() {
		var options = $.extend({
	            speed : 1000,
	            delay : 500,
	            duration : 8000,
	            innerwrap : '.gs-para-wrap',
	            slide : '.para-slide',
	            nav : '.gs-para-arrows'
	        },options),
        	csstransitions = false;
        if($('body').hasClass('csstransitions')){
        	csstransitions = true;
        }
        $(this).each(function(){
            var $this = $(this),
                duration = options.duration,speed = options.speed,delay=options.delay,
                nav = $this.find($(options.nav)),
                innerwrap = $this.find($(options.innerwrap)),
                slide = innerwrap.find($(options.slide)),
                slider = new Object(),
                active,loop;
            slider = ({
            	init: function (){
            		$this.find('img').each(function(){
            			$(this).css({
            				'visibility':'hidden',
            				'position':'absolute',

            			})
            			$(this).attr('src' , $(this).data('src'));
            			var width = $(this).width(),
            				me = $(this);
            			$(this).css({
            				'visibility':'',
            				'position':'',
            				'display' : 'none',
            			})

            			$(this).parent().waitForImages(function () {
            				me.slideDown(speed,function(){
            					me.css('display', '');
            				});
            			});
            		})
            		if(!$('html').hasClass('csstransitions')){
	                    slide.each(function(index, el) {
	                    	if(!$(this).hasClass('active')){
	                        	$(this).hide();
	                    	}
	                    });
	                }
        			slider.startLoop();
        			slider.navinit();
                },
                navinit : function(){
                	nav.find('.arrow-right').on('click touch', function(event) {
                		event.preventDefault();
                		slider.startLoop();
                		slider.slideSwitchNext();
                	});
                	nav.find('.arrow-left').on('click touch', function(event) {
                		event.preventDefault();
                		slider.startLoop();
                		slider.slideSwitchPrev();
                	});
                },
                startLoop : function(){
                	clearInterval(loop);
                	loop = setInterval(function(){
                		slider.slideSwitchNext();
                	},duration)
                },
                slideSwitchNext: function(){
                	var oldel = innerwrap.find($('.active')),
                		newel = (oldel.next().length>0 ? oldel.next() : innerwrap.children().first());
                		//console.log(oldel.next().length)
                	if($('html').hasClass('csstransitions')){
                		slider.slideincss(newel);
	                	slider.slideoutcss(oldel); 
                	}else{ 
	                	slider.slideinjs(newel);
	                	slider.slideoutjs(oldel); 
                	}
                },
                slideSwitchPrev: function(){
                	var oldel = innerwrap.find($('.active')),
                		newel = (oldel.prev().length>0 ? oldel.prev() : innerwrap.children().last());
                		//console.log(oldel.next().length)
                	if($('html').hasClass('csstransitions')){
                		slider.slideincss(newel);
	                	slider.slideoutcss(oldel); 
                	}else{ 
	                	slider.slideinjs(newel);
	                	slider.slideoutjs(oldel); 
                	}
                },
                // Slide animace pro staré prohlížeče - js only
                slideinjs: function(el){
                	//console.log(el)
                	el.css('display', 'block');
                	el.children('div').css({
	                		'opacity': 0,
	                		'top': -200,
	                		'position':'relative'
                	})
                	var childdelay = 1;
                	el.addClass('active').show(speed, function() {
	               		$(this).children('div').each(function() {
	               			var child = $(this);
	               			setTimeout(function(){
		               			child.animate({
			                		'top': 0,
			                		'opacity': 1,
			                	},speed)
	               			},childdelay*delay);
	               		childdelay = childdelay+0.5
	               		})
                	});
                },
                slideoutjs: function(el){
                	var childdelay = 1;
                	el.children('div').each(function() {
	               			var child = $(this);
	               			setTimeout(function(){
		               			child.css({'position':'relative'}).animate({
			                		'top': 200,
			                		'opacity': 0,
			                	},speed)
	               			},childdelay*delay);
	               		childdelay = childdelay+0.5
               		})
                	setTimeout(function(){
               			el.removeClass('active')
               		},delay)
                },
                // Slide pomocí CSS  - manipulace v main.less
                slideincss: function(el){
                	el.addClass('active')
                },
                slideoutcss: function(el){
                	el.removeClass('active')
                }
            });
        	slider.init();
		})
	}
	$.fn.menu = function(options){
		var options = $.extend({

		},options);
		var handler = $('.site-header').find('.menu-slider')
		$(this).on('click touch', function(event) {
			event.preventDefault();

			$(this).parents('.menu-inner-wrap').toggleClass('open');
		});
	}
	$.fn.menudesk = function(options){
		if (window.PointerEvent == false || window.matchMedia("(max-width: 999px)").matches) {
			//console.log('nope');
			lmenu = false;
			var handler = $('.site-header').find('.menu-slider'),
				navback = $('.site-header').find('.navigation-back'),
				ham = $('.site-header').find('.hamburger'),
				speed = 500,
				lang = new Object,
				translation = new Object;
			lang.en = {
				nav : 'Navigation',
				back : '‹ Back'
			}
			lang.vi = {
				nav : 'chuyển hướng',
				back : '‹ trở lại'
			}
			lang.ru = {
				nav : 'навигация',
				back : '‹ назад'
			}
			lang.cs = {
				nav : 'Navigace',
				back : '‹ Zpět'
			}
			if($("body").hasClass('locale-cs')){
				translation = lang.cs;
			}else if($("body").hasClass('locale-vi')){
				translation = lang.vi;
			}else if($("body").hasClass('locale-ru')){
				translation = lang.ru;
			}else{
				translation = lang.en;
			}
			ham.on('click touch', function(event) {
				navback.html(translation.nav)
				handler.animate({
					left: '0',
					"min-height":""
				},
				speed, function() {
					handler.find('.current').removeClass('current')
				});
			});
		//console.log("translation")
			navback.html(translation.nav)
			$(this).each(function(index, el) {
				var first = $(this),
					targetold = $(this).find('ul');
				targetold.each(function(){
				//console.log("removing sth");
					first.find("ul").css({ 
						"visibility": "",
						"display": "",
						"left" : "",
						"z-index":"",
						"position" :""
					})
				});
				if($(this).children('ul').length > 0){
					var heightone = $(this).children('ul').height();
					$(this).children().first().on('click touch', function(event) {
						first.addClass('current')
						heightone = first.find('ul').first().height();
						event.preventDefault();
						//console.log('Oh yep')
						navback.html(translation.back)
						handler.animate({
							left: '-100%',
							"min-height":heightone
							},
							speed, function() {
								/* stuff to do after animation is complete */
							});
						backnav.back1stlvl()
					});
					if($(this).children('ul').find('ul').length > 0){
						var heighttwo = $(this).children('ul').find('ul').height();
						$(this).children('ul').find('.submenu-listener').on('click touch', function(event) {
							$(this).parent().addClass('current-level-2')
							heighttwo = $(this).parent().find('ul').last().height();
							event.preventDefault();
							handler.animate({
								left: '-200%',
								"min-height":heighttwo
								},
								speed, function() {
									/* stuff to do after animation is complete */
								});
							backnav.back2ndlvl(heightone)
						});
					}
				}
				var backnav = {
					back1stlvl : function(){
						navback.off().on('click touch', function(event) {
							event.preventDefault();
							navback.html(translation.nav)
							handler.animate({
								left: '0',
								"min-height":""

							},
							speed, function() {
								handler.find('.current').removeClass('current')
								/* stuff to do after animation is complete */
							});
						});
					},
					back2ndlvl : function(heightone){
						navback.off().on('click touch', function(event) {
							event.preventDefault();
							handler.animate({
								left: '-100%',
								"min-height":heightone
							},
							speed, function() {
								handler.find('.current-level-2').siblings().show();
								handler.find('.current-level-2').removeClass('current-level-2');
								/* stuff to do after animation is complete */
							});
							backnav.back1stlvl()
						});
					}
				}
			});
			return;
		}
		lmenu = true;
		var i = 0;
		$(this).each(function(index, el) {
			i++;
			$(this).addClass("menu-int-"+i)
			var options = $.extend({

				},options),tOut,
				target = $(this).children('ul').first(),
				$this=$(this),
				tc= $this.attr("class"),
				trigger = $(this).children('a').first(),
				navwidth = $this.parents('nav').width(),
				posthis = $this.position().left,
				poff = $this.parents('.site-header').offset().left;

			var resize = function (){
				
				target.css({'position':'absolute', "visibility": "hidden", "display": "block"});
				navwidth = $this.parents('nav').outerWidth();
				posthis = $this.position().left;
				poff = $this.parents('.site-header').offset().left;
				var tawidth = target.outerWidth();
				//console.log('full'+(navwidth-posthis-tawidth))
				//console.log('in'+(navwidth-posthis))
				//console.log('nav'+navwidth +' pos'+posthis+"tawidth"+tawidth)
				if( navwidth < tawidth){
					left = (navwidth - posthis -(tawidth))
				}else if( navwidth - posthis < tawidth){
					left = ($this.width()-(tawidth))
				}else{
					left = 0;
				}

				target.css({ 
					"visibility": "visible",
					"display": "none",
					"left" : left,
					"z-index":999,
				});
			}
			resize();
			$(window).resize(function () { 
				if($(window).width() >=999){
					resize();
				}
					console.log('resized')
			 });
			
			
			if (Modernizr.touch) { 
				trigger.on('touchend mouseenter', function(event) {
					event.preventDefault();
					target.css('height', '');
					target.css('z-index', 9999).stop().slideToggle(500)
					$this.toggleClass("open")
					console.log('trigger in')
					$this.find('.masonry').masonry({
					  columnWidth: 0,
					  itemSelector: '.subin'
					});
				});	
				/*
				trigger.on('touchend', function(event) {
					event.preventDefault();
					target.css('height', '');
					target.css('z-index', 9999).stop().slideToggle(500)
					//console.log('trigger in')
					$this.find('.masonry').masonry({
					  columnWidth: 0,
					  itemSelector: '.subin'
					});
				});	
				$this.find(".main-nav-item").on('touchend', function(event) {
					if($(this).parent().find('ul').length>0){
						event.preventDefault();
					}
					
					clearTimeout(tOut)
					target.css('height', '');
					target.css('z-index', 9999).stop().slideToggle(500)
					console.log('trigger touch')

					$this.find('.masonry').masonry({
					  columnWidth: 0,
					  itemSelector: '.subin'
					});
				});*/
				$('html').on('touchstart mouseover', function(event) {
			        if ( !$(event.target).parents("."+tc.replace(" ", ".").replace(" ", ".")).length) {
			        	if($this.hasClass("open")){
			        		//console.log("."+tc.replace(" ", ".").replace(" ", "."))
				           target.css('z-index', 999).stop().slideUp(500);
				           $this.removeClass("open")
				           //console.log("mimo")			        		
			        	}

			       }else{
			       		console.log("siblings")
			       		//target.css('z-index', 999).stop().slideDown(500);
			       	    //$this.siblings().find("ul").first().css('z-index', 999).stop().slideUp(500);
			       }
			   	})
			}else{
				trigger.on('mouseenter', function(event) {
					event.preventDefault();
					target.css('height', '');
					target.css('z-index', 9999).stop().slideDown(500)
					//console.log('trigger in')
					$this.find('.masonry').masonry({
					  columnWidth: 0,
					  itemSelector: '.subin',
					});
				});	
				target.on('mouseover mouseenter', function(event) {
				target.css('z-index', 9999).stop().slideDown(500)
					//console.log('tar in')
				});	
				target.on('mouseleave', function(event) {
					target.stop().css('z-index', 999).slideUp(500);
					//console.log('tar out')
				});	
				$this.on('mouseleave', function(event) {
					target.css('z-index', 999).stop().slideUp(500);
					//console.log('this out')
				});	
			}
			//console.log("ons")
			
		});
		
	}
	$.fn.horizontalslider = function(options){
		var options = $.extend({

		},options);
		$(this).each(function(index, el) {
			var $this = $(this),
				arrows = $this.find('.gs-para-arrows'),
				larr = arrows.find('.arrow-left'),
				rarr = arrows.find('.arrow-right'),
				slider = $this.find('.slider'),
				speed = 1000;
			rarr.on('click touch', function(event) {
				event.preventDefault();
				method.switchnext()
			});
			larr.on('click touch', function(event) {
				event.preventDefault();
				method.switchprev()
			});
			slider.find('.slide-item').each(function(index, el) {
				$(this).on('click touch', function(event) {
					event.preventDefault();
					$(this).find('.product-list').fadeIn(500,function(){
						$(this).jScrollPane();	
					})
				});
				$(this).on('mouseleave', function(event) {
					event.preventDefault();
					$(this).find('.product-list').fadeOut(500);
				});
			});
			var method = new Object({});
			method = ({
				switchnext : function(){
					var fch = slider.children().first();
						fwidth = fch.width();
					slider.animate({
						"left" : -fwidth,
					},
						speed, function() {
						slider.append(fch).css('left', 0);
					});
				},
				switchprev : function(){
					var fch = slider.children().last();
						fwidth = fch.width();
					slider.prepend(fch).css('left', -fwidth);
					slider.animate({
						"left" : 0,
					},
						speed, function() {
						
					});
				}
			})
		});
	}
	$.fn.inputfile = function(options){
		var options = $.extend({

		},options);
		$(this).each(function(index, el) {
			var $this = $(this);
			$this.parent().append('<div class="file-input" />');
			var butin = $(this).parent().find(".file-input")
			butin.on('click touch', function(event) {
				//console.log('Okkkk')
				$this.click();
				event.preventDefault();
			});
			$this.on('change', function(event) {
				event.preventDefault();
				//console.log('change')
				butin.html($this.val().replace('C:\\fakepath\\',''))
				butin.addClass('changed')
			});
		});
	}
	var getScrollOffsets = function() {
	
	// This works for all browsers except IE versions 8 and before
    if ( window.pageXOffset != null ) {
    	////console.log('regular');
       return {
           x: window.pageXOffset, 
           y: window.pageYOffset, 
       };
	}
    // For browsers in Standards mode
    var doc = window.document;
    if ( document.compatMode === "CSS1Compat" ) {
    	////console.log('comp')
        return {
            x: doc.documentElement.scrollLeft, 
            y: doc.documentElement.scrollTop
	    };
	}
    // For browsers in Quirks mode
    ////console.log('quirks')
	    return { 
	        x: doc.body.scrollLeft,
	        y: doc.body.scrollTop
	    };  
	}
	$.fn.sidebarfixed = function(options){
		var method = new Object;
		var defaults = {
				parental:".sidebar",
				psibling:"article",
				element:'.stickme'
			};
		var options = $.extend(defaults, options);
		if($('body').find(options.parental).length<1){
			return;
		}
		var fromtop,
			parentOffset,
			maxtop,
			ison,
			elwidth,
			hidesb;
		parentOffset = $(options.parental).siblings(options.psibling).offset().top
		$(options.element).css({'position':'relative','display':'block'})
		var method = new Object;
		var elwidth = $(options.element).width();
		var elpositiontop = $(options.element).position().top
		var elementOffset = $(options.element).offset().top
		var maxtop =  $(options.parental).siblings(options.psibling).height() + parentOffset - $(options.element).height();
		method = {
			init : function(){
				elementOffset = $(options.element).offset().top
				$(window).on('touchstart',function(e){
					//e.preventDefault();
				})
				$(window).on('touchmove scroll',function(e){

					maxtop =  $(options.parental).siblings(options.psibling).outerHeight() - $(options.element).height() - (elementOffset  - parentOffset);
					fromtop = getScrollOffsets().y;
					
					method.check();
				})	
			},
			check : function(){
				//console.log($(options.element).height() + (elementOffset  - parentOffset) +' '+ maxtop)
					if( 0 < maxtop ){
						if(fromtop - elementOffset > maxtop){
							method.unstick();
							elwidth = $(options.element).width();
						}else if(fromtop+20>elementOffset){
							method.stick();
						}else {
							method.stickreset();
							elwidth = $(options.element).width();
						}
					}
			},
			stick : function(){
				$(options.element).css({'top': 0, 'position':'fixed',"width":elwidth})
				//fromtop - elementOffset +  ...
				//console.log('stick from>elof'+fromtop+ ' vs '+ elementOffset)
			},
			unstick : function(){
				$(options.element).css({'top': maxtop, 'position':'relative',"width":""});
				//console.log('unstick from>max'+maxtop)
			},
			stickreset: function(){
				$(options.element).css({'top':'', 'position':'relative',"width":""})
				elementOffset = $(options.element).offset().top
				//console.log('reset else')
			},
		}
		method.init();
	}
	$.fn.pojistenibanner = function(options){
		var options = $.extend({
			eltop : '.gs-tabs',
			elbottom : '.pojisteni-banner-home .pojisteni-banner'
		},options);
		if(window.matchMedia("(min-width: 768px)").matches && $(this).find(options.elbottom).length>0){
			var eltop = new Object,elbottom = new Object;
			eltop.el = $(this).find(options.eltop);
			elbottom.el = $(this).find(options.elbottom);

			eltop.posY = eltop.el.offset().top
			elbottom.posY = elbottom.el.offset().top

			eltop.elheight = eltop.el.height()
		//console.log(eltop)
		//console.log(elbottom)
			if(eltop.elheight+eltop.posY < elbottom.posY + 80){
			//console.log("too faaaar");
				elbottom.el.css('margin-top', -((elbottom.posY - 80 ) - (eltop.elheight+eltop.posY)) );
			}
		}
	}
	var menufixer = {
		init: function(){
			$(window).resize(function(event) {
				if($(window).width()>=999 && !lmenu && !ie){
					menufixer.resetEvents();
					$('.main-nav .main-nav-item,.nav-item').menudesk();

				}else if($(window).width()<=999 && lmenu && !ie){
					menufixer.resetEvents();
					$('.hamburger').menu();
					$('.main-nav .main-nav-item,.nav-item').menudesk();
				}
			});
		},
		resetEvents: function (){
		//console.log(ie)
			$(".navs-wrap").add("*").off();

		}
	}
	$.fn.callboxhit = function(options){
		var options = $.extend({
		},options);
		$(".callForm").css({"height":0,"overflow":"hidden"});
		$(this).on('click touch', function(event) { 
			thisel = $(this)
		//console.log('lof')
			$(".callForm").css({"height":""});
			event.preventDefault();
			$(".call-me-block").hide();
			$("#"+$(this).data('id')).stop().show(500,function(){
				$('html, body').animate({
		        scrollTop: $("#"+thisel.data('id')).offset().top
		    	}, 500);
			});			
		});
		$(".callForm .close").on('click touch', function(event) {
			event.preventDefault();
			$(this).parent().hide(500)
			$(".callForm").animate({"height":0},500);
		});
	}
	$.fn.simplereplicator = function(options){
		var options = $.extend({
		},options);
	//console.log("mkay")
		var replicatorObj = new Object();
		replicatorObj = {
			theclone: null,
			init: function(wrap) { 
				replicatorObj.theclone = wrap.find(".form-group").last().clone(true);
				wrap.find(".replicator-remove").off().on('click touch', function(event) {
					event.preventDefault();
				//console.log("bummer")
				}).addClass('disabled');
				replicatorObj.duplicator(wrap, wrap.find(".replicator-add"))

			},
			duplicator : function(wrap,me){
				me.off().on('click touch', function(event) {
					event.preventDefault();
					var clone = wrap.find(".form-group").last().clone(false,false)
					clone.find("label").remove() 
					clone.find(".replicator-remove").removeClass('disabled')
					clone.find("input").each(function(index, el) {
						var cname = $(this).attr("name");
						var cnum = parseInt(cname.match(/\d+/)[0]);
						var cid = $(this).attr("id");
						$(this).attr("id",cid.replace(cnum, cnum+1));
						cname = cname.replace(cnum, cnum+1) 
						$(this).attr("name",cname)
						if(!$(this).hasClass('replicator-remove')) $(this).val("");
					});
					clone.insertBefore(wrap.find(".pure-u-1").last())
					replicatorObj.remover(wrap,clone.find(".replicator-remove"))
				});
			},
			remover:function(wrap,me){
				me.off().on('click touch', function(event) {
				//console.log("tip tip")
					event.preventDefault();
					me.parents(".form-group").first().remove();
				});
			}
		}
		$(this).each(function(index, el) {
			var wrap = $(this);
			replicatorObj.init(wrap);
			
		});
	}
$.fn.largereplicator = function(options){
		var options = $.extend({
		},options);
	//console.log("mkay")
		var replicatorObj = new Object();
		replicatorObj = {
			theclone: null,
			init: function(wrap) { 
				replicatorObj.theclone = wrap.find(".form-group").last().clone(true);
				wrap.find(".replicator-remove").first().off().on('click touch', function(event) {
					event.preventDefault();
				//console.log("bummer")
				}).addClass('disabled');
				replicatorObj.remover(wrap,wrap.find(".replicator-remove").last())
				replicatorObj.duplicator(wrap, wrap.find(".replicator-add"))

			},
			duplicator : function(wrap,me){
				me.off().on('click touch', function(event) {
					event.preventDefault();
					var clone = wrap.find(".repeater-block").last().clone(false,false)
					clone.find(".replicator-remove").removeClass('disabled')
					var cnum;
					clone.find("input,label").each(function(index, el) {
						var cname = $(this).attr("name");
						var cid = $(this).attr("id");
						var cfor = $(this).attr("for");
						if(cname){
							cnum = parseInt(cname.match(/\d+/)[0]);
							$(this).attr("id",cid.replace(cnum, cnum+1));
							cname = cname.replace(cnum, cnum+1) 
							$(this).attr("name",cname)
						}else if(cfor){
							cnum = parseInt(cfor.match(/\d+/)[0]);
							$(this).attr("for",cfor.replace(cnum, cnum+1));
						}else{
							return;
						}
					//console.log(cnum)
						if(!$(this).hasClass('replicator-remove')) $(this).val("");
					});
					clone.find("strong").first().html(clone.find("strong").first().html().replace(cnum+1, cnum+2)) 
					clone.insertBefore(wrap.find(".pure-u-1").last())
					replicatorObj.remover(wrap,clone.find(".replicator-remove"))
				});
			},
			remover:function(wrap,me){
				me.off().on('click touch', function(event) {
				//console.log("tip tip")
					event.preventDefault();
					me.parents(".repeater-block").first().remove();
				});
			}
		}
		$(this).each(function(index, el) {
			var wrap = $(this);
			replicatorObj.init(wrap);
			
		});
	}

	$(document).ready(function() {
		//console.log(window.PointerEvent == false || window.matchMedia("(max-width: 1024px)").matches)
		$('a').each(function() {
		   var a = new RegExp('/' + window.location.host + '/');
		   if (!a.test(this.href)) {
		      $(this).attr("target","_blank");
		   }
		});
		if(ie){
			PointerEventsPolyfill.initialize({});
		}
		$('.hamburger').menu();
		$('.gs-tabs').gStabs();
		$('.slavia-box').expander();
		$('.dropdown').gsDropdown();
		$('.dropdown-top').gsDropdown({topbool:true});
		//$('.gs-para-slider').gsParaSlide();
		$('.main-nav .main-nav-item,.nav-item').menudesk();
		$('.horizontalslider').horizontalslider();
		$('input[type=file]:not(.plain)').inputfile();
		$('body').sidebarfixed();
		$(".showbox").callboxhit()
		$('.presenter-default').pojistenibanner();
		$('fieldset[replicator="items"]').simplereplicator();
		$('.repeater-group').largereplicator();
		$('.contact-catcher').contactCatcher();
		menufixer.init();

		if($(".interactive-guide").length) {
			window.supportConference();
		}
		if(window.matchMedia("(min-width: 768px)").matches){
			var run = true;
			$(window).on("scroll",function(){
				if(run){
					run = false;
					$('.gs-para-slider').gsParaSlide();
					console.log("paraslier")					
				}
				
			})
		}
		
	});
})(jQuery)

function interactive_guide_success() {
	$(".interactive-guide").show();
}

function interactive_guide_error() {
	$(".interactive-guide").hide();
}