all:
	composer install --no-dev
	rm -rf temp/cache/*
	php www/index.php orm:generate-proxies
	php www/index.php o:s:u --force