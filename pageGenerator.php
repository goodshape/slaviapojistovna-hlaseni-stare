<?php
$type = 'personal';
//$type = 'commercial';
$file = __DIR__.'/app/components/Megamenu/'.$type.'.json';

$data = json_decode(file_get_contents($file), TRUE);

$container = require_once __DIR__.'/app/bootstrap.php';

$translator = $container->getByType(\Kdyby\Translation\Translator::class);

$templatePath = __DIR__.'/app/templates/';
$template = <<<LATTE
{layout '../@productLayout.latte'}
{block #content}
<h1 class="h1" n:block="title">%title%</h1>

<p></p>

LATTE;


foreach ($data as $category) {
	$categoryLocale = explode(",",$category['locale']);

	foreach($category['children'] as $child) {
		$locale = ((array) empty($child['locale']) ? $categoryLocale : explode(",", $child['locale']));

		foreach ($locale as $lng) {
			list($presenter, $action) = explode(":", $child['link']);

			$path = $templatePath.'/'.$presenter.'/'.($lng !== 'cs' ? $lng.'/' : '').$action.'.latte';
			if(!is_dir(dirname($path))) {
				mkdir(dirname($path), 0777, TRUE);
			}
			$t = $template;
			if($lng !== 'cs') {
				$t = str_replace('../@productLayout', '../../@productLayout', $t);
			}

			if(TRUE || !file_exists($path)) {
				file_put_contents($path, str_replace('%title%', ucfirst($translator->translate('front.megamenu.'.$type.'.'.$child['title'], NULL, [], NULL, $lng)), $t));
			}
		}
	}
}
 