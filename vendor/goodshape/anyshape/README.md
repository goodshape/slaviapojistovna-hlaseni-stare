Goodshape/anyshape
=================

The Anyshape is GoodShape internal CMS system.

Installation
------------

### Source code

The best way to install Goodshape/anyshape is using  [Composer](http://getcomposer.org/):

```sh
$ composer require goodshape/anyshape:dev-master
```

### Configuration

Edit **app/config/config.neon** and add this:

```yml
extensions:
	anyshape: Goodshape\Anyshape\DI\AnyshapeExtension
```

### Database

Anyshape use **Doctrine 2** as database layer. For update database structure use:

```sh
$ php www/index.php orm:schema-tools:update --force
```
