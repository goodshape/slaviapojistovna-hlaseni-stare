<?php

namespace Goodshape\Anyshape\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class InfoCommand extends Command
{
	protected function configure()
	{
		$this->setName('anyshape:info')
			->setDescription('Show basic information about AnyShape.')
			->setHelp(<<<EOT
The <info>%command.name%</info> shows basic information about AnyShape.
EOT
			);
	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$output->writeln("[<info>OK</info>] AnyShape v1.0.0");
	}
}
