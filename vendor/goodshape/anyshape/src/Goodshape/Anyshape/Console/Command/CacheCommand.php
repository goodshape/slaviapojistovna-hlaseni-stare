<?php

namespace Goodshape\Anyshape\Console\Command;

use Nette\Utils\Finder;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class CacheCommand extends Command
{

	private $wwwDir;

	private $appDir;

	private $tempDir;


	public function __construct($wwwDir, $appDir, $tempDir)
	{
		parent::__construct();

		$this->wwwDir = $wwwDir;
		$this->appDir = $appDir;
		$this->tempDir = $tempDir;
	}


	protected function configure()
	{
		$this->setName('anyshape:cache')
			->setDescription('Clear cache of website.')
			->addArgument('type',
				InputArgument::OPTIONAL,
				'Who do you want to greet?'
			)->setHelp(<<<EOT
The <info>%command.name%</info> shows basic information about AnyShape.
EOT
			);
	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
		if (!$type = $input->getArgument('type')) {
			return $output->writeln("[<comment>Info</comment>] Missing argument, choise between options [<info>all</info>, <info>log</info>, <info>temp</info>, <info>webtemp</info>].");
		}

		switch ($type) {
			case 'log':
				$this->removeFiles($this->appDir . '/log');
				break;
			case 'temp':
				$this->removeFiles($this->tempDir . '/cache');
				$this->removeFiles($this->tempDir . '/proxies');
				$this->removeFiles($this->tempDir . '/sessions');
				break;
			case 'webtemp':
				$this->removeFiles($this->appDir . '/../www/webtemp');
				break;
			case 'all':
				$this->removeFiles($this->appDir . '/log');
				$this->removeFiles($this->tempDir . '/cache');
				$this->removeFiles($this->tempDir . '/proxies');
				$this->removeFiles($this->tempDir . '/sessions');
				$this->removeFiles($this->appDir . '/../www/webtemp');
				break;
			default:
				return $output->writeln("[<error>Error</error>] Bad argument, use only valid arguments.");
		}
		$output->writeln("[<info>OK</info>] Cache type <comment>$type</comment> was cleared.");
	}


	private function removeFiles($sourceDir, $excludesFilles = array('.htaccess', 'web.config', '.gitignore'))
	{
		foreach (Finder::findFiles("*")->exclude($excludesFilles)->from($sourceDir) as $file) {
			@unlink($file->getRealPath());
		}
	}

}
