<?php


namespace Goodshape\Anyshape\UI;


use Nette\Http\Request;

class TemplateFilters
{

	private $basePath;

	function __construct(Request $httpRequest)
	{
		$baseUrl = $httpRequest ? rtrim($httpRequest->getUrl()->getBaseUrl(), '/') : NULL;
		$this->basePath = preg_replace('#https?://[^/]+#A', '', $baseUrl);
	}


	public function wysiwyg($page)
	{
		$page = str_replace('{baseURL}', $this->basePath.'/', $page);
		return $page;


	}

} 