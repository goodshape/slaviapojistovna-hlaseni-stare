<?php


namespace Goodshape\Anyshape\UI;


use Goodshape\Anyshape\Entities\StructureEntity;
use Goodshape\Anyshape\Facades\PageFacade;
use Nette\Application\BadRequestException;
use Nette\Application\UI\Presenter;

class RedirectPresenter extends Presenter
{
	const MAX_RECURION = 10;
	/** @var PageFacade @inject */
	public $pageFacade;


	public function actionDefault($id)
	{
		$page = $this->pageFacade->get($id);

		$dest = $this->getDestination($page);
		if(is_object($dest)) {
			/** @var StructureEntity $page */
			$page = $dest;
			if($page->pageType && $page->pageType->getId() > 0) {
				$presenter = $page->pageType->getPresenter();
				if($presenter) {
					$this->redirect(':'.$presenter.':default', ['id' => $page->getId(), 'locale' => $this->getParameter('locale')]);
				}
			}
		}
		else {
			$this->redirectUrl($dest);
		}

	}

	private function getDestination($page, $recursion = 0)
	{
		if(!$page->isRedirect()) {
			return $page;
		}

		$url = $page->getUrl();

		if(!$url || $recursion > self::MAX_RECURION) {
			throw new BadRequestException();
		}

		if(is_numeric($url)) {
			return $this->getDestination($this->pageFacade->get($url));
		} else {
			return $url;
		}
	}


} 