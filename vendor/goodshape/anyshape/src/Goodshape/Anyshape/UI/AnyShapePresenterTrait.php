<?php


namespace Goodshape\Anyshape\UI;


trait AnyShapePresenterTrait
{
	/** @var \Goodshape\Anyshape\Anyshape @inject */
	public $_anyshape;

	protected function beforeRender()
	{
		parent::beforeRender();

		$this->template->website = $this->_anyshape->getWebsite();
	}


} 