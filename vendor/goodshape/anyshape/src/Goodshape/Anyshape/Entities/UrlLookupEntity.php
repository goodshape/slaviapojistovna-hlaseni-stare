<?php

namespace Goodshape\Anyshape\Entities;

use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\BaseEntity;
use Nette;


/**
 * @ORM\Entity(repositoryClass="\Goodshape\Anyshape\Dao\UrlLookupDao")
 * @ORM\Table(name="data_url_lookup_table")
 *
 * @property StructureEntity $structure
 * @property string $url
 */
class UrlLookupEntity extends BaseEntity
{

	/**
	 * @ORM\Id
	 * @ORM\Column(name="website_id", type="integer")
	 * @var int
	 */
	private $website;

	/**
	 * @ORM\ManyToOne(targetEntity="StructureEntity", inversedBy="urlLookup")
	 * @ORM\JoinColumn(name="structure_id", referencedColumnName="id", onDelete="CASCADE")
	 * @var \Goodshape\Anyshape\Entities\StructureEntity
	 */
	private $structure;

	/**
	 * @ORM\Id
	 * @ORM\Column(name="language_id", length=2)
	 * @var string
	 */
	private $language;

	/**
	 * @ORM\Id
	 * @ORM\Column(name="url_string")
	 * @var string
	 */
	private $url;


	/**
	 * @param string $language
	 * @return UrlLookupEntity
	 */
	public function setLanguage($language)
	{
		$this->language = $language;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getLanguage()
	{
		return $this->language;
	}

	/**
	 * @param int $structure
	 * @return UrlLookupEntity
	 */
	public function setStructure($structure)
	{
		$this->structure = $structure;

		return $this;
	}

	/**
	 * @return StructureEntity
	 */
	public function getStructure()
	{
		return $this->structure;
	}

	/**
	 * @param string $url
	 * @return UrlLookupEntity
	 */
	public function setUrl($url)
	{
		$this->url = $url;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getUrl()
	{
		return $this->url;
	}

	/**
	 * @param int $website
	 * @return UrlLookupEntity
	 */
	public function setWebsite($website)
	{
		$this->website = $website;

		return $this;
	}

	/**
	 * @return int
	 */
	public function getWebsite()
	{
		return $this->website;
	}


}
