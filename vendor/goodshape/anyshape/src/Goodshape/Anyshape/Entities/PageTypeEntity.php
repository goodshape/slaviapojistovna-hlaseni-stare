<?php

namespace Goodshape\Anyshape\Entities;

use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\Attributes\Identifier;
use Kdyby\Doctrine\Entities\BaseEntity;
use Nette;


/**
 * @ORM\Entity(repositoryClass="\Goodshape\Anyshape\Dao\PageTypeDao")
 * @ORM\Table(name="anyshape_page_types")
 */
class PageTypeEntity extends BaseEntity
{

	use Identifier;

	/**
	 * @ORM\Column
	 * @var string
	 */
	private $name;

	/**
	 * @ORM\Column
	 * @var string
	 */
	private $presenter;

	/**
	 * @ORM\Column(name="component_id", nullable=true)
	 * @var string
	 */
	private $component;

	/**
	 * @ORM\Column
	 * @var string
	 */
	private $icon;


	/**
	 * @param mixed $component
	 * @return PageTypeEntity
	 */
	public function setComponent($component)
	{
		$this->component = $component;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getComponent()
	{
		return $this->component;
	}

	/**
	 * @param string $icon
	 * @return PageTypeEntity
	 */
	public function setIcon($icon)
	{
		$this->icon = $icon;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getIcon()
	{
		return $this->icon;
	}

	/**
	 * @param string $name
	 * @return PageTypeEntity
	 */
	public function setName($name)
	{
		$this->name = $name;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param string $presenter
	 * @return PageTypeEntity
	 */
	public function setPresenter($presenter)
	{
		$this->presenter = $presenter;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getPresenter()
	{
		return $this->presenter;
	}


}
