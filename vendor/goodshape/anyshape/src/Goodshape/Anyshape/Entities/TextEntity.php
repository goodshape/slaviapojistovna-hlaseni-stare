<?php

namespace Goodshape\Anyshape\Entities;

use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\BaseEntity;
use Nette;


/**
 * @ORM\Entity(repositoryClass="\Goodshape\Anyshape\Dao\TextDao")
 * @ORM\Table(name="data_texts", indexes={@ORM\Index(name="text", columns={"text"})})
 */
class TextEntity extends BaseEntity
{

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @var int
	 */
	private $id;

	/**
	 * @ORM\Id
	 * @ORM\Column(name="language_id", length=2)
	 * @var string
	 */
	private $language = '';

	/**
	 * @ORM\Column(type="text")
	 * @var string
	 */
	private $text = '';


	/**
	 * @param mixed $id
	 * @return TextEntity
	 */
	public function setId($id)
	{
		$this->id = $id;

		return $this;
	}


	/**
	 * @return mixed
	 */
	public function getId()
	{
		return $this->id;
	}


	/**
	 * @param string $language
	 * @return TextEntity
	 */
	public function setLanguage($language)
	{
		$this->language = $language;

		return $this;
	}


	/**
	 * @return string
	 */
	public function getLanguage()
	{
		return $this->language;
	}


	/**
	 * @param string $text
	 * @return TextEntity
	 */
	public function setText($text)
	{
		$this->text = $text;

		return $this;
	}


	/**
	 * @return string
	 */
	public function getText()
	{
		return $this->text;
	}


}
