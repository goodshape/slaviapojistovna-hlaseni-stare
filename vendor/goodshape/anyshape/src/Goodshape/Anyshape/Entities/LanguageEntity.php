<?php

namespace Goodshape\Anyshape\Entities;

use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\BaseEntity;
use Nette;


/**
 * @ORM\Entity(repositoryClass="\Goodshape\Anyshape\Dao\LanguageDao")
 * @ORM\Table(name="anyshape_languages")
 */
class LanguageEntity extends BaseEntity
{
	const STATUS_DISABLE = 0;
	const STATUS_ENABLE = 1;

	/**
	 * @ORM\Id
	 * @ORM\Column(length=2)
	 * @var string
	 */
	private $id;

	/**
	 * @ORM\Column(length=30)
	 * @var string
	 */
	private $name;

	/**
	 * @ORM\Column(length=30)
	 * @var string
	 */
	private $adverb;

	/**
	 * @ORM\Column(length=2)
	 * @var string
	 */
	private $icon;

	/**
	 * @ORM\Column(name="active", type="smallint")
	 * @var int
	 */
	private $status = self::STATUS_DISABLE;


	/**
	 * @param mixed $id
	 * @return BaseEntity
	 */
	public function setId($id)
	{
		$this->id = $id;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param string $name
	 * @return LanguageEntity
	 */
	public function setName($name)
	{
		$this->name = $name;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param string $adverb
	 * @return LanguageEntity
	 */
	public function setAdverb($adverb)
	{
		$this->adverb = $adverb;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getAdverb()
	{
		return $this->adverb;
	}

	/**
	 * @param string $icon
	 * @return LanguageEntity
	 */
	public function setIcon($icon)
	{
		$this->icon = $icon;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getIcon()
	{
		return $this->icon;
	}

	/**
	 * @param int $status
	 * @return LanguageEntity
	 */
	public function setStatus($status)
	{
		$this->status = $status;

		return $this;
	}

	/**
	 * @return int
	 */
	public function getStatus()
	{
		return $this->status;
	}


}
