<?php

namespace Goodshape\Anyshape\Entities;

use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Goodshape\Anyshape\Mapping as GS;
use Goodshape\Anyshape\Translatable\Translation;
use Kdyby\Doctrine\Entities\Attributes\Identifier;
use Kdyby\Doctrine\Entities\BaseEntity;
use Nette;


/**
 * @ORM\Entity(repositoryClass="\Goodshape\Anyshape\Dao\StructureDao")
 * @ORM\Table(
 *    name="data_structure",
 *    indexes={
 * 		@ORM\Index(name="languages", columns={"languages"}),
 * 		@ORM\Index(name="parent_id", columns={"parent_id"}),
 * 		@ORM\Index(name="position", columns={"position"}),
 * 		@ORM\Index(name="status", columns={"status"}),
 * })
 *
 * @property StructureEntity $parent
 * @property StructureEntity[] $children
 * @property PageTypeEntity $pageType
 */
class StructureEntity extends BaseEntity
{

	use Identifier;

	const STATUS_DISABLE = 0;
	const STATUS_ENABLE = 1;


	const ROBOTS_DISABLE = 0;
	const ROBOTS_ENABLE = 1;

	/**
	 * @ORM\ManyToOne(targetEntity="\Goodshape\Anyshape\Entities\StructureEntity", inversedBy="children")
	 * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="SET NULL")
	 * @var \Goodshape\Anyshape\Entities\StructureEntity
	 */
	private $parent;

	/**
	 * @ORM\OneToMany(targetEntity="\Goodshape\Anyshape\Entities\StructureEntity", mappedBy="parent")
	 * @ORM\OrderBy({"position" = "ASC"})
	 * @var \Goodshape\Anyshape\Entities\StructureEntity[]
	 */
	private $children;

	/**
	 * @ORM\ManyToOne(targetEntity="\Goodshape\Anyshape\Entities\PageTypeEntity", fetch="EAGER")
	 * @ORM\JoinColumn(name="page_type", referencedColumnName="id", onDelete="SET NULL", nullable=true)
	 * @var \Goodshape\Anyshape\Entities\PageTypeEntity
	 */
	private $page_type;

	/**
	 * @ORM\Column(type="translate", nullable=true)
	 * @var string
	 */
	private $name;

	/**
	 * @ORM\Column(type="translate", nullable=true)
	 * @var string
	 */
	private $title;

	/**
	 * @ORM\Column(type="translate", nullable=true)
	 * @var string
	 */
	private $h1;

	/**
	 * @ORM\Column(type="translate", nullable=true)
	 * @var string
	 */
	private $description;

	/**
	 * @ORM\Column(type="translate", nullable=true)
	 * @var string
	 */
	private $keywords;

	/**
	 * @ORM\Column(type="translate", nullable=true)
	 * @var string
	 */
	private $copyright;

	/**
	 * @ORM\Column(type="smallint", nullable=true)
	 * @var int|NULL
	 */
	private $robots = self::ROBOTS_ENABLE;

	/**
	 * @ORM\Column(type="translate", nullable=true)
	 * @var string
	 */
	private $content;

	/**
	 * @ORM\Column(type="text")
	 * @var string
	 */
	private $url;

	/**
	 * @ORM\OneToMany(targetEntity="\Goodshape\Anyshape\Entities\UrlLookupEntity", mappedBy="structure")
	 * @var \Goodshape\Anyshape\Entities\UrlLookupEntity[]
	 */
	private $urlLookup;

	/**
	 * @ORM\Column(type="smallint")
	 * @var int
	 */
	private $status = self::STATUS_DISABLE;

	/**
	 * @ORM\Column(type="integer", nullable=true)
	 * @var int|NULL
	 */
	private $child_limit;

	/**
	 * @ORM\Column(type="integer")
	 * @var int
	 */
	private $position = 0;

	/**
	 * @ORM\Column(length=59, nullable=true)
	 * @var string|NULL
	 */
	private $languages;

	/**
	 * @var string
	 */
	private $language = NULL;

	/**
	 * @ORM\Column
	 * @var string
	 */
	private $host;


	/**
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getChildren()
	{
		return $this->children->matching(Criteria::create()->where(Criteria::expr()->eq('status', self::STATUS_ENABLE))
			->orderBy(['position' => Criteria::ASC]));
	}


	/**
	 * @return mixed
	 */
	public function getParent()
	{
		return $this->parent;
	}


	/**
	 * @param int $child_limit
	 * @return StructureEntity
	 */
	public function setChildLimit($child_limit)
	{
		$this->child_limit = $child_limit;

		return $this;
	}

	/**
	 * @return int
	 */
	public function getChildLimit()
	{
		return $this->child_limit;
	}

	/**
	 * @param int $content
	 * @return StructureEntity
	 */
	public function setContent($content)
	{
		$this->content = $content;

		return $this;
	}

	/**
	 * @return TextEntity
	 */
	public function getContent()
	{
		if ($this->content === NULL) {
			return '';
		}

		return $this->content;
	}

	/**
	 * @param int $copyright
	 * @return StructureEntity
	 */
	public function setCopyright($copyright)
	{
		$this->copyright = $copyright;

		return $this;
	}

	/**
	 * @return TextEntity
	 */
	public function getCopyright()
	{
		if ($this->copyright === NULL && $this->parent) {
			return $this->parent->getCopyright();
		}

		return $this->copyright;
	}

	/**
	 * @param int $description
	 * @return StructureEntity
	 */
	public function setDescription($description)
	{
		$this->description = $description;

		return $this;
	}

	/**
	 * @return TextEntity
	 */
	public function getDescription()
	{
		if ($this->description === NULL && $this->parent) {
			return $this->parent->getDescription();
		}

		return $this->description;
	}

	/**
	 * @param int $h1
	 * @return StructureEntity
	 */
	public function setH1($h1)
	{
		$this->h1 = $h1;

		return $this;
	}

	/**
	 * @return TextEntity
	 */
	public function getH1()
	{
		return $this->h1 ?: $this->name;
	}

	/**
	 * @param int $keywords
	 * @return StructureEntity
	 */
	public function setKeywords($keywords)
	{
		$this->keywords = $keywords;

		return $this;
	}

	/**
	 * @return TextEntity
	 */
	public function getKeywords()
	{
		if ($this->keywords === NULL && $this->parent) {
			return $this->parent->getKeywords();
		}

		return $this->keywords;
	}

	/**
	 * @param array $languages
	 * @return StructureEntity
	 */
	public function setLanguages($languages)
	{
		$this->languages = implode("|", array_map('trim', $languages));

		return $this;
	}

	/**
	 * @return array
	 */
	public function getLanguages()
	{
		return array_filter(explode("|", $this->languages));
	}

	/**
	 * @return string
	 */
	public function getLanguage()
	{
		if ($this->language === NULL) {
			$this->language = $this->getLanguages()[0];
		}

		return $this->language;
	}

	/**
	 * @param string $language
	 * @return StructureEntity
	 */
	public function setLanguage($language)
	{
		$this->language = $language;

		return $this;
	}

	/**
	 * @param int $name
	 * @return StructureEntity
	 */
	public function setName($name)
	{
		$this->name = $name;

		return $this;
	}

	/**
	 * @return TextEntity
	 */
	public function getName()
	{
		if ($this->name === NULL) {
			return '';
		}

		return $this->name;
	}

	/**
	 * @param int $page_type
	 * @return StructureEntity
	 */
	public function setPageType($page_type)
	{
		$this->page_type = $page_type;

		return $this;
	}

	/**
	 * @return int
	 */
	public function getPageType()
	{
		return $this->page_type;
	}

	/**
	 * @param int $parent_id
	 * @return StructureEntity
	 */
	public function setParentId($parent_id)
	{
		$this->parent_id = $parent_id;

		return $this;
	}

	/**
	 * @return int
	 */
	public function getParentId()
	{
		return $this->parent_id;
	}

	/**
	 * @param int $position
	 * @return StructureEntity
	 */
	public function setPosition($position)
	{
		$this->position = $position;

		return $this;
	}

	/**
	 * @return int
	 */
	public function getPosition()
	{
		return $this->position;
	}

	/**
	 * @param int $robots
	 * @return StructureEntity
	 */
	public function setRobots($robots)
	{
		$this->robots = $robots;

		return $this;
	}

	/**
	 * @return TextEntity
	 */
	public function getRobots()
	{
		if ($this->robots === NULL) {
			return new TextEntity();
		}

		return $this->robots;
	}

	/**
	 * @param int $status
	 * @return StructureEntity
	 */
	public function setStatus($status)
	{
		$this->status = $status;

		return $this;
	}

	/**
	 * @return int
	 */
	public function getStatus()
	{
		return $this->status;
	}


	/**
	 * @param int $title
	 * @return StructureEntity
	 */
	public function setTitle($title)
	{
		$this->title = $title;

		return $this;
	}

	/**
	 * @return TextEntity
	 */
	public function getTitle()
	{
		return $this->title ?: $this->getName();
	}

	/**
	 * @param string $host
	 * @return StructureEntity
	 */
	public function setHost($host)
	{
		$this->host = $host;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getHost()
	{
		return $this->host;
	}

	/**
	 * @param string $url
	 * @return StructureEntity
	 */
	public function setUrl($url)
	{
		$this->url = $url;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getUrl()
	{
		return $this->url;
	}

	/**
	 * @param \Goodshape\Anyshape\Entities\UrlLookupEntity $urlLookup
	 * @return StructureEntity
	 */
	public function setUrlLookup($urlLookup)
	{
		$this->urlLookup = $urlLookup;

		return $this;
	}

	/**
	 * @return \Goodshape\Anyshape\Entities\UrlLookupEntity
	 */
	public function getUrlLookup()
	{
		return $this->urlLookup->first();
	}

	public function hasPresenter()
	{
		return $this->pageType && $this->pageType->getPresenter();
	}

	public function getRedirect()
	{
		return $this->url;
	}

	public function isRedirect()
	{
		return $this->pageType === NULL && $this->url;
	}


}
