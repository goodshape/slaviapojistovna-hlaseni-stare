<?php

namespace Goodshape\Anyshape\Components;


use Nette;
use Nette\Application\UI\Control;


class BaseControl extends Control
{
	protected function createTemplate($class = NULL)
	{
		$template = parent::createTemplate($class);
		$ref = $this->getReflection()->getMethod('render')->getDeclaringClass();
		$template->setFile(dirname($ref->fileName) . DIRECTORY_SEPARATOR . lcfirst($ref->shortName) . '.latte');

		return $template;
	}


}