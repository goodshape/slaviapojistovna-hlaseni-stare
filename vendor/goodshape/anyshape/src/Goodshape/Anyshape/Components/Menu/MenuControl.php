<?php

namespace Goodshape\Anyshape\Components\Menu;


use Goodshape\Anyshape\Components\BaseControl;
use Goodshape\Anyshape\Entities\StructureEntity;
use Goodshape\Anyshape\Facades\PageFacade;


class MenuControl extends BaseControl
{

	/**
	 * @var int|string
	 */
	protected $rootNode;

	/**
	 * @var int
	 */
	protected $maxDepth;

	/**
	 * @var PageFacade
	 */
	protected $pageFacade;

	/**
	 * @var array
	 */
	protected $menuTemplates = array();


	/**
	 * @param int|string $root
	 * @param int $maxDepth
	 * @param PageFacade $pageFacade
	 */
	public function __construct($root, $maxDepth = 0, PageFacade $pageFacade)
	{
		$this->rootNode = $root;
		$this->maxDepth = $maxDepth;
		$this->pageFacade = $pageFacade;
	}


	public function render()
	{
		$this->template->pages = $this->getPages();

		foreach ($this->menuTemplates as &$menuTemplate) {
			if ($menuTemplate instanceof \Nette\Templating\IFileTemplate) {
				$menuTemplate = $menuTemplate->getFile();
			}
			if (!file_exists($menuTemplate)) {
				throw new \RuntimeException("Menu template '{$menuTemplate}' does not exist.");
			}
		}

		$this->template->menuTemplates = $this->menuTemplates;

		$this->template->maxDepth = $this->maxDepth;
		$this->template->render();
	}

	public function addMenuTemplate($path)
	{
		$this->menuTemplates[] = $path;
	}


	public function getMenuTemplates()
	{
		return $this->menuTemplates;
	}

	/**
	 * @return int|string
	 */
	public function getRootNode()
	{
		return $this->rootNode;
	}

	/**
	 * @param int|string $rootNode
	 */
	public function setRootNode($rootNode)
	{
		$this->rootNode = $rootNode;
	}

	protected function getPages()
	{
		if (is_numeric($this->rootNode) && $this->rootNode > 0) {
			return (array) $this->pageFacade->getChildrenFromId($this->rootNode);
		} else {
			return  (array) $this->pageFacade->getChildrenFromUrl($this->rootNode);
		}
	}

	public function getChildren(StructureEntity $entity)
	{
		return $entity->getChildren();
	}


}
