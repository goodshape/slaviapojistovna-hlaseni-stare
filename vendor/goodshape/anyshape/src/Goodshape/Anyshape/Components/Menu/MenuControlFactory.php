<?php

namespace Goodshape\Anyshape\Components\Menu;


interface MenuControlFactory
{
	/**
	 * @return \Goodshape\Anyshape\Components\Menu\MenuControl
	 */
	function create($parentId, $maxDepth);
}