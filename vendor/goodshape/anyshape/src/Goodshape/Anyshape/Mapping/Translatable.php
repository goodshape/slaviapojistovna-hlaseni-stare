<?php

namespace Goodshape\Anyshape\Mapping;

use Doctrine\Common\Annotations\Annotation;


/**
 * @Annotation
 * @Target({"PROPERTY","ANNOTATION"})
 */
final class Translatable extends Annotation
{

	/** @var boolean */
	public $translatable = TRUE;

}

