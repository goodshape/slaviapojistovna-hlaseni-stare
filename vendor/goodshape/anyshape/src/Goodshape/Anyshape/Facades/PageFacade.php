<?php

namespace Goodshape\Anyshape\Facades;

use Goodshape\Anyshape\Entities\StructureEntity;
use Nette;


class PageFacade extends Nette\Object
{

	/**
	 * @var \Goodshape\Anyshape\Dao\StructureDao
	 */
	protected $structureDao;

	/**
	 * @var \Nette\Http\Request
	 */
	protected $httpRequest;

	/**
	 * @var \Goodshape\Anyshape\Entities\StructureEntity
	 */
	private $website;

	/**
	 * @var \Goodshape\Anyshape\Entities\StructureEntity
	 */
	private $page;


	/**
	 * @param \Goodshape\Anyshape\Dao\StructureDao $structureDao
	 * @param \Goodshape\Anyshape\Dao\LanguageDao $languageDao
	 */
	public function __construct(\Goodshape\Anyshape\Dao\StructureDao $structureDao, \Nette\Http\Request $httpRequest)
	{
		$this->structureDao = $structureDao;
		$this->httpRequest = $httpRequest;
	}


	/**
	 * @param string|NULL $host
	 * @return \Goodshape\Anyshape\Entities\StructureEntity|NULL
	 */
	public function getWebsite()
	{
		if (!$this->website instanceof StructureEntity) {
			$host = array('', $this->httpRequest->getUrl()->getHost());
			$qb = $this->structureDao->createQueryBuilder('s');
			$qb->select('s')
				->andWhere('s.status = :status')
				->andWhere('s.host IN (:host)')
				->andWhere('s.parent = 0 OR s.parent IS NULL')
				->setParameters([
					'status' => StructureEntity::STATUS_ENABLE,
					'host' => $host,
				]);
			$this->website = $qb->getQuery()->getSingleResult();
		}

		return $this->website;
	}


	/**
	 * @param string $url
	 * @return \Goodshape\Anyshape\Entities\StructureEntity|NULL
	 */
	public function getPage($url)
	{
		if (!$this->page instanceof StructureEntity) {
			$this->page = $this->getPageFromUrl($url);
		}

		return $this->page;
	}

	public function get($id)
	{
		return $this->structureDao->find($id);
	}

	public function getPageByIdLang($id, $lang)
	{
		return $this->structureDao->createQueryBuilder('s')
			->select("s")
			->where('s.id = :id')
			->setParameter('id', $id)
			->andWhere('s.languages LIKE :language OR s.languages IS NULL')
			->setParameter('language', "%$lang%")
			->getQuery()
			->getOneOrNullResult();
	}


	/**
	 * @param string $url
	 * @return \Goodshape\AnyShape\Entities\StructureEntity|NULL
	 */
	public function getPageFromUrl($url)
	{
		return $this->structureDao->createQueryBuilder('structure')
			->innerJoin('structure.urlLookup', 'urlLookup')
			->andWhere('urlLookup.url = :url')
			->setParameter('url', $url)
			->andWhere('urlLookup.language = :language')
			->setParameter('language', $this->getWebsite()->getLanguage())
			->getQuery()
			->getOneOrNullResult();
	}


	/**
	 * @param string $url
	 * @return \Doctrine\Common\Collections\Collection|NULL
	 */
	public function getChildrenFromUrl($url)
	{
		if ($page = $this->getPageFromUrl($url)) {
			return $page->getChildren();
		}

		return NULL;
	}


	/**
	 * @param int $id
	 * @return \Goodshape\Anyshape\Entities\StructureEntity|NULL
	 */
	public function getChildrenFromId($id)
	{
		return $this->structureDao->findBy(
			[
				'parent' => (int)$id,
				'status' => StructureEntity::STATUS_ENABLE,
			],
			[
				'position' => 'ASC'
			]
		);
	}

}