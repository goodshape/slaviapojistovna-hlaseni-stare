<?php

namespace Goodshape\Anyshape;


interface Exception
{

}


class InvalidArgumentException extends \InvalidArgumentException implements Exception
{

}


class InvalidStateException extends \RuntimeException implements Exception
{

}
