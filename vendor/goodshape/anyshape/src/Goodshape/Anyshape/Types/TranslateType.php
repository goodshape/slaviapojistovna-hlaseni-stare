<?php

namespace Goodshape\Anyshape\Types;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;
use Goodshape\Anyshape\Translatable\Translation;


class TranslateType extends Type
{
	const MONEY = 'translate';


	public function getSqlDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
	{
		return 'INT';
	}

	public function convertToPHPValue($value, AbstractPlatform $platform)
	{
		if ($value) {
			return new Translation((int)$value);
		}

		return NULL;
	}

	public function convertToDatabaseValue($value, AbstractPlatform $platform)
	{
		if (is_object($value)) {
			return (int)$value->getId();
		}

		return NULL;
	}

	public function getName()
	{
		return self::MONEY;
	}

	/*
		public function canRequireSQLConversion()
		{
			return TRUE;
		}

		public function convertToPHPValueSQL($sqlExpr, $platform)
		{
			return 'MyMoneyFunction(\''.$sqlExpr.'\') ';
		}

		public function convertToDatabaseValueSQL($sqlExpr, AbstractPlatform $platform)
		{
			return 'MyFunction('.$sqlExpr.')';
		}
	*/

}