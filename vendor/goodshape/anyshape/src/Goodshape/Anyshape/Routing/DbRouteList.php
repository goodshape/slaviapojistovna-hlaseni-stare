<?php

namespace Goodshape\Anyshape\Routing;

use Goodshape;
use Nette;
use Nette\Application;


class DbRouteList extends Nette\Utils\ArrayList implements Nette\Application\IRouter
{
	const PRESENTER_KEY = 'presenter';
	const MODULE_KEY = 'module';

	/**
	 * @var \Kdyby\Doctrine\EntityManager
	 */
	private $entityManager;


	/**
	 * @param \Kdyby\Doctrine\EntityManager $entityManager
	 */
	function __construct(\Kdyby\Doctrine\EntityManager $entityManager)
	{
		$this->entityManager = $entityManager;
	}


	/**
	 * @param Nette\Http\IRequest $httpRequest
	 * @return Application\Request|NULL
	 */
	function match(Nette\Http\IRequest $httpRequest)
	{
		foreach ($this as $route) {
			if ($route instanceof DbRoute) {
				/** @var \Goodshape\Anyshape\Routing\DbRoute $route */
				$route->dao = $this->entityManager->getDao($route->entityName);
			}

			$appRequest = $route->match($httpRequest);
			if ($appRequest !== NULL) {
				return $appRequest;
			}
		}

		return NULL;
	}


	/**
	 * Constructs absolute URL from Request object.
	 * @return string|NULL
	 */
	function constructUrl(Nette\Application\Request $appRequest, Nette\Http\Url $refUrl)
	{
		foreach ($this as $route) {
			if ($route instanceof DbRoute) {
				/** @var \Goodshape\Anyshape\Routing\DbRoute $route */
				$route->dao = $this->entityManager->getDao($route->entityName);
			}

			$url = $route->constructUrl($appRequest, $refUrl);
			if ($url !== NULL) {
				return $url;
			}
		}

		return NULL;
	}


	/**
	 * Adds the router.
	 * @param  mixed
	 * @param  Nette\Application\IRouter
	 * @return void
	 */
	public function offsetSet($index, $route)
	{
		if (!$route instanceof Nette\Application\IRouter) {
			throw new Nette\InvalidArgumentException("Argument must be IRouter descendant.");
		}
		parent::offsetSet($index, $route);
	}

}