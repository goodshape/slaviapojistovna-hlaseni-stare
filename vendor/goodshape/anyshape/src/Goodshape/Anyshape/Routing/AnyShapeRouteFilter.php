<?php


namespace Goodshape\Anyshape\Routing;


use Doctrine\ORM\NoResultException;
use Goodshape\Anyshape\Dao\UrlLookupDao;
use Goodshape\Anyshape\Entities\StructureEntity;
use Goodshape\Anyshape\Entities\UrlLookupEntity;
use Nette\Application\Routers\Route;
use Nette\Application\UI\Presenter;
use Nette\Caching\Cache;
use Nette\Caching\IStorage;
use Nette\Caching\Storages\MemoryStorage;

/**
 * Class AnyShapeRouteFilter
 * @package Goodshape\Anyshape\Routing
 *
 * Offers usage of route filters defined in AnyShape
 *
 * Since AnyShape only allows defining of Presenter for slug URL, default action is called on used presenter.
 *
 * <code>
 * $router[] = new Route('page/<slug>', [
 *     'presenter' => 'SimplePage',
 *     NULL => $anyshapeRouteFilter->getFilters(), //enables global filters IN and OUT
 *   ]
 * );
 */
class AnyShapeRouteFilter
{
	/** @var UrlLookupDao */
	private $urlLookupRepository;
	private $slug = 'slug';
	private $id = 'id';

	private $localeFallback = 'cs';

	public function __construct(UrlLookupDao $urlLookupRepository, IStorage $cacheStorage)
	{
		$this->urlLookupRepository = $urlLookupRepository;
		$this->cache = new Cache($cacheStorage, 'Goodshape.Anyshape.Routing.Filter');
	}


	public function getFilters()
	{
		return [
			Route::FILTER_IN => $this->getInFilter($this->slug, $this->id, Presenter::DEFAULT_ACTION),
			Route::FILTER_OUT => $this->getOutFilter($this->slug, $this->id, Presenter::DEFAULT_ACTION),
		];
	}

	public function getInFilter($slug, $id, $action)
	{

		return function ($params) use ($slug, $id, $action) {

			if (empty($params[$slug])) {
				return NULL;
			}
			list($entityId, $presenter) = $this->cache->load('slug' . $params[$slug].$this->getLocale($params),
				function (&$dep) use ($params, $slug) {
					$dep = [
						Cache::EXPIRATION => '+15min',
						Cache::SLIDING => FALSE,
					];
					$entity = $this->getEntityBySlug($params[$slug], $this->getLocale($params));
					/** @var StructureEntity $structure */
					if(!$entity || !($structure = $entity->getStructure())) {
						return NULL;
					}
					if($structure->isRedirect()) {
						return [$structure->getId(), 'Anyshape:Redirect'];
					}

					if(!$structure->getPageType() || !$structure->getPageType()->getPresenter()) {
						return NULL;
					}

					return [$entity->structure->id, $entity->structure->pageType->presenter];
				});
			if (!$entityId || !$presenter) {
				return NULL;
			}

			$params['id'] = $entityId;
			$params[Presenter::ACTION_KEY] = $action;
			$params['presenter'] = $presenter;
			unset($params['slug']);

			return $params;
		};
	}

	public function getOutFilter($slug, $id, $action)
	{
		return function ($params) use ($slug, $id, $action) {
			if (!isset($params[$id])) {
				return NULL;
			}
			$url = $this->cache->load('id' . $params[$id].$this->getLocale($params), function (&$dep) use ($params, $id) {
				$dep = [
					Cache::EXPIRATION => '+15min',
					Cache::SLIDING => FALSE,
				];
				if($params['action'] !== 'default') {
					return NULL; //as can route only to default
				}
 				$entity = $this->getEntityById($params[$id], $this->getLocale($params));

				if(!$entity || !($structure = $entity->getStructure())) {
					return NULL;
				}

				if(strtolower($params['presenter']) === 'anyshape:redirect' && $structure->isRedirect()) {
					return $entity->url;
				}

				if(!($pageType = $structure->getPageType()) || !($presenter = $pageType->getPresenter()) || $presenter !== $params['presenter']) {
					return NULL;
				}

				return $entity->url;
			});

			if (!$url) {
				return NULL;
			}


			$params[$slug] = $url;
			unset($params[$id]);
			unset($params[Route::PRESENTER_KEY]);
			unset($params[Presenter::ACTION_KEY]);

			return $params;
		};
	}

	private function getEntityBySlug($slug, $locale)
	{
		try {
			return $this->prepareQuery()
				->andWhere('u.url = :url')
				->setParameter('url', $slug)
				->setParameter('locale', $locale)
				->getQuery()->getSingleResult();
		} catch (NoResultException $e) {
			return NULL;
		}
	}

	/**
	 * @param $id
	 * @param $locale
	 * @return UrlLookupEntity
	 *
	 */
	private function getEntityById($id, $locale)
	{
		try {
			/** @var UrlLookupEntity $entity */
			return $this->prepareQuery()
				->andWhere('s.id = :id')
				->setParameter('id', $id)
				->setParameter('locale', $locale)
				->getQuery()
				->getSingleResult();
		} catch (NoResultException $e) {
			return NULL;
		}
	}

	private function prepareQuery()
	{
		return $this->urlLookupRepository->createQueryBuilder('u')
			->select('u,s,t')
			->join('u.structure', 's')
			->leftJoin('s.page_type', 't')
			->andWhere('u.language = :locale');
	}

	private function getLocale($params) {
		return isset($params['locale']) ? $params['locale'] : $this->localeFallback;
	}

} 