<?php

namespace Goodshape\Anyshape\Routing;

use Nette;
use Nette\Application;

/**
 * Class StaticRoute
 * @package Goodshape\Anyshape\Routing
 *
 * Static router allows load of mapping definition from neon file
 *
 * Neon should be structured in format slug:"Presenter:action", eg.
 * ```
 * prihlaseni: "Sign:in"
 * odhlaseni: "Sign:out"
 * ```
 *
 * To enable this router, add it in your RouteFactory
 *
 */
class StaticRoute extends Nette\Object implements Nette\Application\IRouter
{
	const PRESENTER_KEY = 'presenter';
	const MODULE_KEY = 'module';


	/**
	 * @var string[]
	 */
	private $urls;


	/** @var int */
	private $flags;


	/**
	 * @param  string $file config neon file
	 * @param  int $flags flags
	 */
	function __construct($file, $flags = 0)
	{
		$this->urls = (array)Nette\Neon\Neon::decode(file_get_contents($file));
		$this->flags = $flags;
	}


	/**
	 * Maps HTTP request to a Request object.
	 * @param Nette\Http\IRequest $httpRequest
	 * @return Application\Request|NULL
	 */
	public function match(Nette\Http\IRequest $httpRequest)
	{
		if (($url = $httpRequest->getUrl()->getPathInfo()) === '' || !array_key_exists($url, $this->urls)) {
			return NULL;
		}

		$params = $httpRequest->getQuery();

		$presenter = $this->urls[$url];
		$a = strrpos($presenter, ':');
		if (!$a) {
			throw new Nette\InvalidArgumentException("Argument must be array or string in format Presenter:action, '$presenter' given.");
		}
		$params['action'] = $a === strlen($presenter) - 1 ? Application\UI\Presenter::DEFAULT_ACTION : substr($presenter,
			$a + 1);
		unset($params[self::PRESENTER_KEY]);

		return new Application\Request(
			substr($presenter, 0, $a),
			$httpRequest->getMethod(),
			$params,
			$httpRequest->getPost(),
			$httpRequest->getFiles(),
			array(Application\Request::SECURED => $httpRequest->isSecured())
		);
	}

	/**
	 * Constructs absolute URL from Request object.
	 * @param Application\Request $appRequest
	 * @param Nette\Http\Url $refUrl
	 * @return string|NULL
	 */
	public function constructUrl(Nette\Application\Request $appRequest, Nette\Http\Url $refUrl)
	{
		if ($this->flags & self::ONE_WAY) {
			return NULL;
		}
		$params = $appRequest->getParameters();

		// presenter and action name
		$presenter = $appRequest->getPresenterName() . ':' . $params['action'];

		if (($url = array_search($presenter, $this->urls)) === FALSE) {
			return NULL;
		}
		unset($params['action']);

		$url = ($this->flags & self::SECURED ? 'https://' : 'http://') . $refUrl->getAuthority() . $refUrl->getPath() . $url;
		$sep = ini_get('arg_separator.input');
		$query = http_build_query($params, '', $sep ? $sep[0] : '&');
		if ($query != '') { // intentionally ==
			$url .= '?' . $query;
		}

		return $url;
	}


}