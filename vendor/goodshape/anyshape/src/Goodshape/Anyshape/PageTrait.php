<?php

namespace Goodshape\Anyshape;


use Goodshape\Anyshape\Entities\PageTypeEntity;
use Goodshape\Anyshape\Entities\StructureEntity;

trait PageTrait
{

	/**
	 * @inject
	 * @var \Goodshape\Anyshape\Anyshape
	 */
	public $anyshape;

	/** @var StructureEntity */
	protected $page;

	protected function beforeRender()
	{
		parent::beforeRender();
		$this->template->page = $this->anyshape->getPage();
		$this->page = $this->anyshape->getPage();
	}

	public function renderDefault($id)
	{
		//needed for correct processing of ID param (link to this ex.)
	}

}