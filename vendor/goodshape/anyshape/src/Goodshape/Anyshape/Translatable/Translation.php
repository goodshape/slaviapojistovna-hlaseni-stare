<?php


namespace Goodshape\Anyshape\Translatable;


use Nette\Object;

class Translation extends Object
{
	/** @var string */
	private $value;
	/** @var int */
	private $id;
	/** @var string */
	private $locale;

	private $loaded = FALSE;

	public $callback;

	function __construct($id, $locale = NULL, $value = NULL)
	{
		$this->id = $id;
		$this->locale = $locale;
		$this->value = $value;
	}

	private function load()
	{
		if ($this->loaded) {
			return;
		}

		$this->loaded = TRUE;

		$this->value = call_user_func($this->callback, $this);

	}

	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @return string
	 */
	public function getLocale()
	{
		return $this->locale;
	}

	/**
	 * @param string $locale
	 */
	public function setLocale($locale)
	{
		$this->locale = $locale;
	}

	/**
	 * @return string
	 */
	public function getValue()
	{
		$this->load();

		return $this->value;
	}

	/**
	 * @param string $value
	 */
	public function setValue($value)
	{
		$this->load();
		$this->value = $value;
	}

	public function __toString()
	{
		$this->load();

		return (string)$this->getValue();
	}


} 