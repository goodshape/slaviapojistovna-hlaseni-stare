<?php

namespace Goodshape\Anyshape\Translatable;

use Doctrine;
use Doctrine\Common\EventArgs;
use Doctrine\ORM\Query\Expr;
use Goodshape;
use Kdyby;
use Nette;


class TranslatableListener extends Nette\Object implements Kdyby\Events\Subscriber
{

	const TRANSLATE = 'translate';

	const TRANSLATE_ENTITY = 'Goodshape\\Anyshape\\Entities\\TextEntity';

	/**
	 * @var \Goodshape\Anyshape\Dao\TextDao
	 */
	protected $textDao;
	/**
	 * @var \Nette\Application\Application
	 */
	private $application;
	private $ids;
	private $loaded = [];


	function __construct(Nette\Application\Application $application)
	{
		$this->application = $application;
	}


	/**
	 * Returns an array of events this subscriber wants to listen to.
	 *
	 * @return array
	 */
	public function getSubscribedEvents()
	{
		return array(
			'postLoad',
			/*  'postPersist',
				'preFlush',
				'onFlush',
				'loadClassMetadata'*/
		);
	}


	/**
	 * @param \Doctrine\ORM\Event\LifecycleEventArgs $args
	 */
	public function postLoad(EventArgs $args)
	{
		$entity = $args->getEntity();

		if (get_class($entity) === self::TRANSLATE_ENTITY) {
			return;
		}

		$this->textDao = $args->getEntityManager()->getRepository(self::TRANSLATE_ENTITY);

		$meta = $args->getEntityManager()->getClassMetadata(get_class($entity));

		foreach ($meta->getFieldNames() as $field) {
			if ($meta->getFieldMapping($field)['type'] === self::TRANSLATE && !empty($entity->$field) && $entity->$field->getId()) {
				if (!$entity->$field instanceof Translation) {
					throw new Kdyby\Doctrine\InvalidStateException("Column $field is mapped as translatable but the field value invalid.");
				}
				$this->ids[] = $entity->$field->getId();
				$entity->$field->setLocale($this->getLocale());
				$entity->$field->callback = [$this, 'getText'];
			}
		}
	}

	public function getText(Translation $field)
	{

		$key = $field->getId();

		if (!isset($this->loaded[$key])) {


			$missing = array_diff(($this->ids), array_keys($this->loaded));

			$this->load($missing);
		}

		return isset($this->loaded[$key]) ? $this->loaded[$key] : '';
	}


	/**
	 * @return mixed
	 */
	private function getLocale()
	{
		$presenter = $this->application->getPresenter();
		if ($presenter && isset($presenter->locale)) {
			return $presenter->locale;
		}

		return 'cs'; //fallback
	}

	private function load($missing)
	{
		if ($missing && $translates = $this->textDao->findAssoc(array('id' => array_values($missing), 'language' => $this->getLocale()),
				'id')
		) {
			foreach ($translates as $id => $translation) {
				$this->loaded[$id] = $translation->text;
			}
		}
	}

}