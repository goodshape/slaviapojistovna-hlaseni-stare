<?php


namespace Goodshape\Anyshape\Events;


use Doctrine\ORM\Event\LoadClassMetadataEventArgs;
use Doctrine\ORM\Events;
use Kdyby\Doctrine\Mapping\ClassMetadata;
use Kdyby\Events\Subscriber;

class TablePrefixListener implements Subscriber
{

	private $prefix;

	private $excludes = [];

	function __construct($prefix, $excludes)
	{
		$this->excludes = $excludes;
		$this->excludes[] = $prefix;
		$this->prefix = $prefix;
	}


	/**
	 * Returns an array of events this subscriber wants to listen to.
	 *
	 * @return array
	 */
	public function getSubscribedEvents()
	{
		return [Events::loadClassMetadata];
	}

	public function loadClassMetadata(LoadClassMetadataEventArgs $eventArgs)
	{
		/** @var ClassMetadata $classMetadata */
		$classMetadata = $eventArgs->getClassMetadata();
		$tableName = $classMetadata->getTableName();
		if ($this->shouldPrefix($tableName)) {
			$classMetadata->setTableName($this->prefix . $tableName);

		}
		foreach ($classMetadata->getAssociationMappings() as $fieldName => $mapping) {
			if ($mapping['type'] == \Doctrine\ORM\Mapping\ClassMetadataInfo::MANY_TO_MANY) {
				if (isset($classMetadata->associationMappings[$fieldName]) && !empty($classMetadata->associationMappings[$fieldName]['joinTable'])) {
					$mappedTableName = $classMetadata->associationMappings[$fieldName]['joinTable']['name'];
					if ($this->shouldPrefix($mappedTableName)) {
						$classMetadata->associationMappings[$fieldName]['joinTable']['name'] = $this->prefix . $mappedTableName;
					}
				}
			}
		}
	}

	private function shouldPrefix($tableName)
	{
		foreach ($this->excludes as $exclude) {
			if (strpos($tableName, $exclude) === 0) {
				return FALSE;
			}
		}

		return TRUE;
	}


}