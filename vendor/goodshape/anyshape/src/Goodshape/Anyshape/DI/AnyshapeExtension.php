<?php

namespace Goodshape\Anyshape\DI;

use Kdyby\Doctrine\DI\IDatabaseTypeProvider;
use Kdyby\Doctrine\DI\IEntityProvider;
use Nette\DI\CompilerExtension;


class AnyshapeExtension extends CompilerExtension implements IEntityProvider, IDatabaseTypeProvider
{

	/**
	 * Method setings extension.
	 */
	public function loadConfiguration()
	{
		$this->compiler->parseServices(
			$this->getContainerBuilder(),
			$this->loadFromFile(__DIR__ . '/config/services.neon'),
			$this->name
		);

		$builder = $this->getContainerBuilder();

		$latteFactory = $builder->hasDefinition('nette.latteFactory')
			? $builder->getDefinition('nette.latteFactory')
			: $builder->getDefinition('nette.latte');

		$latteFactory->addSetup('addFilter', array('wysiwyg', array($this->prefix('@filters'), 'wysiwyg')));

		$builder->getDefinition('nette.presenterFactory')
			->addSetup('if (method_exists($service, ?)) { $service->setMapping(array(? => ?)); } ' .
				'elseif (property_exists($service, ?)) { $service->mapping[?] = ?; }', array(
					'setMapping', 'Anyshape', 'Goodshape\Anyshape\UI\*Presenter', 'mapping', 'Anyshape', 'Goodshape\Anyshape\UI\*Presenter'
				));
	}


	/**
	 * Returns associative array of Namespace => mapping definition
	 *
	 * @return array
	 */
	public function getEntityMappings()
	{
		return array(
			'Goodshape\Anyshape' => __DIR__ . '/../',
		);
	}

	/**
	 * Returns array of typeName => typeClass.
	 *
	 * @return array
	 */
	function getDatabaseTypes()
	{
		return array(
			'translate' => 'Goodshape\\Anyshape\\Types\\TranslateType',
		);
	}


}
