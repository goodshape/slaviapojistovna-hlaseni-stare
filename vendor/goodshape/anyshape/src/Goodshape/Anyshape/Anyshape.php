<?php

namespace Goodshape\Anyshape;

use Nette;


class Anyshape extends Nette\Object
{

	/**
	 * @var Nette\Application\Application
	 */
	protected $application;

	/**
	 * @var Facades\PageFacade
	 */
	protected $pageFacade;


	/**
	 * @param Nette\Application\Application $application
	 * @param Facades\PageFacade $pageFacade
	 */
	public function __construct(\Nette\Application\Application $application, \Goodshape\Anyshape\Facades\PageFacade $pageFacade)
	{
		$this->application = $application;
		$this->pageFacade = $pageFacade;
	}


	/**
	 * @return \Goodshape\Anyshape\Entities\StructureEntity
	 */
	public function getWebsite()
	{
		return $this->pageFacade->getWebsite();
	}


	/**
	 * @return \Goodshape\Anyshape\Entities\StructureEntity
	 */
	public function getPage()
	{


		if($id = $this->application->getPresenter()->getParameter('id')) {
			$page = $this->pageFacade->get($id);
		} elseif($slug = $this->application->getPresenter()->getParameter('slug', '')) {
			$page = $this->pageFacade->getPage($slug);
		}

		if(!empty($page)) {
			return $page;
		}
		throw new Nette\Application\BadRequestException();


	}

}