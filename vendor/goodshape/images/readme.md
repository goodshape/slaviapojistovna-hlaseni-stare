# Images

This is a simple image storage for [Nette Framework](http://nette.org/) with AnyShape compatibility.

Based on https://github.com/brabijan/images

## Instalation

The best way to install goodshape/images is using  [Composer](http://getcomposer.org/):


```sh
$ composer require goodshape/images:@dev
```

Then you have to register extension in `config.neon`.

```yaml
extensions:
	images: Goodshape\Images\DI\ImagesExtension
```

Files and resized directory config is automatically taken from anyshape config values (see Goodshape/Anyshape).

You can also set your own:

```yaml
images:
    assetsDir: %wwwDir%/upload
    resizedDir: %wwwDir%/upload
    original: #folder for original files
```

Package contains trait, which you will have to use in class, where you want to use image storage. This works only for PHP 5.4+, for older version you can simply copy trait content and paste it into class where you want to use it.

```php

<?php

class BasePresenter extends Nette\Application\UI\Presenter {

	use Goodshape\Images\TImagePipe;
	
}

```

## Usage

### Saving images

```php
/** @var Goodshape\Images\ImageStorage $imageStorage */
$imageStorage->upload($fileUpload); // saves to .../assetsDir/original/filename.jpg

$imageStorage->setNamespace("products")->upload($fileUpload); // saves to .../assetsDir/products/original/filename.jpg
```

### Using in Latte

```html
<a href="{img products/filename.jpg}"><img n:img="filename.jpg, 200x200, fill"></a>
```

output:

```html
<a href="/assetsDir/products/filename.jpg"><img n:img="/assetsDir/200x200_fill/filename.jpg"></a>
```

### Resizing flags

For resizing (third argument) you can use these keywords - `fit`, `fill`, `exact`, `stretch`, `shrink_only`. For details see comments above [these constants](http://api.nette.org/2.0/source-common.Image.php.html#105).
Multiple keywords can be separated by "|".

### Default image

You can specify default image if the requested is not set or does not exists:

```html
<img n:img="$entity->image|assets/default.png, 200x200, fill">
```

If ```$entity->image``` is does not exists (or its empty), assets/default.png will be provided instead. 

Same flags a/or resize options are aplied to default image as well. Path to default image is relative to assetsDir.
