<?php

namespace Goodshape\Images\DI;

use Latte;
use Nette;
use Nette\Configurator;
use Nette\DI\Compiler;

/**
 * @author Jan Brabec <brabijan@gmail.com>
 */
class ImagesExtension extends Nette\DI\CompilerExtension
{

	public function loadConfiguration()
	{
		$config = $this->getConfig();
		$builder = $this->getContainerBuilder();

		$latteFactory = $builder->hasDefinition('nette.latteFactory')
			? $builder->getDefinition('nette.latteFactory')
			: $builder->getDefinition('nette.latte');

		$latteFactory->addSetup('Goodshape\Images\Macros\Latte::install(?->getCompiler())', ['@self']);

		$builder->addDefinition($this->prefix('imagePipe'))->setClass('Goodshape\Images\ImagePipe',
			[$config['assetsDir'],
				$config['resizedDir'], $this->containerBuilder->parameters['wwwDir']]);
		$builder->addDefinition($this->prefix('imageStorage'))->setClass('Goodshape\Images\ImageStorage',
			[$config['assetsDir']]);
		$builder->addDefinition($this->prefix('fileBrowser'))->setClass('Goodshape\Images\FileBrowser');
	}


	/**
	 * @param \Nette\Configurator $config
	 * @param string $extensionName
	 */
	public static function register(Configurator $config, $extensionName = 'imagesExtension')
	{
		$config->onCompile[] = function (Configurator $config, Compiler $compiler) use ($extensionName) {
			$compiler->addExtension($extensionName, new ImagesExtension());
		};
	}


	/**
	 * {@inheritdoc}
	 */
	public function getConfig(array $defaults = NULL, $expand = TRUE)
	{
		//anyshape defaults
		if (isset($this->containerBuilder->parameters['anyshape'])) {
			$defaults = [
				'assetsDir' => rtrim($this->containerBuilder->parameters['anyshape']['site']['files_path'], '/\\'),
				'resizedDir' => rtrim($this->containerBuilder->parameters['anyshape']['site']['data_path'] . '/thumbs',
					'/\\'),
			];
		} else {
			$defaults = [
				'assetsDir' => $this->containerBuilder->parameters['wwwDir'] . '/assets',
				'resizedDir' => $this->containerBuilder->parameters['wwwDir'] . '/assets',
			];
		}

		return parent::getConfig($defaults, $expand);
	}

}
