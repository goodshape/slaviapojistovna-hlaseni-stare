<?php

namespace Goodshape\Images\Macros;

use Goodshape\Images\ImagePipe;
use Latte\Compiler;
use Latte\MacroNode;
use Latte\Macros;
use Latte\PhpWriter;
use Latte\Template;
use Nette;


/**
 * @author Jan Brabec <brabijan@gmail.com>
 * @author Filip Procházka <filip@prochazka.su>
 */
class Latte extends Macros\MacroSet
{

	/**
	 * @var bool
	 */
	private $isUsed = FALSE;


	/**
	 * @param \Latte\Compiler $compiler
	 *
	 * @return \Latte\Macros\MacroSet
	 */
	public static function install(Compiler $compiler)
	{
		$me = new static($compiler);

		/**
		 * {img [namespace/]$name[, $size[, $flags]]}
		 */
		$me->addMacro('img', array($me, 'macroImg'), NULL, array($me, 'macroAttrImg'));

		return $me;
	}


	/**
	 * @param \Latte\MacroNode $node
	 * @param \Latte\PhpWriter $writer
	 * @return string
	 * @throws \Latte\CompileException
	 */
	public function macroImg(MacroNode $node, PhpWriter $writer)
	{
		$this->isUsed = TRUE;
		$arguments = Helpers::prepareMacroArguments($node->args);
		if ($arguments["name"] === NULL) {
			throw new \Latte\CompileException("Please provide filename.");
		}

		$namespace = $arguments["namespace"];
		unset($arguments["namespace"]);
		$arguments = array_map(function ($value) use ($writer) {
			return $value ? $writer->formatWord($value) : 'NULL';
		}, $arguments);

		$command = '$_imagePipe';
		$command .= $namespace !== NULL ? '->setNamespace(' . $writer->formatWord(trim($namespace)) . ')' : '';
		$command .= '->request(' . implode(", ", $arguments) . ')';

		return $writer->write('echo %escape(' . $writer->formatWord($command) . ')');
	}


	/**
	 * @param \Latte\MacroNode $node
	 * @param \Latte\PhpWriter $writer
	 * @return string
	 * @throws \Latte\CompileException
	 */
	public function macroAttrImg(MacroNode $node, PhpWriter $writer)
	{
		$this->isUsed = TRUE;
		$arguments = Helpers::prepareMacroArguments($node->args);
		if ($arguments["name"] === NULL) {
			throw new \Latte\CompileException("Please provide filename.");
		}

		$namespace = $arguments["namespace"];
		unset($arguments["namespace"]);
		$arguments = array_map(function ($value) use ($writer) {
			return $value ? $writer->formatWord($value) : 'NULL';
		}, $arguments);

		$command = '$_imagePipe';
		$command .= $namespace !== NULL ? '->setNamespace(' . $writer->formatWord(trim($namespace)) . ')' : '';
		$command .= '->request(' . implode(", ", $arguments) . ')';

		return $writer->write('?> src="<?php echo %escape(' . $writer->formatWord($command) . ')?>" <?php');
	}


	/**
	 */
	public function initialize()
	{
		$this->isUsed = FALSE;
	}


	/**
	 * Finishes template parsing.
	 *
	 * @return array(prolog, epilog)
	 */
	public function finalize()
	{
		if (!$this->isUsed) {
			return array();
		}

		return array(
			get_called_class() . '::validateTemplateParams($template);',
			NULL
		);
	}


	/**
	 * @param \Nette\Templating\Template $template
	 * @throws \Nette\InvalidStateException
	 */
	public static function validateTemplateParams(Template $template)
	{
		$params = $template->getParameters();
		if (!isset($params['_imagePipe']) || !$params['_imagePipe'] instanceof ImagePipe) {
			$where = isset($params['control']) ?
				" of component " . get_class($params['control']) . '(' . $params['control']->getName() . ')'
				: NULL;

			throw new Nette\InvalidStateException(
				'Please provide an instanceof Img\\ImagePipe ' .
				'as a parameter $_imagePipe to template' . $where
			);
		}
	}

}
