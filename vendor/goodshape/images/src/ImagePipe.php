<?php

namespace Goodshape\Images;

use Nette;
use Nette\Utils\Image;
use Nette\Utils\Image as NImage;

/**
 * @author Jan Brabec <brabijan@gmail.com>
 */
class ImagePipe extends Nette\Object
{

	/** @var string */
	private $assetsDir;

	/** @var string */
	private $wwwDir;

	/** @var string */
	private $path;

	/** @var string */
	private $baseUrl;

	/** @var string|null */
	private $namespace = NULL;

	/** @var array */
	public $onBeforeSaveThumbnail = array();
	/**
	 * @var
	 */
	private $resizedDir;


	/**
	 * @param $assetsDir
	 * @param $resizedDir
	 * @param $wwwDir
	 * @param Nette\Http\Request $httpRequest
	 */
	public function __construct($assetsDir, $resizedDir, $wwwDir, Nette\Http\Request $httpRequest)
	{
		$this->wwwDir = $wwwDir;
		$this->assetsDir = $assetsDir;
		$this->baseUrl = rtrim($httpRequest->url->baseUrl, '/');
		$this->resizedDir = $resizedDir;
	}


	/**
	 * @param $path
	 */
	public function setPath($path)
	{
		$this->path = $path;
	}


	/**
	 * @param $dir
	 */
	public function setAssetsDir($dir)
	{
		$this->assetsDir = $dir;
	}


	/**
	 * @return string
	 */
	public function getAssetsDir()
	{
		return $this->assetsDir;
	}


	/**
	 * @param null $where
	 * @return string
	 */
	public function getPath($where = NULL)
	{
		if ($where === NULL) {
			$where = $this->assetsDir;
		}

		return $this->path !== NULL ? $this->path : $this->baseUrl . str_replace($this->wwwDir, '', $where);
	}

	/**
	 * @throws \Nette\InvalidStateException
	 */
	private function checkSettings()
	{
		if ($this->assetsDir == NULL) {
			throw new Nette\InvalidStateException("Assets directory is not setted");
		}
		if (!file_exists($this->assetsDir)) {
			throw new Nette\InvalidStateException("Assets directory '{$this->assetsDir}' does not exists");
		} elseif (!is_writeable($this->assetsDir)) {
			throw new Nette\InvalidStateException("Make assets directory '{$this->assetsDir}' writeable");
		}
		if ($this->getPath() == NULL) {
			throw new Nette\InvalidStateException("Path is not setted");
		}
	}


	/**
	 * @param $namespace
	 * @return $this
	 */
	public function setNamespace($namespace)
	{
		if (empty($namespace)) {
			$this->namespace = NULL;
		} else {
			$this->namespace = $namespace . "/";
		}

		return $this;
	}


	/**
	 * @param string $image
	 * @param null $size
	 * @param null $flags
	 * @param bool $strictMode
	 * @return string
	 * @throws \Latte\CompileException
	 * @throws FileNotFoundException;
	 */
	public function request($image, $size = NULL, $flags = NULL, $defaultFile = NULL, $strictMode = FALSE)
	{
		$this->checkSettings();
		if ($image instanceof ImageProvider) {
			$this->setNamespace($image->getNamespace());
			$image = $image->getFilename();
		} elseif (empty($image) && !$defaultFile) {
			return "#";
		}
		if ($size === NULL) {
			return $this->getPath() . "/" . $this->namespace . $image;
		}

		$originalFile = $this->assetsDir . "/" . $this->namespace . $image;

		if (!(file_exists($originalFile) && is_file($originalFile)) && $defaultFile !== NULL) {
			return $this->request($defaultFile, $size, $flags, NULL, $strictMode);
		}

		list($width, $height) = explode("x", $size);
		if ($flags == NULL) {
			$flags = Nette\Image::FIT;
		} elseif (!is_int($flags)) {
			$toParse = explode("|", $flags);
			$flags = 0;
			foreach ($toParse as $item) {
				switch (strtolower($item)):
					case "fit":
						$flags |= NImage::FIT;
						break;
					case "fill":
						$flags |= NImage::FILL;
						break;
					case "exact":
						$flags |= NImage::EXACT;
						break;
					case "shrink_only":
						$flags |= NImage::SHRINK_ONLY;
						break;
					case "stretch":
						$flags |= NImage::STRETCH;
						break;
				endswitch;
			}
			if (!isset($flags)) {
				throw new Nette\Latte\CompileException('Mode is not allowed');
			}
		}

		$thumbPath = "/" . $this->namespace . $flags . "_" . $width . "x" . $height . "/" . ltrim($image, '.\\/');
		$thumbnailFile = $this->resizedDir . $thumbPath;

		if (!file_exists($thumbnailFile)) {
			$this->mkdir(dirname($thumbnailFile));
			if (file_exists($originalFile)) {
				$img = NImage::fromFile($originalFile);
				if ($flags & Image::EXACT && $flags & Image::SHRINK_ONLY) {
					if ($img->getWidth() < $width || $img->getHeight() < $height) {
						$flags = NImage::FIT | NImage::SHRINK_ONLY;
					}
				}
				$img->resize($width, $height, $flags);
				

				$this->onBeforeSaveThumbnail($img, $this->namespace, $image, $width, $height, $flags);
				$img->save($thumbnailFile);
			} elseif ($strictMode) {
				throw new FileNotFoundException;
			}
		}
		$this->namespace = NULL;

		return $this->getPath($this->resizedDir) . $thumbPath;
	}


	/**
	 * @param string $dir
	 *
	 * @throws \Nette\IOException
	 * @return void
	 */
	private static function mkdir($dir)
	{
		$oldMask = umask(0);
		@mkdir($dir, 0777, TRUE);
		@chmod($dir, 0777);
		umask($oldMask);

		if (!is_dir($dir) || !is_writable($dir)) {
			throw new Nette\IOException("Please create writable directory $dir.");
		}
	}

}
