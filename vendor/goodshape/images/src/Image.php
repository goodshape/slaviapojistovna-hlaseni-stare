<?php

namespace Goodshape\Images;

use Nette;

/**
 * @author Filip Procházka <filip@prochazka.su>
 *
 * @property-read string $file
 * @property-read Size $size
 */
class Image extends Nette\Object
{

	/** @var string */
	private $file;

	/** @var Size */
	private $size;
	/**
	 * @var
	 */
	private $storagePath;


	/**
	 * @param string $file
	 */
	public function __construct($file, $storagePath)
	{
		$this->file = $file;
		$this->size = Size::fromFile($file);
		$this->storagePath = $storagePath;
	}


	/**
	 * @return bool
	 */
	public function exists()
	{
		return file_exists($this->file);
	}


	/**
	 * @return float|int
	 */
	public function getFile()
	{
		return $this->file;
	}


	/**
	 * @return Size
	 */
	public function getSize()
	{
		return $this->size;
	}

	public function getStorageRelativePath()
	{
		$relative = ltrim(str_replace($this->storagePath, "", $this->file), '\\/');

		$relative = str_replace('//', '/', $relative);

		return $relative;
	}

}