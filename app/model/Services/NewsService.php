<?php


namespace App\Model\Services;


use App\Model\Entities\News;
use Kdyby\Doctrine\EntityManager;
use Nette\Object;

class NewsService extends Object
{
	/** @var \Kdyby\Doctrine\EntityDao  */
	private $newsDao;

	function __construct(EntityManager $em)
	{
		$this->newsDao = $em->getDao(News::getClassName());
	}


	public function getPublished($limit, $offset = 0)
	{
		return $this->newsDao->createQueryBuilder('n')
			->andWhere('n.published = 1')
			->andWhere('n.date <= :now')
			->orderBy('n.date', 'DESC')
			->setParameter('now', new \DateTime())
			->setMaxResults($limit)
			->setFirstResult($offset)
			->getQuery()->getResult();
	}

	public function getPublishedCount()
	{
		return $this->newsDao->createQueryBuilder('n')
			->select("COUNT(n.id)")
			->andWhere('n.published = 1')
			->andWhere('n.date <= :now')
			->setParameter('now', new \DateTime())
			->getQuery()->getSingleScalarResult();
	}

} 