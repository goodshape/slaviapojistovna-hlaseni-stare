<?php


namespace App\Model\Services;


use App\Model\Entities\MenuMap;
use Doctrine\ORM\NoResultException;
use Goodshape\Anyshape\Entities\StructureEntity;
use Kdyby\Doctrine\Dql\Join;
use Kdyby\Doctrine\EntityManager;
use Nette\Object;
use Nette\Utils\Arrays;

class MenuService extends Object
{

	private $em;

	private $menuDao;

	private $menuMap = FALSE;

	public function __construct(EntityManager $em)
	{
		$this->em = $em;
		$this->menuDao = $em->getDao(MenuMap::getClassName());
	}


	public function getChildStructure($type)
	{

		$data = $this->em->getDao(StructureEntity::getClassName())->createQueryBuilder('m')
			->select('m, mc')
			->join(MenuMap::getClassName(), 'mt', Join::WITH, 'mt.node = m.id')
			->join('m.children', 'mc')
			->andWhere('mt.type = :type')
			->setParameters(['type' => $type]);

		$root = $data->getQuery()->getSingleResult();

		return $root->children;

	}

	public function getRootNode($type)
	{
		try {
			if($this->menuMap === FALSE) {
				$this->menuMap = $this->menuDao
					->createQueryBuilder('m')
					->select('s.id, m.type')
					->join('m.node', 's')
					->getQuery()->getScalarResult();
				$this->menuMap = Arrays::associate($this->menuMap, 'type=id');
			}

			if(isset($this->menuMap[$type])) {
				return $this->menuMap[$type];
			}
		} catch (NoResultException $e) {
		}
		return NULL;
	}
}