<?php


namespace App\Model\Services;


use App\Model\Entities\InquiryEntity;
use App\Model\Entities\MailEntity;
use App\Model\Entities\ReportForm;
use Kdyby\Doctrine\EntityManager;
use Nette\Mail\IMailer;
use Nette\Mail\Message;
use Nette\Object;

class MailService extends Object
{
    /** @var IMailer */
    private $mailer;
    /** @var EntityManager */
    private $em;
    /**
     * @var
     */
    private $sender;
	/**
	 * @var
	 */
	private $recipients;

	function __construct($sender, $recipients, EntityManager $em, IMailer $mailer)
    {
        $this->mailer = $mailer;
        $this->sender = $sender;
        $this->em = $em;
		$this->recipients = $recipients;
	}


    public function send(MailEntity $mail)
    {

        $message = new Message();
        foreach ($mail->recipients as $r) {
            $message->addTo($r);
        }
        $message->addTo("info@goodshape.cz");
        $message->setSubject($mail->subject);
        $message->setBody($mail->message);
	
        $message->setHeader("Return-Path", $mail->sender);
        $message->setHeader('Sender', $this->sender);
        $message->setHeader('Reply-To', $mail->sender);
        $message->setFrom($this->sender);

        $this->mailer->send($message);

        $mail->sent = new \DateTime();
        $this->em->persist($mail);
        $this->em->flush($mail);
    }

    public function createContactForm($name, $email, $text, $to = NULL)
    {
        $entity = new MailEntity();
        $entity->sender = "webforms@slavia-pojistovna.cz"; 
        $entity->addRecipient($to ?: $this->recipients['contactForm']);
        $entity->message = "E-mail z kontaktního formuláře z www.slavia-pojistovna.cz\n\n\n" .
            "Jméno: {$name}\n" .
            "E-mail: {$email}\n" .
            "Text: \n" .
            $text;

        $entity->subject = "$email - E-mail z kontaktního formuláře z slavia-pojistovna.cz";

        return $entity;
    }

	public function createCallBack($phone, $to = NULL)
	{
		$entity = new MailEntity();
		$entity->sender = $this->sender;
		$entity->addRecipient($to ?: $this->recipients['contactForm']);
		$entity->message = "E-mail z kontaktního formuláře z www.slavia-pojistovna.cz\n\n\n" .
			"Zavolejte mi zpět: {$phone}\n";

		$entity->subject = 'E-mail z slavia-pojistovna.cz - zpětné volání';

		return $entity;
	}

	public function createReportForm(ReportForm $entity, $message, $subject, $to = NULL)
	{
		$mail = new MailEntity();
		$mail->sender = $this->sender;
		$mail->addRecipient($to ?: $this->recipients['report']);
		$mail->message = $message;
		$mail->subject = $subject;

		return $mail;
	}

    public function createCalculator($cars, $email, $phone)
    {
        $mail = new MailEntity();
        $mail->sender = $email;
        $mail->addRecipient($this->recipients['calculator']);
        $mail->message = "E-mail z kalkulátoru pojištění finanční způsobilosti dopravce:\n\n".
            "Počet provozovaných vozidel: {$cars}\n".
            "E-mail: {$email}\n".
            "Telefon: {$phone}\n";
        $mail->subject = "E-mail z slavia-pojistovna.cz - kalkulace pojištění";

        return $mail;

    }


} 