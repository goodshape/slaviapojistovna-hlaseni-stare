<?php


namespace App\Model\Entities;


use Kdyby\Doctrine\Entities\Attributes\Identifier;
use Kdyby\Doctrine\Entities\BaseEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Banner
 * @package App\Model\Entities
 *
 * @ORM\Entity()
 * @ORM\Table()
 *
 * Properties are extra for each locale to make fallbacks possible (anyshape don't know how to do it)
 *
 */
class Banner extends BaseEntity
{
	use Identifier;

	/**
	 * @ORM\Column(type="string")
	 * @var string
	 */
	protected $url_cs;

	/**
	 * @ORM\Column(type="string")
	 * @var string
	 */
	protected $url_en;

	/**
	 * @ORM\Column(type="string")
	 * @var string
	 */
	protected $url_vi;

	/**
	 * @ORM\Column(type="string")
	 * @var string
	 */
	protected $url_ru;
	/**
	 * @var
	 * @ORM\Column(type="string")
	 */
	protected $image_cs;
	/**
	 * @var
	 * @ORM\Column(type="string")
	 */
	protected $image_en;
	/**
	 * @var
	 * @ORM\Column(type="string")
	 */
	protected $image_ru;
	/**
	 * @var
	 * @ORM\Column(type="string")
	 */
	protected $image_vi;

	/**
	 * @var
	 * @ORM\Column(type="boolean")
	 */
	protected $active;

	/**
	 * @var
	 * @ORM\Column(type="translate")
	 */
	protected $title;

	public function getImage($locale)
	{
		$property = "image_".$locale;
		if(isset($this->$property)) {
			return $this->$property;
		}
		return $this->image_en;
	}

	public function getUrl($locale)
	{
		$property = "url_".$locale;
		if(isset($this->$property)) {
			return $this->$property ?: $this->url_cs;
		}
		return $this->url_cs;
	}

} 