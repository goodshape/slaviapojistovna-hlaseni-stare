<?php


namespace App\Model\Entities;

use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\Attributes\Identifier;
use Kdyby\Doctrine\Entities\BaseEntity;

/**
 * Class News
 * @package App\Model\Entities
 *
 * @ORM\Entity()
 * @ORM\Table()
 */
class News extends BaseEntity
{

	use Identifier;


	/**
	 * @ORM\Column()
	 * @var string
	 */
	protected $title;

	/**
	 * @ORM\Column(type="text")
	 * @var string
	 */
	protected $perex;

	/**
	 * @ORM\Column(type="text")
	 * @var string
	 */
	protected $content;

	/**
	 * @ORM\Column(type="datetime")
	 * @var \DateTime
	 */
	protected $date;

	/**
	 * @ORM\Column(type="boolean")
	 * @var bool
	 */
	protected $published = TRUE;

} 