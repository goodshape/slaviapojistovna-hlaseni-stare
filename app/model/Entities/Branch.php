<?php


namespace App\Model\Entities;


use Kdyby\Doctrine\Entities\Attributes\Identifier;
use Kdyby\Doctrine\Entities\BaseEntity;
use Doctrine\ORM\Mapping as ORM;


/**
 * Class Branch
 * @package App\Model\Entities
 *
 * @ORM\Entity()
 * @ORM\Table()
 */
class Branch extends BaseEntity {

	use Identifier;
	/**
	 * @ORM\Column(type="translate")
	 * @var
	 */
	protected $title;

	/**
	 * @ORM\Column(type="translate", nullable=true)
	 * @var
	 */
	protected $content;

	/**
	 *
	 * @ORM\Column(type="string", unique=true)
	 * @var
	 */
	protected $district;

} 