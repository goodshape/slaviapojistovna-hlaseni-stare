<?php


namespace App\Model\Entities;

use Doctrine\ORM\Mapping as ORM;
use Goodshape\Anyshape\Entities\StructureEntity;
use Kdyby\Doctrine\Entities\Attributes\Identifier;
use Kdyby\Doctrine\Entities\BaseEntity;

/**
 * Class PageAttachment
 * @package App\Model\Entities
 *
 * @ORM\Entity()
 * @ORM\Table(name="data_page_attachment")
 */
class PageAttachment extends BaseEntity
{

	use Identifier;

	/**
	 * @ORM\Column()
	 * @var string
	 */
	protected $title;

	/**
	 * @ORM\Column()
	 * @var string
	 */
	protected $file;

	/**
	 * @ORM\ManyToOne(targetEntity="Goodshape\Anyshape\Entities\StructureEntity", inversedBy="attachments")
	 * @var StructureEntity
	 */
	protected $page;

	/**
	 * @ORM\Column()
	 * @var int
	 */
	protected $sorting;

} 