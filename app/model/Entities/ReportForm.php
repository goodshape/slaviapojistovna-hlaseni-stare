<?php


namespace App\Model\Entities;

use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\Attributes\Identifier;
use Kdyby\Doctrine\Entities\BaseEntity;

/**
 * Class ReportForm
 * @package App\Model\Entities
 *
 * @ORM\Entity()
 * @ORM\Table(name="data_report_forms")
 */
class ReportForm extends BaseEntity
{
	use Identifier;

	/**
	 * @ORM\Column(type="string")
	 * @var string
	 */
	protected $name;
	/**
	 * @ORM\Column(type="string", nullable=true)
	 * @var string
	 */
	protected $surname;
	/**
	 * @ORM\Column(type="string", nullable=true)
	 * @var string
	 */
	protected $mail;
	/**
	 * @ORM\Column(type="string", nullable=true)
	 * @var string
	 */
	protected $phone;
	/**
	 * @ORM\Column(type="datetime")
	 * @var \DateTime
	 */
	protected $date;
	/**
	 * @ORM\Column(type="text")
	 * @var string
	 */
	protected $message;
	/**
	 * @ORM\Column(type="string")
	 * @var string
	 */
	protected $type;

	/**
	 * @ORM\Column(type="string", nullable=true)
	 * @var string
	 */
	protected $attachments;

	function __construct()
	{
		$this->date = new \DateTime();
	}

	public function hasAttachments()
	{
		return $this->attachments !== NULL;
	}


} 