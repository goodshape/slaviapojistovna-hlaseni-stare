<?php

namespace App\Model\Entities;

use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\Attributes\Identifier;
use Kdyby\Doctrine\Entities\BaseEntity;
use Nette;


/**
 * @ORM\Entity
 * @ORM\Table(name="mails")
 *
 * @property-read int $id
 * @property string $from
 * @property array $recipients
 * @property string $subject
 * @property string $message
 * @property \DateTime $created
 */
class MailEntity extends BaseEntity
{

    use Identifier;

    /**
     * @ORM\Column()
     * @var string
     */
    protected $sender;

    /**
     * @ORM\Column(type="json_array")
     * @var string
     */
    protected $recipients = [];

    /**
     * @ORM\Column
     * @var string
     */
    protected $subject;

    /**
     * @ORM\Column(type="text")
     * @var string
     */
    protected $message;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    protected $created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    protected $sent;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    protected $token;

    function __construct()
    {
        $this->created = new \DateTime();
        $this->token = Nette\Utils\Random::generate(30);
    }

    public function addRecipient($recipient)
    {
        $this->recipients[] = $recipient;
    }


}
