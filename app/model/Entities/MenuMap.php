<?php


namespace App\Model\Entities;


use Doctrine\ORM\Mapping as ORM;
use Goodshape\Anyshape\Entities\StructureEntity;
use Kdyby\Doctrine\Entities\Attributes\Identifier;
use Kdyby\Doctrine\Entities\BaseEntity;

/**
 * Class MenuMap
 * @package App\Model\Entities
 *
 * @ORM\Entity()
 * @ORM\Table(name="data_menu_map")
 */
class MenuMap extends BaseEntity
{

	const TOP_SECONDARY = 'top_secondary';
	const USEFUL_ADVICE = 'useful';
	const NEGOTIATE_ONLINE = 'negotiate_online';
	const DAMAGE_REPORT = 'damage_report';
	const DAMAGE_REPORT_DOCUMENTS = 'documents_for_report';
	const PERSONAL_MEGAMENU = 'personal';
	const COMMERCIAL_MEGAMENU = 'commercial';

	use Identifier;


	/**
	 * @ORM\ManyToOne(targetEntity="Goodshape\Anyshape\Entities\StructureEntity")
	 * @var StructureEntity
	 */
	protected $node;

	/**
	 * @ORM\Column(type="string")
	 * @var
	 */
	protected $type;

} 