<?php


namespace App;


use Kdyby\Translation\Translator;
use Nette\Application\Routers\Route;
use Nette\Application\UI\Presenter;
use Nette\Caching\Cache;
use Nette\Caching\IStorage;
use Nette\Neon\Neon;
use Nette\Object;
use Nette\Utils\Strings;

class LocalizationFilter extends Object
{

	private $file;

	/** @var Translator */
	private $translator;

	private $tableIn = [];
	private $tableOut = [];
	private $cache;

	function __construct(Translator $translator, IStorage $storage)
	{
		$this->translator = $translator;
		$this->cache = new Cache($storage, str_replace('\\', '.', get_class($this)));
	}


	public function getFilters()
	{
		return [
			Route::FILTER_OUT => callback($this, 'wayOut'),
			Route::FILTER_IN => callback($this, 'wayIn'),
		];
	}

	public function wayOut($params)
	{
		$this->prepareData(isset($params['locale']) ? $params['locale'] : NULL);

		$key = $params['presenter'] . ':' . $params['action'];

		if (isset($this->tableOut[$key])) {
			$params['slug'] = $this->tableOut[$key];

			unset($params['id']);
			unset($params[Route::PRESENTER_KEY]);
			unset($params[Presenter::ACTION_KEY]);

			return $params;
		}

		return NULL;
	}

	public function wayIn($params)
	{
		$this->prepareData(isset($params['locale']) ? $params['locale'] : NULL);

		if (!isset($params['slug'])) {
			return NULL;
		}

		$key = $params['slug'];
		if (isset($this->tableIn[$key])) {
			$destination = $this->tableIn[$key];
			$pos = strrpos($destination, ':');
			$params['presenter'] = substr($destination, 0, $pos);
			$params['action'] = substr($destination, $pos + 1);

			unset($params['slug']);

			return $params;
		}

		return NULL;
	}

	/**
	 * @param mixed $file
	 */
	public function setFile($file)
	{
		$this->file = $file;
	}

	private function prepareData($locale)
	{
		if ($this->tableIn) {
			return;
		}
		$locale = ($locale ?: $this->translator->getLocale());
		$key = 'data' . $locale;
		if (!$this->cache[$key]) {
			$contents = Neon::decode(file_get_contents($this->file));

			$presenterPrefixes = [];
			$actionsSlugs = [];
			foreach ($contents as $presenter => $actions) {
				foreach ($actions as $action => $slugKey) {
					if (Strings::startsWith($slugKey, "_")) {
						$slug = $this->translator->translate(ltrim($slugKey, '_'), NULL, [], NULL, $locale);
						if ($slug === $slugKey) {
							continue;
						}
						$slug = $this->webalize($slug);
					} else {
						$slug = $slugKey;
					}

					if (!$slug) {
						continue;
					}

					if ($action === 'presenter') {
						$presenterPrefixes[$presenter] = $slug;
					} else {
						$actionsSlugs[$presenter][$action] = $slug;
					}
				}
			}

			$slugs = [];
			foreach ($actionsSlugs as $presenter => $actions) {
				$prefix = '';
				if (isset($presenterPrefixes[$presenter])) {
					$prefix = $presenterPrefixes[$presenter];
				}
				foreach ($actions as $action => $slug) {
					$slugs[($prefix ? $prefix . '/' : '') . $slug] = $presenter . ':' . $action;
				}
			}

			$this->cache->save($key, $slugs, [
				Cache::FILES => [$this->file],
			]);

		}

		$data = $this->cache[$key];
		$this->tableIn = $data;
		$this->tableOut = array_flip($this->tableIn);

	}

	private function webalize($s)
	{
		if (class_exists('Transliterator') && $transliterator = \Transliterator::create('Any-Latin; Latin-ASCII')) {
			$s = $transliterator->transliterate($s);
		}
		return Strings::webalize($s);
	}

	/**
	 * @return array
	 */
	public function getTableOutKey($key, $locale)
	{
		$this->prepareData($locale);
		return isset($this->tableOut[$key]) ? $this->tableOut[$key] : NULL;
	}

	public function getTableInKey($key, $locale)
	{
		$this->prepareData($locale);
		return isset($this->tableIn[$key]) ? $this->tableIn[$key] : NULL;
	}


}