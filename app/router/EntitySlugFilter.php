<?php


namespace App;


use Doctrine\ORM\Query\Expr\Join;
use Goodshape\Anyshape\Entities\TextEntity;
use Kdyby\Doctrine\EntityDao;
use Kdyby\Doctrine\EntityManager;
use Kdyby\Translation\Translator;
use Nette\Application\Routers\Route;
use Nette\Application\UI\Presenter;
use Nette\Caching\Cache;
use Nette\Caching\IStorage;
use Nette\Object;
use Nette\Utils\Strings;

class EntitySlugFilter extends Object
{

	private $file;

	/** @var Translator */
	private $translator;

	private $tableIn = [];
	private $tableOut = [];
	/** @var Cache  */
	private $cache;
	/** @var EntityDao  */
	private $dao;

	function __construct($entity, EntityManager $em, Translator $translator, IStorage $storage)
	{
		$this->translator = $translator;
		$this->cache = new Cache($storage, str_replace('\\', '.', get_class($this).$entity));
		$this->dao = $em->getDao($entity);
	}


	public function getFilters()
	{
		return [
			Route::FILTER_OUT => callback($this, 'wayOut'),
			Route::FILTER_IN => callback($this, 'wayIn'),
		];
	}

	public function wayOut($params)
	{
		if($params['action'] === 'default' || empty($params['id'])) {
			return NULL;
		}

		$id = $params['id'];
		$slug = $this->cache->load($id.$params['locale'], function(&$dependencies) use ($id, $params) {
			$dependencies = [Cache::EXPIRATION => '+15min', Cache::SLIDING => FALSE];
			$metadata = $this->dao->getClassMetadata()->getTypeOfColumn('title');
			if($metadata === 'translate') {
				$q = $this->dao->createQueryBuilder('n')->select('t.text')
					->leftJoin(TextEntity::getClassName(), 't', Join::WITH, 'n.title = t.id AND t.language = :locale')
					->andWhere('n.id = :id')
					->setParameter('id', $id)
					->setParameter('locale', empty($params['locale']) ? $this->translator->getLocale(): $params['locale']);
			} else {
				$q = $this->dao->createQueryBuilder('n')->select('n.title')->andWhere('n.id = :id')
					->setParameter('id', $id);

			}
			$name = $q->getQuery()
				->getSingleScalarResult();
			if($name) {
				return Strings::webalize($name).'-';
			}
			return NULL;
		});

		if($slug) {
			$params['slug'] = $slug;
		}

		return $params;
	}

	public function wayIn($params)
	{
		unset($params['slug']);
		if(!empty($params['id'])) {
			return $params;
		}
	}

	/**
	 * @param mixed $file
	 */
	public function setFile($file)
	{
		$this->file = $file;
	}

}

interface IEntitySlugFilter
{
	/** @return EntitySlugFilter */
	function create($entity);
}