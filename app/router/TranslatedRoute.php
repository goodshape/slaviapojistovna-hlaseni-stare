<?php


namespace App;


use Nette\Application\Routers\Route;

class TranslatedRoute extends Route
{


	public function __construct($mask, $metadata = array(), $flags = 0)
	{
		parent::__construct('<locale=cs cs|en|ru|vi>/'.$mask, $metadata, $flags);
	}
}