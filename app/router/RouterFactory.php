<?php

namespace App;

use App\Model\Entities\Branch;
use App\Model\Entities\News;
use Goodshape;
use Goodshape\Anyshape\Routing\AnyShapeRouteFilter;
use Kdyby\Translation\Translator;
use Nette;
use Nette\Application\Routers\Route;
use Nette\Application\Routers\RouteList;


/**
 * Router factory.
 */
class RouterFactory
{
    /**
     * @var AnyShapeRouteFilter
     */
    private $routeFilter;
	/**
	 * @var LocalizationFilter
	 */
	private $localizationFilter;
	private $em;
	/**
	 * @var IEntitySlugFilter
	 */
	private $entitySlugFilter;
	/** @var Translator */
	private $translator;


	function __construct(AnyShapeRouteFilter $routeFilter, LocalizationFilter $localizationFilter, IEntitySlugFilter $entitySlugFilter, Translator $translator)
    {
        $this->routeFilter = $routeFilter;
		$this->localizationFilter = $localizationFilter;
		$this->entitySlugFilter = $entitySlugFilter;
		$this->translator = $translator;
	}


    /**
     * @return \Nette\Application\IRouter
     */
    public function createRouter()
    {
			$flags = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS']) ? Route::SECURED : null;
        $router = new RouteList();

		$this->localizationFilter->setFile(__DIR__.'/../config/routes.neon');

		// index php one way (BC)
        $router[] = new Route('index.php', 'Default:default', $flags | Route::ONE_WAY);

		// homepage news with slugs
		$router[] = new TranslatedRoute('novinky/<slug .+><id [0-9]+>', [
			'locale' => 'cs',
			'presenter' => 'News',
			'action' => 'detail',
			'id' => NULL,
			'slug' => NULL,
			NULL => $this->entitySlugFilter->create(News::getClassName())->getFilters(),
		], $flags);

		//branches
		$slugFilter = $this->entitySlugFilter->create(Branch::getClassName());
		$router[] = new TranslatedRoute('<presenterSlug .+>/<slug [a-zA-Z\-]*><id [0-9]+>', [
			'locale' => 'cs',
			'presenter' =>  'Branch',
			'action' => 'detail',
			'id' => NULL,
			'slug' => NULL,
			NULL => [
				Route::FILTER_OUT =>
					function($params) use ($slugFilter) {
						$params['presenterSlug'] = ($this->localizationFilter->getTableOutKey('Branch:default', $params['locale']));
						return $slugFilter->wayOut($params);
					},
				Route::FILTER_IN =>
					function($params) {
						if(empty($params['presenterSlug']) || empty($params['id']) || $params['presenterSlug'] !== $this->localizationFilter->getTableOutKey('Branch:default', $params['locale'])) {
							return NULL;
						}
						return $params;
					}
			],
		], $flags);
		//anyshape pages routing
		$router[] = new TranslatedRoute('<presenterSlug [a-z\-]+>/<slug .+>/?', [
			'locale' => NULL,
			'id' => NULL,
			'presenter' => 'Personal',
			NULL => [
				Route::FILTER_OUT =>
					function($params) {
						$params['presenterSlug'] = ($this->localizationFilter->getTableOutKey('Personal:prefix', $params['locale']));
						return call_user_func($this->routeFilter->getOutFilter('slug', 'id', 'default'), $params);
					},
				Route::FILTER_IN =>
					function($params) {
						if(empty($params['presenterSlug']) || $params['presenterSlug'] !== $this->localizationFilter->getTableOutKey('Personal:prefix', $params['locale'])) {
							return NULL;
						}
						unset($params['presenterSlug']);
						return call_user_func($this->routeFilter->getInFilter('slug', 'id', 'default'), $params);
					}
			]
		], $flags);

		//anyshape pages routing
		$router[] = new TranslatedRoute('<presenterSlug [a-z\-]+>/<slug .+>/?', [
			'locale' => NULL,
			'id' => NULL,
			'presenter' => 'Commercial',
			NULL => [
				Route::FILTER_OUT =>
					function($params) {
						$params['presenterSlug'] = ($this->localizationFilter->getTableOutKey('Commercial:prefix', $params['locale']));
						return call_user_func($this->routeFilter->getOutFilter('slug', 'id', 'default'), $params);
					},
				Route::FILTER_IN =>
					function($params) {
						if(empty($params['presenterSlug']) || $params['presenterSlug'] !== $this->localizationFilter->getTableOutKey('Commercial:prefix', $params['locale'])) {
							return NULL;
						}
						unset($params['presenterSlug']);
						return call_user_func($this->routeFilter->getInFilter('slug', 'id', 'default'), $params);
					}
			]
		], $flags);


		//static pages
		$router[] = new TranslatedRoute('<slug .+>/?', [
			'locale' => NULL,
			'id' => NULL,
			NULL => $this->localizationFilter->getFilters(),
		], $flags);


		//anyshape pages routing
		$router[] = new TranslatedRoute('<slug .+>/?', [
			'locale' => NULL,
			'id' => NULL,
			NULL => $this->routeFilter->getFilters(),
		], $flags);

		//default
		$router[] = new TranslatedRoute('<presenter>/<action>[/<id>]', [
			'presenter' => 'Default',
			'action' => 'default',
			'locale' => NULL,
			'id' => NULL,
		], $flags);

        return $router;
    }

}
