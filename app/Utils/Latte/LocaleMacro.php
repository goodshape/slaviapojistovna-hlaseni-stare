<?php


namespace App\Utils\Latte;


use Latte\CompileException;
use Latte\Compiler;
use Latte\MacroNode;
use Latte\Macros\MacroSet;
use Latte\PhpWriter;
use Latte\Template;

class LocaleMacro extends MacroSet
{
	private $used = FALSE;

	public static function install(Compiler $compiler)
	{
		$me = new self($compiler);
		$me->addMacro('locale', array($me, 'macroLocale'), '} unset($_localeParam);');
		$me->addMacro('ifLocale', array($me, 'macroLocale'), '} unset($_localeParam);');

		return $me;
	}

	public function macroLocale(MacroNode $node, PhpWriter $writer)
	{
		$this->used = TRUE;
		$words = [];
		$words = $node->args;
		if (!$words) {
			throw new CompileException("Missing locale in n:{$node->name}.");
		}
		$node = $writer->formatWord($words);
		return $writer->write('$_localeParam = '.$node.'; if($_localeParam === NULL || in_array($locale, (is_array($_localeParam) ? $_localeParam : (array) explode(",",$_localeParam)), TRUE)) {');
	}

	/**
	 */
	public function initialize()
	{
		$this->used = FALSE;
	}



	/**
	 * Finishes template parsing.
	 *
	 * @return array(prolog, epilog)
	 */
	public function finalize()
	{
		if (!$this->used) {
			return array();
		}

		return array(
			get_called_class() . '::validateTemplateParams($template);',
			NULL
		);
	}

	/**
	 * @param \Nette\Templating\Template $template
	 * @throws \Nette\InvalidStateException
	 */
	public static function validateTemplateParams(Template $template)
	{
		$params = $template->getParameters();
		if (!isset($params['locale'])) {
			$where = isset($params['control']) ?
				" of component " . get_class($params['control']) . '(' . $params['control']->getName() . ')'
				: NULL;

			throw new \Nette\InvalidStateException(
				'Please provide an active locale string ' .
				'as a parameter $locale to template' . $where
			);
		}
	}


} 