<?php


namespace App;


use App\Components\Megamenu;

class PersonalInsurancePresenter extends FrontPresenter
{
	use TLocalizedTemplates;


	protected function beforeRender()
	{
		parent::beforeRender();

		$this->template->megamenuOpened = Megamenu::PERSONAL;
	}


} 