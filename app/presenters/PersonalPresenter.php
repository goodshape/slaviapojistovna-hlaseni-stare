<?php


namespace App;


use App\Components\Megamenu;

class PersonalPresenter extends SimplePagePresenter
{

	protected function beforeRender()
	{
		parent::beforeRender();

		$this->template->megamenuOpened = Megamenu::PERSONAL;
	}

}