<?php


namespace App;


use App\Components\IBottomContactFormFactory;
use App\Components\Megamenu;

class CommercialInsurancePresenter extends FrontPresenter
{

	use TLocalizedTemplates;

	protected function beforeRender()
	{
		parent::beforeRender();

		$this->template->megamenuOpened = Megamenu::COMMERCIAL;
		$this->template->person = $this->getPerson();
	}

	protected function createComponentBottomContactForm(IBottomContactFormFactory $factory)
	{
		$control = $factory->create()->setPerson($this->getPerson());

		return $control;
	}

	private function getPerson()
	{
		return (object) [
			'name' => 'Ing. Lucie Ponertová',
			'mail' => 'prumysl@slavia-pojistovna.cz',
		];
	}

} 