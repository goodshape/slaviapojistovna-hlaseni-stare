<?php

namespace App;

use App\Model\Entities\PageAttachment;
use Goodshape\Anyshape\Entities\StructureEntity;
use Goodshape\Anyshape\PageTrait;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\BadRequestException;
use Nette\Application\UI\Form;
use App\Model\Entities\MailEntity;

class SimplePagePresenter extends FrontPresenter
{
    use PageTrait;
	/** @var EntityManager @autowire */
	protected $em;
	/** @var App\Model\Services\MailService @autowire */
	protected $mailService;

	public function renderDefault($id)
	{
		if(!$this->page || $this->page->getStatus() !== StructureEntity::STATUS_ENABLE) {
			throw new BadRequestException;
		}
		$this->template->showForm = in_array($id, array(99,164));
		$this->template->attachments = $this->em->getDao(PageAttachment::getClassName())->findBy(['page' => $id], ['sorting' => 'asc', 'id' => 'desc']);
	}
	public function sendContactCatcher($form, $values){

        $entity = new MailEntity();
        $entity->sender = "noreply@slavia-pojistovna.cz";
        $entity->addRecipient("daniel.probost@slavia-pojistovna.cz");
        $entity->message = "Callback - Pomoc se sjednáním\n\n\n" .
            "Telefonní číslo: {$values->phone}\n" .
            "URL: {$values->url}\n" .

        $entity->subject = "Callback - Slavia pojišťovna - Povinné ručení";
		$this->mailService->send($entity);
	}
	public function createComponentContactCatcher(){
		$form = new Form;
		$form->addCheckbox("hun","Terms And Conditions") 
			->addRule(Form::BLANK);
		//jinak taky HONEYPOT
		$form->addHidden("url",$this->context->httpRequest->url);
		$form->addText("phone","Telefon")
			->setRequired(true)
			->addRule(Form::PATTERN,"Zadejte telefonní číslo",'[\+0-9]{9,24}');
		$form->addSubmit("contactCatcher","Chci pomoc");
		$form->onSuccess[] = [$this, 'sendContactCatcher'];
		return $form;
	}
}