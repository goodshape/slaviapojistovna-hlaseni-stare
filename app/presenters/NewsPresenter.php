<?php


namespace App;


use App\Components\INewsControlFactory;
use App\Model\Entities\News;
use App\Model\Services\NewsService;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\BadRequestException;
use Nette\Utils\Paginator;

class NewsPresenter extends FrontPresenter
{

	/** @var EntityManager @autowire */
	protected $em;
	/** @var NewsService @autowire */
	protected $newsService;

	public function actionDetail($id)
	{
		$this->template->item = $item = $this->em->find(News::getClassName(), $id);

		if(!$item) {
			throw new BadRequestException();
		}

	}

	protected function createComponentNewsList(INewsControlFactory $factory)
	{
		/** @var Paginator $paginator */
		$paginator = $this['paginator']->getPaginator();
		$control = $factory->create()
			->setOffset($paginator->getOffset())
			->setLimit($paginator->getItemsPerPage());

		return $control;
	}

	/**
	 * @return \VisualPaginator
	 */
	protected function createComponentPaginator()
	{
		$control = new \VisualPaginator();

		$p = $control->getPaginator();

		$p->setItemsPerPage(20);

		$p->setItemCount($this->newsService->getPublishedCount());

		return $control;
	}



} 