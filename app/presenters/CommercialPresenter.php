<?php


namespace App;


use App\Components\IBottomContactFormFactory;
use App\Components\ILiabilityCalculatorFactory;
use App\Components\Megamenu;

class CommercialPresenter extends SimplePagePresenter
{

	protected function beforeRender()
	{
		parent::beforeRender();

		$this->template->megamenuOpened = Megamenu::COMMERCIAL;
		$this->template->person = $this->getPerson();
	}

	protected function createComponentBottomContactForm(IBottomContactFormFactory $factory)
	{
		$control = $factory->create()->setPerson($this->getPerson());

		return $control;
	}

	protected function createComponentCalculator(ILiabilityCalculatorFactory $factory)
	{
		$control = $factory->create();

		return $control;
	}

	private function getPerson()
	{
		return (object) [
			'name' => 'Ing. Lucie Ponertová',
			'mail' => 'prumysl@slavia-pojistovna.cz',
		];
	}
}