<?php


namespace App;


use App\Components\FlashMessages\TTranslatedFlashMessages;
use App\Components\IContactFormFactory;

class ContactPresenter extends FrontPresenter
{

	use TLocalizedTemplates;

	public function actionForm()
	{
		$this['contactForm']->onSuccess[] = function($form) {
			$this->flashSuccess('front.flash.contactForm.success');

			$this->redirect('this');
		};
	}


	protected function createComponentContactForm(IContactFormFactory $factory)
	{
		$control = $factory->create();

		return $control;
	}

} 