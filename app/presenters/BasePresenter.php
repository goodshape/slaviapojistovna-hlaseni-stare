<?php

namespace App;

use App\Components;
use Goodshape\Anyshape\UI\AnyShapePresenterTrait;
use Kdyby\Autowired\AutowireComponentFactories;
use Kdyby\Autowired\AutowireProperties;
use Nette;


/**
 * Base presenter for all application presenters.
 *
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter
{
    use AutowireComponentFactories;
    use AutowireProperties;
    use Components\FlashMessages\TTranslatedFlashMessages;
	use AnyShapePresenterTrait {
		AnyShapePresenterTrait::beforeRender as _beforeRender;
	}

    protected function beforeRender()
    {
        parent::beforeRender();
		$this->_beforeRender();
        $this->redrawControl('flashMessages');
        $this->template->useFullAssets = $this->context->parameters['useFullAssets'];
        
        if (!in_array($this->name, ["Report", "Admin"])) {
            http_response_code(404);
            die();
            $this->error(404);
        }
    }


    protected function createComponentFlashMessages()
    {
        return new Components\FlashMessages\FlashMessagesControl();
    }


    protected function createComponentGoogleAnalytics()
    {
        return new Components\GoogleAnalytics\GoogleAnalyticsControl();
    }



}
