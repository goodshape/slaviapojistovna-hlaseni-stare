<?php


namespace App;


use App\Components\CarReportForm;
use App\Components\ICarReportFormFactory;
use App\Components\IKrystofFormFactory;
use App\Components\IReportFormFactory;
use App\Components\KrystofForm;
use App\Components\ITravelReportFormFactory;
use App\Components\Report\DocumentsForm;
use App\Components\Report\IDocumentsFormFactory;
use App\Components\ReportForm;
use Nette\Utils\Strings;
use Tracy\Debugger;
// ROUTE ROUTES nahlasit-skodu --- routy se určují v lang souborech.

class ReportPresenter extends FrontPresenter
{
	protected function startup()
	{
		parent::startup();

		$type = NULL;
		$action = $this->getAction();

		/** @var ReportForm|DocumentsForm|KrystofForm|CarReportForm $control */
		if($action === 'documents') {
			$this->error(404);
			return;
			$control = $this['documentsForm'];
		} elseif($action === 'krystof') {
			$control = $this['krystofForm'];
		} elseif($action === 'travelReportForm') {
			$control = $this['travelReportForm'];
		} else {
			if(Strings::startsWith($action, 'car')) {
				$control = $this['carForm'];
			} else {
				$control = $this['form'];
			}
			$control->setType($action);
		}
		$control->onSuccess[] = function() {
			$this->flashSuccess('control.report.sended');
			$this->redirect('this', ["goal"=>1]);
			$this->redirect('this');
		};
		$control->onError[] = function($control, $e) {
			$this->flashSuccess('control.report.error');
			Debugger::log($e);
		};
		if (preg_match("{https?://[^/]*\.(slavia-pojistovna|goodshape)\.cz\b}", @$_SERVER["HTTP_REFERER"])) {
			header_remove("X-Frame-Options");
		}

	}


	protected function createComponentForm(IReportFormFactory $factory)
	{
		return $factory->create();
	}

	protected function createComponentTravelReportForm(ITravelReportFormFactory $factory)
	{
		return $factory->create();
	}

	protected function createComponentCarForm(ICarReportFormFactory $factory)
	{
		return $factory->create();
	}

	protected function createComponentDocumentsForm(IDocumentsFormFactory $factory)
	{
		return $factory->create();
	}

	protected function createComponentKrystofForm(IKrystofFormFactory $factory)
	{
		return $factory->create();
	}


}
