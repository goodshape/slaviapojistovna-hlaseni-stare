<?php

namespace App;

use App\Components\INewsControlFactory;
use Model;
use Nette;


/**
 * Homepage presenter.
 */
class DefaultPresenter extends FrontPresenter
{


    public function renderDefault()
    {

    }

	protected function createComponentHomepageNews(INewsControlFactory $factory)
	{
		$control = $factory->create()->setLimit(2);

		return $control;
	}

}
