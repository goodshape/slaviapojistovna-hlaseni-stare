<?php


namespace App;


use App\Components\IBannerControlFactory;
use App\Components\IBranchMapFactory;
use App\Components\ICallBoxFactory;
use App\Components\IFooterMenuFactory;
use App\Components\IMegamenuFactory;
use App\Components\IMenuControlFactory;
use App\Components\IReportMenuFactory;
use App\Model\Entities\MenuMap;

abstract class FrontPresenter extends BasePresenter
{

	/** @var string @persistent */
	public $locale;

	protected function beforeRender()
	{
		parent::beforeRender();

		$this->template->locale = $this->locale;
	}

	protected function createComponentUsefulAdvice(IMenuControlFactory $factory)
	{
		$control = $factory->create(MenuMap::USEFUL_ADVICE);
		$control->setDepth(1);
		$control->addMenuTemplate('items.latte');

		return $control;
	}

	protected function createComponentSecondaryMenu(IMenuControlFactory $factory)
	{
		$control = $factory->create(MenuMap::TOP_SECONDARY);
		$control->addMenuTemplate('items.latte');
		$control->setItemClass('nav-item');

		return $control;
	}

	protected function createComponentNegotiateOnline(IMenuControlFactory $factory)
	{
		$control = $factory->create(MenuMap::NEGOTIATE_ONLINE);

		$control->setDepth(1);
		$control->addMenuTemplate('items.latte');


		return $control;
	}

	protected function createComponentCallBox(ICallBoxFactory $factory)
	{
		$control = $factory->create();

		return $control;
	}

	protected function createComponentMegamenu(IMegamenuFactory $factory)
	{
		$control = $factory->create();

		return $control;
	}

	protected function createComponentFooter(IFooterMenuFactory $factory)
	{
		$control = $factory->create();

		return $control;
	}

	protected function createComponentBanner(IBannerControlFactory $factory)
	{
		$control = $factory->create();

		return $control;
	}

	protected function createComponentBranchMap(IBranchMapFactory $factory)
	{
		$control = $factory->create();

		return $control;
	}

	protected function createComponentReportMenu(IReportMenuFactory $factory)
	{
		$control = $factory->create();

		return $control;
	}


} 