<?php


namespace App;


trait TLocalizedTemplates
{

	public function formatTemplateFiles()
	{
		$name = $this->getName();
		$presenter = substr($name, strrpos(':' . $name, ':'));
		$dir = dirname($this->getReflection()->getFileName());
		$dir = is_dir("$dir/templates") ? $dir : dirname($dir);
		return array(
			"$dir/templates/$presenter/{$this->locale}/$this->view.latte",
			"$dir/templates/$presenter/$this->view.latte",
		);
	}

} 