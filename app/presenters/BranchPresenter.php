<?php


namespace App;


use App\Model\Entities\Branch;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\ORMException;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\BadRequestException;

class BranchPresenter extends FrontPresenter
{


	/** @var EntityManager @autowire */
	protected $em;

	public function renderDetail($id)
	{
		try {
			$this->template->page = $page = $this->em->find(Branch::getClassName(), $id);
		} catch (ORMException $e) {
			$page = NULL;
		}

		if(!$page) {
			throw new BadRequestException(404);
		}
	}

} 