<?php


namespace App\Components;


use App\Model\Entities\News;
use App\Model\Services\NewsService;
use Kdyby\Doctrine\EntityManager;

class NewsControl extends BaseControl
{

	private $limit = 1;
	private $offset = 0;
	/** @var NewsService */
	private $newsService;

	public function __construct(NewsService $newsService)
	{
		$this->newsService = $newsService;
	}


	public function render()
	{
		$this->template->items = $this->newsService->getPublished($this->limit, $this->offset);

		$this->template->render();
	}

	/**
	 * @param int $limit
	 */
	public function setLimit($limit)
	{
		$this->limit = $limit;

		return $this;
	}

	/**
	 * @param int $offset
	 */
	public function setOffset($offset)
	{
		$this->offset = $offset;
		return $this;
	}

}


interface INewsControlFactory
{

	/** @return NewsControl */
	public function create();
}
