<?php


namespace App\Components;


use App\Model\Entities\MenuMap;

class ReportMenu extends BaseControl
{

	public function render($liClass = NULL)
	{
		$this->template->liClass = $liClass;
		$this->template->render();
	}

	protected function createComponentDamageReport(IMenuControlFactory $factory)
	{
		$control = $factory->create(MenuMap::DAMAGE_REPORT);
		$control->addMenuTemplate('items.latte');

		return $control;
	}

	protected function createComponentDocumentsForReport(IMenuControlFactory $factory)
	{
		$control = $factory->create(MenuMap::DAMAGE_REPORT_DOCUMENTS);
		$control->addMenuTemplate('items.latte');

		return $control;
	}

}


interface IReportMenuFactory
{

	/** @return ReportMenu */
	public function create();
}
