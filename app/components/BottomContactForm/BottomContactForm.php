<?php

namespace App\Components;

use App\Model\Entities\MailEntity;
use App\Model\Services\MailService;
use Nette\Application\UI\Form;

class BottomContactForm extends BaseControl
{
	/** @var MailService */
	private $mailer;
	private $person;

	public function __construct(MailService $mailer)
	{
		$this->mailer = $mailer;
	}


	public function render()
	{
		$this->template->render();
	}

	/**
	 * @param object $person
	 */
	public function setPerson($person)
	{
		$this->person = $person;

		return $this;
	}


	protected function createComponentContact(IBaseFormFactory $factory)
	{
		$control = $factory->create();

		$control->addText('name', 'control.bottomContact.form.name')
			->setRequired('control.bottomContact.form.error.notFilled');
		$control->addText('mail', 'control.bottomContact.form.mail')
			->setRequired('control.bottomContact.form.error.notFilled')
			->addRule($control::EMAIL, 'control.bottomContact.form.error.emailInvalid')
			->setType('email');
		$control->addTextArea('text', 'control.bottomContact.form.text')
			->setRequired('control.bottomContact.form.error.notFilled');

		$control->addSubmit('send', 'control.bottomContact.send');

		$control->onSuccess[] = callback($this, 'processContactForm');

		return $control;
	}

	protected function createComponentCallBack(IBaseFormFactory $factory)
	{
		$control = $factory->create();

		$control->addText('number', 'control.bottomContact.callback.number')
			->setType('phone')
			->setRequired('control.bottomContact.callback.error.notFilled');

		$control->addSubmit('send');

		$control->onSuccess[] = callback($this, 'processCallForm');

		return $control;
	}

	public function processContactForm(BaseForm $form, $values)
	{
		$mail = $this->mailer->createContactForm($values->name, $values->mail, $values->text, $this->person->mail);

		$this->sendMail($mail);
	}

	public function processCallForm(Form $form, $values)
	{
		$mail = $this->mailer->createCallBack($values->number, $this->person->mail);

		$this->sendMail($mail);
	}

	private function sendMail(MailEntity $mail)
	{
		$mail->addRecipient('info@slavia-pojistovna.cz');

		$this->mailer->send($mail);
		$this->flashMessage('control.bottomContact.form.success');
		$this->presenter->redirect('this');
	}

}


interface IBottomContactFormFactory
{

	/** @return BottomContactForm */
	public function create();
}
