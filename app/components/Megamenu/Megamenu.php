<?php


namespace App\Components;


use App\Model\Services\MenuService;
use Goodshape\Anyshape\Entities\StructureEntity;
use Kdyby\Doctrine\EntityManager;
use Nette\Caching\Cache;
use Nette\Caching\IStorage;
use Nette\Caching\Storages\DevNullStorage;
use Nette\Caching\Storages\MemoryStorage;

class Megamenu extends BaseControl
{

	const COMMERCIAL = 'commercial';

	const PERSONAL = 'personal';
	private $cache;
	private $opened;

	private $em;
	/**
	 * @var MenuService
	 */
	private $menuService;
	/**
	 * @var IMenuGeneratorFactory
	 */
	private $generatorFactory;

	private $images = [
		self::COMMERCIAL => [
			'icon-majetek.png',
			'icon-management.png',
			'icon-odpovednost.png',
			'icon-zpusobilost.png',
			'icon-eet.png',
			'icon-more.png',
		],

		self::PERSONAL => [
			'icon-car.png',
			'icon-compas.png',
			'icon-key.png',
			'icon-doc.png',
			'icon-hammer.png',
			'icon-more.png',
		]
	];
	private $generators;

	function __construct(EntityManager $em, MenuService $menuService, IMenuGeneratorFactory $generatorFactory, IStorage $storage)
	{
		$this->em = $em;
		$this->menuService = $menuService;
		$this->generatorFactory = $generatorFactory;
		$this->cache = new Cache(new MemoryStorage(), get_class($this));
	}


	public function render($type = NULL)
	{
		$key = $type. $this->parent->getName() . $this->presenter->locale;
		if (!isset($this->cache[$key])) {
			ob_start();
			//$ref = $this->getReflection()->getParentClass();
			//$this->template->setFile(dirname($ref->fileName) . DIRECTORY_SEPARATOR . lcfirst($ref->shortName) . '.latte');
			$this->doRender($type);
			$data = ob_get_clean();
			$this->cache->save($key, $data, [
				Cache::EXPIRATION => '+15 min',
			]);
		}
		echo $this->cache[$key];
	}
	/** @internal */
	private function doRender($type)
	{
		$menu = [];
		if (!$type || $type === self::PERSONAL) {
			$menu[self::PERSONAL] = $this->getData(self::PERSONAL);
		}
		if (!$type || $type === self::COMMERCIAL) {
			$menu[self::COMMERCIAL] = $this->getData(self::COMMERCIAL);
		}

		$this->template->opened = $this->opened === NULL ? self::PERSONAL : $this->opened;
		$this->template->menu = array_filter($menu);
		$this->template->render();
	}

	public function renderSmall($type = NULL)
	{

		$this->template->setFile(__DIR__ . '/smallMenu.latte');
		$this->render($type);
	}

	public function renderSmalltop($type = NULL)
	{

		$this->template->setFile(__DIR__ . '/smalltopmenu.latte');
		$this->render($type);
	}

	public function renderBottom($type = NULL)
	{
		$this->template->setFile(__DIR__ . '/bottomMenu.latte');
		$this->render($type);
	}

	public function renderLarge($opened = 'personal')
	{
		$this->opened = $opened;
		$this->template->setFile(__DIR__ . '/largeMenu.latte');
		$this->render();
	}

	private function getData($type)
	{
		$root = $this->menuService->getRootNode($type);
		$g = $this->getGenerator();
		$g->setRootNode($root);
		return $g->getPages();
	}

	protected function createComponentGenerator(IMenuGeneratorFactory $factory)
	{
		$control = $factory->create(NULL, 4);

		return $control;
	}

	/** @return MenuGenerator */
	private function getGenerator()
	{
		return $this['generator'];
	}

	public function getTargetLink(StructureEntity $page)
	{

		if ($page->hasPresenter()) {
			return $this->presenter->link($page->getPageType()->getPresenter().':default', ['id' => $page->id]);
		} elseif($page->isRedirect()) {
			if(is_numeric($page->getUrl())) {
				return $this->presenter->link(":Anyshape:Redirect:default", [
					'id' => $page->getId(),
					'locale' => isset($this->presenter->locale) ? $this->presenter->locale : NULL,
				]);
			} else {
				return $page->getUrl();
			}
		}

		return '#';
	}

	public function getImage($type, $counter)
	{
		if(in_array($this->presenter->locale, ['vi', 'ru'])) {
			return $this->images[$type][3];
		}
		$counter--;
		if($type === self::PERSONAL && $this->presenter->locale !== 'cs' && $counter > 3) {
			$counter++;
		}

		if($type === self::COMMERCIAL && $this->presenter->locale !== 'cs' && $counter === 4) {
			$counter++;
		}

		if(isset($this->images[$type][$counter])) {
			return $this->images[$type][$counter];
		}
		return NULL;
	}

	public function getChildren($type, StructureEntity $entity)
	{
		return $this->getGenerator($type)->getChildren($entity);
	}


}


interface IMegamenuFactory
{

	/** @return Megamenu */
	public function create();
}
