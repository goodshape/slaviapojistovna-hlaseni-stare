<?php


namespace App\Components;


use App\Model\Services\MailService;

class LiabilityCalculator extends BaseControl
{

	private $insurance = [
		1 => 3230, 5025, 6819, 8614, 9830,
		11525, 13220, 14914, 16609, 18304,
		19293, 20928, 22563, 24198, 25833,
		27468, 29103, 30738, 32373, 34008
	];

	/** @var MailService */
	private $mailer;
	/** @var int @persistent */
	public $cars = 0;

	function __construct(MailService $mailer)
	{
		$this->mailer = $mailer;
	}


	public function render()
	{
		$this->template->cars = $this->cars;
		$this->template->render();
	}

	protected function createComponentForm(IBaseFormFactory $factory)
	{
		$control = $factory->create();

		$control->addText("cars", "Počet provozovaných vozidel")
			->setType('number')
			->setRequired("Vyplňte prosím počet provozovaných vozidel");

		$control->addText('mail', 'Váš e-mail')
			->setType('email')
			->setRequired("Vyplňte prosím Váš e-mail")
			->addRule($control::EMAIL, 'Vyplntě prosím email ve správném formátu');

		$control->addText('phone', 'Váš telefon')
			->setType('phone')
			->setRequired("Vyplňte prosím Váš telefon")
			->addRule($control::MIN_LENGTH, 'Vložte správné telefonní číslo',  9);

		$control->addSubmit('send', 'Spočítat kalkulaci');

		$control->onSuccess[] = callback($this, 'processForm');



		return $control;
	}

	public function processForm(BaseForm $form, $values)
	{

		$this->mailer->send($this->mailer->createCalculator($values->cars, $values->mail, $values->phone));

		$this->cars = (int) $values->cars;

		$this->presenter->redirect("this#calculator");

	}

	public function getInsuranceLimit()
	{
		$amoutn = 0;
		if($this->cars === 1) {
			$amoutn = 247518;
		}
		elseif($this->cars > 1 && $this->cars <= 20) {
			$amoutn = 247518 + (($this->cars - 1) * 137510);
		} else {
			return "";
		}

		return $this->formatAmount($amoutn);
	}

	public function getInsurance()
	{
		if(isset($this->insurance[$this->cars])) {
			return $this->formatAmount($this->insurance[$this->cars]);
		}

		return "dle individuální nabídky";
	}

	/**
	 * @param $amoutn
	 * @return string
	 */
	private function formatAmount($amoutn)
	{
		$formatter = \NumberFormatter::create('cs', \NumberFormatter::CURRENCY);
		$formatter->setTextAttribute(\NumberFormatter::CURRENCY_CODE, 'CZK');
		$formatter->setAttribute(\NumberFormatter::FRACTION_DIGITS, 0);

		return $formatter->formatCurrency($amoutn, 'CZK');
	}

}


interface ILiabilityCalculatorFactory
{

	/** @return LiabilityCalculator */
	public function create();
}
