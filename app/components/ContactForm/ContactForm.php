<?php


namespace App\Components;


use App\Model\Services\MailService;
use Kdyby\Translation\Translator;

class ContactForm extends BaseControl
{
	/** @var Translator */
	private $translator;
	/**
	 * @var MailService
	 */
	private $mailService;

	public $onSuccess = [];

	public function __construct(Translator $translator, MailService $mailService)
	{
		$this->translator = $translator;
		$this->mailService = $mailService;
	}


	public function render()
	{
		$this->template->render();
	}

	protected function createComponentForm(IBaseFormFactory $factory)
	{
		$control = $factory->create();

		$group = $control->addGroup('control.contactForm.contact');


		$control->addText('name', 'control.contactForm.name')
			->setRequired('control.contactForm.error.notFilled');
		$control->addText('surname', 'control.contactForm.surname')
			->setRequired('control.contactForm.error.notFilled');
		$control->addText('degree', 'control.contactForm.degree');

		$control->addText('phone', 'control.contactForm.phone')
			->setType('phone');
		$control->addText('mail', 'control.contactForm.mail')
			->setType('email')
			->setRequired('control.contactForm.error.notFilled');

		$control->addText('street', 'control.contactForm.street')
			->setOption('gridNewRow', TRUE)
			->setRequired('control.contactForm.error.notFilled');
		$control->addText('city', 'control.contactForm.city')
			->setRequired('control.contactForm.error.notFilled');
		$control->addText('postal', 'control.contactForm.postal')
			->setRequired('control.contactForm.error.notFilled');

		$control->addGroup();

		$control->addSelect('info', 'control.contactForm.info.label', [
			'damage' => 'control.contactForm.info.items.damage',
			'contract' => 'control.contactForm.info.items.contract',
			'personal' => 'control.contactForm.info.items.personal',
			'commercial' => 'control.contactForm.info.items.commercial',
			'other' => 'control.contactForm.info.items.other'
		])->setPrompt('control.contactForm.placeholder.info')
			->setRequired('control.contactForm.error.notFilled');

		$control->addText('contract', 'control.contactForm.contract');
		$control->addText('additional', 'control.contactForm.additional')
			->setOption('gridNewRow', TRUE);
		$control->addTextArea('text', 'control.contactForm.text')
			->setOption('gridSize', 2)
			->setOption('gridNewRow', TRUE)
			->setRequired('control.contactForm.error.notFilled');

		$control->addSubmit('send', 'control.contactForm.submit');

		$control->onSuccess[] = callback($this, 'processForm');

		return $control;
	}

	public function processForm(BaseForm $form, $values)
	{
		$data = [];
		foreach ($values as $key => $value) {
			if(!$value) {
				continue;
			}
			if($key === 'info') {
				$key .='.label';
				$value = $this->translator->translate('contactForm.info.items.'.$value, NULL, [], 'control', 'cs');
			}
			$data[] = $this->translator->translate('contactForm.'.$key, NULL,[], 'control', 'cs').": ".$value;
		}

		$mail = $this->mailService->createContactForm($values->name.' '.$values->surname, $values->mail, implode("\n", $data));

		$this->mailService->send($mail);

		$this->onSuccess($form);

	}

}


interface IContactFormFactory
{

	/** @return ContactForm */
	public function create();
}
