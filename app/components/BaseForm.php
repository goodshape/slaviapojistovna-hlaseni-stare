<?php


namespace App\Components;


use App\Components\Forms\Renderer;
use Kdyby;
use Nette;
use Nette\Application\UI;
use QX\Forms\Controls\FormControlsTrait;

/**
 * Class BaseForm
 * @package App\Components
 *
 * @method addDynamic($name, $factory, $createDefault = 0, $forceDefault = FALSE)
 */
class BaseForm extends UI\Form
{
	use FormControlsTrait;

	public function __construct()
	{

	}

	public function injectDependencies(Kdyby\Translation\Translator $translator)
	{
		$this->setTranslator($translator);
	}

	/**
	 * @param UI\ITemplateFactory $templateFactory
	 */
	public function injectTemplateFactory(UI\ITemplateFactory $templateFactory)
	{
		$this->setRenderer(new Renderer($templateFactory));
	}

}

interface IBaseFormFactory
{
	/** @return BaseForm */
	function create();
}