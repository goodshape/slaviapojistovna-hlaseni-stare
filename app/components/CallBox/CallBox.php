<?php


namespace App\Components;


use App\Model\Entities\MenuMap;
use App\Model\Services\MailService;
use Kdyby\Translation\Translator;
use Nette\Application\UI\Form;

class CallBox extends BaseControl
{

	/** @var MailService */
	private $mailService;

	private static $renderFlash = TRUE;

	public function __construct(MailService $mailService)
	{
		$this->mailService = $mailService;
	}


	public function render()
	{
		$this->template->renderFlash = self::$renderFlash;
		self::$renderFlash = FALSE; //only once;

		$this->template->render();
	}

	protected function createComponentCallbackForm(IBaseFormFactory $factory)
	{
		$control = $factory->create();

		$control->addText('number', NULL)
			->setType('phone')
			->setRequired('control.callBox.form.error.notFilled');

		$control->addSubmit('send');

		$control->onSuccess[] = callback($this, 'processForm');

		return $control;
	}

	protected function createComponentReportMenu(IReportMenuFactory $factory)
	{
		$control = $factory->create();

		return $control;
	}

	public function processForm(Form $form, $values)
	{
		$mail = $this->mailService->createCallBack($values->number);

		$this->mailService->send($mail);

		$this->flashMessage('control.callBox.form.success');
		$this->presenter->redirect('this');
	}


}


interface ICallBoxFactory
{

	/** @return CallBox */
	public function create();
}
