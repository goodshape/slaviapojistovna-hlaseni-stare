<?php


namespace App\Components;


use App\Model\Entities\Banner;
use Kdyby\Doctrine\EntityManager;
use Kdyby\Translation\Translator;

class BannerControl extends BaseControl
{
	private $em;
	/**
	 * @var Translator
	 */
	private $translator;

	function __construct(EntityManager $em, Translator $translator)
	{
		$this->em = $em;
		$this->translator = $translator;
	}


	public function render()
	{
		$this->template->banner = $this->getData();
		$this->template->render();
	}

	private function getData()
	{
		$dao = $this->em->getDao(Banner::getClassName());

		$count = $dao->createQueryBuilder('b')->select("count(b.id) as count_it")
			->andWhere('(b.image_'.$this->translator->getLocale().' IS NOT NULL AND b.image_'.$this->translator->getLocale().' != \'\')')
			->andWhere('b.active = 1')->getQuery()->getSingleScalarResult();
		if(!$count) {
			return NULL;
		}

		return $dao->createQueryBuilder('b')->andWhere('b.active = 1')
			->andWhere('(b.image_'.$this->translator->getLocale().' IS NOT NULL AND b.image_'.$this->translator->getLocale().' != \'\')')
			->setFirstResult(mt_rand(0,$count-1))->setMaxResults(1)->getQuery()->getSingleResult();
	}

}


interface IBannerControlFactory
{

	/** @return BannerControl */
	public function create();
}
