<?php


namespace App\Components;


use Goodshape\Anyshape\Entities\StructureEntity;
use Goodshape\Anyshape\Facades\PageFacade;
use Nette\Caching\Cache;
use Nette\Caching\IStorage;

class MenuGenerator extends \Goodshape\Anyshape\Components\Menu\MenuControl
{
	private $cache;

	public function __construct($root, $maxDepth = 0, PageFacade $pageFacade, IStorage $storage)
	{
		parent::__construct($root, $maxDepth, $pageFacade);
		$this->cache = new Cache($storage, get_class($this));
	}

	public function render()
	{
		$key = $this->parent->getName() . $this->presenter->locale;
		if(!isset($this->cache[$key])) {
			ob_start();
			$ref = $this->getReflection()->getParentClass();
			$this->template->setFile(dirname($ref->fileName) . DIRECTORY_SEPARATOR . lcfirst($ref->shortName) . '.latte');
			parent::render();
			$data = ob_get_clean();
			$this->cache->save($key, $data, [
				Cache::EXPIRATION => '+15 min',
			]);
		}
		echo $this->cache[$key];
	}


	public function getPages()
	{
		$pages = parent::getPages();

		return $this->filterByLocale($pages);
	}

	/**
	 * @param $pages
	 * @return array
	 */
	protected function filterByLocale($pages)
	{
		$result = [];

		/** @var StructureEntity $item */
		foreach ($pages as $item) {
			$langs = array_filter($item->getLanguages());
			if (count($langs) > 0 && !in_array($this->presenter->locale, $langs)) {
				continue;
			}
			$result[] = $item;
		}

		return $result;
	}

	public function getChildren(StructureEntity $entity)
	{
		return $this->filterByLocale($entity->getChildren());
	}


}

interface IMenuGeneratorFactory  {
	/** @return MenuGenerator */
	function create($parentId, $maxDepth);
}