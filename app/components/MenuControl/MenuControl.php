<?php


namespace App\Components;


use App\Model\Services\MenuService;
use Goodshape\Anyshape\Components\Menu\MenuControlFactory;

class MenuControl extends BaseControl
{

	/** @var MenuService */
	private $menuService;
	/**
	 * @var
	 */
	private $type;

	private $depth = 2;
	private $templates;
	private $itemClass = 'in-item';


	public function __construct($type, MenuService $menuService)
	{
		$this->menuService = $menuService;
		$this->type = $type;
	}


	public function render()
	{
		/** @var \Goodshape\Anyshape\Components\Menu\MenuControl $menu */
		$menu = $this['menu'];
		$menu->setRootNode($this->menuService->getRootNode($this->type));
		if (!$this->templates) {
			$menu->addMenuTemplate(__DIR__ . '/menuControl.latte');
		} else {
			foreach ($this->templates as $t) {
				$menu->addMenuTemplate(__DIR__ . '/' . $t);
			}
		}
		$menu->template->itemClass = $this->itemClass;
		$menu->render();
	}

	/**
	 * @param int $depth
	 */
	public function setDepth($depth)
	{
		$this->depth = $depth;
	}

	/**
	 * @param mixed $itemClass
	 */
	public function setItemClass($itemClass)
	{
		$this->itemClass = $itemClass;
	}

	protected function createComponentMenu(IMenuGeneratorFactory $controlFactory)
	{
		return $controlFactory->create(NULL, $this->depth);
	}

	public function addMenuTemplate($filename)
	{
		$this->templates[] = $filename;
	}

}


interface IMenuControlFactory
{

	/** @return MenuControl */
	public function create($root);
}
