<?php


namespace App\Components;

use App\Model\Entities\Branch;
use Kdyby\Doctrine\EntityManager;

class BranchMap extends BaseControl
{

	/** @var EntityManager */
	private $em;

	public function __construct(EntityManager $em)
	{
		$this->em = $em;
	}


	public function render($active = NULL)
	{
		$this->template->selected = $active;
		$this->template->items = $this->getData();
		$this->template->render();
	}

	private function getData()
	{
		return $this->em->getDao(Branch::getClassName())->findAssoc([], 'district');
	}

}


interface IBranchMapFactory
{

	/** @return BranchMap */
	public function create();
}
