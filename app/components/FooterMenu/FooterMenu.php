<?php


namespace App\Components;


use App\Model\Entities\MenuMap;

class FooterMenu extends BaseControl
{




	public function render()
	{
		$this->template->render();
	}

	protected function createComponentMegamenu(IMegamenuFactory $factory)
	{
		$control = $factory->create();

		return $control;
	}

	protected function createComponentNegotiateOnline(IMenuControlFactory $factory)
	{
		$control = $factory->create(MenuMap::NEGOTIATE_ONLINE);

		$control->addMenuTemplate('items.latte');

		return $control;
	}

}


interface IFooterMenuFactory
{

	/** @return FooterMenu */
	public function create();
}
