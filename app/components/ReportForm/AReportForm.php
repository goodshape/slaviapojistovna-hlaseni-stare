<?php


namespace App\Components;


abstract class AReportForm extends BaseControl
{

	public $onSuccess = [];

	public $onError = [];

	public function render()
	{
		$this->template->setFile(__DIR__.'/reportForm.latte');
		$this->template->render();
	}

} 