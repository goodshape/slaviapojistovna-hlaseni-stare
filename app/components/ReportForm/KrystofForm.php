<?php


namespace App\Components;


use App\Components\Forms\Renderer;
use Exception;

class KrystofForm extends AReportForm
{

	private $formHandler;

	function __construct(FormHandler $formHandler)
	{
		$this->formHandler = $formHandler;
	}
	
	
	public function addFileUploads($form, $title)
	{
		$group = $form->addGroup($title, true);

		$container = $form->addContainer('attachments' . str_replace("-", "", \Nette\Utils\Strings::webalize($title)));

		$container->addUpload('files','attachments.group', true)
			->addRule(BaseForm::MAX_FILE_SIZE, 'maxSize', 250 * 1024 * 1024)
			->setOption('template', '@fileRepeater.latte')
			->setOption(Renderer::GRID_SIZE, 2);

		$group->add($container->getControls());
	}

	protected function createComponentForm(IBaseFormFactory $factory)
	{
		$control = $factory->create();

		$control->setTranslator($control->getTranslator()->domain('control.report'));

		$control->addGroup('insured.group');

		$container = $control->addContainer('insured');

		$container->addText('name', 'insured.name')
			->setRequired('errors.notFilled');
		$container->addText('contract', 'insured.contract')
			->setRequired('errors.notFilled');
		$container->addText('mail', 'insured.mail')
			->setRequired('errors.notFilled')
			->addCondition($control::FILLED)
			->addRule($control::EMAIL, 'errors.emailInvalid');
		$container->addText('address', 'insured.address')
			->setRequired('errors.notFilled');
		$container->addText('postal', 'insured.postal')
			->setRequired('errors.notFilled');
		$container->addText('personalId', 'insured.personalId')
			->setRequired('errors.notFilled');
		$container->addText('phone', 'insured.phone')
			->setRequired('errors.notFilled');
		$container->addText('account', 'insured.account')
			->setRequired('errors.notFilled');

		$control->addGroup('event.group');
		$container = $control->addContainer('event');

		$container->addText('date', 'krystof.event.date')
			->setRequired('errors.notFilled');
		$container->addText('location', 'krystof.event.location')
			->setRequired('errors.notFilled');
		$container->addTextArea('description', 'event.description')
			->setOption(Renderer::GRID_SIZE, 2)
			->setRequired('errors.notFilled');



		$control->addGroup('krystof.offence.group');
		$container = $control->addContainer('offence');
		$container->addUpload('decision', 'krystof.offence.decision')
			->addRule(BaseForm::MAX_FILE_SIZE, 'maxSize', 250 * 1024 * 1024)
			->setOption('template', '@upload.latte')
			->setOption(Renderer::GRID_SIZE, 2);
		$container->addUpload('register', 'krystof.offence.register')
			->addRule(BaseForm::MAX_FILE_SIZE, 'maxSize', 250 * 1024 * 1024)
			->setOption('template', '@upload.latte')
			->setOption(Renderer::GRID_SIZE, 2);
		$container->addUpload('license', 'krystof.offence.license')
			->addRule(BaseForm::MAX_FILE_SIZE, 'maxSize', 250 * 1024 * 1024)
			->setOption('template', '@upload.latte')
			->setOption(Renderer::GRID_SIZE, 2);
		$container->addUpload('technical', 'krystof.offence.technical')
			->addRule(BaseForm::MAX_FILE_SIZE, 'maxSize', 250 * 1024 * 1024)
			->setOption('template', '@upload.latte')
			->setOption(Renderer::GRID_SIZE, 2);
		$container->addUpload('work', 'krystof.offence.work')
			->addRule(BaseForm::MAX_FILE_SIZE, 'maxSize', 250 * 1024 * 1024)
			->setOption('template', '@upload.latte')
			->setOption(Renderer::GRID_SIZE, 2);

		$control->addGroup('krystof.withdrawal.group');
		$container = $control->addContainer('withdrawal');
		$container->addUpload('register', 'krystof.withdrawal.register')
			->addRule(BaseForm::MAX_FILE_SIZE, 'maxSize', 250 * 1024 * 1024)
			->setOption('template', '@upload.latte')
			->setOption(Renderer::GRID_SIZE, 2);
		$container->addUpload('request', 'krystof.withdrawal.request')
			->addRule(BaseForm::MAX_FILE_SIZE, 'maxSize', 250 * 1024 * 1024)
			->setOption('template', '@upload.latte')
			->setOption(Renderer::GRID_SIZE, 2);
		$container->addUpload('damage', 'krystof.withdrawal.damage')
			->addRule(BaseForm::MAX_FILE_SIZE, 'maxSize', 250 * 1024 * 1024)
			->setOption('template', '@upload.latte')
			->setOption(Renderer::GRID_SIZE, 2);
		$container->addUpload('work', 'krystof.withdrawal.work')
			->addRule(BaseForm::MAX_FILE_SIZE, 'maxSize', 250 * 1024 * 1024)
			->setOption('template', '@upload.latte')
			->setOption(Renderer::GRID_SIZE, 2);

		$control->addGroup('krystof.legal.group');
		$container = $control->addContainer('legal');
		$container->addUpload('powerOfAttorney', 'krystof.legal.powerOfAttorney')
			->addRule(BaseForm::MAX_FILE_SIZE, 'maxSize', 250 * 1024 * 1024)
			->setOption('template', '@upload.latte')
			->setOption(Renderer::GRID_SIZE, 2);
		$container->addUpload('court', 'krystof.legal.court')
			->addRule(BaseForm::MAX_FILE_SIZE, 'maxSize', 250 * 1024 * 1024)
			->setOption('template', '@upload.latte')
			->setOption(Renderer::GRID_SIZE, 2);
		$container->addUpload('costs', 'krystof.legal.costs')
			->addRule(BaseForm::MAX_FILE_SIZE, 'maxSize', 250 * 1024 * 1024)
			->setOption('template', '@upload.latte')
			->setOption(Renderer::GRID_SIZE, 2);

		$this->addFileUploads($control, "uploads.documents");

		$control->addGroup();
		$control->addSubmit('send', 'send');

		$control->onSuccess[] = function($form, $values) {
			try {
				$this->formHandler->processForm($form, $values, 'krystof');
			} catch (Exception $e) {
				$this->onError($this, $e);
				return;
			}
			$this->onSuccess($this, $form);
		};


		return $control;
	}

}


interface IKrystofFormFactory
{

	/** @return KrystofForm */
	public function create();
}
