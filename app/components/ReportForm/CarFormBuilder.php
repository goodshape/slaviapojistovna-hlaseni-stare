<?php


namespace App\Components;


use App\Components\Forms\Renderer;
use Nette\Forms\Container;
use Nette\Forms\Form;

class CarFormBuilder
{
	/** @var FormBuilder */
	private $generic;

	function __construct(FormBuilder $generic)
	{
		$this->generic = $generic;
	}


	public function build(BaseForm $form, $type)
	{
		$type = str_replace('car', '', $type);
		$this->{"create$type"}($form);

	}
	
	
	public function addFileUploads($form, $title)
	{
		$group = $form->addGroup($title, FALSE);

		$container = $form->addContainer('attachments' . str_replace("-", "", \Nette\Utils\Strings::webalize($title)));

		$container->addUpload('files','attachments.group', true)
			->addRule(Form::MAX_FILE_SIZE, 'maxSize', 250 * 1024 * 1024)
			->setOption('template', '@fileRepeater.latte')
			->setOption(Renderer::GRID_SIZE, 2);

		$group->add($container->getControls());
	}

	/**
	 * Povinné ručení jako pojištěný (vozidlo)
	 * http://www.slavia-pojistovna.cz/cs/nahlasit-skodu/povinne-ruceni-jako-pojisteny/
	 */
	protected function createInsured(BaseForm $form)
	{
		$this->addInsuredVehicle($form, FALSE, FALSE);
		$this->addReporter($form);
		$this->addInsuredPerson($form, TRUE);
		$this->addEvent($form);
		$this->addDriverInsured($form);
		$this->addDamagedPerson($form, TRUE);
		$this->addDamagedVehicle($form, TRUE, array('km', 'vin', 'damageGuess', 'year', 'type'));
		$this->addWitnesses($form);
		$this->addFileUploads($form, "uploads.file9");
		$this->addFileUploads($form, "uploads.file2");
		$this->addFileUploads($form, "uploads.file3");
		$this->addFileUploads($form, "uploads.file8");
		$this->addNote($form);
	}

	/**
	 * Povinné ručení - jiná škoda (jako pojištěný)
	 * http://www.slavia-pojistovna.cz/cs/nahlasit-skodu/povinne-ruceni-jina-skoda-jako-pojisteny/
	 */
	protected function createOtherInsured(BaseForm $form)
	{
		$this->addInsuredVehicle($form, FALSE, FALSE);
		$this->addReporter($form);
		$this->addInsuredPerson($form, TRUE);
		$this->addEvent($form);
		$this->addDriverInsured($form);
		$this->addDamagedPerson($form, TRUE);
		$this->addDamagedThing($form);
		$this->addWitnesses($form);
		$this->addFileUploads($form, "uploads.file9");
		$this->addFileUploads($form, "uploads.file2");
		$this->addFileUploads($form, "uploads.file3");
		$this->addFileUploads($form, "uploads.file8");
		$this->addNote($form);
	}

	/**
	 * Povinné ručení jako poškozený (vozidlo)
	 * http://www.slavia-pojistovna.cz/cs/nahlasit-skodu/povinne-ruceni-jako-poskozeny/
	 */
	protected function createDamaged(BaseForm $form)
	{
		$this->addInsuredVehicle($form, TRUE);
		$this->addReporter($form);
		$this->addDamagedPerson($form, FALSE);
		$this->addInsuredPerson($form, FALSE);
		$form->getGroup('insured.group')->setOption('label', 'car.damaged.insured.group');
		$this->addEvent($form);
		$this->addDriverDamaged($form);
		$this->addDamagedVehicle($form, TRUE, array('km', 'vin', 'damageGuess'));
		$this->addInspection($form);
		$this->addWitnesses($form);
		$this->addFileUploads($form, "uploads.file9");
		$this->addFileUploads($form, "uploads.file2");
		$this->addFileUploads($form, "uploads.file3");
		$this->addFileUploads($form, "uploads.file1");
		$this->addFileUploads($form, "uploads.file6");
		$this->addFileUploads($form, "uploads.file10");
		$this->addFileUploads($form, "uploads.file11");
		$this->addFileUploads($form, "uploads.file5");
		$this->addFileUploads($form, "uploads.file7");
		$this->addFileUploads($form, "uploads.file8");
		$this->addNote($form);

		$form['insuredVehicle-contract']->caption = 'car.damaged.insuredContract';
	}

	/**
	 * Povinné ručení jako poškozený – jiná škoda (majetek)
	 * http://www.slavia-pojistovna.cz/cs/nahlasit-skodu/povinne-ruceni-jina-skoda-jako-poskozeny/
	 */
	protected function createOtherDamaged(BaseForm $form)
	{
		$this->addInsuredVehicle($form, TRUE);
		$this->addReporter($form);
		$this->addDamagedPerson($form, FALSE);
		$this->addInsuredPerson($form, FALSE);
		$form->getGroup('insured.group')->setOption('label', 'car.damaged.insured.group');
		$this->addEvent($form);
		$this->addDriverDamaged($form);
		$this->addDamagedThing($form, TRUE);
		$this->addInspection($form, FALSE);
		$this->addWitnesses($form);
		$this->addFileUploads($form, "uploads.file9");
		$this->addFileUploads($form, "uploads.file6");
		$this->addFileUploads($form, "uploads.file1");
		$this->addFileUploads($form, "uploads.file10");
		$this->addFileUploads($form, "uploads.file12");
		$this->addFileUploads($form, "uploads.file8");
		$this->addNote($form);

		$form['insuredVehicle-contract']->caption = 'car.damaged.insuredContract';
	}

	private function addInsuredVehicle(BaseForm $form, $damaged = FALSE, $requireVin = TRUE)
	{
		$group = $form->addGroup('insuredVehicle.group', FALSE);

		$container = $form->addContainer('insuredVehicle');
		$container->addText('contract', 'insuredVehicle.contract')
			->setRequired('errors.notFilled');
		if (!$damaged) {
		$container->addText('rz', 'insuredVehicle.rz')
			->setRequired('errors.notFilled');
			$vin = $container->addText('vin', 'insuredVehicle.vin');
			if ($requireVin) $vin->setRequired('errors.notFilled');
			$container->addTextArea('lease', 'insuredVehicle.lease')
				->setOption(Renderer::GRID_SIZE, 3);
		}else{
		$container->addText('rz', 'insuredVehicle.rzdmg')
			->setRequired('errors.notFilled');
		}
		$container->addRadioList('reportedInsurer', 'reporter.reporterInsured', [
			0 => 'no',
			1 => 'yes',
		])->setOption(Renderer::GRID_NEW_ROW, TRUE)
			->setDefaultValue(1)
			->addCondition($form::EQUAL, 0)->toggle('reporter-group');

		$group->add($container->getControls());
	}

	private function addReporter(BaseForm $form)
	{
		$group = $form->addGroup('reporter.group', FALSE);

		$group->setOption('id', 'reporter-group');

		$container = $form->addContainer('reporter');
		$this->addPerson($container, FALSE, FALSE);

		foreach ($container->getControls() as $c) {
			$c->addConditionOn($form['insuredVehicle']['reportedInsurer'], $form::EQUAL,
				0)->setRequired('errors.notFilled');
		}

		$group->add($container->getControls());
	}

	private function addInsuredPerson(BaseForm $form, $asInsured = FALSE)
	{
		$group = $form->addGroup('insured.group', FALSE);

		$container = $form->addContainer('insured');
		$this->addPerson($container, TRUE, TRUE, TRUE, 'person', !$asInsured);

		$group->add($container->getControls());
	}

	private function addEvent(BaseForm $form)
	{
		$group = $form->addGroup('event.group', FALSE);

		$container = $form->addContainer('event');

		$container->addText('date', 'car.event.date');
		$container->addText('time', 'car.event.time');
		$container->addTextArea('description', 'car.event.description')
			->setOption(Renderer::GRID_SIZE, 2);
		$container->addTextArea('location', 'car.event.location')
			->setOption(Renderer::GRID_SIZE, 2);
		$this->setRequired($container);
		$container->addRadioList('investigated', 'car.event.investigated', [
			0 => 'no',
			1 => 'yes',
		])->setOption(Renderer::GRID_NEW_ROW, TRUE);
		$container->addText('police', 'car.event.police')
			->setOption(Renderer::GRID_SIZE, 2);


		$group->add($container->getControls());
	}

	private function addDriverInsured(BaseForm $form)
	{
		$group = $form->addGroup('car.driverInsured.group', FALSE);

		$container = $form->addContainer('driverInsured');

		$this->addPerson($container, TRUE, FALSE, FALSE, 'car.driverInsured', TRUE);

		$container->addText('rp', 'car.driverInsured.rp');
		$container->addText('groups', 'car.driverInsured.groups');

		$group->add($container->getControls());
	}

	private function addDriverDamaged(BaseForm $form)
	{
		$group = $form->addGroup('car.driverDamaged.group', FALSE);

		$container = $form->addContainer('driverInsured');

		$this->addPerson($container, TRUE, FALSE, FALSE, 'car.driverInsured', TRUE);

		$container->addText('rp', 'car.driverInsured.rp');
		$container->addText('groups', 'car.driverInsured.groups');

		$group->add($container->getControls());
	}

	private function addDamagedPerson(BaseForm $form, $asInsured = FALSE)
	{
		$group = $form->addGroup('damaged.group', FALSE);

		$container = $form->addContainer('damaged');
		$this->addPerson($container, TRUE, TRUE, TRUE, 'person', $asInsured);

		$group->add($container->getControls());
	}

	private function addDamagedVehicle(BaseForm $form, $damaged = FALSE, $notRequiredFields = array())
	{
		$group = $form->addGroup('car.damagedVehicle.group', FALSE);

		$container = $form->addContainer('damagedVehicle');

		$container->addText('rz', 'car.damagedVehicle.rz');
		$container->addText('brand', 'car.damagedVehicle.brand');
		$container->addText('year', 'car.damagedVehicle.year');
		$container->addText('type', 'car.damagedVehicle.type');
		$container->addText('km', 'car.damagedVehicle.km');
		$container->addText('vin', 'car.damagedVehicle.vin');

		$container->addRadioList('inspection', 'car.damagedVehicle.inspection')
			->setItems([0 => 'no', 1 => 'yes'])
			->setOption(Renderer::GRID_SIZE, 3);
		$container->addTextArea('damage', 'car.damagedVehicle.damage')
			->setOption(Renderer::GRID_SIZE, 3);
		if (!$damaged) {
			$container->addTextArea('healthDamage', 'car.damagedVehicle.healthDamage')
				->setOption(Renderer::GRID_SIZE, 3);
			$container->addTextArea('otherDamage', 'car.damagedVehicle.otherDamage')
				->setOption(Renderer::GRID_SIZE, 3);
			$container->addRadioList('relation', 'car.damagedVehicle.relation')
				->setItems([0 => 'no', 1 => 'yes'])
				->setOption(Renderer::GRID_SIZE, 2);
			$container->addRadioList('claimJust', 'car.damagedVehicle.claimJust')
				->setItems([0 => 'no', 1 => 'yes'])
				->setOption(Renderer::GRID_SIZE, 2);
			$container->addRadioList('compensated', 'car.damagedVehicle.compensated')
				->setItems([0 => 'no', 1 => 'yes'])
				->setOption(Renderer::GRID_SIZE, 2);
		} else {
			$container->addText('damageGuess', 'car.damagedVehicle.damageGuess')
				->setOption(Renderer::GRID_SIZE, 2);
		}

		$this->setRequired($container, $notRequiredFields);


		$group->add($container->getControls());
	}

	private function addWitnesses(BaseForm $form)
	{
		$group = $form->addGroup('witnesses.group', FALSE);

		$form->addTextArea('witnesses', 'event.witness')
			->setOption(Renderer::GRID_SIZE, 3);

		$group->add($form['witnesses']);
	}

	private function addNote(BaseForm $form)
	{
		$group = $form->addGroup('note.group', FALSE);

		$form->addTextArea('note', 'car.note')
			->setOption(Renderer::GRID_SIZE, 3);
		$form->addCheckbox('affirmation', 'car.affirmation')
			->setRequired('errors.affirmative')
			->setOption(Renderer::GRID_SIZE, 3);

		$group->add($form['note'], $form['affirmation']);
	}

	private function addPerson(Container $container, $required, $rc = TRUE, $contact = TRUE, $prefix = 'person', $addressNotRequired = FALSE)
	{
		$name = $container->addText('name', $prefix . '.name');
		$surname = $container->addText('surname', $prefix . '.surname');
		if ($rc) {
			$rc = $container->addText($prefix . 'alId', $prefix . '.personalId');
		}
		if ($contact) {
			$container->addText('phone', $prefix . '.phone')->setType('phone');
		}
		$container->addText('address', $prefix . '.address');
		if ($contact) {
			$container->addText('city', $prefix . '.city');
			$container->addText('postal', $prefix . '.postal');
			$container->addText('mail', $prefix . '.mail')
				->addCondition(Form::FILLED)
					->addRule(Form::EMAIL, 'errors.emailInvalid');
		}

		if ($addressNotRequired) $notRequiredFields = array('phone', 'address', 'city', 'postal', 'mail');
		else $notRequiredFields = array();

		if ($required) $this->setRequired($container, $notRequiredFields);
	}

	/**
	 * @param Container $container
	 */
	private function setRequired(Container $container, $notRequiredFields = array())
	{
		foreach ($container->getControls() as $name => $c) {
			if (!in_array($name, $notRequiredFields)) $c->setRequired('errors.notFilled');
		}
	}

	private function addInspection(BaseForm $form, $vehicle = TRUE)
	{
		$group = $form->addGroup('car.inspection.group', FALSE);

		$container = $form->addContainer('inspection');

		$container->addText('date', 'car.inspection.servis');
		$container->addText('time', 'car.inspection.otherPlace')
			->setOption(Renderer::GRID_SIZE, 2);
		$container->addText('contact', 'car.inspection.contact')
			->setOption(Renderer::GRID_SIZE, 2);
		$container->addTextArea('healthDamage', 'car.damagedVehicle.healthDamage')
			->setRequired('errors.notFilled')
			->setOption(Renderer::GRID_SIZE, 2);
		$container->addTextArea('otherDamage', 'car.damagedVehicle.otherDamage')
			->setRequired('errors.notFilled')
			->setOption(Renderer::GRID_SIZE, 2);

		if($vehicle) {
			$container->addTextArea('lease', 'insuredVehicle.lease')
				->setOption(Renderer::GRID_SIZE, 3);
		} else {
			$container->addTextArea('claim', 'car.damagedThing.myClaim')
				->setOption(Renderer::GRID_SIZE, 3);
			$container->addTextArea('compensated', 'car.damagedThing.beenCompensated')
				->setOption(Renderer::GRID_SIZE, 3);
		}

		$container->addRadioList('vat', 'insured.vat')
			->setRequired('errors.notFilled')
			->setOption(Renderer::GRID_SIZE, 2)
			->setItems([0 => 'no', 1 => 'yes']);
		$container->addRadioList('accounting', 'insured.accounting')
			->setRequired('errors.notFilled')
			->setOption(Renderer::GRID_SIZE, 2)
			->setItems([0 => 'no', 1 => 'yes']);
		$container->addTextArea('anotherInsurance', 'car.inspection.anotherInsurance')
			->setOption(Renderer::GRID_SIZE, 2);
		if($vehicle) {
			$container->addRadioList('relation', 'car.damagedVehicle.relation')
				->setItems([0 => 'no', 1 => 'yes'])
				->setOption(Renderer::GRID_SIZE, 2)
				->setDefaultValue(0);
		}


		$group->add($container->getControls());
	}

	private function addDamagedThing(BaseForm $form, $damaged = FALSE)
	{
		$group = $form->addGroup('car.damagedThing.group', FALSE);

		$container = $form->addContainer('damagedThing');

		$container->addRadioList('inspection', 'car.damagedThing.inspection')
			->setItems([0 => 'no', 1 => 'yes'])
			->setOption(Renderer::GRID_SIZE, 3);
		$container->addTextArea('damage', 'car.damagedThing.damage')
			->setOption(Renderer::GRID_SIZE, 3);
		$container->addTextArea('what', 'car.damagedThing.what')
			->setOption(Renderer::GRID_SIZE, 3);
		if (!$damaged) {
			$container->addTextArea('healthDamage', 'car.damagedThing.healthDamage')
				->setOption(Renderer::GRID_SIZE, 3);
			$container->addTextArea('otherDamage', 'car.damagedThing.otherDamage')
				->setOption(Renderer::GRID_SIZE, 3);
			$container->addTextArea('compensation', 'car.damagedThing.compensation')
				->setOption(Renderer::GRID_SIZE, 3);
			$container->addTextArea('claimJust', 'car.damagedThing.claimJust')
				->setOption(Renderer::GRID_SIZE, 3);
			$container->addTextArea('compensated', 'car.damagedThing.compensated')
				->setOption(Renderer::GRID_SIZE, 3);
		} else {
		}

		$this->setRequired($container, array("what"));


		$group->add($container->getControls());
	}

} 
