<?php


namespace App\Components;


use Exception;
use Kdyby\Translation\Translator;

class CarReportForm extends AReportForm
{

	/** @var CarFormBuilder */
	private $builder;
	private $type;
	private $formHandler;

	function __construct(CarFormBuilder $builder, FormHandler $formHandler)
	{
		$this->builder = $builder;
		$this->formHandler = $formHandler;
	}

	public function setType($type)
	{
		$this->type = $type;

		return $this;
	}


	protected function createComponentForm(IBaseFormFactory $factory)
	{
		$control = $factory->create();

		/** @var Translator $t */
		$control->setTranslator($control->getTranslator()->domain('control.report'));

		$this->builder->build($control, $this->type);

		$control->addSubmit('send', 'send');

		$control->onSuccess[] = function($form, $values) {
			$group = in_array($this->type, ['damaged', 'otherDamaged']) ? 'damaged' : 'insured';
			if(!empty($values->reporter->name)) {
				$group = 'reporter';
			}
			try {
				$this->formHandler->processForm($form, $values, $this->type, $group);
			} catch (Exception $e) {
				$this->onError($this, $e);
				return;
			}
			$this->onSuccess($this, $form);
		};

		return $control;
	}

}


interface ICarReportFormFactory
{

	/** @return CarReportForm */
	public function create();
}
