<?php


namespace App\Components;


use App\Components\Forms\Renderer;
use Exception;

class TravelReportForm extends AReportForm
{

	private $formHandler;

	function __construct(FormHandler $formHandler)
	{
		$this->formHandler = $formHandler;
	}
	
	
	public function addFileUploads($form, $title)
	{
		$group = $form->addGroup($title, true);

		$container = $form->addContainer('attachments' . str_replace("-", "", \Nette\Utils\Strings::webalize($title)));

		$container->addUpload('files','attachments.group', true)
			->setOption('template', '@fileRepeater.latte')
			->setOption(Renderer::GRID_SIZE, 2);

		$group->add($container->getControls());
	}

	protected function createComponentForm(IBaseFormFactory $factory)
	{
		$control = $factory->create();

		$control->setTranslator($control->getTranslator()->domain('control.report'));

		$control->addGroup('insured.group')
		    ->setOption("container", \Nette\Utils\Html::el("fieldset")->class('tr__report--insured'));

		$container = $control->addContainer('travelreportinsured');

		$container->addText('name', 'insured.name')
			->setRequired('errors.notFilled');
		$container->addText('contract', 'insured.contract')
			->setRequired('errors.notFilled');
		$container->addText('personalId', 'insured.personalId')
			->setRequired('errors.notFilled');
		$container->addText('mail', 'insured.mail')
			->setRequired('errors.notFilled')
			->addCondition($control::FILLED)
			->addRule($control::EMAIL, 'errors.emailInvalid');
		$container->addText('phone', 'insured.phone')
			->setRequired('errors.notFilled');
		$container->addText('address', 'travelreport.address')
			->setRequired('errors.notFilled');
		$container->addText('postal', 'insured.postal')
			->setRequired('errors.notFilled');
		$container->addText('street', 'travelreport.street')
			->setRequired('errors.notFilled');

		$control->addGroup('travelreport.event.label')
			->setOption("container", \Nette\Utils\Html::el("fieldset")->class('tr__report--type'));	
		$container = $control->addContainer('travelreportevent');
		$container->addRadioList('eventClaimType', 'travelreport.event.desc')
			->setItems([
				'medicalAttention' => 'travelreport.event.invoice',
				'invoice' => 'travelreport.event.accident',
				'luggage' => 'travelreport.event.luggage',
				'storno' => 'travelreport.event.storno',
				'delay' => 'travelreport.event.delay',
				'liability' => 'travelreport.event.liability',
				'other' => 'travelreport.other',
			])->setOption(Renderer::GRID_SIZE, 2);

		$container->addText('other', 'travelreport.other')
			->setOption(Renderer::GRID_SIZE, 3);

		$container->addText('datestart', 'travelreport.datestart');
		$container->addText('placestart', 'travelreport.placestart');

		/// Pokud je storno
		$control->addGroup('travelreport.storno.label')
				->setOption("container", \Nette\Utils\Html::el("fieldset")->class('tr__report--storno'));
		$container = $control->addContainer('travelreportstorno');
		$container->addText('travelfrom', 'travelreport.storno.travelfrom');
		$container->addText('travelto', 'travelreport.storno.travelto');
		$container->addText('where', 'travelreport.storno.where');
		$container->addText('travelprice', 'travelreport.storno.travelprice');
		$container->addText('paymentdate', 'travelreport.storno.paymentdate');
		$container->addText('amountpaid', 'travelreport.storno.amountpaid');

		/// Popis události

		$control->addGroup('travelreport.descevent')
				->setOption("container", \Nette\Utils\Html::el("fieldset")->class('tr__report--description'));
		$container = $control->addContainer('travelreporteventdesc');
		$container->addTextArea('description', 'event.description')
			->setOption(Renderer::GRID_SIZE, 2);

		$container->addText('facility', 'travelreport.facility')
			->setOption(Renderer::GRID_SIZE, 2);

		$container->addText('diagnosis', 'travelreport.diagnosis')
			->setOption(Renderer::GRID_SIZE, 2);
		$container->addText('precondition', 'travelreport.precondition')
			->setOption(Renderer::GRID_SIZE, 2);
		$container->addText('yourdoc', 'travelreport.yourdoc')
			->setOption(Renderer::GRID_SIZE, 2);


		$container = $control->addContainer('travelreportliability');
		// $control->addGroup('travelreport.liabilitydamage')
		// 		->setOption("container", \Nette\Utils\Html::el("fieldset")->class('tr__report--damage'));
		$container->addText('injured', 'travelreport.injured')
			->setOption(Renderer::GRID_SIZE, 2);
		$container->addText('witness', 'travelreport.witness')
			->setOption(Renderer::GRID_SIZE, 2);
		$container->addText('damageprice', 'travelreport.damageprice')
			->setOption(Renderer::GRID_SIZE, 2);
		$container->addText('damagepricecz', 'travelreport.damagepricecz');
		//$container->addUpload('file1', 'travelreport.files')
			//->setOption(Renderer::GRID_SIZE, 2);
		//$container->addUpload('file2', 'travelreport.file1')
			//->setOption(Renderer::GRID_SIZE, 2);
		//$container->addUpload('file3', 'travelreport.file2')
			//->setOption(Renderer::GRID_SIZE, 2);
		//$container->addUpload('file4', 'travelreport.file3')
			//->setOption(Renderer::GRID_SIZE, 2);
		//$container->addUpload('file5', 'travelreport.file4')
			//->setOption(Renderer::GRID_SIZE, 2);
		//$container->addUpload('file6', 'travelreport.file5')
			//->setOption(Renderer::GRID_SIZE, 2);

		$control->addGroup("travelreport.pay.label")
		        ->setOption("container", \Nette\Utils\Html::el("fieldset")->class('tr__report--pay'));
		$container = $control->addContainer('travelreportpay');
		$container->addRadioList('eventClaimType', 'travelreport.paymentlabel')
			->setItems([
				'banktransfer' => 'travelreport.banktransfer',
				'posttransfer' => 'travelreport.posttransfer'
			])->setOption(Renderer::GRID_SIZE, 2);
		$container->addText('accno', 'travelreport.accno')
			->setOption(Renderer::GRID_SIZE, 2);
		$container->addText('bankno', 'travelreport.bankno');


		$container->addText('payname', 'travelreport.name');
		$container->addText('payaddress', 'travelreport.address');
		$container->addText('paystreet', 'travelreport.street');
		$container->addText('paypostal', 'insured.postal');

		$this->addFileUploads($control, "uploads.file13");
		$this->addFileUploads($control, "uploads.file14");
		$this->addFileUploads($control, "uploads.file15");
		$this->addFileUploads($control, "uploads.file16");
		$this->addFileUploads($control, "uploads.file17");
		$this->addFileUploads($control, "uploads.file8");

		$control->addGroup();
		$control->addSubmit('send', 'send');

		$control->onSuccess[] = function($form, $values) {
			try {
				$this->formHandler->processForm($form, $values, 'travelReportForm',"travelreportinsured");
			} catch (Exception $e) {
				$this->onError($this, $e);
				return;
			}
			$this->onSuccess($this, $form);
		};
		return $control;
	}

}


interface ITravelReportFormFactory
{

	/** @return TravelReportForm */
	public function create();
}
