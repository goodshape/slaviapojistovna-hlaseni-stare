<?php


namespace App\Components;


use App\Model\Entities\ReportForm as ReportEntity;
use App\Model\Services\MailService;
use Kdyby\Doctrine\EntityManager;
use Kdyby\Translation\Translator;
use Nette\Forms\Container;
use Nette\Forms\Controls\Checkbox;
use Nette\Forms\Controls\CheckboxList;
use Nette\Forms\Controls\RadioList;
use Nette\Forms\Controls\UploadControl;
use Nette\Http\FileUpload;
use Nette\UnexpectedValueException;
use Nette\Utils\ArrayHash;
use Nette\Utils\Random;
use Nette\Utils\Strings;

class FormHandler
{
	/** @var Translator */
	private $translator;
	/**
	 * @var MailService
	 */
	private $mailService;
	/**
	 * @var EntityManager
	 */
	private $em;
	private $fileStorage;
	private $wwwDir;
	private $uploadPaths = [];

	function __construct($wwwDir, $attachmentPath, Translator $translator, MailService $mailService, EntityManager $em)
	{
		$this->translator = $translator;
		$this->mailService = $mailService;
		$this->em = $em;
		$this->fileStorage = rtrim($attachmentPath, '\\/');
		$this->wwwDir = rtrim($wwwDir, '/\\');
	}


	public function processForm(BaseForm $form, $values, $type, $contactGroup = 'insured', $showPaths = false)
	{
		$entity = $this->prepareEntity($values[$contactGroup], "", $type);
		$this->processUploads($entity, $form, $values);
		
		$messages = [];
		foreach ([0, 1] as $index) {
			$message = [];
			foreach ($values as $group => $item) {
				$part = [];
				$container = $form->getComponent($group);
				if ($container instanceof Container) {
					$part[] = $this->getGroupName($group);
					$part[] = '----------------';
	
					foreach ($item as $key => $value) {
						$part[] = $this->getField($container, $key, $value, (bool) $index);
					}
					$part[] = '*********************************';
				} else {
					$part[] = $this->getField($container, NULL, $item, (bool) $index);
				}
	
				$message[] = implode("\n", $part);
			}
			$message = (implode("\n\n", $message));
			$messages[$index] = $message;
		}
		
		
		$entity->message = $messages[0];
		$messageWithPaths = $messages[1];
		$this->em->persist($entity);
		$this->em->flush();

		$this->sendMessage($entity, $type, $messageWithPaths);
	}

	private function sendMessage(ReportEntity $entity, $type, $messageWithPaths)
	{

		$subject = 'Online hlášení z www.slavia-pojistovna.cz - ' . $this->translator->translate('front.report.' . $type . '.title');
		$text1 = $subject . "\n\n" . ($entity->hasAttachments() ? "Přílohy naleznete v administračním rozhraní.\n\n" : "\n") . $messageWithPaths;
		$text2 = $subject . "\n\n\n" . $entity->message;
		if (
			$type == "carInsured" || 
			$type == "carDamaged" ||
			$type == "carOtherInsured" || 
			$type == "carOtherDamaged" || 
			$type == "collision" ||
			$type == "injury"
		   )
			$report = $this->mailService->createReportForm($entity, $text1, $subject, "registrace.auta@slavia-pojistovna.cz");
		else if ($type == "property" || $type == "krystof")
			$report = $this->mailService->createReportForm($entity, $text1, $subject, "registrace.majetek@slavia-pojistovna.cz");
		else
			$report = $this->mailService->createReportForm($entity, $text1, $subject);
		$copy = $this->mailService->createReportForm($entity, $text2, $subject, $entity->mail);

		$this->mailService->send($report);
		$this->mailService->send($copy);
	}

	private function processReplicator($group, $value)
	{
		$result = [];
		foreach ($value as $key => $item) {
			$result[] = $this->getTitle($group[$key]) . ': ' . $item;
		}
		$result[] = '----------------';

		return implode("\n", $result);
	}

	private function getTitle($control, $locale = NULL)
	{
		return $this->translator->translate('control.report.' . $control->caption, NULL, [], NULL, $locale);
	}

	private function getField($container, $key, $value, $showPaths = false)
	{
		if ($container instanceof \Kdyby\Replicator\Container) {
			return $this->processReplicator($container[$key], $value);
		} elseif ($container instanceof Container) {
			return $this->getTitle($container[$key]) . ': ' . $this->getFieldValue($container[$key], $value, $showPaths);
		} else {
			return $this->getTitle($container) . ': ' . $this->getFieldValue($container, $value, $showPaths);
		}
	}

	private function getFieldValue($control, $value, $showPaths = false)
	{
		if ($value === "" || $value === NULL) {
			return "";
		}
		if ($control instanceof RadioList) {
			return $this->translator->translate('report.' . $control->getItems()[$value], NULL, [], 'control');
		} elseif ($control instanceof CheckboxList) {
			$items = $control->getItems();
			$r = [];
			foreach ($value as $i) {
				if(in_array($i, $value)) {
					$r[$i] = $this->translator->translate('control.report.' . $items[$i]);
				}
			}

			return implode(", ", $r);
		} elseif ($control instanceof Checkbox) {
			return $value ? 'Ano' : 'Ne';
		} elseif ($control instanceof UploadControl && $value instanceof FileUpload) {
			if($value->isOk()) {
				return $this->getFilename($control, $value, $showPaths);
			} else {
				return "";
			}
		} elseif ($control instanceof UploadControl && is_array($value)) {
			$result = [];
			$iterator = 1;
			foreach ($value as $item) {
				if($item->isOk()) {
					$result[] = $this->getFilename($control, $item, $showPaths);
					$iterator += 1;
				}
			}
			if (count($result)) {
				return implode("\n", $result);
			} else {
				return "";
			}
		}

		return $value;
	}

	private function getGroupName($group)
	{
		$r = $this->translator->translate('report.' . $group . '.group', NULL, [], 'control');
		if ($r === 'report.' . $group . '.group') {
			$r = $this->translator->translate('report.car.' . $group . '.group', NULL, [], 'control');
		}

		return $r;
	}

	private function handleFileUpload($path, UploadControl $control, FileUpload $value, $prefix = "")
	{
		if (!$value->isOk()) {
			if ($value->getError() === UPLOAD_ERR_NO_FILE) {
				return FALSE;
			}
			throw new UnexpectedValueException($value->getError());
		}

		if ($prefix) {
			$filename = $prefix;
		} else {
			$filename = Strings::webalize($this->getTitle($control, 'en'), '.', true) . '-';
		}
		$filename .= substr(Strings::webalize($value->getSanitizedName(), '.', TRUE), 0, 40);

		$ext = pathinfo($value->getName(), PATHINFO_EXTENSION);
		if(Strings::endsWith($filename, $ext)) {
			$filename = substr($filename, 0, strlen($filename) - strlen($ext)-1);
		}
		$this->mkdir(dirname($path));

		do {
			$name = $filename . '.' . Random::generate(32) . '.' . $ext;
		} while (file_exists($fullPath = $this->wwwDir . '/' . $path . '/' . $name));

		if ($value->move($fullPath)) {
			$this->uploadPaths[$value->getTemporaryFile()] = "//10.0.0.19/hlasenipu/reports" . substr($fullPath, strlen($this->wwwDir) + 14);
			return TRUE;
		}

		return FALSE;
	}

	private function processUploads(ReportEntity $entity, Container $form, $values)
	{
		$path = $this->fileStorage . '/' . date("Y-m-d") . '/hlasenka-' . $entity->id;
		$hasUploads = FALSE;
		foreach ($values as $key => $val) {
			
			if ($form[$key] instanceof Container) {
				$this->processUploads($entity, $form[$key], $val);
				
			} elseif ($val instanceof FileUpload) {
				$prefix = sprintf("%d-%s-", $entity->id, \Nette\Utils\Strings::webalize($this->getGroupName($form[$key]->parent->name)));
				if ($this->handleFileUpload($path, $form[$key], $val, $prefix)) {
					$hasUploads = TRUE;
				}
				
			} elseif (is_array($val)) {
				$iterator = 1;
				foreach ($val as $item) {
					if ($item instanceof FileUpload) {
						$prefix = sprintf("%d-%s-%02d-", $entity->id, \Nette\Utils\Strings::webalize($this->getGroupName($form[$key]->parent->name)), $iterator);
						$iterator += 1;
						if ($this->handleFileUpload($path, $form[$key], $item, $prefix)) {
							$hasUploads = TRUE;
						}
					}
				}
			}
		}

		if ($hasUploads) {
			$entity->attachments = $path;
			$this->em->flush($entity);
		}

	}

	/**
	 * @param $values
	 * @param $message
	 * @param $type
	 * @return array
	 */
	private function prepareEntity($values, $message, $type)
	{
		$entity = new ReportEntity();
		$entity->message = $message;

		$entity->name = isset($values->name) ? $values->name : '';
		$entity->mail = isset($values->mail) ? $values->mail : '';
		$entity->phone = isset($values->phone) ? $values->phone : '';

		$entity->type = $type;

		$this->em->persist($entity);
		$this->em->flush();

		return $entity;
	}

	private function mkdir($path)
	{
		$dir = $this->wwwDir . '/' . $path;
		$oldMask = umask(0);
		@mkdir($dir, 0777, TRUE);
		@chmod($dir, 0777);
		umask($oldMask);

		if (!is_dir($dir) || !is_writable($dir)) {
			throw new \Nette\IOException("Please create writable directory $dir.");
		}
	}

	private function getFilename($control, $value, $showPaths = false)
	{
		if ($showPaths) {
			return $this->uploadPaths[$value->getTemporaryFile()];
		} else {
			return $value->getSanitizedName();
		}
	}

} 