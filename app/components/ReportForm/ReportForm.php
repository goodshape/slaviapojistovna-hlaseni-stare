<?php
namespace App\Components;


use Kdyby\Translation\Translator;
use Symfony\Component\Config\Definition\Exception\Exception;

class ReportForm extends AReportForm
{
	/** @var FormBuilder */
	private $builder;

	private $type;
	/**
	 * @var FormHandler
	 */
	private $formHandler;

	function __construct(FormBuilder $builder, FormHandler $formHandler)
	{
		$this->builder = $builder;
		$this->formHandler = $formHandler;
	}


	/**
	 * @param mixed $type
	 */
	public function setType($type)
	{
		$this->type = $type;

		return $this;
	}


	protected function createComponentForm(IBaseFormFactory $factory)
	{
		$control = $factory->create();

		/** @var Translator $t */
		$control->setTranslator($control->getTranslator()->domain('control.report'));

		$this->builder->build($control, $this->type);

		$control->addSubmit('send', 'send');

		$control->onSuccess[] = function($form, $values) {
			try {
				$this->formHandler->processForm($form, $values, $this->type);
				// return;
			} catch (Exception $e) {
				$this->onError($this, $e);
				return;
			}
			$this->onSuccess($this, $form);
		};

		return $control;
	}

}


interface IReportFormFactory
{

	/** @return ReportForm */
	public function create();
}
