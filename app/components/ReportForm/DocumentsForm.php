<?php


namespace App\Components\Report;


use App\Components\AReportForm;
use App\Components\BaseControl;
use App\Components\FormHandler;
use App\Components\Forms\Renderer;
use App\Components\IBaseFormFactory;
use Exception;
use Nette\Forms\Form;

class DocumentsForm extends AReportForm
{

	private $formHandler;

	function __construct(FormHandler $formHandler)
	{
		$this->formHandler = $formHandler;
	}


	protected function createComponentForm(IBaseFormFactory $factory)
	{
		$control = $factory->create();

		$control->setTranslator($control->getTranslator()->domain('control.report'));

		$control->addGroup('insured.group');
		$container = $control->addContainer('insured');

		$container->addText('number', 'documents.number')
			->setOption(Renderer::GRID_SIZE, 2)
			->setRequired('errors.notFilled');
		$container->addText('contract', 'documents.contract')
			->setOption(Renderer::GRID_SIZE, 2)
			->setRequired('errors.notFilled');
		$container->addText('id', 'documents.id')
			->setOption(Renderer::GRID_SIZE, 2)
			->setRequired('errors.notFilled');
		$container->addText('mail', 'documents.mail')
			->setOption(Renderer::GRID_SIZE, 2)
			->setRequired('errors.notFilled')
			->addCondition(Form::FILLED)
				->addRule(Form::EMAIL, 'errors.emailInvalid');

		$control->addGroup('documents.group');
		$container = $control->addContainer('documents');
		$container->addUpload('idfile', 'documents.files.id')
			->setOption(Renderer::GRID_SIZE, 2);
		$container->addUpload('medicalFirst', 'documents.files.medicalFirst')
			->setOption(Renderer::GRID_SIZE, 2);
		$container->addUpload('medical', 'documents.files.medical')
			->setOption(Renderer::GRID_SIZE, 2);
		$container->addUpload('death', 'documents.files.death')
			->setOption(Renderer::GRID_SIZE, 2);
		$container->addUpload('notary', 'documents.files.notary')
			->setOption(Renderer::GRID_SIZE, 2);
		$container->addUpload('contract', 'documents.files.contract')
			->setOption(Renderer::GRID_SIZE, 2);
		$container->addUpload('court', 'documents.files.court')
			->setOption(Renderer::GRID_SIZE, 2);

		$control->addGroup();
		$control->addSubmit('send', 'send');

		$control->onSuccess[] = function($form, $values) {
			try {
				$this->formHandler->processForm($form, $values, 'documents', 'insured');
			} catch (Exception $e) {
				$this->onError($this, $e);
				return;
			}
			$this->onSuccess($this, $form);
		};


		return $control;
	}

}


interface IDocumentsFormFactory
{

	/** @return DocumentsForm */
	public function create();
}
