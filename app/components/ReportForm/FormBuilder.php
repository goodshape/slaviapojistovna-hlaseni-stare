<?php


namespace App\Components;


use App\Components\Forms\Renderer;
use Nette\Forms\Container;

class FormBuilder
{
	const COLLISION = 'collision';

	private $types = [
		'insured', 'event', 'investigation', 'inspection', 'items',
	];

	public function build(BaseForm $form, $type)
	{
		$this->{"create$type"}($form);

	}
	
	

	public function addFileUploads($form, $title)
	{
		$group = $form->addGroup($title, FALSE);

		$container = $form->addContainer('attachments' . str_replace("-", "", \Nette\Utils\Strings::webalize($title)));

		$container->addUpload('files','attachments.group', true)
			->addRule(BaseForm::MAX_FILE_SIZE, 'maxSize', 250 * 1024 * 1024)
			->setOption('template', '@fileRepeater.latte')
			->setOption(Renderer::GRID_SIZE, 2);

		$group->add($container->getControls());
	}

	/**
	 * Oznámení škodné události z havarijního pojištění
	 * http://www.slavia-pojistovna.cz/cs/nahlasit-skodu/havarijni-pojisteni/
	 */
	public function createCollision(BaseForm $form)
	{
		$this->addInsuredPerson($form, TRUE, TRUE, TRUE, FALSE, FALSE);
		$this->addEvent($form, TRUE, FALSE, TRUE, FALSE, FALSE);
		$this->addInvestigation($form);
		$this->addInspectionCollision($form);
		$this->addItems($form);
		
		$this->addFileUploads($form, "uploads.file1");
		$this->addFileUploads($form, "uploads.file2");
		$this->addFileUploads($form, "uploads.file3");
		$this->addFileUploads($form, "uploads.file4");
		$this->addFileUploads($form, "uploads.file5");
		$this->addFileUploads($form, "uploads.file6");
		$this->addFileUploads($form, "uploads.file7");
		$this->addFileUploads($form, "uploads.file8");

	}

	/**
	 * Oznámení škodné události z pojištění odpovědnosti
	 * http://www.slavia-pojistovna.cz/cs/nahlasit-skodu/pojisteni-odpovednosti/
	 */
	public function createLiability(BaseForm $form)
	{
		$this->addInsuredPerson($form, FALSE, FALSE);
		$this->addDamagedPerson($form, TRUE);
		$this->addEvent($form);
		$this->addInvestigation($form);
		$this->addInspection($form);
		$this->addItems($form);

		$this->addFileUploads($form, "uploads.documents");
	}

	/**
	 * Oznámení škodné události z pojištění majetku
	 * http://www.slavia-pojistovna.cz/cs/nahlasit-skodu/pojisteni-majetku/
	 */
	public function createProperty(BaseForm $form)
	{
		$this->addInsuredPerson($form);
		$this->addEvent($form);
		$this->addInvestigation($form);
		$this->addInspection($form);
		$this->addItems($form);
		$this->addFileUploads($form, "uploads.documents");
	}

	/**
	 * Oznámení škodné události způsobené na zdraví
	 */
	public function createInjury(BaseForm $form)
	{
		$this->addInsuredPerson($form, FALSE, FALSE, FALSE);

		$this->addSeats($form);
		$this->addEvent($form, FALSE, TRUE, FALSE, TRUE);

		/** @var Container $event */
		$event = $form['event'];
		$event['date']->caption = 'injury.event.date';
		$event['type']->setItems(['injury' => 'injury.event.type.items.injury', 'medical' => 'injury.event.type.items.medical'])->caption = 'injury.event.type.label';

		//$this->addAttachments($form);

		$group = $form->addGroup('injuryClaim.group', FALSE);

		$container = $form->addContainer('injuryClaim');

		$container->addRadioList('carAccident', 'injuryClaim.carAccident')
			->setItems([0 => 'no', 1 => 'yes'])
			->setOption(Renderer::GRID_SIZE, 2);
		$container->addRadioList('alcohol', 'injuryClaim.alcohol')->setItems([0 => 'no', 1 => 'yes'])
			->setOption(Renderer::GRID_SIZE, 2)
			->setOption(Renderer::GRID_NEW_ROW, TRUE);
		$container->addRadioList('causedByAnother', 'injuryClaim.causedByAnother')->setItems([0 => 'no', 1 => 'yes'])->setOption(Renderer::GRID_SIZE, 2);
		$container->addTextArea('causedByAnotherName', 'injuryClaim.causedByAnotherName')->setOption(Renderer::GRID_SIZE, 2);
		$container->addTextArea('injuryDesc', 'injuryClaim.injuryDesc')->setOption(Renderer::GRID_SIZE, 2);
		$container->addTextArea('deathContact', 'injuryClaim.deathContact')->setOption(Renderer::GRID_SIZE, 2);

		$group->add($container->getControls());

		$group = $form->addGroup('costsClaim.group', FALSE);

		$container = $form->addContainer('costsClaim');

		$container->addRadioList('costsType', 'costsClaim.costsType.label')
			->setItems([
				'injury' => 'costsClaim.costsType.injury',
				'sickness' => 'costsClaim.costsType.sickness',
			]);
		$container->addCheckboxList('costsProof', 'costsClaim.costsProof.label')
			->setItems([
				'invoice' => 'costsClaim.costsProof.invoice',
				'report' => 'costsClaim.costsProof.report',
				'other' => 'costsClaim.costsProof.other',
			])->setOption(Renderer::GRID_SIZE, 2);
		$container->addTextArea('deathContact', 'costsClaim.deathContact')
			->setOption(Renderer::GRID_SIZE, 3);

		$group->add($container->getControls());


		$this->addInvestigation($form, FALSE);
		$this->addNote($form);
		$this->addFileUploads($form, "uploads.documents");
	}

	public function addInsuredPerson(BaseForm $form, $rz = FALSE, $bankInfo = TRUE, $vat = TRUE, $mailReqired = TRUE, $bankRequired = TRUE)
	{
		$group = $form->addGroup('insured.group', FALSE);

		$container = $form->addContainer('insured');
		$container->addText('name', 'insured.name')
			->setRequired('errors.notFilled');
		$container->addText('contract', 'insured.contract')
			->setRequired('errors.notFilled');
		if ($rz) {
			$container->addText('rz', 'insured.rz')
				->setRequired('errors.notFilled');
		}
		$container->addText('personalId', 'insured.personalId')
			->setRequired('errors.notFilled');
		$mail = $container->addText('mail', 'insured.mail')->setType('email')
			->addCondition($form::FILLED)
			->addRule($form::EMAIL, 'errors.emailInvalid');
		if ($mailReqired) $mail->setRequired('errors.notFilled');
		$container->addText('phone', 'insured.phone')->setType('phone')
			->setRequired('errors.notFilled');
		if ($vat) {
			$container->addRadioList('vat', 'insured.vat')
				->setItems([0 => 'no', 1 => 'yes'])
				->setDefaultValue(0)
				->setRequired('errors.notFilled');
		}
		$container->addText('address', 'insured.address')
			->setRequired('errors.notFilled');
		$container->addText('postal', 'insured.postal')
			->setRequired('errors.notFilled');
		if ($bankInfo) {
			$account = $container->addText('account', 'insured.account');
			if ($bankRequired) $account->setRequired('errors.notFilled');
			$container->addText('specificSymbol', 'insured.specificSymbol');
		}

		$group->add($container->getControls());

	}

	public function addDamagedPerson(BaseForm $form, $bankInfo = FALSE)
	{
		$group = $form->addGroup('damaged.group', FALSE);

		$container = $form->addContainer('damaged');
		$container->addText('name', 'damaged.name')
			->setRequired('errors.notFilled');
		$container->addText('personalId', 'damaged.personalId')
			->setRequired('errors.notFilled');
		$container->addText('phone', 'damaged.phone')->setType('phone')
			->setRequired('errors.notFilled');
		$container->addText('address', 'damaged.address')
			->setRequired('errors.notFilled');
		$container->addText('postal', 'damaged.postal')
			->setRequired('errors.notFilled');
		$container->addRadioList('relation', 'damaged.relation')
			->setOption(Renderer::GRID_NEW_ROW, TRUE)
			->setOption(Renderer::GRID_SIZE, 2)
			->setItems([0 => 'no', 1 => 'yes']);
		$container->addText('relationType', 'damaged.relationType');
		if ($bankInfo) {
			$container->addText('account', 'insured.account');
			$container->addText('specificSymbol', 'insured.specificSymbol');
		}

		$group->add($container->getControls());

	}

	public function addItems(BaseForm $form)
	{
		$group = $form->addGroup('items.group', FALSE);
		$group->setOption(Renderer::REPLICATOR_GROUP, 'items');


		/** @var Container $container */
		$container = $form->addDynamic('items', function (\Nette\Forms\Container $replicatior) use ($group) {
			$replicatior->addText('name', 'items.name')
				->setOption(Renderer::GRID_SIZE, 1.25);
			$replicatior->addText('age', 'items.age')
				->setOption(Renderer::GRID_SIZE, 0.75);
			$replicatior->addText('price', 'items.price')
				->setOption(Renderer::GRID_SIZE, 0.75);

			$i = $replicatior->addSubmit('remove', 'items.remove')
				->setOption(Renderer::GRID_SIZE, 0.25)
				->setValidationScope(FALSE)
				->addRemoveOnClick();
			$i->getControlPrototype()->addClass('replicator-remove');

			$group->add($replicatior->getControls());

		}, 1);

		$i = $container->addSubmit('add', 'items.add')
			->setValidationScope(FALSE)
			->addCreateOnClick(TRUE);
		$i->getControlPrototype()->addClass('replicator-add');

		$group->add($container['add']);
	}

	public function addInspection(BaseForm $form)
	{
		$group = $form->addGroup('inspection.group', FALSE);

		$container = $form->addContainer('inspection');
		$container->addText('place', 'inspection.place')
			->setRequired('errors.notFilled')
			->setOption(Renderer::GRID_SIZE, 2);
		$container->addText('contact', 'inspection.contact')
			->setRequired('errors.notFilled')
			->setOption(Renderer::GRID_SIZE, 2);
		$container->addRadioList('repaired', 'inspection.repaired')
			->setRequired('errors.notFilled')
			->setOption(Renderer::GRID_SIZE, 2)
			->setItems([0 => 'no', 1 => 'yes']);
		$container->addTextArea('reason', 'inspection.reason')
			->setOption(Renderer::GRID_SIZE, 3);

		$group->add($container->getControls());
	}

	public function addInspectionCollision(BaseForm $form)
	{
		$group = $form->addGroup('inspection.groupGlass', FALSE);

		$container = $form->addContainer('inspection');
		$container->addText('place', 'inspection.place')
			->setOption(Renderer::GRID_SIZE, 2);
		$container->addText('contact', 'inspection.contact')
			->setRequired('errors.notFilled')
			->setOption(Renderer::GRID_SIZE, 2);
		$container->addRadioList('repaired', 'inspection.repaired')
			->setRequired('errors.notFilled')
			->setOption(Renderer::GRID_SIZE, 2)
			->setItems([0 => 'no', 1 => 'yes']);
		$container->addTextArea('reason', 'inspection.reason')
			->setOption(Renderer::GRID_SIZE, 3);

		$container->addTextArea('damage', 'car.damagedVehicle.damage')
			->setRequired('errors.notFilled')
			->setOption(Renderer::GRID_SIZE, 3);

		$container->addText('damageGuess', 'car.damagedVehicle.damageGuess')
			->setOption(Renderer::GRID_SIZE, 2);

		$group->add($container->getControls());
	}

	public function addInvestigation(BaseForm $form, $short = TRUE)
	{
		$group = $form->addGroup('investigation.group', FALSE);

		$container = $form->addContainer('investigation');
		$container->addRadioList('investigated', 'investigation.investigated')
			->setRequired('errors.notFilled')
			->setItems([0 => 'no', 1 => 'yes']);
		$container->addText('police', $short ? 'investigation.police' : 'investigation.precinct');

		if($short) {
			$container->addText('reason', 'investigation.reason')
				->setOption(Renderer::GRID_SIZE, 2);
			$container->addTextArea('other', 'investigation.other')
				->setOption(Renderer::GRID_SIZE, 2);
		} else {
			$container->addText('address', 'investigation.address');
			$container->addText('cj', 'investigation.cj');
		}

		$group->add($container->getControls());
	}

	public function addEvent(BaseForm $form, $things = TRUE, $type = FALSE, $witnesses = TRUE, $anotherMedical = FALSE, $requireStorage = TRUE)
	{
		$group = $form->addGroup('event.group', FALSE);

		$container = $form->addContainer('event');
		$container->addText('date', 'event.date')
			->setRequired('errors.notFilled');
		$container->addText('location', 'event.location')
			->setRequired('errors.notFilled')
			->setOption(Renderer::GRID_SIZE, 2);
		if ($type) {
			$container->addRadioList('type');
		}
		$container->addText('cause', 'event.cause')
			->setRequired('errors.notFilled')
			->setOption(Renderer::GRID_SIZE, 2);
		if ($things) {
			$container->addText('culprit', 'event.culprit')
				->setRequired('errors.notFilled')
				->setOption(Renderer::GRID_SIZE, 2);
		}
		$container->addTextArea('description', 'event.description')
			->setRequired('errors.notFilled')
			->setOption(Renderer::GRID_SIZE, 3);
		if ($things) {
			$storage = $container->addText('storage', 'event.storage')
				->setOption(Renderer::GRID_SIZE, 2);
			if ($requireStorage) $storage->setRequired('errors.notFilled');
		}
		if ($witnesses) {
			$container->addTextArea('witness', 'event.witness')
				->setOption(Renderer::GRID_SIZE, 2);
		}

		$this->addAnotherInsurance($container, $anotherMedical);


		$group->add($container->getControls());
	}

	public function addSeats(BaseForm $form)
	{
		$group = $form->addGroup('seats.group', FALSE);
		$group->setOption('class', 'repeater-group');

		$form->addText('vehicle', 'seats.vehicle');
		$form->addText('rz', 'seats.rz');

		$group->add($form['vehicle'], $form['rz']);

		$group->setOption('template', __DIR__ . '/@seats.latte');
		$container = $form->addDynamic('seats', function (Container $container) use ($group) {
			$container->addText('name', 'seats.name');
			$container->addText('seat', 'seats.seat')
				->setType('number');
			$container->addText('address', 'seats.address');
			$container->addText('postal', 'seats.postal');
			$container->addText('pid', 'seats.pid');
			$container->addText('relation', 'seats.relation');
			$i = $container->addSubmit('remove', 'seats.remove')
				->setOption(Renderer::GRID_SIZE, 0.25)
				->setValidationScope(FALSE)
				->addRemoveOnClick();
			$i->getControlPrototype()->addClass('replicator-remove');

			$group->add($container->getControls());
		}, 2);
		$i = $container->addSubmit('add', 'seats.add')
			->setValidationScope(FALSE)
			->addCreateOnClick(TRUE);
		$i->getControlPrototype()->addClass('replicator-add');
		$group->add($container['add']);

	}

	/**
	 * @param $container
	 */
	public function addAnotherInsurance($container, $medical)
	{
		$container->addRadioList('anotherInsurance', 'event.anotherInsurance')
			->setRequired('errors.notFilled')
			->setItems([0 => 'no', 1 => 'yes'])
			->setOption(Renderer::GRID_SIZE, 2);
		$container->addText('anotherInsuranceCompany', 'event.anotherInsuranceCompany');
		$container->addRadioList('anotherInsuranceClaim', 'event.anotherInsuranceClaim')
			->setOption(Renderer::GRID_SIZE, 2)
			->setItems([0 => 'no', 1 => 'yes']);
		$container->addText('anotherInsuranceContract', 'event.anotherInsuranceContract');
		if ($medical) {
			$container->addRadioList('benefitsType', 'event.benefits.medical.label')
				->setItems([
					'insured' => 'event.benefits.medical.items.insured',
					'damaged' => 'event.benefits.medical.items.damaged', 'medFacility' => 'event.benefits.medical.items.medFacility'
				])
				->setOption(Renderer::GRID_SIZE, 2);
			$container->addText('benefitsBank', 'event.benefits.medical.bank')
				->setOption(Renderer::GRID_NEW_ROW, TRUE);
			$container->addText('benefitsAddress', 'event.benefits.medical.address');
			$container->addText('treatmentPhysician', 'event.treatmentPhysician')
				->setOption(Renderer::GRID_SIZE, 2);
			$container->addTextArea('treatmentPlace', 'event.treatmentPlace')
				->setOption(Renderer::GRID_SIZE, 2);
		} else {
			$container->addText('benefitsBank', 'event.benefitsBank')
				->setOption(Renderer::GRID_NEW_ROW, TRUE)
				->setOption(Renderer::GRID_SIZE, 2);
			$container->addText('benefitsAddress', 'event.benefitsAddress');
		}
	}

	public function addAttachments($form)
	{
		$group = $form->addGroup('attachments.group', FALSE);

		$container = $form->addContainer('attachments');

		$container->addUpload('i1','attachments.group')
			->setOption('template', '@nolabel.latte')
			->setOption(Renderer::GRID_SIZE, 2);
		$container->addUpload('i2','attachments.group')
			->setOption('template', '@nolabel.latte')
			->setOption(Renderer::GRID_SIZE, 2);
		$container->addUpload('i3','attachments.group')
			->setOption('template', '@nolabel.latte')
			->setOption(Renderer::GRID_SIZE, 2);
		$container->addUpload('i4','attachments.group')
			->setOption('template', '@nolabel.latte')
			->setOption(Renderer::GRID_SIZE, 2);



		$group->add($container->getControls());

	}

	public function addNote($form)
	{
		$g = $form->addGroup('note.group', FALSE);

		$form->addTextarea('note', 'note.group')
			->setOption('template', '@nolabel.latte')
			->setOption(Renderer::GRID_SIZE, 3);
		$g->add($form['note']);
	}

} 
