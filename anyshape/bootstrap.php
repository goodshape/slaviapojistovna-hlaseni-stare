<?php
define("ANYSHAPE", true);
define('BASEPATH', __DIR__ . '/..');

/** @var \Nette\DI\Container|\SystemContainer $container */
$container = require __DIR__ . '/../app/bootstrap.php';
\Tracy\Debugger::$productionMode = TRUE;
header("Cache-Control: no-cache");
header("Pragma: no-cache");

$config = new Config($container);
define('BASEURL', $config['site']['server_path']);
//require_once(BASEPATH . '/anyshape/core/core.php');
require_once(BASEPATH . '/anyshape/helpers/functions.php');

return $config;

 