<?php if (!defined('BASEPATH')) die('Access denied!');

/**
* Prace s obrazky
*/
class Images extends Core {

	/**
	* Konstruktor
	*/
	public function __construct($config) {
		parent::__construct($config);
	}

	/*
	 * Funkce vrati zadany obrazek v zadanych rozmerech.
	 * Zmena velikosti zachovava pomer stran.
	 * Pokud je obrazek mensi, nez zadane rozmery, jeho velikost zustane zachovana.
	 * Obrazky jsou ukladany do specialniho adresare.
	 * Nazev souboru je ocekavan ve formatu {width}x{height}-{hash}-filename.ext
	 * Nazev souboru musi obrashovat i kontrolni hash
	 *
	 * @param	z url si vezme cestu k souboru
	 * @return	vygeneruje zmenseninu a vrati ji browseru
	 * @author	Hugo
	 */
	public function resize()
	{
		// nachystam promenne
		$request = urls(3, 0);
        $dataPath = $this->config['site']['files_path'];

		$resizedFile = basename($request);
		$filename = substr($resizedFile, strpos($resizedFile, '-') + 6);
		$dirname = dirname($request);
		$source = "$dirname/$filename";
		$mime = $this->getMIME($filename);
		$blank = $this->config['images']['blank_image'];

		// z nazvu souboru vytahneme pozadovane rozmery
		$size = element(0, explode('-', $resizedFile));
		$width = (int) element(0, explode('x', $size));
		$height = element(1, explode('x', $size));
		if (substr($height, -1) == 'c')
		{
			$crop = TRUE;
			$height = (int) substr($height, 0, strlen($height) - 1);
		}
		else
		{
			$crop = FALSE;
			$height = (int) $height;
		}

		if ($request !== resize($source, $width, $height, FALSE, $crop))
		{
			header("HTTP/1.0 403 Forbidden");
			exit();
		}
        $source = str_replace('files', $dataPath, $source);

		if ($width > 0 || $height > 0)
		{
            $relativePath = "$dirname/$resizedFile";
			// pokud pozadovany soubor neexistuje nebo neni obrazek, podstrcime 'blank image' v zadanych rozmerech
			if ($mime == false || !is_file($source) || !@getimagesize($source))
			{
				$source = $blank;
				$mime = $this->getMIME($blank);

				$destination = resize($blank, $width, $height);
				header("HTTP/1.0 404 Not Found");
			}
			else
			{
				// nastavime cilovy nazev souboru
				$destination = $this->config['site']['resized_path'] . "/$relativePath";
			}

			// pokud cilovy soubor jeste nebyl zmensen, provedeme zmenseni
			if (!file_exists($destination))
			{
				// mozna bude nutne zobrazit soubor s vodoznakem
				if (get('watermark_id') > 0)
				{
					// pro resize podstrcim soubor s vodoznakem
					if (!file_exists($this->config->wwwDir .'/'.$this->config['site']['watermark_dir'] . $source)) $this->createWatermarkedImage($source, get('watermark_id'));
					$source = $this->config->wwwDir .'/'.$this->config['site']['watermark_dir'] . $source;
				}

				// nactu helper pro manipulaci s obrazky
				require_once(BASEPATH . "/anyshape/helpers/images.php");

				switch ($code = resizeImage($source, $this->config['site']['resized_dir'] .'/'. $relativePath, $this->config->wwwDir, $width, $height, $crop))
				{
					// uspech
					case 0:		@chmod($destination, 0777);
								break;

					// neni potreba zmensovat, presmerujeme na zdrojovy obrazek
					case 1:		$destination = $source;
								break;

					default:	$destination = $this->config['site']['resized_dir'] . dirname($blank) . '/' . $width . 'x' . $height . '-' . basename($blank);
								header("HTTP/1.0 404 Not Found");
				}

			}

		} else
		{
			// pokud nekdo zada nulove rozmery a soubor existuje, pak ho poslu bez zmeny velikosti
			if (is_file($source))
			{
				$destination = "$dirname/$filename";
			}
			// ale pokud nexistuje, zobrazim prazdny obrazek
			else
			{
				$destination = $blank;
				header("HTTP/1.0 404 Not Found");
			}
		}

		// zavolame znovu Apache, aby provedl request znovu
		header("Content-type: $mime");
		@readfile($destination);

	}

	/*
	 * Funkce dle pripony rozdhodne o MIME typu zadaneho souboru
	 * @param	string	nazev souboru
	 * @param	string	MIME type
	 * @author Hugo
	 */
	public function getMIME($filename)
	{
		$parts = explode('.', $filename);
		$ext = element(count($parts)-1, $parts);

		switch (strtolower($ext)) {

			case 'jpg':
			case 'jpeg':	return 'image/jpg';
			case 'png':		return 'image/png';
			case 'gif':		return 'image/gif';
			case 'bmp':		return 'image/bmp';
			default:		return false;

		}
	}

	/*
	 * Funkce zobrazi jednoduchy captcha obrazek a text ulozi do session. Pomoci promennych v GET lze ovlivnovat
	 * ruzne paramatery vysledneho obrazku.
	 * @param	int		length = velikost retezce (implicitne pet znaku)
	 * @param	int		width = sirka obrazku (implicitne 70px)
	 * @param	int		height = vyska obrazku (implicitne 25px)
	 * @param	int		size = velikost pisma (imlicitne 12px)
	 * @param	string	color = barva pisma ve formatu R|G|B, cisla slozek jsou v desitkove soustave (implicitne cerna)
	 * @param	string	background = barva pozadi ve formatu R|G|B, cisla slozek jsou v desitkove soustave (implicitne svetle seda)
	 * @author Hugo
	 */
	public function captcha()
	{
		// nachystatni promennych
		$str = '';
		$length = get('length', 5);
		$width = get('width', 70);
		$height = get('height', 25);
		$size = get('size', 12);
		$color = explode('|', get('color', '0|0|0'));
		$background = explode('|', get('background', '230|230|230'));

		// vygeneruju nahodny retezec
		for($i = 0; $i < $length; $i++) $str .= chr(rand(97, 122));

		// nachystam obrazek
		$img = imagecreatetruecolor($width, $height);
		$front = imagecolorallocate($img, element(0, $color, 0), element(1, $color, 0), element(2, $color, 0));
		$back = imagecolorallocate($img, element(0, $background, 230), element(1, $background, 230), element(2, $background, 230));
		imagefilledrectangle($img, 0, 0, $width, $height, $back);

		// vytvorim text
		$font = $this->config['site']['data_path'].'anyshape/captcha.ttf';
		$y = ($height / 2) + ($size / 2);
		$x = ($width - ($length * $size)) / 2;
		imagettftext($img, $size, 0, $x, $y, $front, $font, $str);

		// poslu do prohlizece
		setSession('captcha', $str);
		header("Content-Type: image/png");
		imagepng($img);

	}

	/**
	 * Funkce pro vytvoreni watermarku
	 * @param	string	Cesta k souboru relativne ke korenu webu
	 * @param	int		Id watermarku v databazi
	 */
	public function watermark()
	{
		$filename = urls(3, 0);
		$error = -1;

		// existuje vubec soubor
		if (file_exists(BASEPATH . "/" . $filename))
		{
			if (($error = $this->createWatermarkedImage($filename, get('watermark_id'))) == 0)
			{
				// posleme soubor na vystup
				header("Content-type: " . $this->getMIME($filename));
				readfile($this->config['site']['watermark_dir'] . $filename);
				exit(0);
			}
		}

		echo "ERROR: [$error]";
		header("HTTP/1.0 404 Not Found");
		exit(1);

	}

	/**
	 * Funkce pro vytvoreni watermarku
	 * @param	string	Cesta k souboru relativne ke korenu webu
	 * @param	int		Id watermarku v databazi
	 */
	protected function createWatermarkedImage($filename, $watermark_id)
	{
		$watermark = dibi::query("SELECT * FROM [:as:watermarks] WHERE id = %i", $watermark_id)->fetch();

		if ($watermark)
		{
			// nactu helper pro manipulaci s obrazky
			require_once(BASEPATH . "/anyshape/helpers/images.php");

			return applyWatermark($filename, $this->config['site']['watermark_dir'] . $filename, $this->config['site']['files_path'] . $watermark['image'],
				$watermark['position'], $watermark['size'],	$watermark['horizontal_padding'], $watermark['vertical_padding']);
		}
		return -2;
	}

}
