<?php if (!defined('BASEPATH')) die('Access denied!');

/**
* Grafy
*/
class Chart extends Core
{

	/**
	* Konstruktor
	*/
	public function __construct($config)
	{
		parent::__construct($config);
	}

	/**
	 * Funkce zavola skript pro pripravu dat pro graf
	 *
	 * @author	Hugo
	 */
	public function get()
	{
		$componentId = post('component_id', get('component_id'))*1;
		$limit = post('limit', 1000);
		$offset = post('offset', 0);

		// nacteni konfigurace komponenty
		$componentData = dibi::query("SELECT name, config FROM [:as:components] WHERE id = %i LIMIT 1", $componentId)->fetch();
		$componentName = element('name', $componentData);
		if (($xmlConfig = XML(element('config', $componentData))) == false) error('xml_error');

		// kontrola prav
		$this->checkComponentAccess($componentName, 'allow_access', $componentId);

		// zakladni promenne
		$source = (string) $xmlConfig['source'];

		global $config;

		$source = safeStr($source, '\/');
		if (file_exists($config['site']['scripts_path'] . "$source.php"))
		{
			define('CHART', TRUE);
			// nactu pomocne tridy
			require_once(dirname(__FILE__) . '/chart_classes/chartContainer.php');
			require_once(dirname(__FILE__) . '/chart_classes/seriesContainer.php');
			require_once(dirname(__FILE__) . '/chart_classes/chartElement.php');
			require_once(dirname(__FILE__) . '/chart_classes/lineChart.php');
			require_once(dirname(__FILE__) . '/chart_classes/areaChart.php');
			require_once(dirname(__FILE__) . '/chart_classes/barChart.php');
			require_once(dirname(__FILE__) . '/chart_classes/columnChart.php');
			require_once(dirname(__FILE__) . '/chart_classes/pieChart.php');
			require_once(dirname(__FILE__) . '/chart_classes/seriesElement.php');
			try
			{
				require_once($config['site']['scripts_path'] . "$source.php");
			}
			catch (Exception $e)
			{
				error('exception', $e->getMessage());
			}
		}
		else
		{
			error('script_not_found', $config['site']['scripts_path'] . "$source.php");
		}
	}

}
