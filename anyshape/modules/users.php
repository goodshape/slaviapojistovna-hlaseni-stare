<?php
use Nette\Security\Passwords;

if (!defined('BASEPATH')) die('Access denied!');

/**
* Sprava uzivatelskych uctu a skupin
*/
class Users extends Core
{

	/**
	* Konstruktor
	*/
	public function __construct($config)
	{
		parent::__construct($config);
	}

	/*
	* Vrati informace o prihlasenem uzivateli
	* @author	Hugo
	*/
	public function getLoginInfo()
	{
		if ($this->isLogged()) {
			returnXML(array('id' => session('logged_id'),
					'name' => session('logged_name'),
					'email' => session('logged_email'),
					'config' => session('logged_config'),
					'permissions' => $this->getPermissions(session('logged_id')),
					'session_id' => session_id()
			));
		}
		setSession('logged', FALSE);
		error('login_required');
	}

	/*
	* Provede odhlaseni a vrati chybu login_required
	* @author	Hugo
	*/
	public function makeLogout()
	{
		setSession('logged', FALSE);
		$this->log(2, 0, 2);
		error('login_required');
	}

	/*
	* Provede prihlaseni. V pripade spatneho hesla vrati chybu invalid_login.
	* @param	string	uzivatelske jmeno
	* @param	string	heslo
	* @author	Hugo
	*/
	public function makeLogin()
	{
		setSession('logged', FALSE);

		// delky periody, za kterou je mozne provest jedno prihlaseni
		$period = 1;
		// max pocet pozadavku
		$max = 5;

		// zrusime vsechny pozadavky, ktere jsou starsi nez max. perioda
		dibi::query('UPDATE [:as:login_requests] SET status = 2 WHERE UNIX_TIMESTAMP() - UNIX_TIMESTAMP(timestamp) > %i AND status = 0', $max * $period);

		// status:	0 - novy pozadavek
		//			1 - vyrizeny pozadavek
		//			2 - pozadavek vyprsel

		// zjistim pocet pozadavku ve fronte
		$numFront = dibi::query('SELECT COUNT(*) FROM [:as:login_requests] WHERE status = 0')->fetchSingle() + 1;

		// pokud je ve fronte vice pozadavku nez je limit, vyhodime chybu
		if ($numFront > $max) error('too_many_requests');

		// ulozim pozadavek do databaze
		dibi::query('INSERT INTO [:as:login_requests]', array(
			'username'	=> post('username'),
			'password'	=> Passwords::hash(post('password')),
			'ip'		=> element('REMOTE_ADDR', $_SERVER),
			'status'	=> 0
		));

		$id = dibi::insertId();

		sleep($period * $numFront);

		// pozadavek nastavim jako vyrizeny
		dibi::query("UPDATE [:as:login_requests] SET status = 1 WHERE id = %i", $id);

		// overeni hesla pro roota (posleme zpet zasifrovane heslo)
		if (post('username') === 'root' && post('password') == false)
		{
			returnXML(array('reply' => substr($this->config['site']['root_password'], 8)));
		}

		if (post('username') === 'root' && post('password') == substr($this->config['site']['root_password'], 0, 8))
		{
			setSession('logged', TRUE);
			setSession('logged_id', 0);
			setSession('logged_name', 'Root');
			setSession('logged_email', $this->config['email']['from']);
			setSession('logged_config', '');
			$this->log(1, 0, 2);
			$this->getLoginInfo();
		}

		$result = element(0, dibi::query(
			"SELECT [id], [name], [email], [config], [password]  FROM [:as:users] WHERE [username] = %s", post('username'), " AND status = 1"
		)->fetchAll());

		if ($result && Passwords::verify(post('password'), $result->password))
		{
			setSession('logged', TRUE);
			setSession('logged_id', element('id', $result));
			setSession('logged_name', element('name', $result));
			setSession('logged_email', element('email', $result));
			setSession('logged_config', element('config', $result));
			$this->log(1, 0, 2);
			$this->getLoginInfo();
		}

		$this->log(3, 0, 2);

		error('invalid_login');
	}

	/*
	* Provede prihlaseni pod jinym uzivatelem. Muze volat jen root.
	* @param	string	uzivatelske jmeno
	* @param	string	heslo
	* @author	Hugo
	*/
	public function makeLoginAs()
	{
		if ($this->isLogged() && session('logged_id') === 0)
		{
			$result = element(0, dibi::query("SELECT id, name, email, config FROM [:as:users] WHERE [id] = %i", get('id'), "AND status = 1")->fetchAll());
			if ($result)
			{
				setSession('logged', TRUE);
				setSession('logged_id', element('id', $result));
				setSession('logged_name', element('name', $result));
				setSession('logged_email', element('email', $result));
				setSession('logged_config', element('config', $result));
				$this->log(1, 0, 2);
				$this->getLoginInfo();
			}
		} else error('access_denied');
	}

	/*
	* Funkce vrati uzivatelova prava na komponenty jako pole, kde radek pole je ve tvaru [nazev operace, nazev komponenty, id komponenty]
	* @param	int	id uzivatele
	* @author	Hugo
	*/
	private function getPermissions($user_id)
	{
		if ($user_id > 0) {
			$result = dibi::query(
				"SELECT operation, c.name AS component, component_id",
				"FROM [:as:groups_components]",
				"LEFT JOIN [:as:components] AS c ON c.id = component_id",
				"WHERE group_id IN (SELECT group_id FROM [:as:users_groups] WHERE user_id = %i)", $user_id
			)->fetchAll();
			return $result;
		}
		return array();
	}

	/*
	* Funkce vrati seznam uzivatelu
	* @author	Hugo
	*/
	public function getUsers()
	{
		if ($this->isLogged()) {
			returnXML(dibi::query("SELECT id, name, username, status, comment FROM [:as:users] AS users ORDER BY users.name ASC")->fetchAll());
		}
	}

	/*
	* Funkce vrati seznam uzivatelu, skupin a jejich vzajemnych vazeb
	* @author	Hugo
	*/
	public function getAllUserData()
	{
		$this->checkComponentAccess('UserAdmin');
		$this->log(17, 0);
		$users = dibi::query("SELECT id, name, username, status, comment FROM [:as:users] AS users ORDER BY users.name ASC")->fetchAll();
		$groups = dibi::query("SELECT id, name, description FROM [:as:groups] AS groups ORDER BY groups.name ASC")->fetchAll();
		$conn = dibi::query("SELECT user_id, group_id FROM [:as:users_groups] AS conn")->fetchAll();
		returnXML(array('users' => $users, 'groups' => $groups, 'users_groups' => $conn));
	}

	/*
	* Vrati informace o uzivateli
	*/
	public function getUser()
	{
		$this->checkLogin();
		$this->log(18, 0);
		$id = get('id');
		$result = dibi::query('SELECT id, name, username, email, comment, status FROM [:as:users] WHERE [id] = %i', $id)->fetchAll();
		returnXML($result);
	}

	/*
	* Ulozi uzivatelovo nastaveni
	*/
	public function saveUserConfig()
	{
		$this->checkLogin();
		$id = post('id')*1;
		$data = array();
		if (strlen(post('config', '')) > 0) $data['config'] = post('config', '');
		if (strlen(post('name', '')) > 0) $data['name'] = post('name', '');
		if (strlen(post('password', '')) > 0) $data['password'] = Passwords::hash(post('password', ''));
		if (strlen(post('email', '')) > 0) $data['email'] = post('email', '');
		if ($id == session('logged_id') && count($data > 0) && $id > 0) {
			$diff = element(0, dibi::query("SELECT * FROM [:as:users] WHERE id = %i", $id)->fetchAll());
			dibi::query("UPDATE [:as:users] SET", $data, "WHERE id = %i", $id);
			$this->log(30, 0, 2, $diff);
			if (strlen(element('name', $data)) > 0) setSession('logged_name', $data['name']);
			if (strlen(element('email', $data)) > 0) setSession('logged_email', $data['email']);
			if (strlen(element('config', $data)) > 0) setSession('logged_config', $data['config']);
			success(dibi::affectedRows());

		} else error();
	}

	/*
	* Vrati informace o skupine
	*/
	public function getGroup()
	{
		$this->checkLogin();
		$id = get('id');
		if (!$this->groupAccess($id)) error('access_denied');
		$this->log(19, 0);
		$result = dibi::query('SELECT id, name, description FROM [:as:groups] WHERE [id] = %i', $id)->fetchAll();
		returnXML($result);
	}

	/*
	* Vlozi noveho uzivatele
	*/
	public function insertUser()
	{
		$this->checkComponentAccess('UserAdmin', 'add_user');
		$this->log(20, 0, 2);
		$data = array(
			'name' 		=> post('name', NULL),
			'username' 	=> post('username', NULL),
			'password' 	=> Passwords::hash('newpassword'),
			'status'	=> 0
		);
		dibi::query("INSERT INTO [:as:users]", $data);
		success(dibi::insertId());
	}

	/*
	* Vlozi novou skupinu a prida do ni aktualniho uzivatele
	*/
	public function insertGroup()
	{
		$this->checkComponentAccess('UserAdmin', 'add_group');
		$this->checkLogin();
		$this->log(24, 0, 2);
		$data = array('name' => post('name', NULL));
		dibi::query("INSERT INTO [:as:groups]", $data);
		$group_id = dibi::insertId();
		$user_id = session('logged_id');
		if ($user_id > 0) {
			$data = array('user_id' => $user_id, 'group_id' => $group_id);
			dibi::query("INSERT INTO [:as:users_groups]", $data);
		}
		success(1);
	}


	/*
	* Provede update uzivatele
	*/
	public function updateUser()
	{
		$this->checkComponentAccess('UserAdmin', 'edit_user');
		$id = post('id')*1;
		$data = array(
			'name' 		=> post('name', ''),
			'username' 	=> post('username',''),
			'email' 	=> post('email',''),
			'comment' 	=> post('comment',''),
			'status' 	=> post('status', 0)
		);
		if (strlen(post('password' ,'')) > 0) $data['password'] = Passwords::hash(post('password' ,''));
		if ($id > 0) {
			$diff = element(0, dibi::query("SELECT * FROM [:as:users] WHERE id = %i", $id)->fetchAll());
			dibi::query("UPDATE [:as:users] SET", $data, "WHERE id = %i", $id);
			$this->log(19, 0, 2, $diff);
			success(dibi::affectedRows());
		}
	}

	/*
	* Provede update skupiny a prislusnych prav na operace a strukturu
	* @param	int		id skupiny
	* @param	string	seznam idecek komponent a operaci ve tvaru id,nazev_operace|id,nazev_operace|...
	* @param	string	seznam idecek uzlu ve strukture oddeleny svislitky
	* @author	Hugo
	*/
	public function updateGroup()
	{
		$this->checkComponentAccess('UserAdmin', 'edit_group');

		$id = post('id')*1;
		// muze upravovat jen skupinu, ve ktere je sam clenem
		//if (!$this->groupAccess($id)) error('access_denied');

		$count = 0;

		if ($id > 0) {
			/* nachystani dat */
			$data = array(
				'name'	 	=> post('name', ''),
				'description' 	=> post('description','')
			);

			$component_rights = boom(post('component_rights'));
			$structure_rights = boom(post('structure_rights'));

			$structure_ids = array();
			$levels = array();
			foreach ($structure_rights as $structure_right)
			{
				$right = explode('-', $structure_right);
				$structure_ids[] = element(0, $right);
				$levels[] = element(1, $right);
			}
			if (count($structure_ids) != count($levels)) error('incorrect_parameters');

			/* vlastni ulozeni udaju o skupine */
			$diff = element(0, dibi::query("SELECT * FROM [:as:groups] WHERE id = %i", $id)->fetchAll());
			dibi::query("UPDATE [:as:groups] SET", $data, "WHERE id = %i", $id);
			$this->log(23, 0, 2, $diff);

			/* ulozeni prav na operace */
			$data = array();

			foreach ($component_rights as $component_right)
			{
				$component_id = element(0, explode(',', $component_right), null);
				$operation = element(1, explode(',', $component_right), null);
				if ($component_id !== null && $operation !== null)
				{
					// ma dany uzivatel sam toto pravo a muze ho tedy delegovat skupine?
					$this->checkComponentAccess(null, $operation, $component_id);

					// provedemy tedy zmenu prava
					$data[] = array('group_id' => $id, 'component_id' => $component_id, 'operation' => $operation);

				}
			}
			$diff = dibi::query("SELECT * FROM [:as:groups_components] WHERE group_id = %i", $id)->fetchAll();
			dibi::query("DELETE FROM [:as:groups_components] WHERE group_id = %i", $id);
			$count += dibi::affectedRows();
			foreach ($data as $dataItem)
			{
				dibi::query("INSERT INTO [:as:groups_components]", $dataItem);
				$count += dibi::affectedRows();
			}

			$this->log(29, 0, 2, $diff);

			/* ulozeni prav na strukturu */
			foreach ($structure_ids as $key => $structure_id)
			{
				// ma dany uzivatel sam toto pravo a muze ho tedy delegovat skupine?
				$this->checkNodeAccess($structure_id, element($key, $levels, 0));
				$data = array('group_id' => $id, 'structure_id' => $structure_id, 'level' => element($key, $levels, 0));
				dibi::query("REPLACE INTO [:as:groups_structure]", $data);
				$count += dibi::affectedRows();
			}

			// vsechny ostatni prava je mozno smazat
			$diff = null;
			if (count($structure_rights) > 0) {
				$diff = dibi::query("SELECT * FROM [:as:groups_structure] WHERE group_id = %i", $id, " AND structure_id NOT IN (%i", $structure_ids, ")")->fetchAll();
				dibi::query("DELETE FROM [:as:groups_structure] WHERE group_id = %i", $id, " AND structure_id NOT IN (%i", $structure_ids, ")");
				$count += dibi::affectedRows();
			} else {
				$diff = dibi::query("SELECT * FROM [:as:groups_structure] WHERE group_id = %i", $id)->fetchAll();
				dibi::query("DELETE FROM [:as:groups_structure] WHERE group_id = %i", $id);
				$count += dibi::affectedRows();
			}
			/* todo: predelat logovani, neni uplne spravne */
			$this->log(29, 0, 2, $diff);
			success($count);
		}
	}


	/*
	* Vrati seznam modulu, komponent a operaci jako strom spolu s pravy na dane komponenty a operace
	* @param	group_id	skupina, jejiz prava ma funkce vratit
	* @author	Hugo
	*/
	public function getModulesTree()
	{
		$this->checkComponentAccess('UserAdmin', 'edit_group');
		$this->log(28, 0);
		$user_id = session('logged_id');
		$group_id = post('group_id', 0);
		$result = dibi::query("SELECT id, name FROM [:as:modules]")->fetchAll();

		foreach ($result as &$module)
		{
			$module['components'] = (array) dibi::query(
				"SELECT id, name, alias, module_id, type, operations, icon16x16 FROM [:as:components]",
				"WHERE module_id = %i", $module['id']
			)->fetchAll();

			foreach ($module['components'] as &$component)
			{
				if ($user_id == 0) {
					$all = explode('|', $component['operations']);
					$set = (array) dibi::query(
						"SELECT operation FROM [:as:groups_components]",
						"WHERE group_id = %i AND component_id = %i", $group_id, $component['id']
					)->fetchPairs();
					$operations = array();
					foreach ($all as $op) {
						$operations[] = array('name' => $op, 'r' => (int) in_array($op, $set));
					}
					$component['operations'] = $operations;
				} else {
					$all = explode('|', $component['operations']);
					$set = dibi::query(
						"SELECT operation FROM [:as:groups_components]",
						"WHERE group_id = %i AND component_id = %i", $group_id, $component['id']
					)->fetchPairs();
					$allow = dibi::query(
						"SELECT DISTINCT operation FROM [:as:groups_components]",
						"WHERE group_id IN (SELECT group_id FROM [:as:users_groups] WHERE user_id = %i)", $user_id,
						"AND component_id = %i", $component['id']
					)->fetchPairs();
					$operations = array();
					foreach ($all as $op)
						if (in_array($op, $allow))
							$operations[] = array('name' => $op, 'r' => (int) in_array($op, $set));
					}
					$component['operations'] = $operations;
			}
			unset($component);
		}
		returnXML($result);
	}

	/*
	* Vrati strom uzlu ve strukuture spolu s pravy na dane uzly
	* @param	group_id	skupina, jejiz prava ma funkce vratit
	* @author	Hugo
	*/
	public function getStructureTree()
	{
		$this->checkComponentAccess('UserAdmin', 'edit_group');
		$this->log(7, 0);
		$group_id = post('group_id', 0);
		header('Content-type: text/xml');
		echo "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
		echo "<node>";
		echo $this->getBranch(0, $group_id);
		echo "</node>";
	}

	/*
	* Pomocna funkce k getStructureAdmin()
	*/
	private function getBranch($parent_id = 0, $group_id)
	{
		$user_id = session('logged_id');
		$l = $this->language_id;
		$branch = dibi::query(
			"SELECT s.id, name.text AS name, s.status, s.position, t.component_id, t.icon, c.name AS component, r.level ",
			"%if", $user_id > 0, ", MAX(r2.level) as `limit` %end",
			"FROM [:data:structure] AS s",
			// z typu stranky zjistim implicitni komponentu
			"LEFT JOIN [:as:page_types] AS t ON t.id = s.page_type",
			// budu potrebovat nazev komponenty
			"LEFT JOIN [:as:components] AS c ON c.id = t.component_id",
			// nazev je multijazycny
			"LEFT JOIN [:data:texts] AS name ON s.name = name.id AND name.language_id = %s", $this->language_id,
			// prava na uzly
			"LEFT JOIN [:as:groups_structure] AS r ON r.group_id = %i", $group_id, "AND r.structure_id = s.id",
			"%if", $user_id > 0, "INNER JOIN [:as:users_groups] AS g ON g.user_id = %i", $user_id,
			"INNER JOIN [:as:groups_structure] AS r2 ON r2.group_id = g.group_id AND r2.structure_id = s.id %end",
			"%if", $parent_id == 0,
			"WHERE ([parent_id] = 0 OR [parent_id] IS NULL)",
			"%else",
			"WHERE [parent_id] = %i", $parent_id,
			"%end",
			"GROUP BY s.id",
			"ORDER BY [position] ASC")->fetchAssoc("id");
		$xml = '';
		foreach ($branch as $id => $subbranch)
		{
			$name = xmlConvert(element('name', $subbranch, ''));
			$xml .= sprintf(
				'<node name="%s" id="%d" parent_id="%s" component_id="%d" url="%s" component="%s" status="%s" level="%d" limit="%d">',
				strlen($name) > 0 ? $name : "[$id]",
				element('id', $subbranch, ''),
				$parent_id,
				xmlConvert(element('component_id', $subbranch, '')),
				element('url', $subbranch, NULL) === NULL ? '__NULL__' : xmlConvert(element('url', $subbranch, NULL)),
				xmlConvert(element('component', $subbranch, '')),
				xmlConvert(element('status', $subbranch, '')),
				xmlConvert(element('level', $subbranch, '')),
				xmlConvert(element('limit', $subbranch, ''))
			);
			$xml .= $this->getBranch($id, $group_id);
			$xml .= '</node>';
		}
		return $xml;
	}

	/*
	* Smaze uzivatele a souvisejici propojeni
	*/
	public function deleteUsers()
	{
		$this->checkComponentAccess('UserAdmin', 'delete_user');
		$user_ids = boom(post('user_ids'));
		$count = 0;
		foreach ($user_ids as $user_id)
		{
			$diff = dibi::query("SELECT * FROM [:as:users_groups] WHERE user_id = %i", $user_id)->fetchAll();
			dibi::query("DELETE FROM [:as:users_groups] WHERE user_id = %i", $user_id);
			$count += dibi::affectedRows();
			$this->log(27, 0, 2, $diff);
			$diff = element(0, dibi::query("SELECT * FROM [:as:users] WHERE id = %i", $user_id)->fetchAll());
			dibi::query("DELETE FROM [:as:users] WHERE id = %i", $user_id);
			$count += dibi::affectedRows();
			$this->log(21, 0, 2, $diff);
		}
		success($count);
	}

	/*
	* Smaze skupiny a souvisejici propojeni
	*/
	public function deleteGroups()
	{
		$this->checkComponentAccess('UserAdmin', 'delete_group');
		$group_ids = boom(post('group_ids'));
		$count = 0;
		foreach ($group_ids as $group_id)
		{
			// muze mazat jen skupiny, v nichz je sam clenem
			if (!$this->groupAccess($group_id)) error('access_denied');
			$diff = dibi::query("SELECT * FROM [:as:users_groups] WHERE group_id = %i", $group_id)->fetchAll();
			dibi::query("DELETE FROM [:as:users_groups] WHERE group_id = %i", $group_id);
			$count += dibi::affectedRows();
			$this->log(27, 0, 2, $diff);
			$diff = element(0, dibi::query("SELECT * FROM [:as:groups] WHERE id = %i", $group_id)->fetchAll());
			dibi::query("DELETE FROM [:as:groups] WHERE id = %i", $group_id);
			$count += dibi::affectedRows();
			$this->log(25, 0, 2, $diff);
		}
		success($count);
	}

	/*
	* Prida uzivatele do zadanych skupin
	* @param	bud id uzivatele a seznam id skupin
	* @param	nebo seznam id uzivatelu a id skupiny
	* @author	Hugo
	*/
	public function insertUsersIntoGroups()
	{
		$this->checkComponentAccess('UserAdmin', 'add_user_to_group');
		$user_ids = boom(post('user_ids'));
		$group_ids = boom(post('group_ids'));
		$count = 0;
		if (count($user_ids) == 1)
		{
			// pridavani uzivatele do vice skupin
			$user_id = element(0, $user_ids, 0);
			foreach ($group_ids as $group_id)
			{
				// muze pridat jen do skupin, v nichz je sam clenem
				if (!$this->groupAccess($group_id)) error('access_denied');
				// kontrola, jestli uz nahodou dany uzivatel neni ve skupine
				$c = dibi::query("SELECT COUNT(user_id) FROM [:as:users_groups] WHERE user_id = %i", $user_id, " AND group_id = %i", $group_id)->fetchSingle();
				if ($c == 0)
				{
					dibi::query("INSERT INTO [:as:users_groups] ", array('user_id' => $user_id, 'group_id' => $group_id));
					$count += dibi::affectedRows();
					$this->log(26, 0, 2);
				}
			}
		}
		if (count($user_ids) > 1)
		{
			// pridavani vice uzivatelu do jedne skupiny
			$group_id = element(0, $group_ids, 0);
			foreach ($user_ids as $user_id)
			{
				// muze pridat jen do skupin, v nichz je sam clenem
				if (!$this->groupAccess($group_id)) error('access_denied');
				// kontrola, jestli uz nahodou dany uzivatel neni ve skupine
				$c = dibi::query("SELECT COUNT(user_id) FROM [:as:users_groups] WHERE user_id = %i", $user_id, " AND group_id = %i", $group_id)->fetchSingle();
				if ($c == 0)
				{
					dibi::query("INSERT INTO [:as:users_groups] ", array('user_id' => $user_id, 'group_id' => $group_id));
					$count += dibi::affectedRows();
					$this->log(26, 0, 2);
				}
			}
		}
		success($count);
	}

	/*
	* Odebere uzivatele ze zadanych skupin
	* @author	Hugo
	*/
	public function removeUsersFromGroups()
	{
		$this->checkComponentAccess('UserAdmin', 'add_user_to_group');
		$this->checkLogin();
		$user_ids = boom(post('user_ids'));
		$group_ids = boom(post('group_ids'));
		$count = 0;
		// sedi pocet idecek?
		if (count($user_ids) == count($group_ids))
		{
			foreach ($group_ids as $key => $group_id)
			{
				// muze odebrat jen ze skupin, v nichz je sam clenem
				if (!$this->groupAccess($group_id)) error('access_denied');
				$user_id = element($key, $user_ids, 0);
				$diff = element(0, dibi::query("SELECT * FROM [:as:users_groups] WHERE user_id = %i", $user_id, " AND group_id = %i", $group_id)->fetchAll());
				dibi::query("DELETE FROM [:as:users_groups] WHERE user_id = %i", $user_id, ' AND group_id = %i', $group_id);
				$count += dibi::affectedRows();
				$this->log(27, 0, 2, $diff);
			}
		} else error('incorrect_parameters');
		success($count);
	}

	/*
	* Vrati true, pokud je uzivatel v dane skupine nebo root
	* @author	Hugo
	*/
	private function groupAccess($group_id)
	{
		return dibi::query("SELECT CoUNT(*) FROM [:as:users_groups] WHERE user_id = %i", session('logged_id'), " AND group_id = %i", $group_id)->fetchSingle() > 0 || session('logged_id') == 0;
	}

}
