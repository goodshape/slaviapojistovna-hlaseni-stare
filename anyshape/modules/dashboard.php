<?php if (!defined('BASEPATH')) die('Access denied!');

class Dashboard extends Core
{
	/*
	* Konstruktor
	* @author	Hugo
	*/
	public function __construct($config)
	{
		parent::__construct($config);
	}

	/*
	* Vrati komponenty, ktere ma dany uzivatel na nastence
	* @author	Hugo
	*/
	public function getComponents()
	{
		$this->checkLogin();
		$result = dibi::query(
			"SELECT c.id, c.folder_id, c.type, c.name, c.alias, c.dashboard, c.icon16x16, c.icon32x32 FROM [:as:components] AS c",
			"%if", session('logged_id') > 0,
			"INNER JOIN [:as:groups_components] AS p ON p.component_id = c.id AND p.operation = 'allow_access'",
			"INNER JOIN [:as:users_groups] AS g ON g.user_id = %i", session('logged_id'), "AND g.group_id = p.group_id",
			"%end",
			"WHERE c.dashboard > 0 GROUP BY id ORDER BY c.dashboard DESC, c.`order` ASC"
		)->fetchAll();
		returnXML($result);
	}

}
