<?php if (!defined('BASEPATH')) die('Access denied!');

/**
 * Struktura webove aplikace
 */
class Structure extends Core
{

	/**
	 * Konstruktor
	 */
	public function __construct($config)
	{
		parent::__construct($config);
	}

	/**
	 * Vrati strukturu jako XML pro strom
	 * @param	int		parent_id	moznost zadat id uzlu, jehoz poduzly budou vraceny
	 * @return	XML		struktura jako XML
	 */
	public function load()
	{
		$this->checkComponentAccess('StructureAdmin');
		$this->log(7, 0);
		$parent_id = post('parent_id', null);
		header('Content-type: text/xml');
		echo "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
		echo "<node>";
		echo $this->getBranch($parent_id);
		echo "</node>";
	}

	/**
	 * Pomocna funkce k load()
	 */
	private function getBranch($parent_id, $parentLanguages = null)
	{
		$user_id = session('logged_id');
		$l = $this->language_id;

		$branch = dibi::query(
			"SELECT s.id, name.text AS name, host, s.status, s.position, t.component_id, t.icon, t.name AS page_type, ",
				"s.page_type AS page_type_id, c.name AS component, c.type AS component_type, s.child_limit, s.languages, u.url_string",
			"%if", $user_id > 0, ", MAX(r.level) AS level %end",
			"FROM [:data:structure] AS s",
			// z typu stranky zjistim implicitni komponentu
			"LEFT JOIN [:as:page_types] AS t ON t.id = s.page_type",
			// budu potrebovat nazev komponenty
			"LEFT JOIN [:as:components] AS c ON c.id = t.component_id",
			// nazev je multijazycny
			"LEFT JOIN [:data:texts] AS name ON s.name = name.id AND name.language_id = %s", $l,
			// url retezec dle lookup-table
			"LEFT JOIN [:data:url_lookup_table] AS u ON u.structure_id = s.id AND u.language_id = %s", $l,
			// pokud neni root, nactu i prava
			"%if", $user_id > 0,
			"LEFT JOIN [:as:users_groups] AS g ON g.user_id = %i", $user_id,
			"LEFT JOIN [:as:groups_structure] AS r ON r.group_id = g.group_id AND r.structure_id = s.id",
			"%end",
			"WHERE %if",$parent_id == NULL ,"[parent_id] IS NULL %else [parent_id] = %iN", $parent_id, "%end",
			// jen aktualni jazyk
			"AND (s.languages LIKE '%|$l|%' OR s.languages LIKE '%|$l' OR s.languages LIKE '$l|%' OR s.languages LIKE '$l' OR s.languages IS NULL)",
			"GROUP BY s.id",
			"ORDER BY [position] ASC"
		)->fetchAssoc("id");

		$xml = '';

		foreach ($branch as $id => $subbranch)
		{
			$parentLanguages = element('languages', $subbranch, null) === null ? $parentLanguages : xmlConvert(element('languages', $subbranch, null));

			if ($user_id == 0 || element('level', $subbranch, 0) > 0)
				$xml .= sprintf(
					'<node name="%s" id="%d" parent_id="%s" host="%s" component_id="%d" page_type="%s" page_type_id="%s" icon="%s" position="%s" component="%s" component_type="%s" status="%s" child_limit="%s" level="%d" url_string="%s" languages="%s">',
					xmlConvert(element('name', $subbranch, '')),
					element('id', $subbranch, ''),
                    (int) $parent_id,
					element('host', $subbranch, ''),
					xmlConvert(element('component_id', $subbranch, '')),
					xmlConvert(element('page_type', $subbranch, '')),
					xmlConvert(element('page_type_id', $subbranch, 0)),
					xmlConvert(element('icon', $subbranch, '')),
					xmlConvert(element('position', $subbranch, '')),
					xmlConvert(element('component', $subbranch, '')),
					xmlConvert(element('component_type', $subbranch, '')),
					xmlConvert(element('status', $subbranch, '')),
					element('child_limit', $subbranch, null) === null ? '__NULL__' : xmlConvert(element('child_limit', $subbranch, null)),
					xmlConvert(element('level', $subbranch, null)),
					xmlConvert(element('url_string', $subbranch, null)),
					$parentLanguages
				);

			$xml .= $this->getBranch($id, $parentLanguages);

			if ($user_id == 0 || element('level', $subbranch, 0) > 0) $xml .= '</node>';

		}
		return $xml;
	}

	/**
	 * Vrati jeden cely uzel struktury i s pravy
	 */
	public function getItem()
	{
		$id = get('id')*1;

		if (!$this->isAllowed('StructureAdmin', 'page_properties') &&
			!$this->isAllowed('StructureAdmin', 'seo_properties') &&
			!$this->isAllowed('StructureAdmin', 'page_access_control')) error('access_denied');

		$this->checkNodeAccess($id, ADMIN_ACCESS);
		$user_id = session('logged_id');

		$this->log(8, 0);

		$result = dibi::query(
			'SELECT id, parent_id, host, page_type, name, title, h1, description, keywords, copyright, robots, status, child_limit, url, languages ',
			'FROM [:data:structure] AS s',
			'WHERE id = %i', $id
		)->fetch();

		$result = $this->getInheritedProperties($result, array('description', 'keywords', 'copyright', 'languages'));

		$result['rights'] = dibi::query(
			'SELECT g.id, g.name, r.level',
			'FROM [:as:groups] as g',
			'LEFT JOIN [:as:groups_structure] as r ON g.id = r.group_id AND r.structure_id = %i', $id,
			'%if', $user_id > 0, 'INNER JOIN [:as:users_groups] AS u ON u.user_id = %i', $user_id, ' AND u.group_id = g.id %end'
		)->fetchAll();

		$result = $this->loadTexts($result, array('name', 'content', 'title', 'h1', 'description', 'keywords', 'copyright',
			'description_inherited', 'keywords_inherited', 'copyright_inherited'));

		$result['url_strings'] = dibi::query("SELECT url_string AS text, language_id FROM [:data:url_lookup_table] WHERE structure_id = %i", $id)->fetchAll();
        $result->parent_id  = (int) $result->parent_id;
		returnXML($result);
	}

	/**
	 * Projde vysledek dotazu ($result) a pokud nektery ze zadanych sloupcu je null, zjisti
	 * jeho hodnotu od nadrazenych uzlu. Volano z funkce getItem()
	 */
	private function getInheritedProperties($result, $columns)
	{
		$parent_id = element('parent_id', $result, NULL);
		// zjisteni hodnot od rodice
		while (count($columns) > 0 && $parent_id > 0)
		{
				$new_result = dibi::query('SELECT parent_id, ', implode(',', $columns), 'FROM [:data:structure] WHERE [id] = %i', $parent_id)->fetch();
				foreach ($columns as $key => $column) {
					$result[$column.'_inherited'] = element($column, $new_result, null);
					if (element($column, $new_result) !== null) unset($columns[$key]);
				}
				$parent_id = element('parent_id', $new_result);
		}
		return $result;
	}

	/**
	 * Ulozi jeden cely uzel struktury
	 */
	public function saveItem()
	{
		$id = post('id')*1;
		$this->checkNodeAccess($id, EDIT_ACCESS);
		$group_ids = boom(post('group_ids'));
		$right_levels = boom(post('right_levels'));

		// pokud maji byt nektere z textu nastaveny, aby byly dedeny od nadrazenych uzlu, je potreba smazat jejich aktualni obsah z tabulky textu
		$textsToDelete = dibi::query("SELECT title, h1, description, keywords, copyright FROM [:data:structure] WHERE id = %i", $id)->fetch();
		foreach (array('title', 'h1', 'description', 'keywords', 'copyright') as $column)
			if (post($column, null) == null && element($column, $textsToDelete) > 0) dibi::query("DELETE FROM [:data:texts] WHERE id = %i", element($column, $textsToDelete));

		$data = array();

		// nachystam data pro hlavni sekci
		if ($this->isAllowed('StructureAdmin', 'page_properties'))
		{
			$data['page_type']		= post('page_type', 0);
			$data['name']			= $this->saveTexts(post('name', ''));
			$data['host']	 		= post('host', '');
			$data['url']	 		= post('url', null);
			$data['status']			= post('status', 0);
			$data['child_limit']	= post('child_limit', null);
			$data['languages']		= post('languages', null);
		}

		// nachystam data pro seo sekci
		if ($this->isAllowed('StructureAdmin', 'seo_properties'))
		{
			$data['title'] 			= $this->saveTexts(post('title', null));
			$data['h1'] 			= $this->saveTexts(post('h1', null));
			$data['description']	= $this->saveTexts(post('description', null));
			$data['keywords'] 		= $this->saveTexts(post('keywords', null));
			$data['copyright']	 	= $this->saveTexts(post('copyright', null));
			$data['robots']			= post('robots', null);
		}

		if (count($group_ids) != count($right_levels)) error('incorrect_parameters');

		$count = 0;

		if ($id > 0)
		{
			// ulozim prava
			if ($this->isAllowed('StructureAdmin', 'page_access_control'))
			{
				// todo: pri ukladani prav neni logovani
				foreach ($group_ids as $key => $group_id) {
					if (element($key, $right_levels, 0) > 0) dibi::query("REPLACE INTO [:as:groups_structure]", array('group_id' => $group_id, 'structure_id' => $id, 'level' => element($key, $right_levels, 0)));
					else dibi::query("DELETE FROM [:as:groups_structure] WHERE group_id = %i", $group_id, " AND structure_id = %i", $id);
					$count += dibi::affectedRows();
				}
			}

			// ulozim data o strance
			if (count($data) > 0)
			{
				$diff = element(0, dibi::query("SELECT id, page_type, name, title, h1, description, keywords, copyright, robots, url, status FROM [:data:structure] WHERE id = %i", $id)->fetchAll());
				$this->log(11, 0, 2, $diff);
				// ulozeni dat uzlu
				dibi::query("UPDATE [:data:structure] SET ", $data, "WHERE id = %i", $id);
				$count += dibi::affectedRows();
			}

			// ulozim url do lookup table
			if ($this->isAllowed('StructureAdmin', 'seo_properties'))
			{
				dibi::query("DELETE FROM [:data:url_lookup_table] WHERE structure_id = %i", $id);

				$urlStrings = XML(post('url_strings'));

				if ($urlStrings)
					foreach ($urlStrings->children() as $url)
						$this->updateSeoString($id, (string) $url->text, (string) $url->language_id);
			}

			success($count);

		}

	}

	/**
	 * Provedeni updatu
	 *
	 * @param int     $id           Id stranky
	 * @param string  $urlStrings   Retezec pro URL
	 */
	protected function updateSeoString($id, $urlString, $languageId)
	{
		$websiteId = $this->getWebsiteId($id);

		while (dibi::query('SELECT COUNT(*) FROM [:data:url_lookup_table] WHERE website_id = %i AND url_string = %s AND language_id = %s', $websiteId, $urlString, $languageId)->fetchSingle() > 0)
		{
			preg_match('/\-(\d+)$/', $urlString, $matches);
			$i = (int) element(1, $matches);
			$urlString = preg_replace('/\-\d+$/', '', $urlString);
			$urlString .= '-' . ($i + 1);
		}
		dibi::query("REPLACE INTO [:data:url_lookup_table]", array(
			"website_id"	=> $websiteId,
			"structure_id"	=> $id,
			"language_id"	=> $languageId,
			"url_string"	=> $urlString
		));
	}

	/**
	 * Vrati korenovy uzel zadaneho uzlu
	 *
	 * @param int     $id           Id stranky
	 * @return int
	 */
	protected function getWebsiteId($id)
	{
		$parentId = (int) dibi::query("SELECT parent_id FROM [:data:structure] WHERE id = %i", $id)->fetchSingle();
		if ($parentId == 0) return $id;
		return $this->getWebsiteId($parentId);
	}

	/**
	 * Vlozi novy uzel
	 */
	public function newItem()
	{
		$parent_id = (int) post('parent_id');

        if($parent_id == 0) {
            $parent_id = NULL;
        }

		// overeni prav
		if ($parent_id == 0) $this->checkComponentAccess('StructureAdmin', 'add_root');
		$this->checkComponentAccess('StructureAdmin', 'add_page');

		// nebyl prekrocen limit uzlu?
		$limit = dibi::query('SELECT child_limit FROM [:data:structure] WHERE [id] = %i', $parent_id)->fetchSingle();
		$count = dibi::query('SELECT COUNT(id) FROM [:data:structure] WHERE %and', ['parent_id' => $parent_id])->fetchSingle();

		if ($count < $limit || $limit === null || $parent_id == 0)
		{
			$availableLanguages = dibi::query('SELECT id FROM [:as:languages] WHERE active = 1')->fetchPairs();

			// nastaveni podedime od nadrazeneho uzlu
			$parent = dibi::query(
				"SELECT page_type, title, h1, description, keywords, copyright, robots, url, status, languages",
				"FROM [:data:structure] WHERE id = %i", $parent_id
			)->fetch();

			$name = post('name', '');

			$data = array(
				'page_type'		=> element('page_type', $parent, null),
				'name' 			=> $this->saveTexts($name),
				'host' 			=> $parent_id == 0 ? @$_SERVER['SERVER_NAME'] : '',
				'parent_id'		=> $parent_id,
				'title'			=> null,
				'h1'			=> null,
				'description'	=> null,
				'keywords'		=> null,
				'copyright'		=> null,
				'robots'		=> null,
				'url'			=> element('url', $parent, ''),
				'languages'		=> $parent_id > 0 ? null : implode('|', $availableLanguages),
				'position'		=> -1,
				'status'		=> 0
			);

			$this->log(9, 0, 2);
			dibi::query("INSERT INTO [:data:structure]", $data);
			$structure_id = dibi::insertId();

			$nameXML = XML($name);
			if ($nameXML)
				foreach ($nameXML->children() as $url)
					$this->updateSeoString($structure_id, forURL((string) $url->text), (string) $url->language_id);

			$user_id = session('logged_id');

			if ($parent_id > 0)
			{
				// je potreba nastavit prava na uzel stejne jako ma nadrazeny uzel
				$groups = dibi::query("SELECT group_id, level FROM [:as:groups_structure] WHERE structure_id = %i", $parent_id)->fetchAll();
				foreach ($groups as $group)
				{
					$data = array(
						'group_id'		=> element('group_id', $group),
						'structure_id'	=> $structure_id,
						'level'			=> element('level', $group),
					);
					dibi::query("INSERT INTO [:as:groups_structure]", $data);
				}
			}

			success($structure_id);

		} else error('node_limit_violation');
	}

	/**
	 * Smaze uzly (zadava se jako seznam oddeleny svislitky)
	 */
	public function deleteItems()
	{
		$this->checkComponentAccess('StructureAdmin', 'remove_page');
		$ids = explode("|", post('ids'));
		$count = 0;
		foreach ($ids as $id) {
			$this->checkNodeAccess($id, EDIT_ACCESS);
			$count += $this->deleteItem($id);
		}
		success($count);
	}

	/**
	 * Smaze uzel struktury (vcetne referenci do tabulky textu), ale nejdrive i vsechny jeho potomky (rekurzivne)
	 * Volano funkci deleteItems();
	 */
	private function deleteItem($id)
	{
		$count = 0;

		// nejdrive projdu a smazu pripadne vsechny podstranky
		$result = dibi::query('SELECT id FROM [:data:structure] WHERE [parent_id] = %i', $id)->fetchAssoc('id');
		foreach ($result as $key => $row) $count += $this->deleteItem($key);

		if ($id > 0)
		{

			$aff = 0;

			// ma uzivatel pravo na tuto stranku?
			$this->checkNodeAccess($id, EDIT_ACCESS);

			$diff = (array) dibi::query("SELECT * FROM [:data:structure] WHERE id = %i", $id)->fetch();
			$diff = $this->loadTexts($diff, array('name', 'content', 'title', 'h1', 'description', 'keywords', 'copyright'));
			$diff['rights'] = (array) dibi::query("SELECT * FROM [:as:groups_structure] WHERE structure_id = %i", $id)->fetchAll();
			$diff['url_strings'] = (array) dibi::query("SELECT * FROM [:data:url_lookup_table] WHERE structure_id = %i", $id)->fetchAll();

			// nactu a smazu vicejazyccne polozky
			$columns = (array) dibi::query("SELECT name, title, h1, description, keywords, copyright, content FROM [:data:structure] WHERE id = %i", $id)->fetch();
			foreach ($columns as $column)
			{
				if ($column*1 > 0)
				{
					dibi::query("DELETE FROM [:data:texts] WHERE id = %i", $column);
					$aff += dibi::affectedRows();
				}
			}

			// smazu samotny zaznam
			dibi::query("DELETE FROM [:data:structure] WHERE id = %i", $id);
			$aff += dibi::affectedRows();
			$count += $aff;
			if ($aff == 0) error('delete_error');

			// smazu prava na uzly
			dibi::query("DELETE FROM [:as:groups_structure] WHERE structure_id = %i", $id);
			$count += dibi::affectedRows();

			// smazu url strings
			dibi::query("DELETE FROM [:data:url_lookup_table] WHERE structure_id = %i", $id);

			// provedu zaznam do logu
			$this->log(LOG_STRUCTURE_PAGE_DELETE, $this->getComponentId('StructureAdmin'), LOG_WRITE, $diff, element('id', $diff), serialize(element('name', $diff)));

		}

		return $count;
	}

	/**
	 * Prejmenuje jeden uzel struktury
	 */
	public function renameItem()
	{
		$this->checkComponentAccess('StructureAdmin', 'edit_page');
		$id = post('id')*1;
		$data = array('text' => post('name', ''));
		if ($id > 0)
		{
			// todo: logovani + undo
			$text_id = dibi::query("SELECT name FROM [:data:structure] WHERE id = %i", $id)->fetchSingle();
			dibi::query("UPDATE [:data:texts] SET ", $data, "WHERE id = %i", $text_id, " AND language_id = %s", $this->language_id);
			success(dibi::affectedRows());
		}
	}

	/**
	 * Vrati obsah uzlu
	 */
	public function getItemContent()
	{
		$this->checkComponentAccess('SimplePageAdmin');
		$id = post('id', get('id'));
		$this->log(13, 1);
		$result = (array)dibi::query('SELECT id, name, content FROM [:data:structure] WHERE id = %i', $id)->fetch();
		$result = (array)$this->loadTexts($result, array('name', 'content'));
		returnXML($result);
	}

	/**
	 * Ulozi obsah uzlu
	 */
	public function saveItemContent()
	{
		$this->checkComponentAccess('SimplePageAdmin');
		$id = post('id')*1;
		$data = array(
			'name' 		=> $this->saveTexts(post('name', '')),
			'content' 	=> $this->saveTexts(post('content' ,'')),
		);
		if ($id > 0)
		{
			$diff = element(0, dibi::query("SELECT id, name, title, content FROM [:data:structure] WHERE id = %i", $id)->fetchAll());
			$this->log(14, 1, 2, $diff);
			dibi::query("UPDATE [:data:structure] SET ", $data, "WHERE id = %i", $id);
			success(dibi::affectedRows());
		}
	}

	/**
	 * Presouva polozky ve stromu dle zadaneho poradi a parent_id (volano pri drag&drop operacich)
	 * @param	string	ids - seznam idcek, u kterych je potreba provest zmenu
	 * @param	string	parent_id - cilovy rodicovky uzel
	 * @author	Hugo
	 */
	public function moveItems()
	{
		$items = explode('|', post('ids'));
		$parent_id = post('parent_id');
        $parent_id = $parent_id ?: NULL;
		// jedna-li se o korenove uzly, musime overit prava
		if ($parent_id == 0) $this->checkComponentAccess('StructureAdmin', 'add_root');

		$count = 0;
		foreach ($items as $i => $item)
		{
			$this->checkComponentAccess('StructureAdmin', 'move_page');
			$data = array(
				'parent_id' => $parent_id > 0 ? $parent_id : NULL,
				'position' => $i*1
			);
			$diff = element(0, dibi::query("SELECT id, position, parent_id FROM [:data:structure] WHERE id = %i", $item)->fetchAll());
			$this->log(15, 1, 2, $diff);
			dibi::query("UPDATE [:data:structure] SET ", $data, " WHERE id = %i", $item);
			$count += dibi::affectedRows();
		}

		success($count);
	}

	/**
	 * Kopiruje polozky ve stromu dle zadaneho poradi a parent_id (volano pri drag&drop operacich)
	 * @param	string	ids - seznam idcek uzlu, ktere se maji kopirovat
	 * @param	string	parent_id - cilovy rodicovky uzel
	 * @author	Hugo
	 */
	public function copyItems()
	{
		$items = explode('|', post('ids'));
		$parent_id = post('parent_id') ?: NULL;
		// jedna-li se o korenove uzly, musime overit prava
		if ($parent_id == 0) $this->checkComponentAccess('StructureAdmin', 'add_root');

		$count = 0;
		foreach ($items as $i => $item) {
			$this->checkComponentAccess('StructureAdmin', 'add_page');
			$count += $this->copyItem($item, $parent_id);
		}

		success($count);
	}

	/**
	 * Kopiruje polozku vcetne poduzlu. Vyuzivano funkci copyItemItems()
	 * @param	int	id kopirovaneho uzlu
	 * @param	string	parent_id - rodic, ktery se ma nastavit
	 * @param	int		hloubka zanoreni rekurze (kvuli ochrane pred zacyklenim povoluji max. 10 zanoreni)
	 * @param	array	pole jiz vlozenych zaznamu v ramci requestu. v pripade, ze uzivatel zkopiruje uzel
	 * 					sam na sebe nebo do sveho vlastniho potomka, kontrolou proti tomuto poli
	 * 					zamezime dulicitnimu kopirovani v nekonecne smycce.
	 * @author	Hugo
	 */
	private function copyItem($id, $parent_id, $depth = 0, $insertIds = array())
	{
		$count = 0;

		$data = (array) dibi::query(
			"SELECT name, page_type, title, h1, description, keywords, copyright, robots, content, url, status, child_limit, position, languages",
			"FROM [:data:structure] WHERE id = %i", $id
		)->fetch();

		if ($data)
		{
			$this->log(16, 1, 2);
			$data = $this->loadTexts($data, array('name', 'title', 'h1', 'description', 'keywords', 'copyright', 'content'));
			$data['name'] = $this->saveTexts(array2XML($data['name']), true);
			$data['title'] = $this->saveTexts(array2XML($data['title']), true);
			$data['h1'] = $this->saveTexts(array2XML($data['h1']), true);
			$data['description'] = $this->saveTexts(array2XML($data['description']), true);
			$data['keywords'] = $this->saveTexts(array2XML($data['keywords']), true);
			$data['copyright'] = $this->saveTexts(array2XML($data['copyright']), true);
			$data['content'] = $this->saveTexts(array2XML($data['content']), true);
			if ($data['content'] == null) $data['content'] = '';
			$data['parent_id'] = $parent_id;
			dibi::query("INSERT INTO [:data:structure]", $data);
			$insertId = dibi::insertId();
			$insertIds[] = $insertId;
			$count += dibi::affectedRows();

			// je potreba zkopirovat i prava na uzel
			dibi::query(
				"INSERT INTO [:as:groups_structure] (group_id, structure_id, level)",
				"SELECT group_id, %i, level FROM [:as:groups_structure] WHERE structure_id = %i", $insertId, $id
			);

			// provedu update seo retezcu
			$urls = dibi::query('SELECT url_string, language_id FROM [:data:url_lookup_table] WHERE structure_id = %i', $id)->fetchAll();
			if ($urls)
				foreach ($urls as $url)
					if (strlen($url['url_string']) > 0) $this->updateSeoString($insertId, $url['url_string'], $url['language_id']);

			// nyni zkopirujeme poduzly
			if ($depth < 10)
			{
				$data = (array)dibi::query("SELECT id FROM [:data:structure] WHERE parent_id = %i", $id)->fetchPairs();
				foreach ($data as $item)
					if ($item != $insertId && !in_array($item, $insertIds))
						$count += $this->copyItem($item, $insertId, $depth+1, $insertIds);
			}
		} else error('record_not_found');
		return $count;
	}

	/**
	 * Seradi polozky v zadane vetvi dle abecedy (dle aktualniho jazyka)
	 * @param	string	id - uzel, jehoz potomci se maji seradit
	 * @param	string	parent_id - cilovy rodicovky uzel
	 * @author	Hugo
	 */
	public function sortItems()
	{
		$id = ((int) post('id')) ?: NULL;
		$count = 0;

		if ($id > 0)
		{
			$data = dibi::query(
				"SELECT s.id FROM [:data:structure] AS s",
				"LEFT JOIN [:data:texts] AS name ON s.name = name.id AND name.language_id = %s", $this->language_id,
				"WHERE s.parent_id = %s", $id,
				"ORDER BY name.text COLLATE utf8_czech_ci ASC"
			)->fetchPairs();

			for ($i = 0; $i < count($data); $i++)
			{
				dibi::query("UPDATE [:data:structure] SET position = %i WHERE id = %i", $i, $data[$i]);
				$count++;
			}
		}
		success($count);
	}

	/**
	 * Vrati historii uprav pro pripadne undo
	 * @param	string	id - uzel, jehoz potomci se maji seradit
	 * @param	string	parent_id - cilovy rodicovky uzel
	 * @author	Hugo
	 */
	public function getHistory()
	{
		$this->checkComponentAccess('StructureAdmin');
		$data = dibi::query(
			"SELECT id, action, item_name, user_id, timestamp FROM [:as:log] AS log",
			"WHERE action = 10 AND restored = 0 AND component_id = %i ", $this->getComponentId('StructureAdmin'),
			"ORDER BY timestamp DESC"
		)->fetchAll();
		foreach ($data as &$item) {
			$item['item_name'] = unserialize(element('item_name', $item));
		}
		returnXML($data);
	}

	/**
	 * Vrati historii uprav pro pripadne undo
	 * @param	string	id - uzel, jehoz potomci se maji seradit
	 * @param	string	parent_id - cilovy rodicovky uzel
	 * @author	Hugo
	 */
	public function restoreItem()
	{
		$this->checkComponentAccess('StructureAdmin', 'edit_page');
		$id = (int) post('id', get('id'));
		$log = dibi::query("SELECT * FROM [:as:log] AS log WHERE id = %i", $id)->fetch();
		dibi::query("UPDATE [:as:log] SET restored = 1 WHERE id = %i", $id);
		$data = unserialize(element('diff', $log));

		if ($data) {

			// obnovim prava na stranky
			$rights = (array) element('rights', $data);
			unset($data['rights']);
			foreach ($rights as $item) dibi::query("REPLACE INTO [:as:groups_structure]", (array) $item);

			// obnovim url string
			$urls = (array) element('url_strings', $data);
			unset($data['url_strings']);
			foreach ($urls as $item) dibi::query("REPLACE INTO [:data:url_lookup_table]", (array) $item);

			// obnovit vicejazycne texty
			$multi = array('name', 'content', 'title', 'h1', 'description', 'keywords', 'copyright');
			foreach ($multi as $column)
			{
				$text = element($column, $data);
				if ($text === null) $data[$column] = null;
				$data[$column] = (int) element('id', element(0, $text));

				if (is_array($text))
				{
					foreach ($text as $item)
					{
						$textData = Array(
							"id"			=> element('id', $item),
							"language_id"	=> element('language_id', $item),
							"text"			=> element('text', $item)
						);
						dibi::query("REPLACE INTO [:data:texts]", $textData);
					}
				}

			}

			// obnovim samotny zaznam
			dibi::query('INSERT INTO [:data:structure]', $data);

		} else error('server_param_error');

		success('file_restored');
	}

}
