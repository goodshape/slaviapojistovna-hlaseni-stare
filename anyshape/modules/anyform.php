<?php if (!defined('BASEPATH')) die('Access denied!');

class AnyForm extends Core
{

	/*
	* Konstruktor
	* @author	Hugo
	*/
	public function __construct($config)
	{
		parent::__construct($config);
	}

	/**
	 * Nacteni dat formulare
	 * @param	int		componenet_id	id komponenty
	 * @return	XML	vysledek dotazu
	 * @author	Hugo
	 */
	public function load()
	{
		$componentId = (int) post('component_id');

		$masterItemId = post('item_id', '');

		// nacteni konfigurace komponenty
		$componentData = dibi::query("SELECT name, config FROM [:as:components] WHERE id = %i LIMIT 1", $componentId)->fetch();
		$componentName = element('name', $componentData);
		$xmlConfig = XML(element('config', $componentData));

		// kontrola prav
		$this->checkComponentAccess($componentName, 'allow_access', $componentId);

		$xml = new DOMDocument('1.0', 'UTF-8');
		$anyform = $xml->createElement('data');
		$xml->appendChild($anyform);

		// nactu pomocne tridy
		include(dirname(__FILE__) . '/anyform_classes/sources.php');
		include(dirname(__FILE__) . '/anyform_classes/loader.php');

		$sources = new Sources($xmlConfig->sources);

		// prochazim jednotlive panely a nactu pro ne data
		if (count($xmlConfig->panel) > 0)
		{
			foreach ($xmlConfig->panel as $panelConfig)
			{
				// radek tabulky, ktery budu nacitat
				$itemId = strlen($panelConfig['itemId']) > 0 ? (string) $panelConfig['itemId'] : $masterItemId;

				// pokud panel neni zatim nakonfigurovan, vlozime prazdny element
				if ((int) $panelConfig['source'] == 0)
				{
					$anyform->appendChild($xml->createElement('panel'));
					continue;
				}

				// vytvorim instanci tridy loader
				$loader = new Loader($panelConfig['source'], $itemId, $panelConfig['format'], $sources, $xml);

				// sestaveni SQL dotazu
				foreach ($panelConfig->row as $rowConfig)
					foreach ($rowConfig->children() as $itemConfig)
					{
						// nazev sloupce v tabulce
						$column = (string) $itemConfig['column'];

						// reference (false nebo nazev tabulky)
						$referenceSource = (int) $itemConfig['reference'];

						switch ($itemConfig->getName())
						{
							case 'textinput':
							case 'textarea':
							case 'date':
							case 'datetime':
							case 'wysiwyg':
							case 'select':
							case 'autocomplete':
							case 'checkbox':
							case 'set':
							case 'option':
							case 'fileinput':
							case 'structure':
							case 'icon':
							{
								if (empty($column)) continue;
								$loader->addColumn($column, $itemConfig['multiLanguage'] == '1' && $referenceSource == 0);
								break;
							}
						}

						if ($referenceSource > 0) // jedna se o sloupec s referenci?
						{
							switch ($itemConfig->getName())
							{
								// u techto sloupcu se hodi o hodnota z pripojene tabulky
								case 'textinput':
								case 'autocomplete':
								case 'textarea':
								{
									if (empty($column)) continue;
									$loader->addReferenceColumn($column, $referenceSource, (string) $itemConfig['referenceColumn'],
																$itemConfig['multiLanguage'] == '1');
									break;
								}

								case 'multiselect':
								{
									$loader->addMultiReferenceColumn($referenceSource, $sources->getForeignKey1($referenceSource),
																	 $sources->getForeignKey2($referenceSource), $itemConfig['referenceColumn']);
									break;
								}

								case 'multistructure':
								{
									$loader->addMultiReferenceColumn($referenceSource, $sources->getForeignKey1($referenceSource),
																	 $sources->getForeignKey2($referenceSource), 'id');
									break;
								}
							}
						}

					}

				// nastavim jeste nameColumn
				if (strlen($panelConfig['nameColumn']) > 0) $loader->addNameColumn($panelConfig['nameColumn'], $panelConfig['nameColumnMultiLanguage'] == '1');

				// nactu data pro panel z databaze a pridam je do vysledeho XML
				$anyform->appendChild($loader->getData());

				// nastavim pripadne nazev panelu dle vraceneho nameColumn
				if (strlen($loader->name) > 0)
					if ($panelConfig['nameColumnMultiLanguage'] == "1") $anyform->setAttribute('name', translate($loader->name, $this->language_id));
					else $anyform->setAttribute('name', $loader->name);

			}
		}

		header('Content-type: text/xml');
		echo $xml->saveXML();

	}

	/**
	 * Provede ulozeni dat z formulare
	 * @param	int		componenet_id	id komponenty
	 * @param	XML		data			data k ulozeni jako XML
	 * @return	XML	vysledek dotazu
	 * @author	Hugo
	 */
	public function save()
	{

		$componentId = (int) post('component_id');
		$structureId = (int) post('structure_id', 0);

		// nacteni konfigurace komponenty
		$componentData = dibi::query("SELECT name, config FROM [:as:components] WHERE id = %i LIMIT 1", $componentId)->fetch();
		$componentName = element('name', $componentData);
		$xmlConfig = XML(element('config', $componentData));

		// kontrola prav
		$this->checkComponentAccess($componentName, 'allow_access', $componentId);

		$data = XML(post('data'));

		// nactu pomocne tridy
		include(dirname(__FILE__) . '/anyform_classes/sources.php');
		include(dirname(__FILE__) . '/anyform_classes/updater.php');

		// vytvorim instanci zdroju
		$sources = new Sources($xmlConfig->sources);

		// id hlavniho zaznamu
		$masterItemId = post('item_id', '');

		// ukladani probiha ve dvou pruchodech
		// v prvnim pruchodu jsou ulozeny hlavni zaznamy, abych ziskal jejich id
		// v druhem pruchodu jsou ulozeny zavisle zaznamy, ktere bez id nadrazeneho zaznamu nejdou ulozit
		for ($pass = 0; $pass < 2; $pass++)
		{
			$panelIndex = 0;

			// prochazim jednotlive panely z configu, abych opravdu zapsal jen to, co mam
			foreach ($xmlConfig->panel as $panelConfig)
			{
				// cislo zdroje aktalniho panelu
				$sourceId = (int) $panelConfig['source'];

				// v prvnim pruchodu ukladam bezne tabulky, ve druhem zavisle tabulky
				if ($pass == 0 && !$sources->isJoin($sourceId) || $pass == 1 && $sources->isJoin($sourceId))
				{
					$updater = new Updater($sourceId, $sources, $masterItemId, $structureId, $data->panel[$panelIndex], $panelConfig);
					$masterItemId = $updater->save();
				}

				$panelIndex++;

			}
		}

		success($masterItemId);
	}

	/**
	 * Nacteni dat z referencni tabulky
	 * @param	int		component_id	id komponenty
	 * @param	int		panel			poradove cislo panelu, pro ktery jsou data urcena
	 * @param	int		column			sloupec v tabulce, pro ktery jsou data urcena
	 * @return	XML	vysledek dotazu
	 * @author	Hugo
	 */
	public function loadReferenceData()
	{
		$componentId = (int) post('component_id');
		$sourceId = (int) post('source_id');
		$nameColumn = (string) post('name_column');
		$infoColumn = (string) post('info_column');
		$iconColumn = (string) post('icon_column');
		$parentKey = (string) post('parent_key');
		$multiLanguage = (int) post('multilanguage');
		$suggest = (string) post('suggest');

		if ($sourceId == 0 || strlen($nameColumn) == 0) error('server_param_error');

		// nacteni konfigurace komponenty
		$componentData = dibi::query("SELECT name, config FROM [:as:components] WHERE id = %i LIMIT 1", $componentId)->fetch();
		$componentName = element('name', $componentData);
		$xmlConfig = XML(element('config', $componentData));

		// kontrola prav
		$this->checkComponentAccess($componentName, 'allow_access', $componentId);

		$data = array();

		include(dirname(__FILE__) . '/anyform_classes/sources.php');
		$sources = new Sources($xmlConfig->sources);
		$table = false;

		// prochazim jednotlive panely a hledam zadany sloupec
		foreach ($xmlConfig->panel as $panelConfig)
			foreach ($panelConfig->row as $rowConfig)
				foreach ($rowConfig->children() as $itemConfig)
				{
					// kontroluji, jestli pozadava data jsou v ramci konfigurace panelu
					if ($sourceId == (int) $itemConfig['reference'] &&
						$nameColumn == (string) $itemConfig['referenceColumn'] &&
						$infoColumn == (string) $itemConfig['referenceInfoColumn'] &&
						$iconColumn == (string) $itemConfig['referenceIconColumn'] &&
						$parentKey == (string) $itemConfig['parentKey'])
					{
						$table = dibi::getConnection()->translate(tableName($sources->getTableName($sourceId)));
						$primaryKey = $sources->getPrimaryKey($sourceId);
					}
				}

		if ($table)
		{
			if (!$multiLanguage)
			{
				$data = (array) dibi::query(
					"SELECT DISTINCT [table].%n AS [value],", $primaryKey,
						"[table].%n as [name]", $nameColumn,
						"%if", strlen($infoColumn) > 0,
							", [table].%n as [add]", $infoColumn,
						"%end",
						"%if", strlen($iconColumn) > 0,
							", [table].%n as [icon]", $iconColumn,
						"%end",
						"%if", strlen($parentKey) > 0,
							", [table].%n as [parent]", $parentKey,
						"%end",
					"FROM %n AS [table]", $table,
					"WHERE 1",
						"%if", strlen($suggest) > 0,
							"AND [table].%n LIKE %like~", $nameColumn, $suggest,
						"%end",
					"ORDER BY [table].[$nameColumn] ASC"
				)->fetchAll();
			}
			else
			{
				// pro vicejazycne reference je SQL dotaz mirne slozitejsi
				$data = (array) dibi::query(
					"SELECT DISTINCT [table].%n AS [value],", $primaryKey,
						"[text].[text] as [name]",
						"%if", strlen($infoColumn) > 0,
							", [table].%n as [add]", $infoColumn,
						"%end",
						"%if", strlen($iconColumn) > 0,
							", [table].%n as [icon]", $iconColumn,
						"%end",
						"%if", strlen($parentKey) > 0,
							", [table].%n as [parent]", $parentKey,
						"%end",
					"FROM %n AS [table]", $table,
					"LEFT JOIN [:data:texts] AS [text] ON [text].[id] = [table].%n AND [text].[language_id] = %s", $nameColumn, $this->language_id,
					"WHERE 1",
						"%if", strlen($suggest) > 0,
							"AND [text].[text] LIKE %like~", $suggest,
						"%end",
					"ORDER BY [table].[$nameColumn] ASC"
				)->fetchAll();
			}

		}

		returnXML($data);

	}

	/**
	 * Vrati seznam datovych tabulek
	 */
	public function getTables()
	{
		$this->checkLogin();

		$result = array();

		$tables = dibi::query("SHOW TABLES LIKE %s", $this->config['database']['data_prefix'] . '%')->fetchPairs();
		foreach ($tables as $table) $result[] = str_replace($this->config['database']['data_prefix'], '', $table);

		$tables = dibi::query("SHOW TABLES LIKE %s", $this->config['database']['prefix'] . '%')->fetchPairs();
		foreach ($tables as $table) $result[] = str_replace($this->config['database']['prefix'], '%', $table);

		returnXML($result);
	}

	/**
	 * Vrati seznam editacnich komponent
	 */
	public function getAnyFormEditComponents()
	{
		$this->checkLogin();
		$result = dibi::query('SELECT id, name, alias, icon32x32 FROM [:as:components] WHERE type = 2')->fetchAll();
		returnXML($result);
	}

	/**
	 * Vrati seznam sloupcu vsech tabulek
	 */
	public function getAllColumns()
	{
		$this->checkLogin();
		$result = array();

		$tables = dibi::query("SHOW TABLES LIKE %s", $this->config['database']['data_prefix'] . '%')->fetchPairs();
		foreach ($tables as $table) {
			$tableName = str_replace($this->config['database']['data_prefix'], '', $table);
			$columns = dibi::query("SHOW COLUMNS FROM [$table]")->fetchAll();
			$result[$tableName] = $columns;
		}

		$tables = dibi::query("SHOW TABLES LIKE %s", $this->config['database']['prefix'] . '%')->fetchPairs();
		foreach ($tables as $table) {
			$tableName = str_replace($this->config['database']['prefix'], '%', $table);
			$columns = dibi::query("SHOW COLUMNS FROM [$table]")->fetchAll();
			$result[$tableName] = $columns;
		}

		returnXML($result);
	}

}
