<?php if (!defined('BASEPATH')) die('Access denied!');

/**
* Sprava uzivatelskych uctu a skupin
*/
class File_System extends Core
{
	var $serverCP;

	/**
	* Konstruktor
	*/
	public function __construct($config)
	{
		$this->serverCP = $config['site']['filesystem_charset'];
		parent::__construct($config);
	}

	/**
	 * Funkce provede synchronizaci tabulky souboru z realnym file systemem.
	 * Vrati pocet pridanych/smazanych zaznamu
	 */
	public function synchronize()
	{
		$this->checkComponentAccess('FileManager');
		$fs = array();
		$this->synchronizeHelper(directoryMap($this->config['site']['files_path']), $fs);
		$db = (array) dibi::query("SELECT CONCAT(path, '/', filename) AS path FROM [:data:filesystem]")->fetchPairs();
		$add = array_diff($fs, $db);
		$del = array_diff($db, $fs);
		foreach ($add as $item) $this->updateFile(basename($item), trim(dirname($item), '/'), basename($item));
		foreach ($del as $item) $this->updateFile(basename($item), trim(dirname($item), '/'));
		success(count($add) . '/' . count($del));
	}

	/**
	 * Pomocna rekurzivni funkce k funkci synchronize().
	 * Narovna strom souboru do pole
	 */
	private function synchronizeHelper($tree, &$array, $path = '')
	{
		foreach ($tree as $dir => $file) {
			if (is_array($file)) {
					$array[] = "$path/$dir";
					$this->synchronizeHelper($file, $array, $path == '' ? $dir : "$path/$dir");
			}
			else $array[] = "$path/$file";
		}
	}

	/**
	 * Funkce vlozi, smaze nebo provede update souboru v databazi, podle toho zda-li
	 * existuje ve file systemu
	 *
	 * @param	string	Nazev souboru ve file systemu
	 * @param	string	Cesta k souboru ve file systemu (bez nazvu souboru)
	 * @param	string	Puvodni nazev souboru
	 * @param	string	Popis
	 * @param	int		Vlastnik (reference do anyshape_users)
	 * @param	bool	Soukroma slozka
	 * @param	int		Pocet stazeni souboru
	 *
	 */
	private function updateFile($filename, $path, $name = null, $description = null, $tags = null, $owner = null, $private = null, $num_downloads = null)
	{
		if ($path == '.') $path = '';
		$path = trim($path, '/');
		$fullpath = $this->config['site']['files_path'] . $path . (strlen($path) > 0 ? '/' . $filename : $filename);

		if (file_exists($fullpath))
		{
			// soubor existuje - ulozime do databaze
			$data = array(
				'filename'		=> $filename,
				'path'			=> $path,
				'type'			=> (int) is_dir($fullpath),
				'time%t'		=> @filemtime($fullpath),
				'size'			=> @filesize($fullpath)
			);
			if ($name !== null) $data['name'] = $name;
			if ($description !== null) $data['description'] = $description;
			if ($tags !== null) $data['tags'] = $tags;
			if ($owner !== null) $data['owner'] = $owner;
			if ($private !== null) $data['private'] = $private;
			if ($num_downloads !== null) $data['num_downloads'] = $num_downloads;
			dibi::query('INSERT INTO [:data:filesystem]', $data, "ON DUPLICATE KEY UPDATE %a", $data);
		}
		else
		{
			// soubor neexistuje - smazeme z databaze
			dibi::query('DELETE FROM [:data:filesystem] WHERE filename = %s AND path = %s', $filename, $path);

		}

	}

	/**
	 * Funkce provede update informaci o souboru/slozce v databazi.
	 */
	public function updateFileProperties()
	{
		$this->checkComponentAccess('FileManager', 'modify_file');

		$filename = post('filename', '');
		$path = post('path', '');
		if ($path == '.') $path = '';
		$path = trim($path, '/');
		$owner = dibi::query("SELECT owner FROM [:data:filesystem] WHERE filename = %s AND path = %s", $filename, $path)->fetchSingle();

		if ($owner != session('logged_id')) $this->checkComponentAccess('FileManager', 'modify_other_users_file');

		if (strlen($filename) > 0)
			$this->updateFile($filename, $path, post('name'), post('description', null), post('tags', null), post('owner', null));
		else
			error('file_not_found');

		success(1);
	}

	/**
	 * Funkce provede update watermarku pro zadanou slozku
	 */
	public function updateWatermark()
	{
		$this->checkComponentAccess('FileManager');

		$filename = post('filename');
		$path = post('path');
		if ($path == '.') $path = '';
		$path = trim($path, '/');

		$owner = dibi::query("SELECT owner FROM [:data:filesystem] WHERE filename = %s AND path = %s", $filename, $path)->fetchSingle();

		if ($owner != session('logged_id')) $this->checkComponentAccess('FileManager', 'modify_other_users_file');

		$newWatermarkId = (int) post('watermark');
		if (strlen($filename) > 0)
		{
			// nejdrive si zjistim puvodni id watermarku
			$oldWatermarkId = dibi::query("SELECT [watermark] FROM [:data:filesystem] WHERE filename = %s AND path = %s AND type = 1", $filename, $path)->fetchSingle();
			if ($oldWatermarkId <> $newWatermarkId)
			{
				// pokud ma dojit ke zmene, provedeme update
				dibi::query('UPDATE [:data:filesystem] SET watermark = %i WHERE filename = %s AND path = %s AND type = 1', $newWatermarkId, $filename, $path);

				$this->removeCached(post('path') . $filename);

				// vygenerujeme novy .htaccess, ktery hlida pristup k souboru s watermarkem
				$dirs = dibi::query("SELECT filename, path, watermark FROM [:data:filesystem] WHERE type = 1")->fetchAll();
				$htaccess1 =
					"# tento soubor je generovan automaticky.\n# provedene upravy budou prepsany!\n" .
					"RewriteEngine on\n\n" .
					//"RewriteBase /\n\n" .
					"# slozky, ktere jsou oznacene pro aplikaci watermarku\n" .
					"RewriteCond %{REQUEST_FILENAME} -f\n";
				$htaccess2 =
					"# tento soubor je generovan automaticky.\n# provedene upravy budou prepsany!\n" .
					"RewriteEngine on\n\n" .
					//"RewriteBase /\n\n" .
					"# slozky, ktere jsou oznacene pro aplikaci watermarku\n" .
					"RewriteCond %{REQUEST_FILENAME} !-f\n";
				foreach ($dirs as $dir)
				{
					if (element('watermark', $dir) > 0)
					{
						$dirname = element('path', $dir) . (strlen(element('path', $dir)) > 0 ? '/' . element('filename', $dir) : element('filename', $dir));
						$htaccess1 .= "RewriteRule ^(" . $dirname . "/[^/]*?\.(jpg|jpeg|png|gif|bmp))$ ../data/watermarked/files/$1?watermark_id=$newWatermarkId [L]\n";
						$htaccess2 .= "RewriteRule ^(files/" . $dirname . "/[^/]*?\.(jpg|jpeg|png|gif|bmp))$ ../../admin.php?q=/images/resize/$1&watermark_id=$newWatermarkId [L]\n";
					}
				}
				$htaccess2 .=
					"\n# ostatni obrazky budou automaticky zmenseny bez watermarku\n".
					"RewriteCond %{REQUEST_FILENAME} !-f\n" .
					"RewriteRule ^(.*) ../../admin.php?q=/images/resize/$1 [L]";
				file_put_contents($this->config['site']['files_path'] . '.htaccess', $htaccess1);
				file_put_contents($this->config['site']['resized_path'] . '.htaccess', $htaccess2);

			}
		}
		else error('file_not_found');
		success(1);
	}

	/**
	* Vrati obsah zadaneho adresare jako XML
	* @param	string	Cesta k adresari jako string
	* @return	XML
	* @author	Hugo
	*/
	public function getDirectoryContent()
	{
		$this->checkComponentAccess('FileManager');
		$path = trim(post('path', ''), '/');
		header('Content-type: text/xml');
		echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		echo "<files>";

		// vyhledavani
		$strings = post('search', '');
		if (strlen($strings) > 0)
		{
			$search = '';
			$strings = preg_replace('/\s/', ' ', $strings);
			$strings = explode(' ', $strings);
			foreach ($strings as $str) {
				if (strlen($str) > 0) $search .= "AND (f.filename LIKE '%$str%' OR f.name LIKE '%$str%' OR f.description LIKE '%$str%' OR f.tags LIKE '%$str%')";
			}

			$files = dibi::query(
				"SELECT f.name, f.path, f.filename, f.description, f.tags, f.owner, u.name AS owner_name, f.time, f.size FROM [:data:filesystem] f",
				"LEFT JOIN (SELECT id, name FROM [:as:users] UNION SELECT 0, 'root') AS u ON u.id = f.owner",
				"WHERE type = 0 $search ORDER BY f.name ASC", $path
			)->fetchAll();
		}
		else
		{
			$files = dibi::query(
				"SELECT f.name, f.path, f.filename, f.description, f.tags, f.owner, u.name AS owner_name, f.time, f.size FROM [:data:filesystem] f",
				"LEFT JOIN (SELECT id, name FROM [:as:users] UNION SELECT 0, 'root') AS u ON u.id = f.owner",
				"WHERE type = 0 AND path = %s ORDER BY f.name ASC", $path
			)->fetchAll();
		}

		foreach ($files as $file)
		{
			printf('<file name="%s" fullpath="%s" description="%s" tags="%s" owner_id="%s" owner="%s" time="%s" size="%s"/>',
				htmlspecialchars(element('name', $file)),
				htmlspecialchars(strlen(element('path', $file)) > 0 ? element('path', $file) . '/' . element('filename', $file) : element('filename', $file)),
				htmlspecialchars(element('description', $file)),
				htmlspecialchars(element('tags', $file)),
				htmlspecialchars(element('owner', $file)),
				htmlspecialchars(element('owner_name', $file)),
				htmlspecialchars(element('time', $file)),
				htmlspecialchars(element('size', $file))
			);
		}
		echo "</files>";
	}

	/**
	 * Vrati obsah zadaneho adresare jako XML
	 * @param	string	Cesta k adresari jako string
	 * @return	XML
	 * @author	Hugo
	 */
	public function getDirectoryTree()
	{
		$this->checkComponentAccess('FileManager');
		$t = microtime(TRUE);

		$dirs = dibi::query(
			"SELECT name, description, owner, watermark, time, REPLACE(IF(CHAR_LENGTH(path) > 0, CONCAT(path, '/', filename), filename), '/', '#') AS fullpath",
			"FROM [:data:filesystem] WHERE type = 1 ORDER BY REPLACE(IF(CHAR_LENGTH(path) > 0, CONCAT(path, '/', name), name), '/', '#') ASC"
		)->fetchAll();

		header('Content-type: text/xml');
		echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
		echo '<dir name="root" fullpath="">';

		$prevDepth = -1;

		foreach ($dirs as $dir)
		{
			$depth = substr_count(element('fullpath', $dir), '#');

			$diff = $prevDepth - $depth + 1;
			if ($diff > 0) echo str_repeat("</dir>", $diff);

			echo "\n";

			printf('<dir name="%s" fullpath="%s" description="%s" owner="%s" watermark="%d" time="%s">',
				   htmlspecialchars(element('name', $dir)),
				   htmlspecialchars(str_replace('#', '/', element('fullpath', $dir))),
				   htmlspecialchars(element('description', $dir)),
				   htmlspecialchars(element('owner', $dir)),
				   htmlspecialchars(element('watermark', $dir)),
				   htmlspecialchars(element('time', $dir))
			);

			$prevDepth = $depth;

		}

		echo str_repeat("</dir>", $depth + 1);

		if (count($dirs) > 0) echo "</dir>";
		echo '<!-- ' . (microtime(TRUE) - $t) . '-->';
	}

	/**
	 * Vytvoreni nove slozky v souborovem systemu AnyShapu
	 * @param	string	path - cesta k souboru/slozce
	 * @return	XML	success nebo error
	 * @author	Hugo
	 */
	public function makeDir()
	{
		$this->checkComponentAccess('FileManager', 'create_folder');
		$path = trim(post('path'), '/');
		$name = post('name', 'New folder');
		$filename = forURL($name);
		$fullpath = $this->config['site']['files_dir'] . $path . '/' . $filename;

		if (strlen($filename) == 0 || file_exists($this->config->wwwDir.'/'.$fullpath)) error('access_denied');

		if (isSafe($fullpath) && mkPath($fullpath, $this->config->wwwDir))
		{
			$this->updateFile($filename, $path, $name, '', '', session('logged_id'));
			success(1);
		}
		else
		{
			error('access_denied');
		}
	}

	/**
	 * Presune soubor nebo prazdnou slozku do kose
	 * @param	string	path - cesta k souboru/slozce
	 * @return	XML	success nebo error
	 * @author	Hugo
	 */
	public function deleteFiles()
	{
		$this->checkLogin();
		$files = boom(post('filenames'));
		$count = 0;
		foreach ($files as $file)
		{
			$fullpath = $this->config['site']['files_path'] . $file;
			$path = dirname($file);
			if ($path == '.') $path = '';
			if (isSafe(realpath($fullpath)))
			{
				if (is_file($fullpath))
				{
					$this->checkComponentAccess('FileManager', 'delete_file');
					// v kosi by mela byt zachovana stejna adresarova struktura
					if (!@mkPath($this->config['site']['recycle_bin_dir'].'/'.$path)) error('access_denied');
					// presun souboru do kose
					if (rename($fullpath, $this->config['site']['recycle_bin_path'].'/'.$file)) $count++;
					else error('access_denied');
					@touch($this->config['site']['recycle_bin_dir'].$file);
					$this->removeCached($file);
				}
				else {
					$this->checkComponentAccess('FileManager', 'delete_folder');
					// vytvoreni slozky v kosi
					if (!@mkPath($this->config['site']['recycle_bin_dir'].$file)) error('access_denied');
					// smazani puvodni slozky
					if (@rmdir($fullpath)) $count++;
					else error('access_denied');
				}
			} elseif(realpath($fullpath) === FALSE) {
                //only delete file from db - does not exists
            } else {
                error('access_denied');
            }
			// update databaze
			$this->updateFile(basename($file), $path);
		}
		success($count);
	}

	/**
	 * Smaze dany soubor z cache (tj. s adresare data/resized/ a data/watermark/)
	 * @param	string	Soubor s cestou relativne k adresari /files/.
	 * 					Pokud je zadan adresar, jsou odstraneny vsechny soubory daneho adresare (bez podadresaru)
	 * @author Hugo
	 */
	protected function removeCached($filename)
	{
		if (strlen($filename) == 0) return;
		if (is_dir($this->config['site']['files_path'] . $filename))
		{
			$files = directoryMap($this->config->wwwDir.'/'.$this->config['site']['files_dir'] . $filename, false);
			$path = trim($filename, "/") . "/";
			foreach ($files as $file) $this->removeCached($path . $file);
		}
		else
		{
			$path = dirname($filename) . "/";
			$filename = basename($filename);
			$files = directoryMap($this->config['site']['resized_path'] . $this->config['site']['files_dir'] . $path, false);
			if (is_array($files)) {
				foreach ($files as $file) {
					if ($filename == preg_replace('/^\d+x\d+-\w\w\w\w-/', '', $file))
						if (strlen($this->config['site']['resized_path']) > 0) @unlink($this->config['site']['resized_path'] . $this->config['site']['files_dir'] . $path . $file);
						else error('no_resized_dir_variable');
				}
			}
			if (file_exists($this->config['site']['watermark_path'] . $this->config['site']['files_dir'] . $path . $filename))
				if (strlen($this->config['site']['watermark_path']) > 0) @unlink($this->config['site']['watermark_path'] . $this->config['site']['files_dir'] . $path . $filename);
				else error('no_watermark_dir_variable');
		}
	}

	/**
	 * Resi upload souboru. Soubor je nahran do slozky specifikovane v promenne path
	 * @param	string	path - cesta (relativne ke koreni aplikace)
	 * @param	file	file - soubor
	 * @param	int		overwrite = 1 - pripadny existujici soubor je prepsan, jinak vyhodi error
	 * @return	XML	success (s nazvem vytvoreneho souboru) nebo error
	 * @author	Hugo
	 */
	public function uploadFile()
	{
		$this->checkComponentAccess('FileManager', 'upload_file');

		if (element('action', $_REQUEST) == 'upload')
		{
			// nachystam promenne
			$dir = str_replace($this->config['site']['files_dir'], '', element('path', $_REQUEST));
			$path = $this->config['site']['files_dir'] . $dir;
			if (strlen($dir) > 0) $path .= '/';
			$name = element('name', $_FILES['file']);
			$filename = forURL($name);
			$filetemp = element('tmp_name', $_FILES['file']);

			if (!file_exists($path.$filename) || (post('overwrite', 0)*1 == 1))
			{
				if (!file_exists($path)) @mkPath($dir);

				if (isSafe($path.$filename))
				{
					// premistim docasny soubor
					$filestatus = @move_uploaded_file($filetemp, $path.$filename);
					// prava
					@chmod($path.$filename, 0777);
					// kontrola uspechu
					if (file_exists($path.$filename)) {
						// update databaze
						$this->updateFile($filename, $dir, $name, '', '', session('logged_id'));
						success($filename);
					}
					error('upload_io_error');
				} else error('access_denied');

			} else error('file_already_on_server');
		}
		error('unknown_error');
	}

	/**
	 * Funkce vrati kompletni obsah kose (smazane soubory) jako seznam.
	 */
	public function getRecycleBin()
	{
		$this->checkComponentAccess('FileManager', 'access_recycle_bin');
		$list = $this->getRecycleBinHelper();
		usort($list, array('file_system', 'dateSort'));
		returnXML($list);
	}

	/**
	 * Pomocna funkce pro setrideni pole se soubory podle casu
	 */
	public function dateSort($a, $b)
	{
		$a = element('date', $a);
		$b = element('date', $b);
		if ($a == $b) return 0;
		return ($a < $b) ? -1 : 1;
	}

	/**
	 *	Pomocna rekurzivni funkce pro vypis souboru v kosi
	 */
	private function getRecycleBinHelper($dir = '')
	{
		$fulldir = $this->config['site']['recycle_bin_path'] . $dir;
		if ($fp = @opendir($fulldir))
		{
			$result = array();
			while (FALSE !== ($file = readdir($fp)))
			{
				if (@is_dir($fulldir.$file) && substr($file, 0, 1) != '.')
				{
					$temp_array = $this->getRecycleBinHelper($dir.$file."/");
					if (count($temp_array) > 0) $result = array_merge($result, $temp_array);
				}
				elseif (substr($file, 0, 1) != ".")
				{
					$result[] = array(
						'filename'	=> $dir . $file,
						'file'		=> $file,
						'dir'		=> $dir,
						'size' 		=> filesize($fulldir . $file),
						'date'		=> date("Y-m-d H:i:s", filemtime($fulldir . $file))
					);
				}
			}
			return $result;
		} return false;
	}

	/**
	 * Obnovi soubory z kose.
	 * @param string files - seznam souboru (oddelenych svilistky), ktere maji byt obnoveny
	 * @return succes s poctem obnovenych a neobnovenych souboru (ve formatu X/Y)
	 */
	public function restoreFiles()
	{
		$this->checkComponentAccess('FileManager', 'restore_files');
		$files = boom(post('files'));
		$good = 0;
		$bad = 0;
		foreach ($files as $file) {
			$fullpath = $this->config['site']['files_path'].$file;
			$path = dirname($file);
			// mela byt zachovana stejna adresarova struktura
			if (!@mkPath($this->config['site']['files_dir'].$path)) error('access_denied');
			// presun souboru z kose
			if (@rename($this->config['site']['recycle_bin_path'].$file, $fullpath)) $good++;
			else $bad++;
		}
		$this->synchronize();
		success("$good/$bad");
	}

	/**
	 * Nenavratne vymaze vsechny soubory z kose.
	 * @return succes nebo error
	 */
	public function emptyRecycleBin()
	{
		$this->checkComponentAccess('FileManager', 'empty_recycle_bin');
		$this->emptyRecycleBinHelper($this->config['site']['recycle_bin_path']);
		success(1);
	}

	/**
	 * Pomocna rekurzivni funkce k funci emptyRecyclebin().
	 */
	private function emptyRecycleBinHelper($dir)
	{
		if ($fp = @opendir($dir)) {
			while (FALSE !== ($file = readdir($fp))) {
				if (@is_dir($dir . $file) && substr($file, 0, 1) != '.') {
						$this->emptyRecycleBinHelper($dir . $file . "/");
						@rmdir($dir . $file);
				}
				elseif (substr($file, 0, 1) != ".") @unlink($dir . $file);
			}
		}
	}

	/**
	 * Metoda nacte vsechny watermarky v systemu
	 */
	public function loadWatermarks()
	{
		$this->checkComponentAccess('FileManager', 'setup_watermarks');
		$result = dibi::query("SELECT * FROM [:as:watermarks]")->fetchAll();
		returnXML($result);
	}

	/**
	 * Metoda ulozi zadane XML jako watermarky v systemu
	 */
	public function saveWatermarks()
	{
		$this->checkComponentAccess('FileManager', 'setup_watermarks');

		$data = XML(post('data'));

		$ids = array();

		foreach ($data->node as $item)
		{
			$id = (int) $item->id;
			$updateData = array(
				'name'					=> (string) $item->name,
				'image'					=> (string) $item->image,
				'position'				=> (int) $item->position,
				'size'					=> (int) $item->size,
				'horizontal_padding'	=> (int) $item->horizontal_padding,
				'vertical_padding' 		=> (int) $item->vertical_padding
			);

			if ($id > 0)
			{
				dibi::query("UPDATE [:as:watermarks] SET", $updateData, "WHERE id = %i", $id);
			}
			else
			{
				dibi::query("INSERT INTO [:as:watermarks]", $updateData);
				$id = dibi::insertId();
			}

			$ids[] = $id;

		}

		if (count($ids) > 0) dibi::query("DELETE FROM [:as:watermarks] WHERE id NOT IN %in", $ids);

		// smazeme z cache vytvorene watermarky
		$watermarked = dibi::query("SELECT filename, path FROM [:data:filesystem] WHERE type = 1 AND watermark > 0")->fetchAll();

		foreach ($watermarked as $item)
		{
			$this->removeCached(element('path', $item) == '' ? element('filename', $item) : element('path', $item) . '/' . element('filename', $item));
		}

		success();
	}

}
