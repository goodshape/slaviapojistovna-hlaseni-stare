<?php

/**
 * Trida pro nacteni dat
 */
class Loader {

	// xml
	public $xml;

	// zdroje
	public $sources;

	// id hlavniho zdroje
	public $sourceId;

	// nazev hlavni tabulky
	public $table;

	// nazev pripojene tabulky
	public $reference;

	// primarni klic
	public $primaryKey;

	// propojovaci tabulka pro vazby M:N
	public $junction;

	// nazev ciziho klice v pripojene tabulce
	public $foreignKey;

	// nazev vlastniho klice v propojovaci tabulce
	public $foreignKey1;

	// nazev ciziho klic v propojovaci tabulce
	public $foreignKey2;

	// nazev nadrazene tabulky (ma smysl je pro zavisle tabulky)
	public $parentTable;

	// nacitany radek tabulky (tj. hodnota primarniho klice)
	public $itemId;

	// format dat
	public $format;

	// sloupce, podle kterych se radi zaznamy ze zavisle tabulky
	public $sorting;

	// nazev zaznamu (dle nameColumn)
	public $name;

	// seznam sloupcu, ktere budou nacitany
	protected $columns = array();

	// seznam pripojenych tabulek
	protected $joins = array();

	// seznam separatnich selectu pro multireference
	protected $multiReferences = array();

	// seznam sloupcu, ktere jsou vicejazycne
	protected $multiLanguageColumns = array();

	/**
	 * Konstruktor
	 * @author	Hugo
	 */
	public function __construct($sourceId, $itemId, $format, $sources, $xml)
	{
		// sdilene datove struktury
		$this->sources = $sources;
		$this->xml = $xml;

		// zakladni udaje
		$this->sourceId = (int) $sourceId;
		$this->itemId = $itemId;
		$this->format = $format;
		$this->table = dibi::getConnection()->translate(tableName($this->sources->getTableName($sourceId)));
		$this->primaryKey = $this->sources->getPrimaryKey($sourceId, 'id');

		$parentId = $this->sources->getParentId($this->sourceId);
		if ($parentId > 0) {
			$this->parentTable = dibi::getConnection()->translate(tableName($this->sources->getTableName($parentId)));
			$this->parentPrimaryKey = $this->sources->getPrimaryKey($parentId);
		}

		// pro pripojene tabulky jsou k dispozici jeste dalsi atributy
		if ($this->sources->is1NJoin($this->sourceId)) {
			$this->reference = dibi::getConnection()->translate(tableName($this->sources->getTableName($this->sourceId)));
			$this->foreignKey = $this->sources->getForeingKey($this->sourceId);
		}

		// pro zavisle tabulky jsou k dispozici jeste dalsi atributy
		if ($this->sources->isMNJoin($this->sourceId)) {
			$this->junction = dibi::getConnection()->translate(tableName($this->sources->getJunction($this->sourceId)));
			$this->foreignKey1 = $this->sources->getForeignKey1($this->sourceId);
			$this->foreignKey2 = $this->sources->getForeignKey2($this->sourceId);
		}

		// razeni
		if ($this->sources->isJoin($this->sourceId)) {
			$this->sorting = $this->sources->getSorting($this->sourceId);
		}
	}

	/**
	 * Pridani bezneho sloupce
	 */
	public function addColumn($columnName, $multiLanguage = false)
	{
		// nazev sloupce vcetne tabulky
		$fullColumnName = "src" . $this->sourceId . "_" . $columnName;

		$this->columns[] = "[" . $this->table . "].[" . $columnName . "] AS [" . $fullColumnName . "]";

		if ($multiLanguage) $this->multiLanguageColumns[$fullColumnName] = $fullColumnName;
	}

	/**
	 * Pridani referencniho sloupce
	 */
	public function addReferenceColumn($columnName, $referenceSource, $referenceColumn, $multiLanguage = false)
	{
		// referencni sloupce
		$referenceTable = dibi::getConnection()->translate(tableName($this->sources->getTableName($referenceSource)));
		$referenceKey = $this->sources->getPrimaryKey($referenceSource, 'id');
		$referenceFullColumnName = "src" . $referenceSource . "_" . $referenceColumn;
		$this->columns[] = "[" . $referenceTable . "].[" . $referenceColumn . "] AS [" . $referenceFullColumnName . "]";
		if (!isset($this->joins[$referenceTable]))
			$this->joins[$referenceTable] = "LEFT JOIN [" . $referenceTable . "] ON [" . $referenceTable . "].[" . $referenceKey . "] = [" . $this->table . "].[" . $columnName . "]";

		if ($multiLanguage) $this->multiLanguageColumns[$referenceFullColumnName] = $referenceFullColumnName;
	}

	/**
	 * Pridani reference M:N
	 */
	public function addMultiReferenceColumn($referenceSource, $foreignKey1, $foreignKey2, $referenceColumn)
	{
		$this->multiReferences[] = array(
			'name'		=> "src" . $referenceSource . "_" . $referenceColumn,
			'junction'	=> dibi::getConnection()->translate(tableName($this->sources->getJunction($referenceSource))),
			'foreign1'	=> $foreignKey1,
			'foreign2'	=> $foreignKey2
		);
	}

	/**
	 * Pridani sloupce, ktery reprezentuje nazev zaznamu
	 */
	public function addNameColumn($columnName, $multiLanguage = false)
	{
		$this->columns[] = "[" . $this->table . "].[" . $columnName . "] AS [anyFormPanelNameColumn]";
		if ($multiLanguage) $this->multiLanguageColumns['anyFormPanelNameColumn'] = 'anyFormPanelNameColumn';
	}

	/**
	 * Slozi SQL dotaz a vrati data jako XML
	 */
	public function getData()
	{

		$data = array();

		$this->addColumn($this->primaryKey);

		try
		{
			// provedeni SQL dotazu
			if (count($this->columns) > 0 && strlen($this->itemId) > 0)
			{
				// sestavim SQL dotaz

				switch ($this->format)
				{
					// normalni tabulka
					case 'table':
					{
						$data = (array) dibi::query(
							"SELECT %sql", implode(", ", $this->columns),
							"FROM %n", $this->table,
							"%sql", $this->joins,
							"WHERE %n.%n = %s", $this->table, $this->primaryKey, $this->itemId
						)->fetchAll();
						foreach ($this->multiReferences as $m)
						{
							$data[0][$m['name']] = (array) dibi::query(
								"SELECT [" . $m['foreign2'] . "]",
								"FROM [" . $m['junction'] . "]",
								"WHERE [" . $m['foreign1'] . "] = %s", $this->itemId
							)->fetchPairs();
						}
						break;
					}

					// zavisla tabulka
					case 'related_table':
					{
						if ($this->sources->isMNJoin($this->sourceId))
						{
							$data = (array) dibi::query(
								"SELECT %sql", implode(", ", $this->columns),
								"FROM %n", $this->table,
								"INNER JOIN %n ON %n.%n = %s AND %n.%n = %n.%n", $this->junction,
									$this->junction, $this->foreignKey1, $this->itemId,
									$this->junction, $this->foreignKey2, $this->table, $this->primaryKey,
								"%sql", $this->joins,
								"%if", count($this->sorting) > 0,
									"ORDER BY %n.%n %sql", $this->table, $this->sorting[0]->column, $this->sorting[0]->dir,
								"%end",
								"%if", count($this->sorting) > 1,
									", %n.%n %sql", $this->table, $this->sorting[1]->column, $this->sorting[1]->dir,
								"%end",
								"%if", count($this->sorting) > 2,
									", %n.%n %sql", $this->table, $this->sorting[2]->column, $this->sorting[2]->dir,
								"%end"
							)->fetchAll();
						}

						if ($this->sources->is1NJoin($this->sourceId))
						{
							$data = (array) dibi::query(
								"SELECT %sql", implode(", ", $this->columns),
								"FROM %n", $this->table,
								"%sql", $this->joins,
								"WHERE %n.%n = %s", $this->reference, $this->foreignKey, $this->itemId,
								"%if", count($this->sorting) > 0,
									"ORDER BY %n.%n %sql", $this->table, @$this->sorting[0]->column, @$this->sorting[0]->dir,
								"%end",
								"%if", count($this->sorting) > 1,
									", %n.%n %sql", $this->table, @$this->sorting[1]->column, @$this->sorting[1]->dir,
								"%end",
								"%if", count($this->sorting) > 2,
									", %n.%n %sql", $this->table, @$this->sorting[2]->column, @$this->sorting[2]->dir,
								"%end"
							)->fetchAll();
						}

						break;
					}

				}

			}
		} catch (Exception $e)
		{
			$data = array();
			trace(dibi::$sql);
			error('sql_error', $e->getMessage());
		}

		return $this->data2XML($data);
	}

	/**
	 * Prevede zadana data (vysledek SQL dotazu) na XML, kteremu rozumi AnyForm
	 */
	protected function data2XML($data)
	{
		// vytvorim XML uzel
		$panel = $this->xml->createElement('panel');

		foreach ($data as $item)
		{
			$item = (array) $item;
			$record = $this->xml->createElement('record');
			$record->setAttribute($this->primaryKey, element("src" . $this->sourceId . "_" . $this->primaryKey, $item));
			$panel->appendChild($record);

			// nactu preklady vicejazycnych sloupcu
			foreach ($this->multiLanguageColumns as $multi)
				$item[$multi] = array2XML($this->loadText(element($multi, $item)), 'data', false, false);

			ksort($item);

			// vysledek prelouskam jako XML
			$prevSourceId = -1;
			foreach ($item as $columName => $columnContent)
			{
				// nejedena se nahodou o nameColumn?
				if ($columName == 'anyFormPanelNameColumn')
				{
					$this->name = $columnContent;
					continue;
				}

				$sourceId = (int) str_replace('src', '', element(0, explode('_', $columName)));
				if ($sourceId != $prevSourceId)
				{
					$source = $this->xml->createElement('source');
					$source->setAttribute('id', $sourceId);
					$record->appendChild($source);
				}

				$column = $this->xml->createElement(str_replace("src{$sourceId}_", '', $columName));
				if (is_array($columnContent))
				{
					foreach ($columnContent as $columnContentItem)
					{
						$value = $this->xml->createElement('value');
						$value->appendChild($this->xml->createTextNode($columnContentItem));
						$column->appendChild($value);
					}
				}
				else $column->appendChild($this->xml->createTextNode($columnContent));
				$source->appendChild($column);

				$prevSourceId = $sourceId;
			}

		}

		return $panel;
	}

	/*
	* Funce vrati pole s prekladem textu zadaneho parametrym
	* @param	array	Pole vysledku, ve kterem maji byt texty doplneny
	* @return	array	Pole sloupcu ve vysledku, ktere maji byt doplneny (nazvy indexu v poli $result)
	* @author	Hugo
	*/
	protected function loadText($id)
	{
		return (array) dibi::query("SELECT * FROM [:data:texts] WHERE id = %i", $id)->fetchAll();
	}

}
