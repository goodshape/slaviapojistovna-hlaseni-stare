<?php

/**
 * Trida pro ulozeni a aktualizaci dat jednoho panelu
 */
class Updater {

	// zdroje
	public $sources;

	// id hlavniho zdroje
	public $sourceId;

	// id hlavniho zaznamu
	public $masterItemId;

	// vazba na strukturu
	public $structureId;

	// nazev hlavni tabulky
	public $table;

	// nazev primarniho klice
	public $primaryKey;

	// propojovaci tabulka pro vazby M:N
	public $junction;

	// nazev ciziho klice v pripojene tabulce
	public $foreignKey;

	// nazev vlastniho klice v propojovaci tabulce
	public $foreignKey1;

	// nazev ciziho klic v propojovaci tabulce
	public $foreignKey2;

	// hodnota ciziho klice pro omezeni zavislych tabulek
	public $foreignItemId;

	// xml list zaznamu k ulozeni
	public $panelContent;

	// xml konfigurace panelu
	public $panelConfig;

	// hodnoty k ulozeni (pole zaznamu)
	protected $values;

	// primarni klice zaznamu v poli $values (indexy se shoduji s polem $values)
	protected $itemIds;

	// pole zaznamu obsahujicich pole sloupcu, ktere jsou vicejazycne (indexy se shoduji s polem $values)
	protected $multiLanguageColumns;

	// seznam multireferenci pro separatni updaty
	protected $multiReferences = array();

	// pocet polozek v panelu
	protected $numRecords;

	/**
	 * Konstruktor
	 * @author	Hugo
	 */
	public function __construct($sourceId, $sources, $masterItemId, $structureId, $panelContent, $panelConfig)
	{
		// sdilene datove struktury
		$this->sources = $sources;
		$this->panelContent = $panelContent;
		$this->panelConfig = $panelConfig;

		// zakladni udaje
		$this->sourceId = (int) $sourceId;
		$this->masterItemId = $masterItemId;
		$this->structureId = $structureId;

		// nazev tabulky
		$this->table = dibi::getConnection()->translate(tableName($this->sources->getTableName($this->sourceId)));
		// primarni klic
		$this->primaryKey = $this->sources->getPrimaryKey($this->sourceId);

		// pocet polozek v panelu
		$this->numRecords = count($panelContent->record);

		// atributy pro zavisle tabulky
		// 1:N
		if ($this->sources->is1NJoin($this->sourceId))
		{
			$this->foreignKey = $this->sources->getForeingKey($this->sourceId);
		}
		// M:N
		if ($this->sources->isMNJoin($this->sourceId))
		{
			$this->junction = dibi::getConnection()->translate(tableName($this->sources->getJunction($this->sourceId)));
			$this->foreignKey1 = $this->sources->getForeignKey1($this->sourceId);
			$this->foreignKey2 = $this->sources->getForeignKey2($this->sourceId);
		}
		$this->foreignItemId = strlen($this->panelConfig['itemId']) > 0 ? (string) $this->panelConfig['itemId'] : $this->masterItemId;
	}

	/**
	 * Metoda provede rozparsovani dat a jejich ulozeni
	 */
	public function save()
	{
		$this->parseData();
		$this->saveData();
		return $this->masterItemId;
	}

	/**
	 * Metoda prevede vstupni XML na pole vhodne pro SQL INSERT nebo SQL UPDATE
	 */
	protected function parseData()
	{
		$this->itemIds = array();
		$this->values = array();
		$this->multiLanguageColumns = array();

		for ($recordIndex = 0; $recordIndex < $this->numRecords; $recordIndex++)
		{
			// z obsahu panelu vyberu jeden zaznam
			$record = $this->panelContent->record[$recordIndex];

			$itemId = (string) $record[$this->primaryKey];
			$this->itemIds[$recordIndex] = $itemId;
			$this->values[$recordIndex] = array();
			$this->multiLanguageColumns[$recordIndex] = array();
			$this->multiReferences[$recordIndex] = array();

			// nachystani dat z panelu
			foreach ($this->panelConfig->row as $rowConfig)
				foreach ($rowConfig->children() as $itemConfig)
				{
					// nazev a typ sloupce v tabulce
					$column = (string) $itemConfig['column'];
					$type = $itemConfig->getName();
					$value = '';

					if ($type == 'multiselect' || $type == 'multistructure') // pro multireference se uklada jinam
					{
						if ($type == 'multistructure') $column = "id";
						else $column = (string) $itemConfig['referenceColumn'];
						// z dane polozky vyberu jen tu cast, ktera odpovida zdroji
						foreach ($record->source as $sourceXML)
							if ((int) $sourceXML['id'] == (int) $itemConfig['reference']) $value = (string) $sourceXML->$column;
					}
					else
					{
						// zpracovavam jen polozky urcene k editaci
						if (empty($column) || (string) $itemConfig['editable'] === "0") continue;

						// z dane polozky vyberu jen tu cast, ktera odpovida zdroji
						foreach ($record->source as $sourceXML)
							if ((int) $sourceXML['id'] == $this->sourceId) $value = (string) $sourceXML->$column;
					}

					switch ($type)
					{
						case 'textinput':
						case 'textarea':
						case 'wysiwyg':
						case 'icon':
						{
							// nazvy textovych vicejazycnych polozek si ulozim pro dodatacne zpracovani
							if ($itemConfig['multiLanguage'] == "1") $this->multiLanguageColumns[$recordIndex][$column] = $column;
						}

						case 'date':
						case 'datetime':
						case 'select':
						case 'autocomplete':
						case 'checkbox':
						case 'set':
						case 'option':
						case 'fileinput':
						case 'structure':
						{
							$this->values[$recordIndex][$column] = $value;
							break;
						}

						case 'multiselect':
						case 'multistructure':
						{
							$values = XML($value);
							$referenceSource = (int) $itemConfig['reference'];
							if ($values)
							{
								foreach ($values->children() as $valueItem)
									$this->multiReferences[$recordIndex][$referenceSource][] = (string) $valueItem;
							}
							else
							{
								if (!isset($this->multiReferences[$recordIndex][$referenceSource]))
									$this->multiReferences[$recordIndex][$referenceSource] = array();
							}
							break;
						}
					}
				}

			if ($this->structureId > 0 && !$this->sources->isJoin($this->sourceId)) $this->values[0]['structure_id'] = $this->structureId;
		}
	}

	/**
	 * Metoda ulozi hodnotu pole $values do databaze
	 */
	protected function saveData()
	{
		// nejdrive ulozime vsechny nachystane zaznamy
		foreach ($this->values as $recordIndex => $values)
		{
			// panel nemusi obsahovat zadna data
			if (count($values) == 0) continue;

			// radek tabulky, ktery budu ukladat
			$itemId = $this->itemIds[$recordIndex];

			// pokud neni zadano itemId a nejedna se o zavislou tabulku, mel bych pouzit masterItemId
			if (strlen($itemId) == 0 && strlen($this->masterItemId) > 0 && !$this->sources->isJoin($this->sourceId)) $itemId = $this->masterItemId;

			// ulozim preklady
			foreach ($this->multiLanguageColumns[$recordIndex] as $multi)
				$values[$multi] = $this->saveTexts($values[$multi]);

			// pokud je zadan primarni klic, jedna se o UPDATE
			if (strlen($itemId) > 0)
			{
				// pokud se meni primarni klic, musime pohlidat, aby si uzivatel neprepsal jiz existujici zaznam
				if (isset($values[$this->primaryKey]) && strlen($values[$this->primaryKey]) > 0
					&& $itemId <> $values[$this->primaryKey] && $this->itemExists($values[$this->primaryKey]))
					error('duplicate_primary_key', "{$this->primaryKey} = '{$values[$this->primaryKey]}'");

				try
				{
					// ulozeni data provedene zmeny (pokud existuje sloupec 'modified')
					if (dibi::query("SHOW COLUMNS FROM %n LIKE 'modified'", $this->table)->fetchSingle() == 'modified') 
						$values['modified%sql'] = 'NOW()';

					// ulozeni autora provedene zmeny (pokud existuje sloupec 'modified_by')
					if (dibi::query("SHOW COLUMNS FROM %n LIKE 'modified_by'", $this->table)->fetchSingle() == 'modified_by') 
						$values['modified_by'] = session('logged_id');

					// provedeme update
					dibi::query("UPDATE %n", $this->table, "SET", $values, "WHERE %n = %s", $this->primaryKey, $itemId);
				}
				catch (Exception $e)
				{
					//trace(dibi::$sql);
					error('sql_error', $e->getMessage());
				}
			}
			else // pokud neni zadan primarni klic, jedna se o INSERT
			{
				// pokud se jedna o zakladni tabulku a komponenta ma explicitne definovano itemId, pouziji ho
				if (!$this->sources->isJoin($this->sourceId) && strlen($this->panelConfig['itemId']) > 0)
					$values[$this->primaryKey] = (string) $this->panelConfig['itemId'];

				// zavisla 1:N tabulka musi obsahovat vazbu na nadrazenou tabulku
				if ($this->sources->is1NJoin($this->sourceId))
				{
					if (strlen($this->panelConfig['itemId']) > 0) $values[$this->foreignKey] = (string) $this->panelConfig['itemId'];
					else $values[$this->foreignKey] =  $this->masterItemId;
				}

				// pokud je zadan primarni klic, musime zkontrolovat, jestli uz nahodou neexistuje
				if (isset($values[$this->primaryKey]) && strlen($values[$this->primaryKey]) > 0
					&& $this->itemExists($values[$this->primaryKey]))
				{
					error('duplicate_primary_key', "{$this->primaryKey} = '{$values[$this->primaryKey]}'");
				}

				// provedu insert se zachycenim vyjimky
				try {

					// ulozeni data vytvoreni (pokud existuje sloupec 'created')
					if (dibi::query("SHOW COLUMNS FROM %n LIKE 'created'", $this->table)->fetchSingle() == 'created') 
						$values['created%sql'] = 'NOW()';

					// ulozeni autora zaznamu (pokud existuje sloupec 'created_by')
					if (dibi::query("SHOW COLUMNS FROM %n LIKE 'created_by'", $this->table)->fetchSingle() == 'created_by') 
						$values['created_by'] = session('logged_id');

					dibi::query("INSERT INTO %n", $this->table, $values);
				}
				catch (Exception $e)
				{
					//trace(dibi::$sql);
					error('sql_error', $e->getMessage());
				}

				try {
					// zjistim id vlozeneho zaznamu (funguje jen pro primarni klice s auto-increment)
					$insertId = dibi::insertId();
				}
				catch (Exception $e)
				{
					// pokud neni auto-increment, insertId jiz znam
					$insertId = element($this->primaryKey, $values);
				}

				// pridam do seznamu idcek, abych je pak nesmazal
				$this->itemIds[] = $insertId;

				// po ulozeni zaznamu v prvnim pruchodu ziskame hlavni itemId
				if (!$this->sources->isJoin($this->sourceId) && strlen($this->masterItemId) == 0) $this->masterItemId = $insertId;

				// pri propojeni M:N je potreba provest insert do vazebni tabulky
				if ($this->sources->isMNJoin($this->sourceId))
				{
					$isJoined = (int) dibi::query(
						"SELECT COUNT(*) FROM %n", $this->junction,
						"WHERE %n = %s AND %n = %s", $this->foreignKey1, $this->conditionItemId, $this->foreignKey2, $insertId
					)->fetchSingle() > 0;

					if (!$isJoined)
					{
						$joinData = array(
							$this->foreignKey1 => $this->foreignItemId,
							$this->foreignKey2 => $insertId
						);
						dibi::query("INSERT INTO %n", $this->junction, $joinData);
					}
				}
			}
		}

		// pro zavisle tabulky provedeme odstraneni zaznamu, ktere nebyly specifikovany
		if ($this->sources->is1NJoin($this->sourceId))
		{
			dibi::query(
				"DELETE FROM %n", $this->table,
				"WHERE %n = %s", $this->foreignKey, $this->foreignItemId,
				"%if", count($this->itemIds) > 0,
				"AND %n NOT IN (%s)", $this->primaryKey, $this->itemIds,
				"%end"
			);
		}
		if ($this->sources->isMNJoin($this->sourceId))
		{
			dibi::query(
				"DELETE FROM %n", $this->junction,
				"WHERE %n = %s", $this->foreignKey1, $this->foreignItemId,
				"%if", count($this->itemIds) > 0,
				"AND %n NOT IN (%s)", $this->foreignKey2, $this->itemIds,
				"%end"
			);
		}

		// nyni ulozime multireference
		foreach ($this->multiReferences as $recordIndex => $multiReference)
		{
			foreach ($multiReference as $sourceId => $values)
			{
				// zjistim propojovaci tabulky
				$junction = dibi::getConnection()->translate(tableName($this->sources->getJunction($sourceId)));
				// nazvy sloupcu
				$foreign1 = $this->sources->getForeignKey1($sourceId);
				$foreign2 = $this->sources->getForeignKey2($sourceId);

				// nactu aktulani stav
				$current = dibi::query("SELECT %n FROM %n WHERE %n = %s", $foreign2, $junction, $foreign1, $this->masterItemId)->fetchPairs();

				// zjistim, co je treba smazat a co vlozit
				$toDelete = array_diff($current, $values);
				$toInsert = array_diff($values, $current);

				// a provedu mazani
				if (count($toDelete) > 0) dibi::query(
					"DELETE FROM %n", $junction,
					"WHERE %n = %s", $foreign1, $this->masterItemId,
					"AND %n IN (%s)", $foreign2, $toDelete
				);

				// a vlozeni
				foreach ($toInsert as $value) dibi::query(
					"INSERT INTO %n", $junction,
					"SET %n = %s,", $foreign1, $this->masterItemId,
					"%n = %s", $foreign2, $value
				);
			}
		}

	}

	/**
	 * Vrati true, pokud v tabulce existuje zaznam se zadanym $itemId
	 */
	protected function itemExists($itemId)
	{
		return (dibi::query(
			"SELECT COUNT(*) FROM %n WHERE %n = %s", $this->table, $this->primaryKey, $itemId
		)->fetchSingle() > 0);
	}

	/*
	* Funkce rozparsuje zadane XML dle jazyku a ulozi jednotlive texty do tabulky data_texts
	* @param	string	XML s ruznymi jazykovymi variantami textu
	* @param	boolean	pokud je true, texty se ulozi pod novym id (jako nove)
	* @return	int		id ulozenych textu (unikatni jen ve spojeni s language_id)
	* @author	Hugo
	*/
	protected function saveTexts($xmlString, $new = false)
	{
		$xml = XML($xmlString);
		if ($xml == NULL) return NULL;
		$id = 0;
		$count = 0;
		foreach ($xml->children() as $object) {
			$count++;
			if ($object->id*1 > 0) {
				$id = $object->id*1;
				break;
			}
		}
		if ($count == 0) return null;
		if ($id == 0 || $new) $id = dibi::query("SELECT id FROM [:data:texts] ORDER BY id DESC LIMIT 1")->fetchSingle() * 1 + 1;
		$savedLanguages = array();
		foreach ($xml->children() as $object) {
			$data = Array(
				"id"			=> $id,
				"language_id"	=> (string)$object->language_id,
				"text"			=> (string)$object->text
			);
			dibi::query("REPLACE INTO [:data:texts]", $data);
			$savedLanguages[] = (string)$object->language_id;
		}
		// ostatni jazyky, ktere nebyly zadany, je mozno z dabataze odstranit
		if (count($savedLanguages) > 0) dibi::query("DELETE FROM [:data:texts] WHERE id = %i", $id, " AND language_id NOT IN (%s", $savedLanguages, ")");
		else dibi::query("DELETE FROM [:data:texts] WHERE id = %i", $id);
		return $id;
	}

}