<?php

/**
 * Trida je abstrakci tzv. zdroju dat pro formulare
 */
class Sources {

	public $config;

	/**
	 * Konstruktor
	 * Vytvori instanci objektu classes dle zadane XML konfigurace
	 * @author	Hugo
	 */
	public function __construct($config)
	{
		$this->config = $config;
	}

	/**
	 * Vrati nazev tabulky dle zadaneho $sourceId
	 */
	public function getTableName($sourceId)
	{
		return $this->getAttributeById($sourceId, 'name');
	}

	/**
	 * Vrati primarni klic dle zadaneho $sourceId
	 */
	public function getPrimaryKey($sourceId, $default = false)
	{
		return $this->getAttributeById($sourceId, 'key', $default);
	}

	/**
	 * Vrati nazev propojovaci tabulky pro zavislou tabulku typu M:N
	 */
	public function getJunction($sourceId, $default = false)
	{
		return $this->getAttributeById($sourceId, 'junction', $default);
	}

	/**
	 * Vrati cizi sloupec s pripojovane tabulky
	 */
	public function getForeingKey($sourceId, $default = false)
	{
		return $this->getAttributeById($sourceId, 'foreign', $default);
	}

	/**
	 * Vrati vlastni sloupec z propojovaci tabulky pro zavislou tabulku typu M:N
	 */
	public function getForeignKey1($sourceId, $default = false)
	{
		return $this->getAttributeById($sourceId, 'foreign1', $default);
	}

	/**
	 * Vrati cizi sloupec z propojovaci tabulky pro zavislou tabulku typu M:N
	 */
	public function getForeignKey2($sourceId, $default = false)
	{
		return $this->getAttributeById($sourceId, 'foreign2', $default);
	}

	/**
	 * Vrati pole sloupcu, podle kterych se maji seradit
	 * zaznamy z pripojene tabulky
	 */
	public function getSorting($sourceId)
	{
		$result = array();
		$sort1 = $this->getAttributeById($sourceId, 'sortColumn1');

		if (strlen($sort1) > 0) {

			$result[] = (object) array(
				'column' => $sort1,
				'dir' => $this->getAttributeById($sourceId, 'sortDir1') == 'DESC' ? 'DESC' : 'ASC'
			);

			$sort2 = $this->getAttributeById($sourceId, 'sortColumn2');

			if (strlen($sort2) > 0) {

				$result[] = (object) array(
					'column' => $sort2,
					'dir' => $this->getAttributeById($sourceId, 'sortDir2') == 'DESC' ? 'DESC' : 'ASC'
				);

				$sort3 = $this->getAttributeById($sourceId, 'sortColumn3');

				if (strlen($sort3) > 0)
					$result[] = (object) array(
						'column' => $sort3,
						'dir' => $this->getAttributeById($sourceId, 'sortDir3') == 'DESC' ? 'DESC' : 'ASC'
					);
			}
		}

		return $result;
	}

	/**
	 * Vrati TRUE, pokud je zadany zdroj zavisla tabulka
	 */
	public function isJoin($sourceId)
	{
		return $this->is1NJoin($sourceId) || $this->isMNJoin($sourceId);
	}

	/**
	 * Vrati TRUE, pokud je zadany zdroj zavisla tabulka s vazbou 1:N
	 */
	public function is1NJoin($sourceId)
	{
		return $this->getNameById($sourceId) == 'join';
	}

	/**
	 * Vrati TRUE, pokud je zadany zdroj zavisla tabulka s vazbou M:N
	 */
	public function isMNJoin($sourceId)
	{
		return $this->getNameById($sourceId) == 'mjoin';
	}

	/**
	 * Vrati id nadrazeneho zdroje
	 */
	public function getParentId($sourceId)
	{
		if (!$this->config) return $default;
		foreach ($this->config->table as $tableConfig)
		{
			foreach ($tableConfig->children() as $joinConfig)
			{
				if ((int) $sourceId == (int) $joinConfig['id']) return (int) $tableConfig['id'];
			}
		}

		return false;
	}

	/**
	 * Vrati atribut zdroje dle zadaneho $sourceId
	 */
	protected function getAttributeById($sourceId, $attributeName, $default = false)
	{
		if (!$this->config) return $default;
		
		foreach ($this->config->table as $tableConfig)
		{
			if ((int) $sourceId == (int) $tableConfig['id']) return (string) $tableConfig[$attributeName];

			foreach ($tableConfig->children() as $joinConfig)
			{
				if ((int) $sourceId == (int) $joinConfig['id']) return (string) $joinConfig[$attributeName];
			}
		}
		
		return $default;
	}

	/**
	 * Vrati nazev elementu zdroje dle zadaneho $sourceId
	 */
	protected function getNameById($sourceId, $default = false)
	{
		if (!$this->config) return $default;
		
		foreach ($this->config->table as $tableConfig)
		{
			if ((int) $sourceId == (int) $tableConfig['id']) return (string) $tableConfig->getName();

			foreach ($tableConfig->children() as $joinConfig)
			{
				if ((int) $sourceId == (int) $joinConfig['id']) return (string) $joinConfig->getName();
			}
		}
		
		return $default;
	}

}
