<?php if (!defined('BASEPATH')) die('Access denied!');

/**
 * Katalog
 */
class Catalog extends Core {

	/**
	 * Konstruktor
	 */
	public function __construct($config) {
		parent::__construct($config);
	}

	/**
	 * Funkce vrati seznam polozek jako XML
	 * 
	 * @param	int		component_id - id katalogove komponenty
	 * @param	string	order - sloupce pro razeni. zadava se jako seznam indexu oddeleny svislitky
	 * 					- zaporny index znamena sestupne razeni,
	 * 					- cislovani zacina od jednicky
	 * @param	int		structure_id - 0 = tabulka nema structure_id
	 * @param	int		limit
	 * @param	int		offset
	 * @return	XML		vysledek dotazu
	 * @author	Hugo
	 */
	public function getList()
	{
		$componentId = post('component_id')*1;

		// trideni a limit beru od klienta
		$order = boom(post('order', '-1'));
		$limit = post('limit', 30);
		$offset = post('offset', 0);
		// zarazeni do struktury
		$structureId = post('structure_id', get('structure_id', 0));

		// filtry
		$filters = XML(post('filters'));

		// log
		$this->log(31, $componentId, LOG_READ);

		list($data, $count) = $this->getListData($componentId, $order, $structureId, $limit, $offset, $filters);

		returnXML($data, 'data', $count);
	}

	/**
	 * Funkce vrati seznam polozek jako pole
	 * 
	 * @param	int		component_id - id katalogove komponenty
	 * @param	string	order - sloupce pro razeni. zadava se jako seznam indexu oddeleny svislitky
	 * 					- zaporny index znamena sestupne razeni,
	 * 					- cislovani zacina od jednicky
	 * @param	int		structure_id - 0 = tabulka nema structure_id
	 * @param	int		limit
	 * @param	int		offset
	 * @return	XML		vysledek dotazu
	 *
	 */
 	public function getListData($componentId, $order, $structureId, $limit, $offset, $filters)
	{

		// nacteni konfigurace komponenty
		$componentData = dibi::query("SELECT name, config FROM [:as:components] WHERE id = %i LIMIT 1", $componentId)->fetch();
		$componentName = element('name', $componentData);
		if (($xmlConfig = XML(element('config', $componentData))) == false) error('xml_error');

		// kontrola prav
		$this->checkComponentAccess($componentName, 'allow_access', $componentId);

		// zakladni promenne
		$table = tableName($xmlConfig['table']);

		// struktura s vazbou M:N
		if (strlen($xmlConfig['multistructure']) > 0)
		{
			$segments = explode(".", $xmlConfig['multistructure']);
			if (count($segments) == 3)
			{
				$structureMulti = tableName($segments[0]);
				$structureColumn1 = $segments[1];
				$structureColumn2 = $segments[2];
			}
			else error("server_param_error", "Není specifikována propojovací tabulka se strukturou nebo její sloupce.");
		}
		else $structureMulti = FALSE;

		$columns = array();
		$joins = array();
		$wheres = array();
		$orders = array();
		$index = 1;

		// cyklus, ktery prochazi jednolive sloupce konfigurace
		foreach ($xmlConfig->list->children() as $item)
		{
			$type = $item->getName();
			$column = (string) $item['name'];
			$reference = (string) $item['reference'];

			if ($type == 'action' || $type == 'styles') continue;

			if (strlen($reference) > 0)
			{
				// pripojeni referencni tabulky
				$ref_table = tableName(element(0, explode(".", $reference)));
				$ref_alias = $ref_table . $index;
				$ref_column = element(1, explode(".", $reference));
				$ref_column_add = element(2, explode(".", $reference));

				if ($type == 'multireference')
				{
					$junction = explode(".", $item['junction']);
					$foreign1 = element(1, $junction);
					$foreign2 = element(2, $junction);
					$junction = tableName(element(0, $junction));
					$source = "(SELECT GROUP_CONCAT([$ref_table].[$ref_column] SEPARATOR ',') FROM [$ref_table]" .
						"WHERE [$ref_table].[id] IN (SELECT [$foreign2] FROM [$junction] WHERE [$foreign1] = [$table].[id]))";
				}
				else
				{
					$joins[] = "LEFT JOIN [$ref_table] AS [$ref_alias] ON [$ref_alias.id] = [$table].[$column]";

					if ($item['multilang']*1 == 1)
					{
						// multijazycna reference
						$text_table = "text_".$ref_column.$index;
						$joins[] = "LEFT JOIN [:data:texts] AS [$text_table] ON [$text_table].[id] = [$ref_alias].[$ref_column] AND [$text_table].[language_id] = '".$this->language_id."'";
						if (strlen($ref_column_add) > 0) $source = "CONCAT_WS(' - ', [$text_table].[text], [$ref_alias].[$ref_column])";
						else $source = "[$text_table].[text]";
					}
					else
					{
						// obycejna reference
						if (strlen($ref_column_add) > 0) $source = "CONCAT_WS(' - ', [$ref_alias].[$ref_column], [$ref_alias].[$ref_column_add])";
						else $source = "[$ref_alias].[$ref_column]";
					}

					// pro odkazy budu potrebovat i puvodni hodnotu
					if (substr($item['link'], 0, 5) == 'edit/') $columns[] = "[$table].[$column] AS [__ref_$column]";
				}

			}
			else
			{
				if ($item['multilang']*1 == 1)
				{
					// pripojeni tabulky s textem v pripade vicejazycneho sloupce
					$text_table = "text_".$column.$index;
					$source = "[$text_table].[text]";
					$joins[] = "LEFT JOIN [:data:texts] AS [$text_table] ON [$text_table].[id] = [$table].[$column] AND [$text_table].[language_id] = '".$this->language_id."'";

				}
				else
				{
					if ($type == 'virtual')
					{
						// virtualni sloupec
						$sql = (string) $item['sql'];
						$source = "($sql)";
					}
					else
					{
						// obycejny sloupec
						$source = "[$table].[$column]";
					}
				}

			}

			// razeni
			foreach ($order as $key => $item) if (abs($item) == $index) $orders[$key] = $source.($item < 0 ? ' DESC' : ' ASC');

			// filtrovaci podminky
			if (count($filters) > 0)
				foreach ($filters as $filter)
				{
					if ($filter['index'] == $index)
					{
						switch ($filter['type'])
						{
							case 'text':
							{
								if (strlen((string) $filter->string) > 0) array_push($wheres, "AND $source LIKE %s", '%'.(string) $filter->string.'%');
								break;
							}

							case 'number':
							{
								if (strlen((string) $filter->from) > 0) array_push($wheres, "AND $source >= %f", (string) $filter->from);
								if (strlen((string) $filter->to) > 0) array_push($wheres, "AND $source <= %f", (string) $filter->to);
								if (strlen((string) $filter->equal) > 0) array_push($wheres, "AND $source = %f", (string) $filter->equal);
								break;
							}

							case 'date':
							{
								if (strlen((string) $filter->from) > 0) array_push($wheres, "AND DATE($source) >= %d", (string) $filter->from);
								if (strlen((string) $filter->to) > 0) array_push($wheres, "AND DATE($source) <= %d", (string) $filter->to);
								if (strlen((string) $filter->equal) > 0) array_push($wheres, "AND DATE($source) = %d", (string) $filter->equal);
								break;
							}

							case 'checkbox':
							case 'options':
							{
								if (strlen((string) $filter->mask) > 0)
								{
									array_push($wheres, "AND (0");
									$mask = (int) $filter->mask;
									for ($i = 0; $i < 32; $i++) if (($mask & (1 << $i)) > 0) array_push($wheres, "OR $source = %i", $i);
									array_push($wheres, ")");
								}
								break;
							}

							case 'set':
							{
								if (strlen($filter->mask) > 0)
									array_push($wheres, "AND ($source & %i", (int) $filter->mask, ') = %i', (int) $filter->mask);
								break;
							}

							case 'reference':
							{
								if (strlen((string)$filter->equal) > 0)
									if ($type == 'multireference') array_push($wheres, "AND (SELECT COUNT(*) FROM [$junction] WHERE [$foreign1] = [$table].[id] AND [$foreign2] = %i) > 0", (string) $filter->equal);
									else array_push($wheres, "AND [$table].[$column] = %i", (string) $filter->equal);
								break;
							}

						}

					}
				}

			// vlozim sloupec do pole sloupcu
			$columns[] = "$source AS [$column]";

			$index++;

		}

		// filtrace dle struktury (1:N i M:N)
		if ($structureId > 0)
		{
			if ($structureMulti)
			{
				$joins[] = "INNER JOIN [$structureMulti] ON [$structureMulti].[$structureColumn1] = [$table].[id] AND [$structureMulti].[$structureColumn2] = %i";
				$joins[] = $structureId;
			}
			else
			{
				$wheres[] = "AND [$table].[structure_id] = %i";
				$wheres[] = $structureId;
			}
		}

		if (strlen($xmlConfig->list['condition']) > 0)
		{
			$wheres[] = "AND (" . $xmlConfig->list['condition'] . ")";
		}

		$query = array();
		$query[] = "SELECT COUNT(*) FROM [$table]";
		array_push($query, "%ex", $joins, "WHERE 1");
		$query =  array_merge($query, $wheres);
		$count = dibi::query($query)->fetchSingle();

		$query = array();
		$query[] = "SELECT " . implode(',', $columns) . " FROM [$table]";
		array_push($query, "%ex", $joins, "WHERE 1");
		$query =  array_merge($query, $wheres);
		ksort($orders);
		if (count($orders) > 0)	array_push($query, "ORDER BY %sql", implode(', ', $orders));
		array_push($query, "%lmt %ofs", $limit, $offset*$limit);
		$data = dibi::query($query)->fetchAll();

		if ((int) $xmlConfig['debug'] == 1) trace(dibi::$sql);

		// doplnim vicejazycne multireference (je potreba z duvodu delat dodatecne z duvodu "performance")
		foreach ($xmlConfig->list->children() as $item)
		{
			$type = $item->getName();
			$column = (string) $item['name'];
			$reference = (string) $item['reference'];

			if (strlen($reference) > 0 && $type == 'multireference' && $item['multilang']*1 == 1)
			{
				// get used ids
				$ids = array();
				foreach ($data as $row) if (strlen($row[$column]) > 0) $ids = array_merge($ids, explode(',', $row[$column]));
				$ids = array_unique($ids);

				// get required texts
				$texts = dibi::query("SELECT id, text FROM [:data:texts] WHERE id IN %in AND language_id = %s", $ids, $this->language_id)->fetchPairs();
				//trace($texts);

				foreach ($data as $row)
				{
					if (strlen($row[$column]) > 0)
					{
						$ids = explode(',', $row[$column]);
						foreach ($ids as &$id) $id = $texts[$id];
						$row[$column] = implode(", ", $ids);
					}
				}

 			}
		}

		return array($data, $count);
	}

	/**
	 * Funkce vraci jeden zaznam pro potreby editace jako XML
	 * 
	 * @param	int		id komponenty
	 * @param	int		id zaznamu
	 * @return	XML	vysledek dotazu
	 * @author	Hugo
	 */
	public function getItem()
	{
		$componentId = post('component_id')*1;
		$id = post('id')*1;

		// log
		$this->log(32, $componentId, LOG_READ, NULL, $id);

		$data = $this->getItemData($componentId, $id);

		returnXML($data);

	}

	/**
	 * Funkce vraci jeden zaznam pro potreby editace.
	 * 
	 * @param	int		id komponenty
	 * @param	int		id zaznamu
	 * @return	Array	vysledek dotazu
	 * @author	Hugo
	 */
	public function getItemData($componentId, $id)
	{
		// nacteni konfigurace komponenty
		$componentData = dibi::query("SELECT name, config FROM [:as:components] WHERE id = %i LIMIT 1", $componentId)->fetch();
		$componentName = element('name', $componentData);
		$xmlConfig = XML(element('config', $componentData));

		// kontrola prav
		$this->checkComponentAccess($componentName, 'allow_access', $componentId);

		// zakladni promenne
		$table = tableName($xmlConfig['table']);

		$columns = array();
		$multi = array();

		// vytvorim seznam sloupcu pro SQL dotaz
		if( isset($xmlConfig->edit) )
			foreach ($xmlConfig->edit->children() as $item)
			{
				// typ sloupce (label, textinput, textarea, select, apod.)
				$type = $item->getName();
				// nazev sloupce
				$column = (string) $item['name'];
				// pripadna reference do jine tabulky
				$reference = (string) $item['reference'];

				// typy sloupcu, ktere nemaji vazbu na sloupce tabulky preskocime
				if (in_array($type, array('action', 'multiselect', 'multipopup', 'multistructure', 'block'))) continue;

				// pokud je sloupec vicejazycny, vlozim ho do pole $multi (reference ale nevadi)
				if ($item['multilang'] == 1 && strlen($reference) == 0) $multi[] = $column;
				$colAliases[] = "[$table].[$column] AS [$column]";

			}

		// nacteni samotnych dat
		if ($id > 0 && count($colAliases) > 0)
		{
			$data = (array)dibi::query(
				"SELECT ", implode(', ', $colAliases),
				"FROM [$table]",
				"WHERE [id] = %i", $id,
				"LIMIT 1"
			)->fetch();
		}
		else $data = false;

		// nacteni id pripadnych multireferenci
		if( isset($xmlConfig->edit) )
			foreach ($xmlConfig->edit->children() as $item) {

				$type = $item->getName();
				$column = (string) $item['name'];
				$multiref = (string) $item['relationship'];

				if (in_array($type, array('multiselect', 'multipopup', 'multistructure'))) {

					$rel_table = tableName(element(0, explode(".", $multiref)));
					$self_column = element(1, explode(".", $multiref));
					$foreign_column = element(2, explode(".", $multiref));
					$data[$column] = implode('|', (array) dibi::query("SELECT [rel].[$foreign_column] FROM [$rel_table] AS [rel] WHERE [rel].[$self_column] = %i", $id)->fetchPairs());

				}

			}

		// nacteni textovych dat u multijazycnych komponent
		if (count($multi) > 0) $data = $this->loadTexts($data, $multi);

		return $data;

	}

	/**
	 * Funkce vrati tabulku referencnich hodnot pro selecty.
	 *
	 * @param	int		componenet_id	id komponenty
	 * @param	string	column			nazev referencniho sloupce
	 * @param	string	ids				omezeni jen na urcita id
	 * @param	int		limit			pro strankovani
	 * @param	int		offset			pro strankovani
	 * @param	bool	used_only		jen reference, ktere jsou v danem sloupci pouzite
	 * @param	bool	list			zjistit data o referenci z listovaci casti definice katalogu
	 * @param	XML		filters			filtry (pri vyberu referenci v popup okne)
	 * @return	XML	vysledek dotazu
	 * @author	Hugo
	 */
	public function getReferenceData()
	{
		$componentId = post('component_id')*1;
		$ids = boom(post('ids'));
		$limit = (int) post('limit', 1000);
		$offset = (int) post('offset', 0);
		$used = post('used_only', 0) == 1;
		$filters = XML(post('filters'));
		$suggest = post('suggest');
		$count = 0;

		// nacteni konfigurace komponenty
		$componentData = dibi::query("SELECT name, config FROM [:as:components] WHERE id = %i LIMIT 1", $componentId)->fetch();
		$componentName = element('name', $componentData);
		$xmlConfig = XML(element('config', $componentData));

		// kontrola prav
		$this->checkComponentAccess($componentName, 'allow_access', $componentId);

		// zakladni promenne
		$table = tableName($xmlConfig['table']);
		$requestedColumn = post('column');
		$columns = post('list') == 1 ? $xmlConfig->list->children() : $xmlConfig->edit->children();

		// log
		$this->log(33, $componentId, LOG_READ, NULL, 0, $table);

		// nacteni dat
		$data = array();
		foreach ($columns as $item)
		{
			$type = $item->getName();
			$column = (string) $item['name'];
			$reference = (string) $item['reference'];
			$junction = (string) $item['junction'];
			$multilang = $item['multilang'] == 1;

			if (strlen($reference) > 0 && $column == $requestedColumn) {

				// pripojovana tabulka a sloupce
				$ref_table = tableName(element(0, explode(".", $reference)));
				$ref_column = element(1, explode(".", $reference));
				$ref_column_add = element(2, explode(".", $reference));

				// pro stromy je treba pridat i referenci na rodice
				$parent_id = $item['tree'] == 1 ? ', [table].[parent_id] AS [parent]' : '';

				// dodatecny sloupec
				if (strlen($ref_column_add) > 0) $ref_column_add = ", [table].[$ref_column_add] AS [add]";

				// filtry
				$wheres = array();
				if (count($filters) > 0)
					foreach ($filters as $filter)
					{
						switch ($filter['type'])
						{
							case 'text':
								if (strlen( (string) $filter->string) > 0)
								{
									if ($multilang)
										array_push($wheres, "AND [text].[text] LIKE %s", '%' . (string) $filter->string . '%');
									else
										array_push($wheres, "AND [table].[$ref_column] LIKE %s", '%' . (string) $filter->string . '%');
								}
								break;

							case 'number':
								if (strlen((string) $filter->from) > 0) array_push($wheres, "AND [table].[id] >= %f", (string) $filter->from);
								if (strlen((string) $filter->to) > 0) array_push($wheres, "AND [table].[id] <= %f", (string) $filter->to);
								if (strlen((string) $filter->equal) > 0) array_push($wheres, "AND [table].[id] = %f", (string) $filter->equal);
								break;
						}
					}

				if (strlen($suggest) > 0)
					if ($multilang)
						array_push($wheres, "AND [text].[text] LIKE %s", $suggest . '%');
					else
						array_push($wheres, "AND [table].[$ref_column] LIKE %s", (string) $suggest . '%');

				if ($multilang)
				{
					// pro vicejazycne reference je SQL dotaz mirne slozitejsi
					$data = (array) dibi::query(
						"SELECT [table].[id] AS [value]$parent_id, [text].[text] as [name]$ref_column_add FROM [$ref_table] AS [table]",
						"LEFT JOIN [:data:texts] AS [text] ON [text].[id] = [table].[$ref_column] AND [text].[language_id] = %s", $this->language_id,
						"WHERE 1 = 1",
						"%if", count($ids) > 0,
							"AND [table].[id] IN %in", $ids,
						"%end",
						"%sql", $wheres,
						"ORDER BY [text].[text] ASC",
						"%lmt %ofs", $limit, $offset * $limit
					)->fetchAll();

					$count = (int) dibi::query(
						"SELECT COUNT(*)",
						"FROM [$ref_table] AS [table]",
						"LEFT JOIN [:data:texts] AS [text] ON [text].[id] = [table].[$ref_column] AND [text].[language_id] = %s", $this->language_id,
						"WHERE 1 = 1",
						"%if", count($ids) > 0,
							"AND [table].[id] IN %in", $ids,
						"%end",
						"%sql", $wheres
					)->fetchSingle();

				}
				else
				{
					// multireference
					if ($junction) {

						$junction_table = tableName(element(0, explode(".", $junction)));
						$junction_column_1 = element(1, explode(".", $junction));
						$junction_column_2 = element(2, explode(".", $junction));

						$data = (array) dibi::query(
							"SELECT DISTINCT [table].[id] AS [value]$parent_id, [table].[$ref_column] as [name]$ref_column_add",
							"FROM [$ref_table] AS [table]",
							"%if", $used,
								"INNER JOIN [$junction_table] ON [$junction_table].[$junction_column_1] IN (SELECT id FROM [$table]) AND [$junction_table].[$junction_column_2] = [table].[id]",
							"%end",
							"WHERE 1 = 1",
							"%if", count($ids) > 0,
								"AND [table].[id] IN %in", $ids,
							"%end",
							"%sql", $wheres,
							"ORDER BY [table].[$ref_column] ASC",
							"%lmt %ofs", $limit, $offset * $limit
						)->fetchAll();

						$count = (int) dibi::query(
							"SELECT COUNT(DISTINCT([table].[id]))",
							"FROM [$ref_table] AS [table]",
							"%if", $used,
								"INNER JOIN [$junction_table] ON [$junction_table].[$junction_column_1] IN (SELECT id FROM [$table]) AND [$junction_table].[$junction_column_2] = [table].[id]",
							"%end",
							"WHERE 1 = 1",
							"%if", count($ids) > 0,
								"AND [table].[id] IN %in", $ids,
							"%end",
							"%sql", $wheres
						)->fetchSingle();

					}
					else 
					{
						$data = (array) dibi::query(
							"SELECT DISTINCT [table].[id] AS [value]$parent_id, [table].[$ref_column] as [name]$ref_column_add",
							"FROM [$ref_table] AS [table]",
							"%if", $used,
								"INNER JOIN [$table] ON [$table].[$column] = [table].[id]",
							"%end",
							"WHERE 1 = 1",
							"%if", count($ids) > 0,
								"AND [table].[id] IN %in", $ids,
							"%end",
							"%sql", $wheres,
							"ORDER BY [table].[$ref_column] ASC",
							"%lmt %ofs", $limit, $offset * $limit
						)->fetchAll();

						$count = (int) dibi::query(
							"SELECT COUNT(DISTINCT([table].[id]))",
							"FROM [$ref_table] AS [table]",
							"%if", $used,
								"INNER JOIN [$table] ON [$table].[$column] = [table].[id]",
							"%end",
							"WHERE 1 = 1",
							"%if", count($ids) > 0,
								"AND [table].[id] IN %in", $ids,
							"%end",
							"%sql", $wheres
						)->fetchSingle();

					}

				}

				break;

			}
		}

		returnXML($data, $requestedColumn, $count);

	}

	/**
	 * Funkce ulozi jeden zaznam
	 * @param	string	jmeno tabulky
	 * @param	string	jmena sloupcu
	 * @param	string	jmena referencnich sloupcu
	 * @return	XML	vysledek dotazu
	 * @author	Hugo
	 */
	public function saveItem()
	{
		$componentId = post('component_id')*1;

		// nacteni konfigurace komponenty
		$componentData = dibi::query("SELECT name, config FROM [:as:components] WHERE id = %i LIMIT 1", $componentId)->fetch();
		$componentName = element('name', $componentData);
		$xmlConfig = XML(element('config', $componentData));

		// zakladni promenne
		$table = tableName($xmlConfig['table']);
		$id = post('id')*1;

		// @janb - prehozeni z pohledu na tabulku
		// To umoznuje ukldat data do tabulek z pohledu.
		// Pozor, nebezpecne.
		$table = str_replace('_helper', '', $table);

		$columns = array();
		$multi = array();

		// nachystatni dat pro ulozeni
		$data = array();
		foreach ($xmlConfig->edit->children() as $item)
		{
			$type = $item->getName();
			$column = (string) $item['name'];
			$multiref = (string) $item['relationship'];
			$reference = (string) $item['reference'];

			if ($type == 'action') continue;

			if (!empty($column))
			{
				// jen sloupce, ktere nejsou multireference
				if ($type !== 'multiselect' && $type !== 'multipopup' && $type !== 'multistructure' && $type !== 'label' && $type !== 'hidden') {

					$value = post("__data_$column", '');

					if (($item['multilang'] == 1 || $type == 'multistructure') && strlen($reference) == 0)
					{
						$data[$column] = $this->saveTexts($value);
					}
					else
					{
						if ($value == '__NULL__') $data[$column] = NULL;
						else $data[$column] = $value;
					}

				}

				// skryta pole a hodnoty
				if ($type == 'hidden')
				{
					$hiddenType = (string) $item['type'];
					if ($hiddenType == 'modified') $data[$column.'%sql'] = 'NOW()';
					if ($hiddenType == 'created' && $id == 0) $data[$column.'%sql'] = 'NOW()';
					if ($hiddenType == 'user') $data[$column] = (int) session('logged_id');
					if ($hiddenType == 'const') $data[$column] = (string) (string) $item['value'];
				}

			}

		}

		if ((int) $xmlConfig['structure'] == 1 && (int) post('__data_structure_id') > 0) $data['structure_id'] = post('__data_structure_id');

		if (count($data) > 0)
		{
			if ($id > 0)
			{
				$this->checkComponentAccess($componentName, 'edit_item', $componentId);

				// uprava existujiciho zaznamu
				$diff = dibi::query("SELECT * FROM [$table] WHERE id = %s", $id)->fetch();
				dibi::query("UPDATE [$table] SET ", $data, "WHERE id = %s", $id);

				// log
				$diff['__post'] = $_POST;
				$this->log(34, $componentId, LOG_WRITE, $diff, $id);
			}
			else
			{
				$this->checkComponentAccess($componentName, 'add_item', $componentId);

				// vlozeni noveho zaznamu
				dibi::query("INSERT INTO [$table] ", $data);
				$id = dibi::insertId();

				// log
				$diff['__post'] = $_POST;
				$this->log(35, $componentId, LOG_WRITE, $diff, $id);
			}

			// ulozeni pripadnych multireferenci
			foreach ($xmlConfig->edit->children() as $item)
			{
				$type = $item->getName();
				$column = (string) $item['name'];
				$multiref = (string) $item['relationship'];

				if ($type == 'multiselect' || $type == 'multipopup' || $type == 'multistructure')
				{
					$rel_table = tableName(element(0, explode(".", $multiref)));
					$self_column = element(1, explode(".", $multiref));
					$foreign_column = element(2, explode(".", $multiref));
					$values = boom(post("__data_$column", ''));
	
					$diff = dibi::query("SELECT * FROM [$rel_table] WHERE [$self_column] = %i", $id)->fetchAll();
					$numChanges = 0;

					foreach ($values as $value)
					{
						$data = array($foreign_column => $value, $self_column => $id);
						dibi::query("REPLACE INTO [$rel_table]", $data);
						$numChanges += dibi::affectedRows();
					}
					if (count($values) > 0) dibi::query("DELETE FROM [$rel_table] WHERE [$self_column] = %i", $id, "AND [$foreign_column] NOT IN %in", $values);
					else dibi::query("DELETE FROM [$rel_table] WHERE [$self_column] = %i", $id);
					$numChanges += dibi::affectedRows();

					// log
					if ($numChanges > 0)
						$this->log(36, $componentId, LOG_WRITE, $diff, $id, $rel_table);

				}
			}

			success($id);
		}

	}

	/**
	 * Funkce smaze zaznamy
	 * @param	string	idcka zaznamu
	 * @return	XML	success nebo error
	 * @author	Hugo
	 */
	public function deleteItems()
	{
		$componentId = post('component_id')*1;
		$ids = explode("|", post('ids'));

		// nacteni konfigurace komponenty
		$componentData = dibi::query("SELECT name, config FROM [:as:components] WHERE id = %i LIMIT 1", $componentId)->fetch();
		$componentName = element('name', $componentData);
		$xmlConfig = XML(element('config', $componentData));

		// kontrola prav
		$this->checkComponentAccess($componentName, 'remove_item', $componentId);

		// jmeno tabulky
		if (strlen($xmlConfig['deltable']) > 0 ) $table = tableName($xmlConfig['deltable']);
		else $table = tableName($xmlConfig['table']);

		// @jakubf - prehozeni z pohledu na tabulku
		$table = str_replace('_helper', '', $table);

		// log
		foreach ($ids as $id)
		{
			$diff = $this->getItemData($componentId, $id);
			$this->log(37, $componentId, LOG_WRITE, $diff, $id);
		}

		$multilang = boom(safeStr(post('multilang')));
		$rows = $this->deleteTexts($table, $ids, $multilang, true);
		success($rows);
	}

	/**
	 * U zadanych zaznamu zmeni structure_id
	 */
	public function changeStructure()
	{
		$componentId = post('component_id')*1;

		// nacteni konfigurace komponenty
		$componentData = dibi::query("SELECT name, config FROM [:as:components] WHERE id = %i LIMIT 1", $componentId)->fetch();
		$componentName = element('name', $componentData);
		$xmlConfig = XML(element('config', $componentData));

		// kontrola prav
		$this->checkComponentAccess($componentName, 'edit_item', $componentId);

		// jmeno tabulky
		$table = tableName($xmlConfig['table']);
		$multiStructure = (string) $xmlConfig['multistructure'];
		$newStructureId = post('new_structure_id');
		$currentStructureId = post('current_structure_id');
		$ids = explode("|", post('ids'));

		if (count($ids) > 0 && $newStructureId > 0)
		{
			if ($multiStructure)
			{
				// vazba je M:N
				$segments = explode(".", $multiStructure);

				if (count($segments) == 3)
				{
					$structureTable = tableName($segments[0]);
					$structureColumn1 = $segments[1];
					$structureColumn2 = $segments[2];
					dibi::query("UPDATE [$structureTable] SET [$structureColumn2] = %i", $newStructureId,
						"WHERE [$structureColumn1] IN %in AND [$structureColumn2] = %i",$ids, $currentStructureId);

					trace(dibi::$sql);

					success(1);
				}
			}
			else
			{
				// vazba je 1:N
				dibi::query("UPDATE [$table] SET [structure_id] = %i WHERE id IN %in", $newStructureId, $ids);

				success(1);
			}
		}

		error('server_param_error');
	}

}
