<?php if (!defined('BASEPATH')) die('Access denied!');

/**
* Spousteni PHP "custom" skriptu z administracniho rozhrani
*/
class Scripts extends Core
{
	/**
	* Konstruktor
	*/
	public function __construct($config)
	{
		parent::__construct($config);
	}

	/*
	* Funkce spusti dany skript
	* @param	string	Jmeno skriptu, ktery se ma spustit. Soubor v adresari anyshape/scripts bez koncovky .php
						Je mozno pridat i jeden podadresar a query string
	* @return	XML	succes nebo error
	* @author	Hugo
	*/
	public function run()
	{
		global $config;
		$url = url(3, 'default') . (url(4) != '' ? '/'.url(4) : '');
		$name = safeStr(post('script', $url), '\/');
		if (file_exists($config['site']['scripts_path'] . "$name.php"))
		{
			require_once($config['site']['scripts_path'] . "$name.php");
		} else error('script_not_found');
	}

}
