<?php if (!defined('BASEPATH')) die('Access denied!');

/**
* Sprava uzivatelskych uctu a skupin
*/
class Search extends Core {

	/**
	* Konstruktor
	*/
	public function __construct($config) {
		parent::__construct($config);
	}

	/**
	* Provede indexaci webu
	*/
	public function index() {

		@set_time_limit(500);

		// nacteni konfigurace
		$componentId = dibi::query("SELECT id FROM [:as:components_web] WHERE name = 'FullTextSearch' LIMIT 1")->fetchSingle();
		$xmlConfig = XML(post('config', dibi::query("SELECT config FROM [:as:components_web] WHERE id = %i", $componentId, " LIMIT 1;")->fetchSingle()));
		if (!$xmlConfig) error('xml_error');

		// puvodni databazi smazeme
		dibi::query("TRUNCATE [:data:fulltext]");

		$total = 0;

		foreach ($xmlConfig->tables->table as $table) {

			$tableName = (string) $table['name'];
			$langs = (string) $table['langs'];
			$title  = (string) $table['title'];
			$columns = boom( (string) $table['content'] );
			$struct = ( (string) $table['structure'] ) == "1" ? ', [table].[structure_id] AS [_structure_id]' : '';
			$trigger = strlen( (string) $table['trigger'] ) > 0 ? "WHERE ([table].[".(string) $table['trigger']."] > 0 AND [table].[".(string) $table['trigger']."] IS NOT NULL)" : '';

			if ($langs == '') {	// jednojazycne polozky

				foreach ($columns as &$columnRef) $columnRef = "table.$columnRef";
				$result = dibi::query("SELECT [table].[id] AS [_id], [table].[$title] AS [_title], CONCAT_WS(' ', %n) AS [_content]$struct FROM [:data:$tableName] AS [table] $trigger", $columns)->fetchAll();

				foreach ($result as $item) {

					$titleData = html_entity_decode(strip_tags(element('_title', $item)));
					$contentData = html_entity_decode(strip_tags(element('_content', $item)));

					if (!empty($titleData)) {

						$data = array(
							'table'			=> $tableName,
							'item_id'		=> element('_id', $item),
							'structure_id'	=> element('_structure_id', $item, 0)*1,
							'title'			=> $titleData,
							'content'		=> empty($contentData) ? $titleData : $contentData
						);
						dibi::query("INSERT INTO [:data:fulltext]", $data);
						$total++;

					}

				}

			} else {	// vicejazycne  polozky

				$langs = boom($langs);

				foreach ($langs as $lang) {

					$query = array("SELECT [table].[id] AS [_id], [text_$title].[text] AS [_title], CONCAT_WS(' ', %n) AS [_content]$struct FROM [:data:$tableName] AS [table]");
					$subquery = array();
					foreach ($columns as $column) $subquery[] = "text_$column.text";
					$query[] = $subquery;
					$query[] = "LEFT JOIN [:data:texts] AS [text_$title] ON [text_$title].[id] = [table].[$title] AND [text_$title].[language_id] = '$lang'";
					foreach ($columns as $column) $query[] = "LEFT JOIN [:data:texts] AS [text_$column] ON [text_$column].[id] = [table].[$column] AND [text_$column].[language_id] = '$lang'";
					$query[] = $trigger;
					$result = dibi::query($query)->fetchAll();

					foreach ($result as $item) {

						$titleData = html_entity_decode(strip_tags(element('_title', $item)));
						$contentData = html_entity_decode(strip_tags(element('_content', $item)));

						if (!empty($titleData)) {
							$data = array(
								'table'			=> $tableName,
								'item_id'		=> element('_id', $item),
								'structure_id'	=> element('_structure_id', $item, 0)*1,
								'title'			=> $titleData,
								'content'		=> empty($contentData) ? $titleData : $contentData,
								'language_id'	=> $lang
							);
							dibi::query("INSERT INTO [:data:fulltext]", $data);
							$total++;
						}

					}
				}
			}
		}

		success($total);

	}

}

?>