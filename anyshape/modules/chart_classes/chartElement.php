<?php

/**
 * Graf (obecně)
 *
 * @author Petr Dvorak <petr.dvorak@goodshape.cz>
 *
 */
abstract class ChartElement extends SeriesContainer
{
	/** @var array */
	protected $attributes = array();
	protected $scales = array();

	/**
	 * Převede element na XML
	 */
	public function toXML()
	{
		$attributes = '';
		foreach ($this->attributes as $name => $value)
			$attributes .= $name . '="' . htmlspecialchars($value, ENT_COMPAT, 'UTF-8') . '" ';

		$result = "<chart $attributes>";
		foreach ($this->seriesArray as $series)
		{
			$result .= $series->toXML();
		}
		$result .= '<scales>';
		foreach ($this->scales as $value => $name)
		{
			$result .= '<scale value="' . htmlspecialchars($value, ENT_COMPAT, 'UTF-8') . '">' . htmlspecialchars($name, ENT_COMPAT, 'UTF-8'). '</scale>';
		}
		$result .= '</scales>';
		$result .= "</chart>";
		return $result;
	}

}

/**
 * Typy dat zobrazovaných na osách
 *
 * @author Hugo
 *
 */
class DataTypes
{
	const DATE_MONTHS = 'months';
	const DATE_HOURS = 'hours';
	const DATE_DAYS = 'days';
	const NUMBERS = 'numbers';
	const CATEGORIES = 'categories';
}
