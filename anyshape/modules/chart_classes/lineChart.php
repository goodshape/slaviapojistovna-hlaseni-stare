<?php

/**
 * Graf typu LineChart
 *
 * @author Petr Dvorak <petr.dvorak@goodshape.cz>
 *
 */
class LineChart extends ChartElement
{
	/**
	 * Konstruktor grafu
	 *
	 * @param String $name             Název grafu
	 * @param String $horizontalData   Typ dat na horizontální ose (DATE_HOURS, DATE_DAYS, NUMBERS, CATEGORIES)
	 * @param String $verticalData     Typ dat na vertikální ose (DATE_HOURS, DATE_DAYS, NUMBERS, CATEGORIES)
	 * @param Array  $scales           Seznam možných měřítek pro zobrazení (např. týden, měsíc, apod.)
	 * @param int    $scale            Aktuální měřítko
	 * @param offset $offset           Aktuální posunutí na ose x (udáváno v počtup jednotek měřítka)
	 * @throws Exception
	 */
	public function __construct($name, $horizontalData, $verticalData, $scales, $scale = 0, $offset = 0)
	{
		if (strlen($name) == 0) throw new Exception('Series name not specified.');
		$this->attributes['type'] = 'line';
		$this->attributes['name'] = $name;
		$this->attributes['horizontalData'] = $horizontalData;
		$this->attributes['verticalData'] = $verticalData;
		$this->attributes['scale'] = $scale;
		$this->attributes['offset'] = $offset;
		$this->scales = $scales;
	}
}