<?php

/**
 * Serie hodnot pro graf (jedna krivka v grafu)
 *
 * @author Petr Dvorak <petr.dvorak@goodshape.cz>
 *
 */
class SeriesElement extends SeriesContainer
{
	protected $name;
	protected $visible;
	protected $open;

	/** @var Array **/
	protected $data = array();

	/**
	 * Konstruktor
	 *
	 * @param String $name
	 * @param Boolean $visible
	 * @param Boolean $open
	 * @throws Exception
	 */
	public function __construct($name, $visible = FALSE, $open = FALSE)
	{
		if (strlen($name) == 0) throw new Exception('Series name not specified.');
		$this->name = (string) $name;
		$this->visible = (boolean) $visible;
		$this->open = (boolean) $open;
	}

	/**
	 * Vrati pocet datovych polozek
	 */
	public function count()
	{
		return count($this->data);
	}
	/**
	 * Přidání nové hodnoty do série
	 */
	public function addData($x, $y, $name = NULL, $description = NULL)
	{
		$this->data[] = array($x, $y, $name, $description);
	}

	/**
	 * Převede kontejner na XML
	 */
	public function toXML()
	{
		$result = '<series name="' . htmlspecialchars($this->name, ENT_COMPAT, 'UTF-8') .
			'" visible="' . ($this->visible ? 'true' : 'false') .
			'" open="' . ($this->open ? 'true' : 'false') . '">';

		if (count($this->seriesArray) > 0)
		{
			foreach ($this->seriesArray as $series)
			{
				$result .= $series->toXML();
			}
		}

		if (count($this->data) > 0)
		{
			$result .= '<data>';
			foreach ($this->data as $item)
			{
				$result .= '<item>';
				$result .= '<x>' . htmlspecialchars($item[0], ENT_COMPAT, 'UTF-8') . '</x>';
				$result .= '<y>' . htmlspecialchars($item[1], ENT_COMPAT, 'UTF-8') . '</y>';
				if ($item[2]) $result .= '<name>' . htmlspecialchars($item[2], ENT_COMPAT, 'UTF-8') . '</name>';
				if ($item[3]) $result .= '<description>' . htmlspecialchars($item[3], ENT_COMPAT, 'UTF-8') . '</description>';
				$result .= '</item>';
			}
			$result .= "</data>";
		}
		$result .= "</series>";

		return $result;
	}

}