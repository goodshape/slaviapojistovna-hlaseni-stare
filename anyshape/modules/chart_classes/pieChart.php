<?php

/**
 * Graf typu PieChart
 *
 * @author Petr Dvorak <petr.dvorak@goodshape.cz>
 *
 */
class PieChart extends ChartElement
{
	/**
	 * Konstruktor grafu
	 *
	 * @param String $name             Název grafu
	 * @param Array  $scales           Seznam možných měřítek pro zobrazení (např. týden, měsíc, apod.)
	 * @param int    $scale            Aktuální měřítko
	 * @param offset $offset           Aktuální posunutí na ose x (udáváno v počtup jednotek měřítka)
	 * @throws Exception
	 */
	public function __construct($name, $scales, $scale = 0, $offset = 0)
	{
		if (strlen($name) == 0) throw new Exception('Series name not specified.');
		$this->attributes['type'] = 'pie';
		$this->attributes['name'] = $name;
		$this->attributes['scale'] = $scale;
		$this->attributes['offset'] = $offset;
		$this->scales = $scales;
	}
}