<?php

/**
 * Kontejner, který obsahuje série pro graf
 *
 * @author Petr Dvorak <petr.dvorak@goodshape.cz>
 *
 */
abstract class SeriesContainer
{
	/** @var Array **/
	public $seriesArray = array();

	/**
	 * Přidání elementu do kontejneru
	 *
	 * @param SeriesElement $seriesElement
	 */
	public function addSeries(SeriesElement $seriesElement)
	{
		$this->seriesArray[] = $seriesElement;
	}

	/**
	 * Převede kontejner na XML
	 */
	public function toXML()
	{
		$result = "<series>";
		foreach ($this->seriesArray as $series)
		{
			$result .= $series->toXML();
		}
		$result .= "</series>";
		return $result;
	}

}