<?php

/**
 * Kontejner, který obsahuje jednotlivé grafy
 *
 * @author Petr Dvorak <petr.dvorak@goodshape.cz>
 *
 */
class ChartContainer
{
	/** @var Array **/
	public $chartArray = array();
	protected $limits = array();
	protected $limitUnits = array();

	/**
	 * Přidání elementu do kontejneru
	 *
	 * @param SeriesElement $seriesElement
	 */
	public function addChart(ChartElement $chartElement)
	{
		$this->chartArray[] = $chartElement;
	}

	/**
	 * Převede kontejner na XML
	 */
	public function toXML()
	{
		$result = "<charts>";
		foreach ($this->chartArray as $chart)
		{
			$result .= $chart->toXML();
		}
		$result .= "</charts>";
		return $result;
	}

}
