<?php if (!defined('BASEPATH')) die('Access denied!');

/**
* Modul pro spravu a odesilani newsletteru
*/
class Newsletter extends Core
{

	// stavy newsletteru
	const NEWSLETTER_STATUS_NEW = 0;
	const NEWSLETTER_STATUS_WAITING = 1;
	const NEWSLETTER_STATUS_SENDING = 2;
	const NEWSLETTER_STATUS_SENT = 3;
	const NEWSLETTER_STATUS_GENERATING = 4;
	const NEWSLETTER_STATUS_ERROR_NO_TEMPLATE = 5;
	const NEWSLETTER_STATUS_ERROR_NO_GROUPS_SELECTED = 6;
	const NEWSLETTER_STATUS_ERROR_EMAIL_SENT_WITH_ERRORS = 7;
	const NEWSLETTER_STATUS_ERROR_ATTACHMENT_OVERSIZED = 8;
	const NEWSLETTER_STATUS_ERROR_ATTACHMENT_NOT_SENT = 9;
	const NEWSLETTER_STATUS_ERROR_ATTACHMENT_NOT_FOUND = 10;
	const NEWSLETTER_STATUS_ERROR_INVALID_QUEUE_AUTO_INCREMENT = 11;

	// stavy emailu ve fronte
	const MAIL_STATUS_WAITING = 0;
	const MAIL_STATUS_SENT = 1;
	const MAIL_STATUS_SEND_ERROR = 2;
	const MAIL_STATUS_CANCELED = 3;
	const MAIL_STATUS_TO_BE_DELETED = 4;
	const MAIL_STATUS_ATTACHMENT_ERROR = 5;

	/**
	* Konstruktor
	*/
	public function __construct($config)
	{
		parent::__construct($config);
	}

	/*
	* Funkce vrati seznam e-mailu prihlasenych k odberu newletteru
	* @param	int	limit
	* @param	int	offset
	* @return	XML	vysledek dotazu
	* @author	Hugo
	*/
	public function exportMailingList()
	{
		$this->checkComponentAccess('Subscribers');

		// vypis musi odpovidat aktualne nastavenym filtrum v katalogu

		$filters = XML(post('filters'));
		$columns = array(
			1 => 'id',
			2 => 'email',
			3 => 'first_name',
			4 => 'last_name',
			5 => 'note',
			6 => 'language_id',
			7 => 'groups',
			8 => 'num_sent',
			9 => 'num_views',
			10 => 'num_img_views',
			11 => 'num_clicks',
			12 => 'source',
			13 => 'ip',
			14 => 'created',
			15 => 'modified'
		);

		$wheres = array();

		if (count($filters) > 0)
		{
			foreach ($filters as $filter)
			{
				$source = $columns[(int) $filter['index']];

				switch ($filter['type'])
				{
					case 'text':
					{
						if (strlen((string)$filter->string) > 0) array_push($wheres, "AND $source LIKE %s", '%' . (string) $filter->string . '%');
						break;
					}

					case 'number':
					{
						if (strlen((string) $filter->from) > 0) array_push($wheres, "AND $source >= %f", (string) $filter->from);
						if (strlen((string) $filter->to) > 0) array_push($wheres, "AND $source <= %f", (string) $filter->to);
						if (strlen((string) $filter->equal) > 0) array_push($wheres, "AND $source = %f", (string) $filter->equal);
						break;
					}

					case 'date':
					{
						if (strlen((string) $filter->from) > 0) array_push($wheres, "AND DATE($source) >= %d", (string) $filter->from);
						if (strlen((string) $filter->to) > 0) array_push($wheres, "AND DATE($source) <= %d", (string) $filter->to);
						if (strlen((string) $filter->equal) > 0) array_push($wheres, "AND DATE($source) = %d", (string) $filter->equal);
						break;
					}

					case 'checkbox':
					case 'options':
					{
						if (strlen((string) $filter->mask) > 0)
						{
							array_push($wheres, "AND (0");
							$mask = (int)$filter->mask;
							for ($i = 0; $i < 32; $i++) if (($mask & (1 << $i)) > 0) array_push($wheres, "OR $source = %i", $i);
							array_push($wheres, ")");
						}
						break;
					}

					case 'set':
					{
						if (strlen($filter->mask) > 0)
							array_push($wheres, "AND ($source & %i", (int)$filter->mask, ') = %i', (int)$filter->mask);
						break;
					}

					case 'reference':
					{
						if (strlen((string) $filter->equal) > 0)
							if ($source == 'groups') array_push($wheres, "AND (SELECT COUNT(*) FROM [:data:newsletters_groups_subscribers] WHERE [:data:newsletters_groups_subscribers].[subscriber_id] = [:data:newsletters_mailing_list].[id] AND [:data:newsletters_groups_subscribers].[group_id] = %i) > 0", (string) $filter->equal);
							else array_push($wheres, "AND $source = %i", (string) $filter->equal);
						break;
					}

				}
			}
		}

		if (get('csv') == 1)
		{
			// vypis jako csv

			set_time_limit(0);
			if (strlen(session_id()) > 0) session_write_close();

			ob_start();
			@apache_setenv('no-gzip', 1);

			header('Pragma: public');
			header('Cache-Control: public, no-cache');
			header('Content-Type: text/csv; charset=utf-8');
			header('Content-Disposition: attachment; filename="mailing_list_export.csv"');
			header('Content-Transfer-Encoding: binary');
			$groups = dibi::query(
				"SELECT g.id, CONCAT('\"', name.text, '\"') AS name FROM [:data:newsletters_groups] AS g",
				"LEFT JOIN [:data:texts] AS name ON name.id = g.name AND name.language_id = %s", post('language_id')
			)->fetchPairs();
			echo '"' . t('all', $this->language_id) . '",' . implode(",", $groups);
			echo ',"' . t('none', $this->language_id) . '"' . "\n";

			$offset = 0;
			$limit = 1000;

			while (1 == 1)
			{
				$data = (array) dibi::query(
					"SELECT id, email FROM [:data:newsletters_mailing_list]",
					"WHERE 1 = 1 %ex", $wheres, "%lmt %ofs", $limit, $offset
				)->fetchPairs();

				if (count($data) == 0) exit();

				$groupData = (array) dibi::query(
					"SELECT subscriber_id, group_id",
					"FROM [:data:newsletters_groups_subscribers]",
					"WHERE subscriber_id IN %in", array_keys($data)
				)->fetchAll();

				$groupConnections = array();
				foreach ($groupData as $groupDataItem) {
					$g = (int) $groupDataItem['group_id'];
					$s = (int) $groupDataItem['subscriber_id'];
					if (!isset($groupConnections[$s])) $groupConnections[$s] = array();
					$groupConnections[$s][] = $g;
				}

				foreach ($data as $id => $email)
				{
					$columns = array();
					$numEmails = 0;
					$columns[] = '"' . $email . '"';
					foreach ($groups as $groupId => $group)
					{
						if (isset($groupConnections[(int) $id]) && in_array($groupId, $groupConnections[(int) $id])) 
						{
							$columns[] = '"' . $email . '"';
							$numEmails++;
						}
						else $columns[] = '""';
					}
					if ($numEmails == 0) $columns[] = '"' . $email . '"';
					else $columns[] = '""';
					echo implode(",", $columns);
					echo "\n";
				}

				$offset += $limit;

				ob_flush();
				flush();
			}
		}
		else
		{
			// textovy vypis

			$data = (array) dibi::query("SELECT email FROM [:data:newsletters_mailing_list] WHERE 1 = 1 %ex", $wheres)->fetchPairs();
			$num = count($data);

			echo "<h3>" . t('newsletter_mailing_List', $this->language_id) . "</h3>";

			if (count($filters) > 0)
			{
				$total = dibi::query("SELECT COUNT(id) FROM [:data:newsletters_mailing_list]")->fetchSingle();
				echo sprintf(t('export_filtered_n_of_m', $this->language_id), $num, $total) . ":<br/></br>";
			}
			else
			{
				echo sprintf(t('export_filtered_all_of_m', $this->language_id), $num) . ":<br/></br>";
			}

			echo implode(', ', $data);
		}
	}

	/**
	 * Brana, pres kterou jdou vsechny linky v newsletterech.
	 * Umi provst zobrazeni emailu na serveru, odhlaseni z newsletteru
	 * a pocita statistiky.
	 *
	 * @author Hugo
	 */
	public function gateway()
	{
		$id = $this->decryptInt(url(3));

		switch (url(4))
		{
			// zobraezni emailu na serveru

			case 'view':
			{
				$this->viewEmail($id);
				break;
			}

			// odhlaseni newsletteru

			case 'unsubscribe':
			{
				$languages = explode(" ", getConfigVariable("supportedLanguages"));
				if (count($languages) > 0) $l = $this->language_id . "/";
				else $l = '';
				redirect(BASEURL . $l . "newsletter/unsubscribe/" . url(3));
				break;
			}

			// ostatni pozadavky

			default:
			{
				if (url(4) == 'external' && isset($this->config['newsletter']['server_path']))
					$url = $this->config['newsletter']['server_path'] . urls(5);
				else
					$url = $this->config['site']['server_path'] . urls(4);

				// nactu z databaze potrebne udaje o danem emailu
				$email = dibi::query("SELECT newsletter_id, subscriber_id, to_email, num_img_views, num_clicks FROM [:data:newsletters_emails_sent] WHERE id = %i", $id)->fetch();

				if (preg_match('/(\.jpg|\.jpeg|\.gif|\.png)$/', $url) > 0) // jedna se o zobrazeni obrazku?
				{
					// upravim statistiku emailu
					dibi::query("UPDATE [:data:newsletters_emails_sent] SET num_img_views = num_img_views + 1 WHERE id = %i", $id);

					// pripadne upravim i statistiku newsletteru a uzivatele (pokud dosud upravena nebyla)
					if (element('num_img_views', $email) == 0)
					{
						dibi::query("UPDATE [:data:newsletters_archive] SET num_img_views = num_img_views + 1 WHERE id = %i", element('newsletter_id', $email));
						dibi::query("UPDATE [:data:newsletters_mailing_list] SET num_img_views = num_img_views + 1 WHERE id = %i", element('subscriber_id', $email));
					}
				}
				else // ... nebo jen kliknuti na link?
				{
					// upravim statistiku emailu
					dibi::query("UPDATE [:data:newsletters_emails_sent] SET num_clicks = num_clicks + 1 WHERE id = %i", $id);

					// upravim i statistiku newsletteru a uzivatele
					dibi::query("UPDATE [:data:newsletters_archive] SET num_clicks = num_clicks + 1 WHERE id = %i", element('newsletter_id', $email));
					dibi::query("UPDATE [:data:newsletters_mailing_list] SET num_clicks = num_clicks + 1 WHERE id = %i", element('subscriber_id', $email));

					// jsou-li zadany, pridam merici kody
					$utm = dibi::query(
						"SELECT utm_source, utm_medium, utm_campaign, utm_content FROM [:data:newsletters_archive] WHERE id = %i", element('newsletter_id', $email)
					)->fetch();

					$query = isset($_GET) ? $_GET : array();
					if (isset($query['q'])) unset($query['q']);
					$query = http_build_query($query);
					if (strlen($query) > 0) $query = "?" . $query;
					$url .= $query;

					$codes = '';
					if (strlen(element('utm_source', $utm)) > 0) $codes .= "&utm_source=" . urlencode(element('utm_source', $utm));
					if (strlen(element('utm_medium', $utm)) > 0) $codes .= "&utm_medium=" . urlencode(element('utm_medium', $utm));
					if (strlen(element('utm_campaign', $utm)) > 0) $codes .= "&utm_campaign=" . urlencode(element('utm_campaign', $utm));
					if (strlen(element('utm_content', $utm)) > 0) $codes .= "&utm_content=" . urlencode(element('utm_content', $utm));
					if (strlen($codes) > 0)
					{
						// pokud jeste url nema query string, musim jako prvni znak dat otaznik
						if (strpos($url, "?") === FALSE) $codes[0] = '?';

						// pripojim merici kody k URL
						$url .= $codes;
					}

				}

				// presmeruji na pozadovane url
				redirect($url);
			}
		}
	}

	/**
	 * Zobrazeni emailu z fronty na vystup
	 *
	 * @author Hugo
	 */
	protected function viewEmail($id)
	{
		// nactu z databaze potrebne udaje o danem emailu
		$email = dibi::query("SELECT subscriber_id, newsletter_id, to_email, num_views FROM [:data:newsletters_emails_sent] WHERE id = %i", $id)->fetch();

		if ((int) element('newsletter_id', $email) == 0) die('Email nebyl nalezen!');

		// upravim statistiku emailu
		dibi::query("UPDATE [:data:newsletters_emails_sent] SET num_views = num_views + 1 WHERE id = %i", $id);

		// pripadne upravim i statistiku newsletteru a uzivatele (pokud dosud upravena nebyla)
		if (element('num_views', $email) == 0)
		{
			dibi::query("UPDATE [:data:newsletters_archive] SET num_views = num_views + 1 WHERE id = %i", element('newsletter_id', $email));
			dibi::query("UPDATE [:data:newsletters_mailing_list] SET num_views = num_views + 1 WHERE id = %i", element('subscriber_id', $email));
		}

		// nactu vygenerovany email
		$body = dibi::query("SELECT generated_body FROM [:data:newsletters_archive] WHERE id = %i", element('newsletter_id', $email))->fetchSingle();
		// doplnim kody
		$body = strtr($body, array(
			"{emailCode}" => $this->encryptInt($id),
			"{userCode}" => $this->encryptInt(element('subscriber_id', $email)),
		));
		// zobrazim email na vystup
		echo $body;

		exit();
	}

	/*
	 * Funkce provede odhlaseni/prenastaveni newsltteru
	 * @param	int		id - id polozky v mailinglistu
	 * @param	string	hash - kontrolni hash
	 * @param	string	lang - zkratka jazyka
	 *
	 * @return	XML	succes/error
	 * @author	Hugo
	 */
	protected function unsubscribe($id)
	{
		global $config;
		$vars = array();
		$language = get('language', 'cs');

		// nactu z databaze potrebne udaje o danem emailu
		$email = dibi::query(
			"SELECT newsletter_id, subscriber_id, to_email, num_views, num_img_views, num_clicks, num_unsubscribed",
			"FROM [:data:newsletters_emails_sent]",
			"WHERE id = %i", $id
		)->fetch();
		$vars['email'] = element('to_email', $email);

		// zjistim id uzivatele
		$subscriberId = (int) dibi::query(
			"SELECT id FROM [:data:newsletters_mailing_list]",
			"WHERE id = %i AND language_id = %s", element('subscriber_id', $email), $language
		)->fetchSingle();

		if ($subscriberId === $this->decryptInt(get('code')))
		{
			if (post('confirm') == 1)
			{
				if (isValidEmail(element('to_email', $email)))
				{
					$current = dibi::query("SELECT [group_id] FROM [:data:newsletters_groups_subscribers] WHERE [subscriber_id] = %i", $subscriberId)->fetchPairs();
					$values = post('group') ? (array) post('group') : array();

					// zjistim, co je treba smazat a co vlozit
					$toDelete = array_diff($current, $values);
					$toInsert = array_diff($values, $current);

					// a provedu mazani
					if (count($toDelete) > 0)
					{
						dibi::query("DELETE FROM [:data:newsletters_groups_subscribers] WHERE [subscriber_id] = %i AND [group_id] IN (%in)", $subscriberId, $toDelete);

						// doslo k odhlaseni, upravim statistiku
						dibi::query("UPDATE [:data:newsletters_emails_sent] SET num_unsubscribed = num_unsubscribed + 1 WHERE id = %i", $id);

						// pripadne upravim i statistiku newsletteru (pokud dosud upravena nebyla)
						if (element('num_unsubscribed', $email) == 0)
							dibi::query("UPDATE [:data:newsletters_archive] SET num_unsubscribed = num_unsubscribed + 1 WHERE id = %i", element('newsletter_id', $email));

					}

					// provedu vlozeni
					foreach ($toInsert as $value)
						dibi::query("INSERT INTO [:data:newsletters_groups_subscribers] SET [subscriber_id] = %s, [group_id] = %s", $subscriberId, $value);

					// odstranim z fronty emaily, ktere by uzivatel jiz nemel obdrzet
					$this->removeUnsubscribedFromQueue($subscriberId, element('to_email', $email));

					$vars['result'] = 'success';

				} else $vars['result'] = 'invalid_email';
			}
		}
		else
		{
			$vars['error'] = 'invalid_hash';
		}

		$vars['groups'] = dibi::query(
			"SELECT [groups].[id], [name].[text] AS [name], [description].[text] AS [description], [subscribrers].[subscriber_id] > 0 AS [selected]",
			"FROM [:data:newsletters_groups] AS [groups]",
			"LEFT JOIN [:data:newsletters_groups_subscribers] AS [subscribrers] ON [subscribrers].[subscriber_id] = %i AND [subscribrers].[group_id] = [groups].[id]", $subscriberId,
			"LEFT JOIN [:data:texts] AS [name] ON [name].[id] = [groups].[name] AND [name].[language_id] = %s", $language,
			"LEFT JOIN [:data:texts] AS [description] ON [description].[id] = [groups].[description] AND [description].[language_id] = %s", $language,
			"WHERE [groups].[public] = 1 OR [subscribrers].[subscriber_id] > 0"
		)->fetchAssoc('id');

		// nacteni jazykovych konstant pro sablonu
		getLang('Newsletter/unsubscribe', $language, false);
		// zobrazeni sablony
		echo template('Newsletter/unsubscribe', $vars);

	}
	/**
	 * Odstrani z fronty emailu vsechny, ktere by uzivatele nemel vzhledem
	 * k aktualnimu nastaveni odeberu obdrzet
	 *
	 * @param int $subscriberId
	 */
	protected function removeUnsubscribedFromQueue($subscriberId, $email)
	{
		// odstranim z fronty emailu vsechny, ktere uz by uzivateli nemely prijit
		$current = dibi::query("SELECT [group_id] FROM [:data:newsletters_groups_subscribers] WHERE [subscriber_id] = %i", $subscriberId)->fetchPairs();

		$inQueue = dibi::query(
			"SELECT q.id, g.group_id FROM [:data:newsletters_email_queue] AS q",
			"LEFT JOIN [:data:newsletters_groups_archive] AS g ON g.newsletter_id = q.newsletter_id",
			"WHERE to_email = %s", $email
		)->fetchAssoc("id,group_id");

		foreach ($inQueue as $emailId => $emails)
		{
			$groupIds = array_keys($emails);
			$intersect = array_intersect($current, $groupIds);
			// odstranim emaily, ktere byly urceny skupinam, ktere jiz nejsou mezi aktulnimi prihlasenymi
			if (count($intersect) == 0) dibi::query("UPDATE [:data:newsletters_email_queue] SET status = %i WHERE id = %i", self::MAIL_STATUS_TO_BE_DELETED, $emailId);
		}

		// tyto emaily presunu do tabulky odstranenych
		dibi::query(
			"INSERT INTO [:data:newsletters_emails_sent] (id, newsletter_id, subscriber_id, to_email, status, send_on, created)", 
			"SELECT id, newsletter_id, subscriber_id, to_email, %i, send_on, created FROM [:data:newsletters_email_queue] WHERE status = %i", 
				self::MAIL_STATUS_CANCELED, self::MAIL_STATUS_TO_BE_DELETED
		);

		dibi::query("DELETE FROM [:data:newsletters_email_queue] WHERE status = %i", self::MAIL_STATUS_TO_BE_DELETED);
	}


	/**
	 * Zobrazi telo emailu v zadane sablone
	 * @param	subject			Předmět emailu
	 * @param	message			Tělo emailu
	 * @param	template		Použitá HTML šablona
	 * @param	language		Jazyk emailu (dvoupísmenná konstanta)
	 *
	 * @author	Hugo
	 */
	public function preview()
	{
		$data = anyFormData(post('data'));
		// nachystam si promenne
		$subject = $data['subject'];
		$message = $data['message'];
		$language = $data['language'];
		$template = dibi::query('SELECT filename FROM [:data:newsletters_templates] WHERE id = %i', $data['template'])->fetchSingle();

		// nacteni jazykovych konstant pro sablonu
		getLang($template, $language, false);

		$vars['baseURL'] = $this->config['site']['server_path'];
		$vars['langURL'] = $language.'/';
		$vars['subject'] = $subject;
		$vars['message'] = $message;
		$vars['language'] = $language;
		$vars['unsubscribeURL'] = '#';
		$vars['viewURL'] = '#';
		$vars['userId'] = 0;

		echo template("../../files/" . preg_replace('/\.php$/', '', $template), $vars);
	}

	/**
	 * Metoda pro sestavovani a odesilani fronty emailu, ktera je urcena pro volani jako CronJob.
	 * Mela by byt volana priblizne kazdych 5 minut.
	 *
	 * @param	limit		Maximální počet odeslaných emailů (default: 100)
	 * @author	Hugo
	 */
	public function cronJob()
	{
		set_time_limit(3600);

		header('Content-Type: text/plain; charset=utf-8');

		// spustim logovani
		dibi::query("INSERT INTO [:data:newsletters_cronjob_log]", array("script_start%sql" => "NOW()"));
		$logId = dibi::insertId();

		// spustim casomiru
		$t1 = microtime(TRUE);

		// vstup do kriticke sekce
		lock("newsletter_cronjob");

		// logovani - delka kriticke cekani na zamek
		$t2 = microtime(TRUE);
		dibi::query(
			"UPDATE [:data:newsletters_cronjob_log] SET blocking_duration = %f", $t2 - $t1,
			"WHERE id = %i", $logId
		);

		// vytvori frontu emailu, ktere maji byt odeslany
		$result = $this->fillEmailQueue();

		// logovani - delka vytvareni newsletteru
		$t3 = microtime(TRUE);
		dibi::query(
			"UPDATE [:data:newsletters_cronjob_log] SET",
			"creating_duration = %f, ", $t3 - $t2,
			"num_newsletters_used = %i, ", element('newsletters', $result),
			"num_emails_created = %i", element('emails', $result),
			"WHERE id = %i", $logId
		);

		// najednou odešleme zadany pocet emailu (implicitne 100)
		$limit = (int) get('limit', 100);
		$result = $this->sendEmailsInQueue($limit);
		// pokud nektere emaily nebyly odslany, bude to opakovat, aby byl splnen pozadovany limit
		while ($result['failed'] > 0)
			$result = $this->sendEmailsInQueue($result['failed']);


		// logovani - delka odesilani newsletteru
		$t4 = microtime(TRUE);
		dibi::query(
			"UPDATE [:data:newsletters_cronjob_log] SET",
			"script_stop = NOW(), ",
			"sending_duration = %f, ", $t4 - $t3,
			"total_duration = %f, ", $t4 - $t1,
			"num_emails_sent = %i, ", element('succeed', $result),
			"num_emails_failed = %i", element('failed', $result),
			"WHERE id = %i", $logId
		);

		unlock("newsletter_cronjob");
	}

	/**
	 * Na základě parametrů vygeneruje frontu emailů k odeslání.
	 *
	 * @return	XML	succes/error
	 * @author	Hugo
	 */
	protected function fillEmailQueue()
	{
		// pokud je newsletter novy, ale nema byt zatim odeslan, nastavim mu priznak "Ceka na odeslani"
		dibi::query("UPDATE [:data:newsletters_archive] SET status = %i WHERE status = %i AND send_immediately = 2 AND send_on > NOW()", self::NEWSLETTER_STATUS_WAITING, self::NEWSLETTER_STATUS_NEW);

		// zjistim newslettery, ktere maji byt odeslany
		$newsletters = dibi::query(
			"SELECT * FROM [:data:newsletters_archive]",
			"WHERE status < %i ", self::NEWSLETTER_STATUS_WAITING, // nove, dosud neodeslane nebo cekajici na odeslani
			"	AND (send_immediately = 1", // maji byt odeslany ihned
			"		OR send_immediately = 2 AND send_on <= NOW()", // nebo v zadane datum a to datum jiz probehlo
			"			AND send_on IS NOT NULL AND send_on != '0000-00-00 00:00:00')" // a je realne
		)->fetchAll();

		$total = 0;

		foreach ($newsletters as $newsletter)
		{
			// nachystam si promenne
			$id = element('id', $newsletter);
			$subject = element('subject', $newsletter);
			$message = element('message', $newsletter);
			$language = element('language', $newsletter);
			$targetGroups = dibi::query("SELECT [group_id] FROM [:data:newsletters_groups_archive] WHERE newsletter_id = %i", $id)->fetchPairs();
			$template = dibi::query('SELECT filename FROM [:data:newsletters_templates] WHERE id = %i', element('template', $newsletter))->fetchSingle();
			if (element('status', $newsletter) == self::NEWSLETTER_STATUS_SENDING
				&& strtotime(element('send_on', $newsletter)) > time()) $send_on = element('send_on', $newsletter);
			else $send_on = date("Y-m-d H:i:s");

			$isDelayed = element('is_delayed', $newsletter) == 1;
			$delayHours = (int) element('delay_hours', $newsletter);
			switch (element('delay_volume', $newsletter))
			{
				case 0: $delayVolume = 1 - 1/3; break;
				case 1: $delayVolume = 1 - 1/2; break;
				case 2: $delayVolume = 1 - 2/3; break;
				default: $delayVolume = 1;
			}

			// kontrola sablony
			if (!file_exists($this->config['site']['template_path'] . "../../files/" . preg_replace('/\.php$/', '', $template) . '.php'))
			{
				// chyba - no_template
				dibi::query("UPDATE [:data:newsletters_archive] SET status = %i WHERE id = %i", self::NEWSLETTER_STATUS_ERROR_NO_TEMPLATE, $id);
				continue;
			}
			// kontrola skupin
			if (count($targetGroups) == 0)
			{
				dibi::query("UPDATE [:data:newsletters_archive] SET status = %i WHERE id = %i", self::NEWSLETTER_STATUS_ERROR_NO_GROUPS_SELECTED, $id);
				continue;
			}
			// kontrola velikosti prilohy
			$attachments = dibi::query("SELECT id, CONCAT('" . $this->config['site']['files_path'] . "', file) FROM [:data:newsletters_attachments] WHERE newsletter_id = %i", $id)->fetchPairs();
			if (is_array($attachments) && count($attachments) > 0)
			{
				$size = 0;
				foreach ($attachments as $attachment)
				{
					if (!file_exists($attachment)) 
					{
						$size = FALSE; 
						break;
					}
					$size = filesize($attachment);
				}
				if ($size > 1024 * 1024 * 5)
				{
					dibi::query("UPDATE [:data:newsletters_archive] SET status = %i WHERE id = %i", self::NEWSLETTER_STATUS_ERROR_ATTACHMENT_OVERSIZED, $id);
					continue;
				}
				if ($size === FALSE)
				{
					dibi::query("UPDATE [:data:newsletters_archive] SET status = %i WHERE id = %i", self::NEWSLETTER_STATUS_ERROR_ATTACHMENT_NOT_FOUND, $id);
					continue;
				}
			}
			// kontrola auto-increment v tabulce fronty mailu
			$table = dibi::getConnection()->translate("[:data:newsletters_email_queue]");
			$table = str_replace("`", "'", $table);
			$queueTableStatus = dibi::query("SHOW TABLE STATUS LIKE $table")->fetch();
			$nextQueueId = element('Auto_increment', $queueTableStatus);
			$nextSentId = dibi::query("SELECT MAX(id) + 1 FROM [:data:newsletters_emails_sent]")->fetchSingle();
			if ($nextQueueId < $nextSentId)
			{
				dibi::query("UPDATE [:data:newsletters_archive] SET status = %i WHERE id = %i", self::NEWSLETTER_STATUS_ERROR_INVALID_QUEUE_AUTO_INCREMENT, $id);
				continue;
			}

			// nachystam promenne pro sablonu
			$vars['langURL'] = $language . '/';
			$vars['subject'] = $subject;
			$vars['message'] = $message;
			$vars['language'] = $language;
			$vars['baseURL'] = $this->config['site']['server_path'];
			$vars['viewURL'] = $vars['baseURL'] . 'view/';
			$vars['unsubscribeURL'] = $vars['baseURL'] . "unsubscribe/?language=$language&amp;code={userCode}";

			// nacteni jazykovych konstant pro sablonu
			getLang("../../files/" . preg_replace('/\.php$/', '', $template), $language, false);

			// vygenerovani emailu z sablony
			$body = template("../../files/" . preg_replace('/\.php$/', '', $template), $vars);

			// nahrazeni URL tak, aby smerovala na branu
			$baseURL = trim($vars['baseURL'], '/');
			$body = str_replace($baseURL, $baseURL . "/admin/newsletter/gateway/{emailCode}", $body);

			// ve specialnich pripadech, kdy je admin na jine domene nebo v jinem adresari nez cilovy web newsletteru
			if (isset($this->config['newsletter']['server_path']) && $this->config['newsletter']['server_path'] != $vars['baseURL'])
				$body = str_replace($this->config['newsletter']['server_path'], $vars['baseURL'] . "admin/newsletter/gateway/{emailCode}/external/", $body);

			// ulozim vygenerovany email, ktery se bude posilat
			dibi::query("UPDATE [:data:newsletters_archive] SET generated_body = %s WHERE id = %i", $body, $id);

			// nactu emaily uzivatelu, kteri si dany newsletter objednali
			$list = dibi::query(
				"SELECT m.id, m.email FROM [:data:newsletters_mailing_list] AS m",
				"INNER JOIN [:data:newsletters_groups_subscribers] AS g ON g.subscriber_id = m.id AND g.group_id IN %in", (array) $targetGroups,
				"WHERE language_id = %s", $language
			)->fetchPairs();

			$counter = 0;

			if (count($list) > 0)
			{
				foreach ($list as $subscriberId => $email)
				{
					$data = array(
						'newsletter_id'	=> $id,
						'subscriber_id'	=> $subscriberId,
						'to_email'		=> $email,
						'status'		=> self::MAIL_STATUS_WAITING,
						'send_on'		=> $send_on,
						'created%sql'	=> 'NOW()'
					);

					if ($isDelayed && $counter / count($list) > $delayVolume)
					{
						$data['send_on'] = date("Y-m-d H:i:s", strtotime("+$delayHours hours", strtotime($send_on)));
					}

					dibi::query("INSERT INTO [:data:newsletters_email_queue]", $data);

					$counter++;

				}
			}

			// nastavime priznak "probiha odesilani" a nastavime datum zacatku odeslani
			dibi::query(
				"UPDATE [:data:newsletters_archive] SET status = %i, sending_begin = NOW()",
				self::NEWSLETTER_STATUS_SENDING, "WHERE id = %i", $id
			);

			$total += $counter;

		}

		$num = count($newsletters);
		if ($num > 0) echo sprintf(t('num_emails_has_been_created', $this->language_id), $total, $num) . "\n";
		else echo t('no_newsletter_ready', $this->language_id) . "\n";

		return array('newsletters' => $num, 'emails' => $total);

	}

	/**
	 * Dekoduje cislo a overi kontrolni hash
	 *
	 * @param   string  $hash
	 * @return  int
	 */
	protected function decryptInt($hash)
	{
		$num = substr($hash, 4, 8);
		$num = (int) hexdec($num);
		$check = substr(md5($num . $this->config['site']['encryption_key']), 0, 8);
		if (substr($hash, 0, 4) . substr($hash, 12, 4) === $check) return $num ^ 65489456;
		else FALSE;
	}

	/**
	 * Zakoduje cislo a prida kontrolni hash
	 *
	 * @param  int  $num
	 * @return string
	 */
	protected function encryptInt($num)
	{
		$num = $num ^ 65489456;
		$hash = md5($num . $this->config['site']['encryption_key']);
		$hash = substr($hash, 0, 4) . sprintf("%08X", $num) . substr($hash, 4, 4);
		return strtolower($hash);
	}

	/**
	 * Z fronty je vyzvednuty zadany pocet emailu a jsou odeslany na zadanou adresu.
	 *
	 * @param	limit		Maximální počet odeslaných emailů (default: 100)
	 * @author Hugo
	 */
	protected function sendEmailsInQueue($limit)
	{
		// načtu frontu emailů
		$emails = dibi::query("SELECT * FROM [:data:newsletters_email_queue] WHERE status = %i AND send_on <= NOW() ORDER BY id ASC %lmt", self::MAIL_STATUS_WAITING, $limit)->fetchAll();
		$newsletters = array();

		$succeed = 0;
		$failed = 0;


		if (count($emails) > 0)
		{
			foreach ($emails as $email)
			{
				$newsletterId = element('newsletter_id', $email);

				if (!isset($newsletters[$newsletterId]))
				{
					$newsletters[$newsletterId] = dibi::query("SELECT * FROM [:data:newsletters_archive] WHERE id = %i", $newsletterId)->fetch();
					$sender = (array) dibi::query("SELECT [name], [email] FROM [:data:newsletters_senders] WHERE id = %i", element('sender', $newsletters[$newsletterId]))->fetch();
					$newsletters[$newsletterId]['from_email'] = element('email', $sender);
					$newsletters[$newsletterId]['from_name'] = element('name', $sender);
					$newsletters[$newsletterId]['attachments'] = dibi::query("SELECT id, CONCAT('" . $this->config['site']['files_path'] . "', file) FROM [:data:newsletters_attachments] WHERE newsletter_id = %i", $newsletterId)->fetchPairs();
				}

				$result = FALSE;
				$numAttachments = 0;

				if ($newsletters[$newsletterId] && $newsletters[$newsletterId]['from_email'] && $newsletters[$newsletterId]['from_name'])
				{
					// odešlu email
					require_once(BASEPATH.'/anyshape/libraries/phpmailer/class.phpmailer.php');

					$mail = new PHPMailer();

					// konfigurace pro STMP
					if (isset($config['email']['smtp']) && $config['email']['smtp'] == TRUE)
					{
						$mail->IsSMTP();
						$mail->Host = $this->config['email']['smtp_host'];
						$mail->Port = $this->config['email']['smtp_port'];
						$mail->Username = $this->config['email']['smtp_username'];
						$mail->Password = $this->config['email']['smtp_password'];
						if ($this->config['email']['smtp_ssl']) $mail->SMTPSecure == 'ssl';
						else if ($this->config['email']['smtp_tls']) $mail->SMTPSecure == 'tls';
					}

					$mail->From = $newsletters[$newsletterId]['from_email'];
					$mail->FromName = $newsletters[$newsletterId]['from_name'];
					$mail->AddAddress(element('to_email', $email));
					$mail->CharSet = 'UTF-8';
					$mail->Subject = $newsletters[$newsletterId]['subject'];
					$body = strtr($newsletters[$newsletterId]['generated_body'], array(
							"{emailCode}" => $this->encryptInt(element('id', $email)),
							"{userCode}" => $this->encryptInt(element('subscriber_id', $email)),
						));
					$mail->AltBody = html2text($body);
					$mail->MsgHTML($body);
					if (is_array($newsletters[$newsletterId]['attachments']) && count($newsletters[$newsletterId]['attachments']) > 0)
					{
						foreach ($newsletters[$newsletterId]['attachments'] as $attachment)
							if ($mail->AddAttachment($attachment)) $numAttachments++;
					}

					$result = $mail->Send();
				}

				$email['status'] = $result ? self::MAIL_STATUS_SENT : self::MAIL_STATUS_SEND_ERROR;
				if (is_array($newsletters[$newsletterId]['attachments']) 
					&& count($newsletters[$newsletterId]['attachments']) > 0
					&& count($newsletters[$newsletterId]['attachments']) != $numAttachments)
					$email['status'] = self::MAIL_STATUS_ATTACHMENT_ERROR;

				$email['sent_on%sql'] = 'NOW()';

				// presunu email z fronty do tabulky odeslanych emailu
				dibi::query("INSERT INTO [:data:newsletters_emails_sent]", $email);
				dibi::query("DELETE FROM [:data:newsletters_email_queue] WHERE id = %i", element('id', $email));

				// upravime statistiku newsletteru a uzivatele
				dibi::query("UPDATE [:data:newsletters_archive] SET num_sent = num_sent + 1 WHERE id = %i", element('newsletter_id', $email));
				dibi::query("UPDATE [:data:newsletters_mailing_list] SET num_sent = num_sent + 1 WHERE email = %s", element('to_email', $email));

				if ($result) $succeed++; else $failed++;

			}

			if ($succeed > 0) echo sprintf(t('emails_has_been_sent', $this->language_id), $succeed) ."\n";
			else t('no_email_has_been_sent', $this->language_id) . "\n";

			if ($failed > 0) echo sprintf(t('sending_emails_failed', $this->language_id), $failed) . "\n";

		}
		else
		{
			echo t('email_queue_is_empty_nothing_to_send', $this->language_id) . "\n";
		}

		// kontrola ukonceni rozesilani
		$active = dibi::query("SELECT id FROM [:data:newsletters_archive] WHERE status = %i", 
			self::NEWSLETTER_STATUS_SENDING)->fetchPairs();
		foreach ($active as $id)
		{
			// zjistim pocet emailu, ktere cekaji na odeslani
			$numQueue = dibi::query(
				"SELECT COUNT(id) FROM [:data:newsletters_email_queue] WHERE status = %i AND newsletter_id = %i", 
					self::MAIL_STATUS_WAITING, $id
			)->fetchSingle();

			// pokud jiz neni co rozesilat, prepnu newsletter do stavu "Odeslan" a nastavim datum ukonceni rozesilani
			if ($numQueue == 0) 
			{
				$status = self::NEWSLETTER_STATUS_SENT;

				// nastaly chyby?
				$fatal = dibi::query("SELECT COUNT(*) FROM [:data:newsletters_emails_sent] WHERE status = %i",
					self::MAIL_STATUS_SEND_ERROR)->fetchSingle();
				if ($fatal > 0) 
				{
					$status = self::NEWSLETTER_STATUS_ERROR_EMAIL_SENT_WITH_ERRORS;
				}
				else
				{
					$notFatal = dibi::query("SELECT COUNT(*) FROM [:data:newsletters_emails_sent] WHERE status = %i", 
						self::MAIL_STATUS_ATTACHMENT_ERROR)->fetchSingle();
					if ($notFatal > 0) $status = self::NEWSLETTER_STATUS_ERROR_ATTACHMENT_NOT_SENT;
				}
				dibi::query("UPDATE [:data:newsletters_archive] SET status = %i, sending_end = NOW() WHERE id = %i", $status, $id);
			}
		}

		return array('succeed' => $succeed, 'failed' => $failed);

	}

	/**
	 * Zruseni odesilani, tj. nastaveni vsech neodeslanych emailu ve fronte do stavu 4 = zruseno
	 */
	public function cancelQueue()
	{
		$this->checkComponentAccess('EmailQueue');

		dibi::query("UPDATE [:data:newsletters_email_queue] SET status = %i WHERE status = %i",
			self::MAIL_STATUS_CANCELED, self::MAIL_STATUS_WAITING);

		$num = dibi::affectedRows();

		dibi::query(
			"INSERT INTO [:data:newsletters_emails_sent] (id, newsletter_id, subscriber_id, to_email, status, send_on, created)",
			"SELECT id, newsletter_id, subscriber_id, to_email, status, send_on, created FROM [:data:newsletters_email_queue] WHERE status = %i",
			self::MAIL_STATUS_CANCELED
		);

		dibi::query("DELETE FROM [:data:newsletters_email_queue] WHERE status = %i",
			self::MAIL_STATUS_CANCELED);

		if ($num > 0)
			success(sprintf(t('email_queue_cancelled', $this->language_id), $num));
		else
			success("email_queue_is_empty");
	}

	/**
	 * Provedeni importu
	 */
	public function import()
	{
		$this->checkComponentAccess('MailingListImport');

		set_time_limit(0);
		register_shutdown_function(array($this, 'handleShutdown'));
		error_reporting(0);

		// nactu data z formulare
		$xml = XML(post('data'));
		$data = (string) $xml->panel[0]->record[0]->source[0]->import_data;
		$language = (string) $xml->panel[0]->record[0]->source[0]->language;
		$source = (string) $xml->panel[0]->record[0]->source[0]->source;
		$groupsXML = XML((string) $xml->panel[0]->record[0]->source[1]->name);

		if (count($groupsXML) == 0) error("no_group_selected_error");

		// nactu id skupin
		$groups = array();
		foreach ($groupsXML->children() as $g) $groups[] = (int) $g;

		// uvolnim pamet
		$xml = null;

		// nactu aktualni obsah mailing listu
		$current = dibi::query("SELECT REPLACE(LOWER(email), ' ', ''), id FROM [:data:newsletters_mailing_list]")->fetchPairs();

		$dup = 0;
		$new = 0;
		$inv = 0;

		$import = array();
		$email = '';

		// parsuju zadana data
		for ($i = 0; $i < strlen($data); $i++)
		{
			// dalsi znak
			$c = $data[$i];
			if (!$this->isSeparator($c)) $email .= strtolower($c);

			if ($this->isSeparator($c) || $i == strlen($data) - 1)
			{
				if (strlen($email) > 0)
				{
					if (isValidEmail($email)) // je email platny?
					{
						if (!isset($current[$email])) // neni duplikat?
						{
							// vlozim
							$id = $this->insertEmail($email, $language, $source, $groups);
							$current[$email] = $id;
							$new++;
						}
						else
						{
							$this->updateEmail($current[$email], $language, $source, $groups);
							$dup++;
						}
					}
					else
					{
						$inv++;
					}
					$email = '';
				}
			}
		}

		// ulozim log
		dibi::query("INSERT INTO [:data:newsletters_import_log]", array(
			'import_data'	=> $data,
			'language'	 	=> $language,
			'source'	 	=> $source,
			'created%sql'	=> "NOW()",
			'num_imported'	=> $new,
			'num_invalid'	=> $inv,
			'num_duplicates'=> $dup
		));

		$logId = dibi::insertId();

		foreach ($groups as $g) dibi::query("INSERT INTO [:data:newsletters_import_log_groups]", array(
			'group_id'	=> $g,
			'import_id'	=> $logId
		));

		if ($new > 1) $result = sprintf(t("mailinglist_import_new_many", $this->language_id), $new);
		else if ($new == 1) $result = t("mailinglist_import_new_one", $this->language_id);
		else $result = t("mailinglist_import_new_none", $this->language_id);

		if ($dup > 1) $result .= "\n" . sprintf(t("mailinglist_import_dup_many", $this->language_id), $dup);
		else if ($dup == 1) $result .= "\n" . t("mailinglist_import_dup_one", $this->language_id);

		if ($inv > 1) $result .= "\n" . sprintf(t("mailinglist_import_inv_many", $this->language_id), $inv);
		else if ($inv == 1) $result .= "\n" . t("mailinglist_import_inv_one", $this->language_id);

		success($result . "\n\n");

	}

	/**
	 * V pripade potizi zobrazi hlasku uzivateli
	 */
	protected function handleShutdown()
	{
        $error = error_get_last();

        if ($error !== NULL && @$error['type'] === E_ERROR)
        {
        	error('server_param_error', $error['message']);
        }
    }

	/**
	 * Vrati true, pokud je zadany znak oddelovac
	 */
	private function isSeparator($c) {
		return ($c == " " || $c == "\n" || $c == "\r" || $c == "," || $c == ";" || $c == "\t");
	}

	/**
	 * Vlozi emailovou adresu do mailing listu
	 */
	private function insertEmail($email, $language, $source, $groups)
	{
		dibi::query("INSERT INTO [:data:newsletters_mailing_list]", array(
			'email'	=> $email,
			'language_id' => $language,
			'created%sql' => 'NOW()',
			'source' => $source
		));

		$id = dibi::insertId();

		foreach ($groups as $g)
		{
			dibi::query("REPLACE INTO [:data:newsletters_groups_subscribers]", array(
				'group_id' => $g,
				'subscriber_id' => $id)
			);
		}

		return $id;
	}

	/**
	 * Vlozi emailovou adresu do mailing listu
	 */
	private function updateEmail($id, $language, $source, $groups)
	{
		dibi::query("UPDATE [:data:newsletters_mailing_list] SET", array(
			'language_id' => $language,
			'source' => $source
		), "WHERE id = %i", $id);

		foreach ($groups as $g)
		{
			dibi::query("REPLACE INTO [:data:newsletters_groups_subscribers]", array(
				'group_id' => $g,
				'subscriber_id' => $id)
			);
		}
	}

	/**
	 * Duplikace newsletteru
	 */
	public function duplicate()
	{
		$this->checkComponentAccess('Newsletters');

		// id označených záznamů
		$selected = boom(post('selected'));

		if (count($selected) > 0)
		{
			foreach ($selected as $id)
			{
				$data = dibi::query("SELECT * FROM [:data:newsletters_archive] WHERE id = %i", $id)->fetch();

				$data['id'] = NULL;
				$data['generated_body'] = "";
				$data['created'] = date("Y-m-d H:i:s");
				$data['send_immediately'] = 0;
				$data['status'] = self::NEWSLETTER_STATUS_NEW;
				$data['sending_begin'] = NULL;
				$data['sending_end'] = NULL;
				$data['num_sent'] = 0;
				$data['num_views'] = 0;
				$data['num_img_views'] = 0;
				$data['num_clicks'] = 0;
				$data['num_unsubscribed'] = 0;

				dibi::query("INSERT INTO [:data:newsletters_archive]", $data);
				$newId = dibi::insertId();

				dibi::query(
					"INSERT INTO [:data:newsletters_groups_archive] (group_id, newsletter_id)",
					"SELECT group_id, %i FROM [:data:newsletters_groups_archive] WHERE newsletter_id = %i", $newId, $id
				);
			}
			success(t("num_copies_created", $this->language_id) . ": " . count($selected));
		}
		else
		{
			error("no_item_selected");
		}
	}
}
