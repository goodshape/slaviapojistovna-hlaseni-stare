<?php if (!defined('BASEPATH')) die('Access denied!');

/* Konstanty */

/* Urovene pristupovych prav */

define('EDIT_ACCESS', 1);
define('ADMIN_ACCESS', 2);

/* Logovane operace */
define('LOG_STRUCTURE_PAGE_LOAD', 8);
define('LOG_STRUCTURE_PAGE_INSERT', 9);
define('LOG_STRUCTURE_PAGE_DELETE', 10);
define('LOG_STRUCTURE_PAGE_UPDATE', 11);
define('LOG_STRUCTURE_PAGE_RENAME', 12);

/* Typy logovany operaci */
define('LOG_READ', 1);
define('LOG_WRITE', 2);

/* Typy komponent */
define('SYSTEM_COMPONENT', 0);
define('LISTING_COMPONENT', 1);
define('EDIT_COMPONENT', 2);
define('CHART_COMPONENT', 3);
define('FOLDER_COMPONENT', 4);

/* Operace dle typu komponenty */
define('SYSTEM_COMPONENT_OPERATIONS', 'allow_access');
define('LISTING_COMPONENT_OPERATIONS', 'allow_access|add_item|edit_item|remove_item');
define('EDIT_COMPONENT_OPERATIONS', 'allow_access|edit_item');

/**
 * Základní třída, ze které jsou odvozeny další moduly. Třída obsahuje základní operace pro
 * práci ze systémem.
 */
class Core {

	/**
	 * Konfigurace
	 */
	public $config;

	/**
	 * Nastaveny frontendovy jazyk
	 */
	protected $language_id;

	/**
	 * Konstruktor
	 * Navaze spojeni s databazi a nastavit jazyk frontendu
	 * @author	Hugo
	 */
	public function __construct($config)
	{
		$this->config = $config;

		$class = safeStr(url(1));
		$method = safeStr(url(2));

		try
		{
            dibi::setConnection(getDibiConnection());
		}
		catch (DibiException $e)
		{
			error('database_error');
		}

		$this->language_id = post('language_id', session('language_id'));
		if ($this->language_id == false) {
			if (count(dibi::query("SHOW TABLES LIKE %s", $this->config['database']['prefix'].'languages')))
				$this->language_id = dibi::query("SELECT id FROM [:as:languages] LIMIT 1")->fetchSingle();
			else
			$this->language_id = 'cz';
		}
		if (strlen($this->language_id) != 2) $this->language_id = 'cz';
		setSession('language_id', $this->language_id);

		// simulace pomaleho spojeni a chyb
		//sleep(2);
		//echo '-';
		//error('bb', 'aa');
	}

	/**
	 * Funkce vrati seznam jazyku pro administraci i frontend nebo primo cely XML soubor s jazykem pro backend
	 * @param	string	neni-li zadana promenna name, vrati seznam moznych jazyku, jinak vrati XML soubor pro backend
	 * @return	XML
	 * @author	Hugo
	 */
	public function getLang()
	{
		$name = str_replace('/../', '', get('name', ''));
		$dir = dirname(__FILE__).'/languages/';
		if ($name == '') {
			$files = directoryMap($dir);
			$langs = array();
			foreach ($files as $file) {
				if (!is_array($file)) {
					$xml = simplexml_load_file($dir.$file);
					$backend[] = array(
						'filename' => $file,
						'id' => (string) $xml['id'],
						'name' => (string) $xml['name'],
						'adverb' => (string) $xml['adverb'],
						'icon' => (string)$xml['icon']
					);
				}
			}
			$frontend = dibi::query("SELECT * FROM [:as:languages] WHERE active = 1")->fetchAll();
			returnXML(array('backend' => $backend, 'frontend' => $frontend));
		} else {
			if (file_exists($dir.$name)) {
				header('Content-type: text/xml');
				readfile($dir.$name);
			}
			else {
				error('language_not_found');
			}
		}
	}

	/**
	 * Funkce vrati licencni cislo
	 * @return	XML
	 * @author	Hugo
	 */
	public function getLicence() {
		echo $this->config['site']['license_key'];
	}

	/**
	 * Funkce provede zapis akce do Logu
	 * @param	int	action - id provedene akce
	 * @param	int	component_id - id komponenty, ktera akci provedla (0 = jadro, 1 = basicHTML, 2 = catalog, 3 = filesystem ...)
	 * @param	int	typ akce - 1 = cteni, 2 = zapis
	 * @param	mixed	diff - v pripade destruktivni akce obsahuje provedene zmeny
	 * @return	boolean	true - v pripade, ze dojde k vlozeni zaznamu do logu - jinak false
	 * @author	Hugo
	 *
	 * Definované akce:
	 *
	 *    1 - přihlášení do systému
	 *    2 - odhlášení ze systému
	 *    3 - neplatné přihlášení
	 *    4 - načtení seznamu modulů
	 *    5 - načtení seznamu komponent
	 *    6 - načtení detailu komponenty
	 *    7 - načtení struktury webu
	 *    8 - načtení jednoho uzlu struktury
	 *    9 - vložení nového uzlu do struktury
	 *    10 - smazání uzlu ve struktuře
	 *    11 - provedení změn v uzlu struktury
	 *    12 - přejmenovaní uzlu ve struktuře
	 *    13 - načtení HTML obsahu uzlu
	 *    14 - uložení HTML obsahu uzlu
	 *    15 - přesun nebo změna pořadí uzlů ve struktuře
	 *    16 - kopírování uzlů ve struktuře
	 *    17 - načtení informací o uživatelích a skupinách
	 *    18 - načtení informací o uživateli
	 *    19 - uložení informací o uživateli
	 *    20 - vytvoření nového uživatele
	 *    21 - smazání uživatele
	 *    22 - načtení informací o skupině
	 *    23 - uložení informací o skupině
	 *    24 - vytvoření nové skupiny
	 *    25 - smazání skupiny
	 *    26 - zařazení uživatelů do skupin
	 *    27 - odstranění uživatelů ze skupin
	 *    28 - načtení stromu modulů, komponent a operací
	 *    28 - nastavení práv na komponenty
	 *    29 - nastavení práv na operace
	 *    30 - uložení uživatelského nastavení
	 *    31 - načtení seznamu v katalogu
	 *    32 - načtení položky v katalogu
	 *    33 - načtení seznamu v katalogu z referenční tabulky (pro select)
	 *    34 - uložení položky v katalogu
	 *    35 - vložení nové položky do katalogu
	 *    36 - změna dat v souvisejcí tabulce, při ukládání položky v katalogu
	 *    37 - smazání položky v katalogu
	 */
	protected function log($action, $component_id, $type = 1, $diff = null, $item_id = 0, $item_name = '') {
		if ($this->config['site']['log'] == 1 && $type == 1) return false;
		if ($this->config['site']['log'] == 0) return false;
		$data = array(
			'component_id' 	=> $component_id,
			'action' 		=> $action,
			'item_id' 		=> $item_id,
			'item_name' 	=> $item_name,
			'user_id' 		=> session('logged_id'),
			'diff' 			=> serialize($diff),
			'ip'			=> element('REMOTE_ADDR', $_SERVER),
			'user_agent'	=> element('HTTP_USER_AGENT', $_SERVER)
		);
		dibi::query("INSERT INTO [:as:log]", $data);
		return (dibi::insertId() > 0);
	}

	/**
	 * Funkce rozparsuje zadane XML dle jazyku a ulozi jednotlive texty do tabulky data_texts
	 * @param	string	XML s ruznymi jazykovymi variantami textu
	 * @param	boolean	pokud je true, texty se ulozi pod novym id (jako nove)
	 * @return	int		id ulozenych textu (unikatni jen ve spojeni s language_id)
	 * @author	Hugo
	 */
	protected function saveTexts($xmlString, $new = false) {
		$xml = XML($xmlString);
		if ($xml == NULL) return NULL;
		$id = 0;
		$count = 0;
		foreach ($xml->children() as $object) {
			$count++;
			if ($object->id*1 > 0) {
				$id = $object->id*1;
				break;
			}
		}
		if ($count == 0) return null;
		if ($id == 0 || $new) $id = dibi::query("SELECT id FROM [:data:texts] ORDER BY id DESC LIMIT 1")->fetchSingle() * 1 + 1;
		$savedLanguages = array();
		foreach ($xml->children() as $object) {
			$data = Array(
				"id"			=> $id,
				"language_id"	=> (string)$object->language_id,
				"text"			=> (string)$object->text
			);
			dibi::query("REPLACE INTO [:data:texts]", $data);
			$savedLanguages[] = (string)$object->language_id;
		}
		// ostatni jazyky, ktere nebyly zadany, je mozno z dabataze odstranit
		if (count($savedLanguages) > 0) dibi::query("DELETE FROM [:data:texts] WHERE id = %i", $id, " AND language_id NOT IN (%s", $savedLanguages, ")");
		else dibi::query("DELETE FROM [:data:texts] WHERE id = %i", $id);
		return $id;
	}

	/**
	 * Funce v poli $result (vysledek dotazu z databaze) nahradi polozky zadane pomoci pole $columns, polem s jednotlivymi
	 * preklady.
	 * @param	array	Pole vysledku, ve kterem maji byt texty doplneny
	 * @return	array	Pole sloupcu ve vysledku, ktere maji byt doplneny (nazvy indexu v poli $result)
	 * @return	bool	Vrati jednotliva pole serializovana jako string
	 * @author	Hugo
	 */
	protected function loadTexts($result, $columns) {
		foreach ($columns as $column) {
			if (element($column, $result)*1 > 0) $result[$column] = $this->loadText(element($column, $result));
		}
		return $result;
	}

	/**
	 * Funce vrati pole s prekladem textu zadaneho parametrym
	 * @param	array	Pole vysledku, ve kterem maji byt texty doplneny
	 * @return	array	Pole sloupcu ve vysledku, ktere maji byt doplneny (nazvy indexu v poli $result)
	 * @author	Hugo
	 */
	protected function loadText($id) {
		return (array) dibi::query("SELECT * FROM [:data:texts] WHERE id = %i", $id)->fetchAll();
	}

	/**
	 * Funkce smaze z databaze texty, na ktere odkazuje zadany zaznam
	 * @param	string	Nazev tabulky, ze ktere se maji mazat zaznamy
	 * @param	array	Idcka zaznamu v tabulce, ktere je treba smazat (jako pole cisel nebo jedno cislo)
	 * @param	array	Nazvy sloupcu v tabulce, ktere jsou vicejazycne, tj. predstavuji reference do tabulky textu
	 * @param	boolean	Ma se smazat i samotny zaznam v tabulce (true) nebo jen napojene texty (false)?
	 * @param	string	Nazev primarniho klice v dane tabulce - casem mozno rozsirit na array s vice klici - HonzaB
	 * @return	int		Pocet ovlivnenich zaznamu v DB
	 * @author	Hugo
	 * @author	HonzaB
	 */
	protected function deleteTexts($table, $ids, $columns, $delete = false, $db_key = 'id')
	{
		if (!is_array($ids)) $ids = array($ids);
		$rows = 0;
		foreach ($ids as $id)
			if ($id > 0)
			{
				if (is_array($columns) && count($columns) > 0)
				{
					$record = dibi::query("SELECT * FROM [$table] WHERE %sql = %i", $db_key, $id)->fetch();

					foreach ($columns as $column)
					{
						if (element($column, $record)*1 > 0)
						{
							dibi::query("DELETE FROM [:data:texts] WHERE id = %i", element($column, $record));
							$rows += dibi::affectedRows();
						}
					}
				}
				if ($delete) dibi::query("DELETE FROM [$table] WHERE %sql = %i", $db_key, $id);
			}
		$rows += dibi::affectedRows();
		return $rows;
	}

	/**
	 * Stav prihlaseni
	 * @return	boolean	true = prihlasen, false = neprihlasen
	 * @author	Hugo
	 */
	public function isLogged()
	{
		return session('logged');
	}

	/*
	 * Kontrola prihlaseni uzivatele
	 * @return	xml	Neni-li nikdo prihlasen, vrati chybu login_required u ukonci skript
	 * @author	Hugo
	 */
	public function checkLogin()
	{
		if (!$this->isLogged()) error('login_required');
	}

	/**
	 * Vrati id modulu dle jeho nazvu
	 * @param	string	nazev modulu
	 * @return	int	id modulu
	 * @author	Hugo
	 */
	protected function getModuleId($module)
	{
		return dibi::query(	"SELECT id FROM [:as:modules] WHERE name = %s", $module)->fetchSingle()*1;
	}

	/**
	 * Vrati id komponenety dle jejiho nazvu
	 * rovnou skonci errorem.
	 * @param	int		nazev komponenty
	 * @return	int		id komponenty
	 * @author	Hugo
	 */
	protected function getComponentId($componentName)
	{
		return 	(int) dibi::query("SELECT id FROM [:as:components] WHERE name = %s", $componentName)->fetchSingle();
	}

	/*
	* Kontrola opravneni aktualne prihlaseneho uzivatele na zadanou komponentu a operaci. Pri neopravnem pristupu
	* rovnou skonci errorem.
	* @param	int		nazev komponenty (pokud je zadano null, kontroluje se jen dle component_id)
	* @param	string	nazev operace (mozne operace viz modules.php, implicitne allow_access)
	* @param	int		id komponenty v pripade, ze komponenta muze mit vice instanci
	* @return	xml		Neni-li pravo na danou operaci, vrati chybu access_denied a skonci
	* @author	Hugo
	*/
	protected function checkComponentAccess($component, $operation = 'allow_access', $component_id = 0)
	{
		if ($this->isAllowed($component, $operation, $component_id)) return true;
		error('access_denied');
	}

	/**
	 * Kontrola opravneni aktualne prihlaseneho uzivatele na zadanou komponentu a operaci
	 * @param	int		nazev komponenty (pokud je zadano null, kontroluje se jen dle component_id)
	 * @param	string	nazev operace (mozne operace viz modules.php, implicitne allow_access)
	 * @param	int		id komponenty v pripade, ze komponenta muze mit vice instanci
	 * @return	xml		Neni-li pravo na danou operaci, vrati chybu access_denied a skonci
	 * @author	Hugo
	 */
	protected function isAllowed($component, $operation = 'allow_access', $component_id = 0)
	{
		if (!$this->isLogged()) error('login_required');

		$user_id = session('logged_id');

		if ($user_id == 0) return true;
		if ($component === null && $component_id == 0) return false;

		$result = dibi::query(
			"SELECT COUNT(*) FROM [:as:groups_components] AS rights",
			"INNER JOIN [:as:components] AS component ON 1",
			// pokud je zadan nazev komponenty
			"%if", $component !== null,
			"AND component.name = %s", $component,
			"%end",
			// pokud je zadano id komponenty
			"%if", $component_id > 0,
			"AND component.id = %i", $component_id,
			"%end",
			"INNER JOIN [:as:users_groups] AS grp ON grp.user_id = %i", $user_id, "AND grp.group_id = rights.group_id AND rights.component_id = component.id",
			"WHERE rights.operation = %s", $operation
		)->fetchSingle();

		return ($result > 0);
	}

	/**
	 * Kontrola opravneni aktualne prihlaseneho uzivatele na zadany uzel
	 * @param	int		id uzlu
	 * @param	int		parametr je povazovan za uroven (level) opraveneni.
	 *					level = 1 - editace, level = 2 - administrace
	 * @return	xml		Neni-li pravo na danou operaci, vrati chybu access_denied a skonci
	 * @author	Hugo
	 */
	protected function checkNodeAccess($id, $level = 0)
	{
		if (!$this->isLogged()) error('login_required');
		$user_id = session('logged_id');
		if ($user_id == 0) return true;
		$result = dibi::query(
					"SELECT COUNT(groups_structure.structure_id) FROM [:as:groups_structure] AS groups_structure",
					"INNER JOIN [:as:users_groups] AS users_groups ON users_groups.user_id = %i", $user_id, "AND users_groups.group_id = groups_structure.group_id",
					"WHERE (groups_structure.structure_id = %i", $id, " AND groups_structure.level >= %i", $level, ");"
		)->fetchSingle();
		if ($result == 0) error('access_denied');
	}

	/**
	 * Vrati seznam modulu v systemu
	 * @author	Hugo
	 */
	public function getModules()
	{
		$this->checkComponentAccess('ModuleAdmin');
		$this->log(4, 0);
		$result = dibi::query("SELECT id, name FROM [:as:modules]")->fetchAll();
		returnXML($result);
	}

	/**
	 * Vrati seznam modulu v systemu
	 * @author	Hugo
	 */
	public function addModule()
	{
		$this->checkComponentAccess('ModuleAdmin', 'administrate_components');
		dibi::query("INSERT INTO [:as:modules] (`name`) VALUES (%s)", post('name'));
		success(1);
	}

	/**
	 * Smazani modulu
	 * @param	int	id zaznamu
	 * @return	XML	success nebo error
	 * @author	Hugo
	 */
	public function deleteModule()
	{
		$this->checkComponentAccess('ModuleAdmin', 'administrate_components');
		$id = post('id');
		$count = 0;
		if ($id > 0) {
			dibi::query("DELETE FROM [:as:modules] WHERE id = %i", $id);
			$count = dibi::affectedRows();
		}
		success($count);
	}

	/**
	 * Vrati seznam komponent v systemu
	 * @author	Hugo
	 */
	public function getComponents()
	{
		$this->checkComponentAccess('ModuleAdmin');
		$this->log(5, 0);
		$result = dibi::query("SELECT id, IFNULL(folder_id, '-') AS folder_id, name, alias, module_id, type, dashboard, `order`, icon16x16, icon32x32 FROM [:as:components]")->fetchAll();
		returnXML($result);
	}

	/**
	 * Vrati jeden zaznam komponenty v systemu vcetne jeji konfigurace a nastaveni jazyku.
	 * Komponentu je nutne specifikovat pomoci promenne id nebo pomoci jejiho nazvu.
	 * @author	Hugo
	 */
	public function getComponent()
	{
		$this->checkLogin();
		$this->log(6, 0);
		$id = post("id", get("id"))*1;
		if ($id == 0)
			$result = dibi::query("SELECT id, folder_id, name, alias, module_id, type, dashboard, `order`, config, icon16x16, icon32x32 FROM [:as:components] WHERE name = %s", post('name'))->fetchAll();
		else
			$result = dibi::query("SELECT id, folder_id, name, alias, module_id, type, dashboard, `order`, config, icon16x16, icon32x32 FROM [:as:components] WHERE id = %i", $id)->fetchAll();
		returnXML($result);
	}

	/**
	 * Vrati komponenty, ktere je mozno priradit do struktury
	 * @author	Hugo
	 */
	public function getAssignableComponents()
	{
		$this->checkLogin();
		$this->log(5, 0);
		$result = dibi::query(
			"SELECT components.id, components.name, components.alias, components.module_id, components.type, modules.name AS module, icon16x16",
			"FROM [:as:components] AS components ",
			"INNER JOIN [:as:modules] AS modules ON components.module_id = modules.id",
			"ORDER BY modules.id"
		)->fetchAll();
		returnXML($result);
	}

	/**
	 * Vrati komponenty typu slozka
	 * @author	Hugo
	 */
	public function getFolderComponents()
	{
		$this->checkLogin();
		$this->log(5, 0);
		$result = dibi::query(
			"SELECT components.id, components.name, components.alias, components.module_id, components.type, modules.name AS module, icon16x16",
			"FROM [:as:components] AS components ",
			"INNER JOIN [:as:modules] AS modules ON components.module_id = modules.id",
			"WHERE components.type = %i", FOLDER_COMPONENT,
			"ORDER BY modules.id"
		)->fetchAll();
		$result = array_merge(array("-"), $result);
		returnXML($result);
	}

	/**
	 * Vytvori nebo provede update komponenty (vcetne souvisejicich operaci)
	 * @return	XML	success nebo error
	 */
	public function updateComponent()
	{
		$this->checkComponentAccess('ModuleAdmin', 'administrate_components');
		$id = post('id')*1;
		$module_id = (int) post('module_id');
		$config = post('config', false);
		if ($config === false || $module_id == 0) error('server_param_error');

		switch ((int) post('type', 1))
		{
			case LISTING_COMPONENT: $operations = LISTING_COMPONENT_OPERATIONS; break;
			case EDIT_COMPONENT: $operations = EDIT_COMPONENT_OPERATIONS; break;
			default: $operations = SYSTEM_COMPONENT_OPERATIONS;
		}

		if ($id == 0) {

			// vytvoreni nove komponenty

			$data = array(
				'folder_id'	=> ((int) post('folder_id')) ?: NULL,
				'name'		=> post('component'),
				'module_id'	=> $module_id,
				'type'		=> (int) post('type', 1),
				'alias'		=> post('alias'),
				'config'	=> $config,
				'dashboard'	=> post('dashboard', 0),
				'order'		=> post('order', 0),
				'operations'=> $operations,
				'icon16x16'	=> post('icon16x16', 'EMPTY_NODE'),
				'icon32x32'	=> post('icon32x32', 'BLANK')
			);
			dibi::query("INSERT INTO [:as:components] ", $data);
			$id = dibi::insertId();

		} else {

			// update existujici komponenty

			$data = array(
				'folder_id'	=> ((int) post('folder_id')) ?: NULL,
				'name'		=> post('component'),
				'module_id'	=> $module_id,
				'type'		=> (int) post('type', 1),
				'alias' 	=> post('alias'),
				'config' 	=> $config,
				'dashboard'	=> post('dashboard', 0),
				'order'		=> post('order', 0),
				'operations'=> $operations,
				'icon16x16'	=> post('icon16x16', 'EMPTY_NODE'),
				'icon32x32'	=> post('icon32x32', 'BLANK')
			);
			dibi::query("UPDATE [:as:components] SET ", $data, "WHERE id = %i", $id);

		}

		success($id);
	}

	/**
	 * Vytvori nebo provede update komponenty (vcetne souvisejicich operaci)
	 * @return	XML	success nebo error
	 */
	public function updateComponentConfig()
	{
		$this->checkComponentAccess('ModuleAdmin', 'administrate_components');
		$id = post('id')*1;
		$config = post('config', false);
		if ($config === false) error('server_param_error');

		// update existujici komponenty

		$data = array(
				'config' 	=> $config,
		);
		dibi::query("UPDATE [:as:components] SET ", $data, "WHERE id = %i", $id);

		success($id);
	}

	/**
	 * Smazani komponenty
	 * @param	int	id zaznamu
	 * @return	XML	success nebo error
	 * @author	Hugo
	 */
	public function deleteComponent()
	{
		$this->checkComponentAccess('ModuleAdmin', 'administrate_components');
		$id = post('id');
		$count = 0;
		if ($id > 0) {
			dibi::query("DELETE FROM [:as:components] WHERE id = %i", $id);
			$aff = dibi::affectedRows();
			$count += $aff;
			dibi::query("DELETE FROM [:as:groups_components] WHERE component_id = %i", $id);
			$aff = dibi::affectedRows();
			$count += $aff;
		}
		success($count);
	}

	/**
	 * Vrati seznam sablon pro layouty
	 * @return	XML	success nebo error
	 * @author	Hugo
	 */
	public function getLayouts()
	{
		$this->checkLogin();
		$this->checkComponentAccess('PageTypeAdmin');
		$result = directoryMap('layouts');
		if ($result) foreach ($result as &$item) $item = str_replace('.php', '', $item);
		returnXML($result);
	}

	/**
	 * Vrati seznam typu stranek
	 * @return	XML	success nebo error
	 * @author	Hugo
	 */
	public function getPageTypes()
	{
		$this->checkLogin();
		$result = dibi::query("SELECT *, 
			(SELECT type FROM [:as:components] WHERE id = component_id) AS component_type
			FROM [:as:page_types] ORDER BY name ASC")->fetchAll();
		returnXML($result);
	}

	/**
	 * Ulozi pole typu stranek
	 * @return	XML	success nebo error
	 * @author	Hugo
	 */
	public function savePageTypes()
	{
		$this->checkLogin();
		$this->checkComponentAccess('PageTypeAdmin');
		$data = XML("<node>".post('data')."</node>") or error('xml_error');
		$ids = array();
		foreach ($data->children() as $node) {
			$id = (int) $node->id;
			$save = array(
				'name' 			=> (string) $node->name,
				'main_layout' 	=> (string) $node->main_layout,
				'detail_layout'	=> (string) $node->detail_layout,
				'component_id'	=> (string) $node->component_id,
				'icon'			=> (string) $node->icon
			);
			if ($id > 0) {
				dibi::query('UPDATE [:as:page_types] SET', $save, 'WHERE id = %i', $id);
				$ids[] = $id;
			} else {
				dibi::query('INSERT INTO [:as:page_types]', $save);
				$ids[] = dibi::insertId();
			}
		}
		if (count($ids) > 0) dibi::query('DELETE FROM [:as:page_types] WHERE id NOT IN %in', $ids);
		else dibi::query('DELETE FROM [:as:page_types]');
		success(1);
	}

	/**
	 * Vytvoreni zalohy databaze
	 */
	public function databaseBackup()
	{
		$this->checkLogin();
		$this->checkComponentAccess('GlobalSettings', 'backup_database');

		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Content-Type: application/x-gzip");
		$time = date("Y-m-d_H-i-s");
		header("Content-Disposition: attachment; filename=db-backup-$time.sql.gz;");
		header("Content-Transfer-Encoding: binary");

		ob_start('gzencode', 1e6);

		$driver = dibi::getConnection()->getDriver();
		$limit = 1000;

		echo "-- AnyShape dump\n";
		echo "SET NAMES utf8;\n";
		echo "SET foreign_key_checks = 0;\n";
		echo "SET time_zone = 'SYSTEM';\n";
		echo "SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';\n\n";

		dibi::query("SET NAMES utf8");

		foreach (dibi::query('SHOW TABLES') as $row)
		{
			$table = reset($row);

			echo "DROP TABLE IF EXISTS `$table`;\n\n";
			$row = dibi::fetch('SHOW CREATE TABLE %n', $table);
			echo $row['Create Table'], ";\n\n";

			$offset = 0;
			$columnsQuoted = array();
			$count = dibi::query('SELECT COUNT(*) FROM %n', $table)->fetchSingle();
			$columns = (array) dibi::query('SHOW COLUMNS FROM %n', $table)->fetchAssoc('Field');
			foreach ($columns as $column) $columnsQuoted[] = "`" . $column['Field'] . "`";

			while ($offset < $count)
			{
				echo "INSERT INTO `$table` (" . implode(', ', $columnsQuoted) . ") VALUES\n";

				$result = dibi::query('SELECT * FROM %n %lmt %ofs', $table, $limit, $offset)->fetchAll();
				$i = 0;

				foreach ($result as $row)
				{
					foreach ($row as $field => $value)
					{
						if ($value === NULL) {
							$row[$field] = 'NULL';
						}
						else {
							if (substr($columns[$field]['Type'], 0, 3)  == 'int' || substr($columns[$field]['Type'], 0, 7)  == 'tinyint') {
								$row[$field] = $value;
							} else {
								$type = dibi::TEXT;
								$row[$field] = $driver->escape($value, $type);
							}
						}
					}
					echo "(" . preg_replace('/,\s$/', '', implode(",\t", (array) $row)) . ")";
					if ($i + 1 == count($result)) echo ";"; else echo ",";
					echo "\n";
					$i++;
				}

				$offset += $limit;

				echo "\n";
			}

		}

		exit(0);
	}

}

