<?php if (!defined('BASEPATH')) die('Access denied!');

require_once __DIR__."/bootstrap.php";
return;

	require_once(BASEPATH . '/anyshape/helpers/config.php');

	$config = array();

	/* název aplikace */
	$config['site']['title'] = 'AnyShape';

	/* verze AnyShape, která se bude zobrazovat v administračním rozhraní */
	$config['site']['version'] = '2.1.2';

	/* licencni klic */
	$config['site']['licence_key'] = '';

	/* heslo spravce (zakodovane) */
	$config['site']['root_password'] = 'feced49e2caa95110b88bdae';

	/* url aplikace ukoncene lomitkem */
	$config['site']['server_path'] = detectServerPath();

	/* sul pro hashovani */
	$config['site']['encryption_key'] = getConfigVariable('encryptionKey');

	/* uroven logovani (0 - nelogovat, 1 - logovat jen zapis, 2 - logovat zapis i cteni */
	$config['site']['log'] = 1;

	/* prefix pro session */
	$config['site']['session_prefix'] = 'anyshape_';

	/* moznost logovani do FireBug konzole (pomoci FirePHP pluginu) */
	$config['site']['debug'] = isLocal();

	/* slozka dostupna ve spravci souboru v AnyShape */
	$config['site']['files_dir'] = 'files/';
	$config['site']['files_path'] = BASEPATH . '/' . $config['site']['files_dir'];
	/* slozka pro ukladani souboru nedostupnych ve spravci souboru */
	$config['site']['data_dir'] = 'data/';
	$config['site']['data_path'] = BASEPATH . '/' . $config['site']['data_dir'];
	/* slozka pro ukladani cache souboru */
	$config['site']['cache_dir'] = $config['site']['data_dir'].'cache/';
	$config['site']['cache_path'] = BASEPATH . '/' . $config['site']['cache_dir'];
	/* slozka pro ukladani zmensenych obrazku */
	$config['site']['resized_dir'] = $config['site']['data_dir'].'resized/';
	$config['site']['resized_path'] = BASEPATH . '/' . $config['site']['resized_dir'];
	/* slozka pro ukladani temp souboru */
	$config['site']['temp_dir'] = $config['site']['data_dir'].'temp/';
	$config['site']['temp_path'] = BASEPATH . '/' . $config['site']['temp_dir'];
	/* slozka pro ukladani obrazku po aplikaci vodoznaku */
	$config['site']['watermark_dir'] = $config['site']['data_dir'].'watermarked/';
	$config['site']['watermark_path'] = BASEPATH . '/' . $config['site']['watermark_dir'];
	/* slozka pro kos, kde jsou presouvany smazane soubory */
	$config['site']['recycle_bin_dir'] = $config['site']['data_dir'].'recycle_bin/';
	$config['site']['recycle_bin_path'] = BASEPATH . '/' . $config['site']['recycle_bin_dir'];
	/* slozka pro skripty volane pomoci 'call' */
	$config['site']['scripts_dir'] = 'app/scripts/';
	$config['site']['scripts_path'] = BASEPATH . '/' . $config['site']['scripts_dir'];
	/* slozka se sablonami */
	$config['site']['template_dir'] = 'app/templates/';
	$config['site']['template_path'] = BASEPATH . '/' . $config['site']['template_dir'];

	/* znakova sada pro pristup k souborum na serveru */
	$config['site']['filesystem_charset'] = 'ISO-8859-2';

	/* spojeni s databazi */
	$config['database'] = getAppDatabaseConfig();

	/* ftp hack pro safe_mode */
	$config['ftp']['hack'] = ini_get('safe_mode');
	$config['ftp']['host'] = 'localhost';
	$config['ftp']['username'] = '';
	$config['ftp']['password'] = '';
	/* umisteni korenoveho adresare aplikace na ftp (zakonceno lomitkem) */
	$config['ftp']['path'] = '/web/';

	/* implicitni prazdny obrazek */
	$config['images']['blank_image'] = $config['site']['data_path'] . 'anyshape/blank.png';
	/* kvalita ztratove komprese (0-100) */
	$config['images']['quality'] = 90;

	/* zasilani emailu */

	/* pouzit pro odesilani SMTP server? */
	$config['email']['smtp'] = false;
	$config['email']['smtp_host'] = 'smtp.goodshape.cz';
	$config['email']['smtp_port'] = 25;
	$config['email']['smtp_ssl'] = false;
	$config['email']['smtp_tls'] = false;
	$config['email']['smtp_username'] = '';
	$config['email']['smtp_password'] = '';

	/* systemove emaily a kopie vsech zasilanych emailu budou zasilany na */
	$config['email']['system'] = "helpdesk@goodshape.cz";
	/* vsechny odesilane emaily budou min nasledujiciho odesilatele */
	$config['email']['from'] = "helpdesk@goodshape.cz";
	$config['email']['from_name'] = "GoodShape";

	/* URL kam smerovat odkazy v newsletteru. Je nutne menit jen v pripade, ze admin bezi
	 * na jine domene nebo v jinem adresari nez samotny web */
	$config['newsletter']['server_path'] = $config['site']['server_path'];
