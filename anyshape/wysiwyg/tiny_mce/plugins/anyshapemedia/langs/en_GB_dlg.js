tinyMCE.addI18n('en.anyshapemedia_dlg',{
title:"Insert or edit files",
settings:"Settings",
flash_video:"Flash video (FLV)",
youtube_video:"Youtube video",

flv_file:"Video file",
img_file:"Start image",
dimensions:"Dimensions",
autoplay:"Autoplay",
volume:"Volume",
bar:"Control bar",
show_bar:"Show control bar",
always:"Always",
never:"Never",
autohide:"Autohide",
show_stop:"Show stop button",
show_volume:"Show volume control",
show_fullscreen:"Allow fullscreen",
show_time:"Show time",

code:"Video code",
put_code_here:"Put generated video code here..."

});