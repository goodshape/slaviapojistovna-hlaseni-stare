<?php
require_once __DIR__ .'/../bootstrap.php';
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		
		<title>wysiwiyg</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8"/>

		<style type="text/css">
			v\:* {behavior:url(#default#VML);}
			* { padding: 0; margin: 0; }
			body { width: 100%; height: 100%; overflow: hidden; }
			#iframe_wysiwyg { width: 100%; height: 100%; }
		</style>

		<script type="text/javascript" src="./tiny_mce/tinymce.min.js"></script>
		<script type="text/javascript" src="./tiny_mce/plugins/compat3x/plugin.js"></script>

		<script type="text/javascript">
// 			tinyMCE_GZ.init({
// 				plugins : "table,advimage,advlink,inlinepopups,contextmenu,paste,advimagescale,media,anyshapemedia,multicss",
// 				themes : 'advanced',
// 				languages : 'en,cs',
// 				disk_cache : true,
// 				debug : false
// 			});
		</script>

		<script type="text/javascript">

			//<![CDATA[

				tinymce.init({
					// General options
					mode: "textareas",
					theme: "modern",
					plugins: [
						'advlist autolink lists link image charmap print preview hr anchor pagebreak',
						'searchreplace wordcount visualblocks visualchars code fullscreen',
						'insertdatetime media nonbreaking save table contextmenu directionality',
						'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc',
						'anyshapemedia multicss',
					],
					width: "100%",
					max_width: 900,
					height: "100%",
					
					toolbar1: 'undo redo | insert | styleselect formatselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media | pastetext paste |  preview | codesample',
					toolbar2: '',
					media_live_embeds: false,
					image_advtab: true,
					menu: {
						file: {title: 'File', items: 'newdocument'},
						edit: {title: 'Edit', items: 'undo redo | cut copy paste pastetext | selectall'},
						insert: {title: 'Insert', items: 'link media | template hr'},
						view: {title: 'View', items: 'visualaid'},
						format: {title: 'Format', items: 'bold italic underline strikethrough superscript subscript | formats formatselect | removeformat'},
						table: {title: 'Table', items: 'inserttable tableprops deletetable | cell row column'},
						tools: {title: 'Tools', items: 'spellchecker code'}
					},
					block_formats: 'Paragraph=p;Heading 2=h2;Heading 3=h3;Heading 4=h4',

					// Image resize plugin
					cleanup_on_startup: false,
					advimagescale_max_width: 400,
					advimagescale_max_height: 400,
					advimagescale_min_width: 16,
					advimagescale_min_height: 16,

					// Povolene elementy
					theme_advanced_blockformats : "p,h2,h3,h4",
					valid_elements :
						"@[id|class|title|style]," +
						"a[rel|rev|name|href|target|title|style]," +
						"-strong/b,-em/i,-strike,-u," +
						"-sub,-sup," +
						"#p[align|style]," +
						"+span[style]," +
						"-ol[type|compact]," +
						"-ul[type|compact]," +
						"-li," +
						"br,img[src|alt=|style|title|hspace|vspace|width|height|align]," +
						"-h2,-h3,-h4,-h5,-h6," +
						"+table[class|style|width|border|cellspacing|cellpadding|bgcolor|align],+tbody,+thead,+tfoot," +
						"+tr[class|style|bgcolor]," +
						"+td[class|style|width|height|colspan|rowspan|align|valign|bgcolor]," +
						"+th[class|style|width|height|colspan|rowspan|align|valign|bgcolor]," +
						"+object[*],+embed[*],+param[*]",
					verify_css_classes: true,
					inline_styles: false,
					// imlicitini id layoutu
					body_id: 'default',

					// styly pro editor (vsechny v jednom souboru rozlisene pomoci id)
					content_css: '<?=$config['site']['server_path']?>webtemp/css/mce.css',

					// nazvy a id ruznych rozvrzeni v ramci css (ve fromatu Nazev=id,Nazev2=id2,...)
					css_ids: 'Hlavní styl=default,Alternativní styl=alternative',
					
					// jednotlive styly, ktere budou uzivatelum k dispozici v nabidce editoru ve skupinach dle id layoutu
					style_formats : [
						{title: 'Šedá', inline: 'span', classes: 'c-shade'},
						{title: 'Oranžová', inline: 'span', classes: 'c-brick'},
						{title: 'VELKÁ', inline: 'span', classes: 'upper'},
						{title: 'malé písmo', inline: 'span', classes: 'small'},

					],

					init_instance_callback: wysiwygInit,
					entity_encoding: 'raw',
					language: '<?= get('lang', 'cz') == 'cz' ? 'cs_CZ' : 'en_GB' ?>',
					file_browser_callback: anyshapeFileBrowser,
					relative_urls: false,
					remove_script_host: false,
					document_base_url: '<?=$config['site']['server_path']?>',
					external_link_list_url: '<?=$config['site']['server_path']?>anyshape/wysiwyg/urllist.php',
					verify_html : false,
					paste_auto_cleanup_on_paste: true,
					extended_valid_elements :"*[*]"

				});

				var wysiwygInit = function(editor) {
					// event handlery na klavesy (ESC)
					editor.on("KeyPress", keyHandler);
				}

				function anyshapeFileBrowser(field_name, url, type, win) {
					var anyShape = parent.document.getElementById('AnyShape');
					browserWin = win;
					type = type.replace(/^media$/, 'video')
					if (type == "image" || type == "file" || type == "video") anyShape.showFileManager(type, url);
					last_field_name = field_name;
				}
				
				function insertFile(src, name, description) {
					var hrefInput = browserWin.document.getElementById(last_field_name);
					var altInput = browserWin.document.getElementById('alt');
					var titleInput = browserWin.document.getElementById('title');
					if (hrefInput) hrefInput.value = src;
					if (altInput && description) altInput.value = description;
					if (titleInput && name) titleInput.value = name;
				}

				function keyHandler(event) {
					var anyShape = parent.document.getElementById('AnyShape');
					if (event.keyCode == 27 && anyShape) anyShape.closePanel();
				}

				function setContent(text) {
					tinymce.activeEditor.setContent(unescape(text));
				}

				function getContent() {
					return escape(tinymce.activeEditor.getContent());
				}
			//]]>
		</script>
	</head>

	<body>
		<textarea id="iframe_wysiwyg" name="wysiwyg" rows="15" cols="80"></textarea>
	</body>
	<style>
		.mce-edit-area {
			height: calc(100vh - 111px);
		}
	</style>
</html>