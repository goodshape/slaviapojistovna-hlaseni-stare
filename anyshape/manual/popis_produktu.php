<p>
Systém <strong>AnyShape CMS</strong> je moderní systém pro správu obsahu pro internet a intranet. Klientská část využívá moderní technologii Adobe Flex. Díky tomu je možné systém považovat za Rich Internet Application se všemi výhodami z toho plynoucími (klávesové zkratky, rychlejší reakce grafického rozhraní, uživatelsky přívětivé chování, drag &amp; drop, apod.). Instalace systému je implementována na míru každému internetovému projektu dle jeho potřeb, čímž nejsou klientovy požadavky limitovány technickými či grafickými omezeními. Systém umožňuje komplexní správu uživatelských práv.
</p>

<p>Systém <strong>AnyShape CMS</strong> najde využití všude tam, kde je potřeba spravovat a publikovat obsah pro internet či intranet. Mezi obvyklé nasazení produktu patří internetové firemní prezentace, webové portály a elektronické obchody (eshopy). Produkt umožňuje jednoduše, přehledně a intuitivně spravovat a publikovat nejrůznější druhy dat od jednoduchých či strukturovaných textů, katalogů, tabulek a seznamů až po soubory, obrázky či videoklipy. 
</p>

<h4>Vlastnosti produktu</h4>
<ul>
	<li>Rich Internet Application</li>
	<li>Klávesové zkratky</li>
	<li>Pohodlná správa struktury webu (drag &amp; drop, obnova smazaných stránek)</li>
	<li>Snadná správa databázových dat (vyhledávání a řazení)</li>
	<li>Hromadné akce nad spravovanými záznamy (export, tisk, mailing, apod.)</li>
	<li>Intuitivní souborový manažer (upload více souborů najednou, náhledy, koš)</li>
	<li>Důraz na jednoduchost a intuitivnost (rychlá orientace v systému i pro laiky)</li>
	<li>Panely pro práci s více záznamy současně</li>
	<li>Cena</li>
</ul>

