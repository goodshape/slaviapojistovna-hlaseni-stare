<h4>Instalace a konfigurace</h4>

<p>
Instalace systému AnyShape CMS je prováděna vývojáři společnosti GoodShape již při vývoji samotné webové prezentace. V aktuální verzi tedy není třeba provádět žádné zásahy do nastavení systému.
</p>

<h4>Požadavky na server</h4>
<ul>
	<li>PHP 5.2 a vyšší</li>
	<li>Apache 2.0 s mod_rewrite</li>
	<li>MySQL 5.0.32 a vyšší</li>
</ul>

<h4>Požadavky na klienta</h4>
<ul>
	<li>Firefox 1.5+, Internet Explorer 6.0+, Opera 9.5+, Google Chrome 1.0+ nebo Safari 3+</li>
	<li>Adobe Flash 9.5 a vyšší</li>
</ul>

