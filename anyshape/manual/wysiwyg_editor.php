<p>
	Pro práci s formátovaným textem je AnyShape vybaven <a href="http://cs.wikipedia.org/wiki/WYSIWYG">WYSIWYG</a> HTML editorem. 
	Abyste plně porozuměli způsobu práce s WYSIWYG editorem, je třeba uvést několik základních faktů a některé zákonitosti, které se týkají formátovaného textu na webových stránkách. Tyto informace Vám usnadní práci a naučíte se tak dosahovat požadovaného rozvržení textu bez zbytečných nedorozumění a frustrací.
</p>

<p class="image">
	<img src="img13.png" alt="Obr.13 WYSIWYG editor"/>
	Obr.13 WYSIWYG editor
</p>

<h4>Struktura textu</h4>

<p>Text webových stránek je strukturován do tzv. <strong>elementů</strong>. Rozlišujeme:</p>
<ul><li><strong>elementy blokové</strong> (např. odstavec, nadpis, seznam s odrážkami, číslovaný seznam, tabulka, apod.) a</li><li><strong>elementy řádkové</strong> (např. tučný text, text se specifickým stylem, odkaz, apod.).</li></ul>
<p>
	<strong>Řádkové elementy</strong> jsou součástí blokových elementů a lze je kombinovat (např. text může být zároveň tučný i podtržený). Řádkový element si lze například představit jako část textu v rámci jednoho odstavce, který se formátováním odlišuje od ostatního textu. Řádkový element vznikne například vytvořením odkazu z části věty.
</p>

<p style="border: 1px solid #C0C0C0; background: #f8f8f8; margin: 20px 0; padding: 5px; font-size: 11px;">
	Text v rámci blokového elementu, text v rámci blokového elementu, text v rámci blokového elementu, <span style="border: 1px solid gray; background: #e0e0e0; ">řádkový element, řádkový element, řádkový element, řádkový element, řádkový element</span>, text v rámci blokového elementu, text v rámci blokového elementu, text v rámci blokového elementu, text v rámci blokového elementu, 
	text v rámci blokového elementu, text v rámci blokového elementu
</p>

<p class="image">
	Obr.14 Vztah řádkového a blokového HTML elementu
</p>

<p>
	<strong>Blokové elementy</strong> nelze kombinovat, vždy začínají na novém řádku a tvoří tak ve stránce vertikálně uspořádané pravoúhlé oblasti stejné šířky, mezi kterými může být definováno odsazení. Šířka bloků je dána celým volným prostorem vymezeným pro daný obsah. Jako příklad rozvržení HTML stránky může posloužit následující schéma na obr.&nbsp;15. Je zřejmě, že všechny elementy zobrazené na obr.&nbsp;15 jsou blokové až na obrázky, které jsou řádkové, jelikož jsou zobrazeny v rámci blokového elementu odstavec. 
</p>

<div style="border: 1px solid #C0C0C0; background: #fafafa; margin: 20px 0; padding: 5px;">
	<span class="small">Webová stránka</span>
	<div style=" background: #e0e0e0; margin-left: 20%; margin-right: 20%; margin-bottom: 20px; padding: 10px;">
		<span class="small">Prostor vymezený pro HTML text</span>
		<div style="border: 1px dotted gray; background: #f0f0f0; margin-bottom: 5px; padding: 5px;" >
			<span class="small"><strong>Nadpis</strong></span>
		</div>
		<div style="border: 1px dotted gray; background: #f0f0f0; margin-bottom: 5px; padding: 5px; height: 70px;" >
			<span class="small">Odstavec</span>
			<div style="border: 1px dotted gray; background: #e0e0e0; margin-bottom: 5px; padding: 5px; height: 30px; width: 40px;" >
				<span class="small">Obrázek</span>
			</div>
		</div>
		<div style="border: 1px dotted gray; background: #f0f0f0; margin-bottom: 5px; padding: 5px; height: 50px;" >
			<span class="small">Odstavec</span>
		</div>
		<div style="border: 1px dotted gray; background: #f0f0f0; margin-bottom: 5px; padding: 5px;" >
			<span class="small"><strong>Nadpis</strong></span>
		</div>
		<div style="border: 1px dotted gray; background: #f0f0f0; margin-bottom: 5px; padding: 5px; height: 50px;" >
			<span class="small">Odstavec</span>
		</div>
		<div style="border: 1px dotted gray; background: #f0f0f0; margin-bottom: 5px; padding: 5px; height: 50px;" >
			<span class="small">Tabulka</span>
		</div>
		<div style="border: 1px dotted gray; background: #f0f0f0; margin-bottom: 5px; padding: 5px; height: 40px;" >
			<span class="small">Číslovaný seznam</span>
		</div>
		<div style="border: 1px dotted gray; background: #f0f0f0; margin-bottom: 5px; padding: 5px; height: 70px;" >
			<span class="small">Odstavec</span>
			<div style="border: 1px dotted gray; background: #e0e0e0; margin-bottom: 5px; padding: 5px; height: 30px; width: 40px;" >
				<span class="small">Obrázek</span>
			</div>
		</div>
	</div>
</div>

<p class="image">
	Obr.15 Příklad HTML stránky jako blokové schéma
</p>

<h4>Význam tlačítek</h4>

<table>
	<tr>
		<td><img src="buttons01.png" alt="Tlačíka"/></td>
		<td><strong>Vrácení a opakování provedených operací</strong> (Zpět / Opakovat)</td>
	</tr>

	<tr>
		<td><img src="buttons02.png" alt="Tlačíka"/></td>
		<td><strong>Změna stylu písma</strong> (Tučné / Kurzíva / Podtržené / Přeškrtnuté / Dolní index / Horní index</td>
	</tr>

	<tr>
		<td><img src="buttons03.png" alt="Tlačíka"/></td>
		<td><strong>Změna zarovnání aktuálního bloku</strong> (Doleva / Na střed / Doprava / Do bloku)
		</td>
	</tr>

	<tr>
		<td><img src="buttons04.png" alt="Tlačíka"/></td>
		<td><strong>Vytvoření specifických blokových elementů</strong> (Seznam s odrážkami / Číslovaný seznam / Tabulka)</td>
	</tr>

	<tr>
		<td><img src="buttons05.png" alt="Tlačíka"/></td>
		<td><strong>Výběr rozvržení pro editor.</strong> Je vhodné vybrat rozvržení odpovídající stylu stránky, do které bude editovaný text vložen. Tak bude zajištěno, že v editovaný text bude již při editaci zobrazen stejně jako ve stránce.
		</td>
	</tr>

	<tr>
		<td><img src="buttons06.png" alt="Tlačíka"/></td>
		<td>
			<strong>Výběr předdefinovaných stylů,</strong> které lze aplikovat jak na řádkové tak i blokové elementy. Tyto styly jsou dány grafickým návrhem
			projektu.
		</td>
	</tr>

	<tr>
		<td><img src="buttons07.png" alt="Tlačíka"/></td>
		<td><strong>Změna typu blokového elementu.</strong> Např. změna odstavce na nadpis apod.</td>
	</tr>

	<tr>
		<td><img src="buttons08.png" alt="Tlačíka"/></td>
		<td><strong>Vytvoření specifických řádkových elementů</strong> (Odkaz / Zrušit odkaz / Vložit kotvu / Vložit obrázek / Vložit video)</td>
	</tr>

	<tr>
		<td><img src="buttons09.png" alt="Tlačíka"/></td>
		<td><strong>Zobrazení zdrojového kódu HTML pro daný text.</strong>. Funkce je vhodná je pro pokročilé uživatele se znalostí značkovacího jazyka HTML.</td>
	</tr>
</table>

<h4>Tvorba bloků</h4>

<p>
	Pokud otevřete WYSIWYG editor na prázdné stránce, je implicitně vytvořen nový blokový element <strong>odstavec</strong>. Změnit charakter aktuálního bloku, tj. bloku, kde se nachází kurzor, lze pomocí nabídky formát. Můžete tak udělat z odstavce nadpis apod.
</p>
<p>
	<strong>Nový blok vytvoříte pomocí klávesy &lt;ENTER&gt;</strong>. Rozhodně nepoužívejte klávesu &lt;ENTER&gt; k přechodu na nový řádek! K tomu slouží kombinace kláves <strong>&lt;SHIFT&gt;+&lt;ENTER&gt;</strong>. Ačkoliv toto chování je běžné i v textových editorech jako je např. Microsoft Word, většina uživatelů si není této skutečnosti vědoma a vytváří tak pro každý řádek nový odstavec.
	Pokud stisknete klávesu &lt;ENTER&gt; uprostřed existujícího bloku, je tento blok v daném místě rozdělen na dva bloky.
</p>
<p>
	Bloky typu číslovaný seznam, seznam s odrážkami a tabulka je nutné vkládat pomocí ikon umístěných v horním panelu. 
</p>

<h4>Originál obrázku na kliknutí (Lightbox)</h4>

<p>
	Pokud je nutné vložit do textu zmenšený obrázek, který se po kliknutí zobrazí v původní velikosti (tzv. Lightbox), doporučujeme následující postup:
</p>

<ul>
	<li>Vložte do textu obrázek a zmenšete jej na požadovanou velikost</li>
	<li>Ponechejte obrázek vybraný a vytvořte z něj odkaz pomocí ikony pro vložení odkazu</li>
	<li>Jako <strong>URL odkazu</strong> zvolte obrázek v původní velikosti</li>
	<li>Pokud si přejete u obrázku zobrazit popis, vložte jej do pole <strong>Titulek</strong></li>
	<li>Na kartě <strong>Rozšířené</strong> nastavte položku <strong>Vztah stránky k cíli</strong> jako <strong>Lightbox</strong></li>
</ul>

<p>
	Pro správnou funkci tohoto postupu je nutné, aby byla funkce Lightbox v projektu implementována a aktivní.
</p>