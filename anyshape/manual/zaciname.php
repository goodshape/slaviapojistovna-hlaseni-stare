<h4>Základní koncepce</h4>

<p>
Každá webová prezentace, portál, eshop či aplikace (dále jen projekt) postavená na <strong>AnyShape CMS</strong> se rozděluje na dvě části:
</p>

<ul>
	<li><p><strong>Frontend</strong> - hlavní a prezentační část projektu, která je dostupná všem návštěvníkům webové prezentace. Tato část je obvykle dostupná po zadání URL adresy projektu do adresního řádku prohlížeče (např. http://www.mujprojekt.cz/). Pojetí frontendu se liší projekt od projektu a vždy vychází z požadavků klienta.</p></li>
	<li><p><strong>Backend</strong> - administrátorská část projektu, která je přístupná jen autorizovaným uživatelům a slouží pro administraci obsahu projektu, který je prezentován na frontendu. Tato část je obvykle dostupná po zadání URL adresy projektu spolu s příponou "admin" (např. http://www.mujprojekt.cz/admin/). <strong>Tato příručka tvoří uceleného průvodce celým tímto administračním rozhraním.</strong> </p></li>
</ul>

<h4>Přihlášení</h4>

<p>
	Pro přihlášení do administrační části projektu použijte URL adresu projektu spolu s příponou "admin" (např. http://www.mujprojekt.cz/admin/). Budete vyzvání k zadání Vašeho uživatelského jména a hesla. Během přihlašování je možné si vybrat i jazyk administrátorského rozhraní. V aktuální verzi AnyShape CMS je podporována čeština a angličtina.
</p>

<p class="image">
	<img src="img01.png" alt="Obr.1 Přihlašovací obrazovka"/>
	Obr.1 Přihlašovací obrazovka
</p>

<p>
	Platnost přihlášení je vždy omezena na určitou dobu. Tato doba může být různá a je dána nastavením serveru, na kterém projekt běží. V případě delší uživatelovy nečinnosti tedy platnost přihlášení vyprší a je nutné provést přihlášení znovu. V takovém případě však nedojde k přerušení rozdělané práce, ale zobrazí dialogové okno, ve kterém je uživatel vyzván k opakovanému zadání svých přihlašovacích údajů. Po jejich zadání může pokračovat v práci na stejném místě, kde se nacházel před vypršením platnosti přihlášení.
</p>

<h4>Rozvržení uživatelského rozhraní</h4>

<p>
	Uživatelské administrační rozhraní je rozvrženo do tří částí:	
</p>

<ul>
	<li>Pracovní plocha (hlavní panel)</li>

	<li>Panel struktury projektu (levý panel)</li>

	<li>Navigační lišta (horní pruh)</li>
	
</ul>

<p class="image">
	<img src="img02.png" alt="Obr.2 Uživatelské rozhraní AnyShape CMS"/>
	Obr.2 Uživatelské rozhraní AnyShape CMS
</p>

<h5>Pracovní plocha</h5>
	<p>Pracovní plocha, která zabírá největší část uživatelského rozhraní, obsahuje aktuálně spuštěnou <strong>komponentu</strong>. Pojem komponenta v AnyShape CMS označuje funkční součást systému sloužící pro správu některé specifické části projektu. Jako příklady komponent můžeme uvést například Souborový manažer, pro správu souborů na serveru, komponentu Anketa pro správu anket prezentovaných na stránkách projektu, komponentu Blog pro psaní článků apod. Po přihlášení se zobrazí komponenta Nástěnka, která obsahuje přehled všech implementovaných komponent v projektu a umožňuje tak jejich rychlé spouštění. </p>

<h5>Navigační lišta</h5>
<p>Navigační lišta se nachází v horní části uživatelského rozhraní a obsahuje seznam právě otevřených panelů, pro jejich snadné přepínání. Jelikož je každá spouštěná komponenta otevřena jako nový panel, je možné pracovat s více otevřenými komponentami současně a volně mezi nimi přecházet, což umožňuje pracovat efektivněji (usnadňuje to např. přenášení dat mezi jednotlivými záznamy apod.).</p>

<p>V pravé části navigační lišty se nachází tři servisní tlačítka:</p>

<ul>
	<li>tlačítko pro vyvolání nápovědy,</li>
	<li>tlačítko pro změnu uživatelského nastavení a</li>
	<li>tlačítko pro odhlášení ze systému.</li>
</ul>

<h5>Přepínání panelů</h5>

<p>Mezi panely lze efektivně přecházet pomocí horkých kláves. Pro přechod na vedlejší panel lze použít klávesovou zkratky &lt;CTRL&gt;+&lt;PAGE&nbsp;UP&gt; (na levý panel) nebo &lt;CTRL&gt;+&lt;PAGE&nbsp;DOWN&gt; (na pravý panel). Při změně panelu tímto způsobem dojde k přepnutí okamžitě.</p>
<p>Druhou možností přechodu mezi panely je použití klávesové zkratky &lt;CTRL&gt;+&lt;TAB&gt;, čímž je možné panely procházet podobně jako funguje přepínaní aplikací v operačním systému. Panely jsou přepínány ve stejném pořadí, v němž byly aktivovány. Jednoduché stisktutí kláves &lt;CTRL&gt;+&lt;TAB&gt; tak způsobí přepnutí na naposledy aktivní panel. K přepnutí na daný panel nedojde okamžitě, ale až po uvolnění klávesy &lt;CTRL&gt;. Lze použít i kombinaci kláves &lt;CTRL&gt;+&lt;SHIFT&gt;+&lt;TAB&gt; a procházet panely v opačném pořadí, tj. první je nastaven panel, který byl nejdéle neaktivní.</p>

<h5>Panel struktury projektu</h5>
	<p>Panel struktury projektu, který se nachází v levé části uživatelského rozhraní, obsahuje stromovou strukturu projektu nebo její část. Panel struktury lze v případě nutnosti zmenšit, zvětšit nebo úplně skrýt pomocí jezdce, který se nachází na hraně mezi panelem struktury a pracovní plochou. Funkce panelu struktury je závislá na aktuálním kontextu (spuštěné komponentě). Po přihlášení do systému se panel zobrazí spolu s komponentou Nástěnka a má zde úlohu rychlého spouštění komponent. Dvojitým kliknutím na stránku ve struktuře se spustí komponenta asociovaná s danou stránkou. Tato komponenta obvykle slouží pro úpravu obsahu dané stránky. </p>
