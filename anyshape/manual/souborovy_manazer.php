<p>
Pro manipulaci se soubory všech typů slouží komponenta Souborový manažer. Umožňuje pohodlně na server nahrávat soubory, vytvářet adresářovou strukturu a procházet celý souborový systém.
</p>

<p>
Komponenta je rozvržena jako typický souborový manažer známý z operačních systémů. V levé části se nachází adresářová struktura souborového systému a v pravé části je k dispozici náhled vybrané složky.
Obsah složky je možno zobrazit ve třech různých režimech:
</p>

<ul>
	<li><strong>Náhledy</strong> - v přehledu souborů jsou vidět jejich miniatury (vhodné zejména pro efektivní výběr ze seznamu obrázků).</li>
	<li><strong>Detaily</strong> - přehled souborů je zobrazen jako tabulka s následujícími sloupci: jméno souboru, velikost, datum, vlastník, popis.</li>
	<li><strong>Seznam</strong> - přehled souborů je zobrazen jako seznam s horizontálním posouváním.</li>
</ul>

<p class="image">
	<img src="img08.png" alt="Obr.8 Souborový manažer"/>
	Obr.8 Souborový manažer
</p>

<p>
Nahrávání souborů na server je realizováno pomocí speciální fronty. Lze tak na server nahrávat více souborů současně. Nahrávání může probíhat na pozadí a během něj je možné pokračovat v další práci. Postup pro nahrávání je následující:
</p>

<ul>
	<li>Uživatel vybere soubory ze svého lokálního úložiště. Může vybrat i více souborů najednou. <strong>Soubory jsou vždy nahrávány do aktuálně vybrané složky.</strong></li>
	<li>Pomocí tlačítka "Nahrát frontu na server" začnou být soubory na server nahrávány jeden po druhém. Během nahrávání souborů je uživatel o průběhu informován pomocí ukazatele.</li>
	<li>Po dokončení operace je uživatel upozorněn a seznámen s případným úspěchem či neúspěchem operace.</li>
</ul>

<p class="image">
	<img src="img09.png" alt="Obr.9 Souborový manažer - upload souborů"/>
	Obr.9 Souborový manažer - upload souborů
</p>

<p>
	Souborový manažer obsahuje i tzv. Koš. Všechny smazané soubory jsou tedy přesouvány do koše, odkud mohou být později obnoveny. Koš je k dispozici v menu, které se vyvolává kliknutím na tlačítko v pravé horní části komponenty. Mazání souboru je k dispozici v kontextovém menu, které se vyvolává pomocí pravého tlačítka myši.
</p>

<p>
	V případě, že se rozhodnete soubory nahrát se server pomocí protokolu FTP či SFTP, je potřeba provést synchronizaci souborového systému s obsahem adresáře na serveru, jinak soubory nebudou v manažeru vidět. Tato operace se nazývá "Synchronizovat s FTP" a je k dispozici v menu, které se vyvolává kliknutím na tlačítko v pravé horní části komponenty.
</p>

