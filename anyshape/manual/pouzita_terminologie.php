<h4>Projekt</h4>

<p>Každá webová prezentace, portál, eshop či aplikace postavená na AnyShape CMS je jednotně označována jako <strong>projekt</strong>.
</p>

<h4>Frontend</h4>

<p>Hlavní a prezentační část projektu, která je dostupná všem návštěvníkům webové prezentace. Tato část je obvykle dostupná po zadání URL adresy projektu do adresního řádku prohlížeče (např. http://www.mujprojekt.cz/). Pojetí frontendu se liší projekt od projektu a vždy vychází z požadavků klienta.</p>

<h4>Backend</h4>
<p>Administrátorská část projektu, která je přístupná jen autorizovaným uživatelům a slouží pro administraci obsahu projektu, který je prezentován na frontendu. Tato část je obvykle dostupná po zadání URL adresy projektu spolu s příponou "admin" (např. http://www.mujprojekt.cz/admin/). Tato příručka tvoří uceleného průvodce celým tímto administračním rozhraním.</p>

<h4>Komponenta</h4>

<p>
Funkční součást systému sloužící pro správu některé specifické části projektu. Jako příklady komponent můžeme uvést Souborový manažer, pro správu souborů na serveru, komponentu Anketa pro správu anket prezentovaných na stránkách projektu, komponentu Blog pro psaní článků apod. Po přihlášení do administračního rozhraní se zobrazí komponenta Nástěnka, která obsahuje přehled všech implementovaných komponent a umožňuje tak jejich rychlé spouštění. Komponenty je možné dále dělit dle jejich funkce (např. komponentu Články dále můžeme rozdělit na komponentu Přehled článků a komponentu Úprava článku). Komponenty se sdružují do celků zvaných moduly.
</p>

<h4>Modul</h4>

<p>
Soubor komponent sloužící pro správu některé specifické části projektu. Jako příklad můžeme uvést například modul Články, který se může skládat např. z komponent Blog, Novinky a Tiskové zprávy. Dalším příkladem je modul E-shop, mezi jehož komponenty mimo jiné patří i komponenta Katalog zboží, komponenta Objednávky, komponenta Nastavení slev, atd. 
</p>

<h4>Panel</h4>

<p>
Soubor ovládacích prvků aplikace, které tvoří tematickou skupinu. Mezi jednotlivými panely lze volně přepínat a pracovat tak s více záznamy současně. Typicky panel obsahuje spuštěnou komponentu, případně pomocný panel struktury.
</p>