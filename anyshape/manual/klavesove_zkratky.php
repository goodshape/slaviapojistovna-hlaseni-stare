<h4>Globální klávesy</h4>

<table>
	<tr><td><strong>&lt;TAB&gt;</strong></td><td>přejít na další prvek (tlačítko, políčko apod.)</td></tr>
	<tr><td><strong>&lt;SHIFT&gt;+&lt;TAB&gt;</strong></td><td>přejít na předchozí prvek (tlačítko, políčko apod.)</td></tr>
	<tr><td><strong>&lt;ESC&gt;</strong></td><td>zavření aktuálního panelu</td></tr>
	<tr><td><strong>&lt;CTRL&gt;+&lt;W&gt;</strong></td><td>zavření aktuálního panelu</td></tr>
	<tr><td><strong>&lt;CTRL&gt;+&lt;TAB&gt;</strong></td><td>přechod na předchozí aktivní panel</td></tr>
	<tr><td><strong>&lt;CTRL&gt;+&lt;SHIFT&gt;+&lt;TAB&gt;</strong></td><td>přechod na panel, který byl nejdéle neaktivní</td></tr>
	<tr><td><strong>&lt;CTRL&gt;+&lt;PAGE DOWN&gt;</strong></td><td>přechod na další panel (napravo od aktuálního)</td></tr>
	<tr><td><strong>&lt;CTRL&gt;+&lt;PAGE UP&gt;</strong></td><td>přechod na předchozí panel (nalevo od aktuálního)</td></tr>
</table>

<h4>Pomocný levý panel se strukturou</h4>

<table>
	<tr><td><strong>kurzorové klávesy</strong></td><td>výběr stránky</td></tr>
	<tr><td><strong>&lt;ENTER&gt;</strong></td><td>upravit obsah vybrané stránky</td></tr>
</table>

<h4>Úprava struktury webu</h4>

<table>
	<tr><td><strong>kurzorové klávesy</strong></td><td>výběr stránky</td></tr>
	<tr><td><strong>&lt;SHIFT&gt;+kurzorové klávesy</strong></td><td>výběr více stránek za sebou</td></tr>
	<tr><td><strong>&lt;ENTER&gt;</strong></td><td>upravit vlastnosti vybrané stránky</td></tr>
	<tr><td><strong>&lt;INSERT&gt;</strong></td><td>vložit novou stránku</td></tr>
	<tr><td><strong>&lt;DELETE&gt;</strong></td><td>smazat vybrané stránky</td></tr>
	<tr><td><strong>&lt;F4&gt;</strong></td><td>upravit obsah vybrané stránky</td></tr>
	<tr><td><strong>&lt;F5&gt;</strong></td><td>znovu načít strukturu ze serveru (refresh)</td></tr>
</table>

<h4>Správa datových záznamů</h4>

<table>
	<tr><td><strong>kurzorové klávesy</strong></td><td>výběr záznamu</td></tr>
	<tr><td><strong>&lt;SHIFT&gt;+kurzorové klávesy</strong></td><td>výběr více záznamů za sebou</td></tr>
	<tr><td><strong>&lt;ENTER&gt;</strong></td><td>upravit obsah vybraného záznamu</td></tr>
	<tr><td><strong>&lt;INSERT&gt;</strong></td><td>vložit nový záznam</td></tr>
	<tr><td><strong>&lt;DELETE&gt;</strong></td><td>smazat vybrané záznamy</td></tr>
	<tr><td><strong>&lt;F5&gt;</strong></td><td>znovu načít data ze serveru(refresh)</td></tr>
</table>

<h4>Úprava datového záznamu</h4>

<table>
	<tr><td><strong>&lt;F2&gt;</strong></td><td>uložit editovaný záznam</td></tr>
	<tr><td><strong>&lt;CTRL&gt;+&lt;S&gt;</strong></td><td>uložit editovaný záznam</td></tr>
</table>
