<p>
Společným jmenovatelem všech projektů je jejich struktura. Jedná se o databázovou strukturu typu strom. Nejvyšší položka stromu (tzv. kořen) reprezentuje samotný projekt. Zanořené položky zobrazeného stromu (tzv. uzly a listy) reprezentují jednotlivé stránky projektu. Tuto strukturu lze tedy chápat například i jako tzv. mapu webu. V závislosti na typu projektu, mohou tyto stránky reprezentovat i další entity (např. kategorie produktů v eshopu, zpravodajské rubriky na informačním portále, apod.). Struktura může obsahovat více kořenových uzlů, což je běžné v případech, kdy je z administračního rozhraní spravováno více projektů současně nebo pokud projekt obsahuje více jazykových verzí s rozdílnou strukturou. 
</p>

<h4>Úprava struktury projektu</h4>
<p>
Pro úpravu struktury slouží komponenta Úprava struktury projektu. Tato komponenta umožňuje v závislosti na přidělených uživatelských oprávněních provádět následující operace:
</p>

<ul>
	<li>Vytvářet nové projekty</li>
	<li>Vytvářet nové stránky projektu</li>
	<li>Upravovat vlastnosti stránek</li>
	<li>Přesouvat, kopírovat a mazat stránky v rámci struktury</li>
</ul>

<p class="image">
	<img src="img06.png" alt="Obr.6 Úprava struktury projektu - přehledový strom"/>
	Obr.6 Úprava struktury projektu - přehledový strom
</p>

<p>
Ve spodní části komponenty se nachází ovládací panel. Ten obsahuje tlačítka pro rychlé otevření a uzavření jednotlivých větví struktury a tlačítka pro přidávání a mazání stránek, případně celých nových projektů.
</p>

<p>
Dvojklikem na položku struktury se otevře komponenta pro nastavení vlastností stránky. Význam jednotlivých položek je popsán v kapitole Vlastnosti stránky.
</p>

<p>
Pomoci myši lze jednotlivé stránky ve struktuře přesouvat nebo kopírovat. Základem těchto operací je vždy operace typu drag &amp; drop. Ta spočívá v chycení dané položky kurzorem myši a přesunu dané položky na požadované místo ve struktuře (to vše za stálého držení levého tlačítka myši). Tímto je možné jednotlivé stránky přesouvat napříč strukturou nebo třídit a řadit stránky v rámci jedné větve. Pokud budete při přesunu stránky držet klávesu &lt;SHIFT&gt;, je možné položky vložit dovnitř jiné stránky (jako její pod-stránku). Budete-li při přesunu stránky držet klávesu &lt;CTRL&gt;, nebude položka přesunuta, ale zkopírována.
</p>

<p>
Další operace, které je možné provádět nad jednotlivými stránkami, se nachází v kontextovém menu (vyvolává se pravým tlačítkem myši). Význam těchto operací je:
</p>

<ul>
	<li><strong>Vlastnosti stránky</strong> - otevře komponentu pro nastavení vlastností stránky (stejně jako dvojklik či klávesa &lt;ENTER&gt;)</li>
	<li><strong>Editovat obsah stránky</strong> - otevře komponentu přiřazenou k dané stránce (stejně jako klávesa &lt;F4&gt;)</li>
	<li><strong>Přidat stránku</strong> - vložení nové stránky (stejně jako tlačítko na ovládacím panelu nebo klávesa &lt;INS&gt;)</li>
	<li><strong>Odstranit stránku</strong> - smazání vybraných stránek (stejně jako tlačítko na ovládacím panelu nebo klávesa &lt;DEL&gt;)</li>
	<li><strong>Obnovit</strong> - opakované načtení dat struktury ze serveru (také klávesa &lt;F5&gt;)</li>
	<li><strong>Seřadit stránky podle abecedy</strong> - seřadí všechny pod-stránky aktuálně vybrané větve dle abecedy</li>
	<li><strong>Obrátit pořadí stránek</strong> - obrátí pořadí všech pod-stránek v aktuálně vybrané větvi</li>
</ul>

<p>
Na ovládacím panelu úplně vpravo se také nachází tlačítko pro obnovení omylem smazaných stránek.
</p>

<h4>Vlastnosti stránky</h4>

<p>
Pro správnou funkčnost projektu je nutné všem stránkám nastavit jejich požadované vlastnosti. Obvykle je tato činnost prováděna již při vývoji projektu samotnými vývojaři. Koncový uživatel zpravidla tyto úpravy potřebuje provádět jen výjimečně. Jednotlivé vlasnosti stránky se děli do tří skupin.
</p>

<p class="image">
	<img src="img07.png" alt="Obr.7 Úprava struktury projektu - vlastnosti stránky"/>
	Obr.7 Úprava struktury projektu - vlastnosti stránky
</p>

<h5>Vlastnosti stránky</h5>

<p>Mezi základní vlastnosti stránky patří:
</p>

<h6>Identifikační číslo (id)</h6>
<p>Toto číslo je automaticky přiděleno při vytvoření stránky a je pro každou stránku v celém systému jedinečné. Toto číslo nelze změnit.</p>

<h6>Název stránky</h6>
<p>Název stránky, který je použit na frontendu. Může být vícejazyčný, pokud má stránka více jazykových verzí.</p>

<h6>Typ stránky</h6>
<p>Každá stránka musí mít nastaven svůj typ. Jednotlivé typy stránek jsou navrženy již při vývoji projektu a jejich počet odpovídá rozsahu a složitosti projektu. Typ stránky rozhoduje o tom, jakým způsobem bude výsledná stránka zobrazena na frontendu a jaká komponenta se použije pro editaci jejího obsahu. Každý typ stránky mívá obvykle jiné rozvržení prvků, takže se jednotlivé typy stránek liší jak vzhledem tak i zdrojem zobrazovaných dat.</p>

<h6>Omezení počtu pod-stránek</h6>
<p>V některých případech grafický návrh projektu počítá s předem stanoveným počtem stránek. V těchto případech je tedy nutné uživatelům zamezit ve vkládání stránek nad určitou mez, aby se zamezilo nechtěných grafickým chybám (jako např. příliš dlouhé menu zasahující za okraj stránky, seznam položek přeteče mimo vyhrazený box, apod.).</p>

<h6>Stav</h6>
<p>Každá stránka je buď <strong>aktivní</strong> či <strong>neaktivní</strong>. Neaktivní stránky se nezobrazují na frontendu.</p>

<h6>Dostupné jazyky</h6>
<p>Pro vícejazyčné projekty je možné pro každou stránku nastavit jazykové verze, ve kterých bude dostupná. Tuto vlastnost není nutné nastavovat pro každou stránku zvlášť, ale stačí ji nastavit pro nadřazenou stránku a všechny pod-stránky tuto od ní vlastnost převezmou.</p>

<h5>Optimalizace pro vyhledávače</h5>

<p>Tato skupina vlastností slouží pro optimální nastavení parametrů pro vyhledávače. Těmto vlastnostem tedy věnujte zvýšenou pozornost. U všech těchto vlastností (až na titulek) platí, že je není nutné explicitně vyplňovat. Jejich hodnoty jsou implicitně přebírány od nadřazených stránek. Stačí tedy tyto položky vyplnit jen u kořenového uzlu a tyto vlastností se projeví u všech stránek projektu.
</p>

<h6>Titulek</h6>
<p>Titulek stránky je použit pro název okna internetové prohlížeče. Zároveň se zobrazuje jako titulek jednotlivých výsledků ve vyhledávačích. Je to tedy to první, co uživatelé při vyhledávání uvidí. Pro jednotlivé stránky je automaticky sestavován titulek ve tvaru "titulek položky - titulek stránky - titulek projektu". Jednotlivé části titulku se nemusí vždy zobrazit. Na titulní stránce projektu se typicky zobrazuje jen titulek projektu, tedy název kořenového uzlu struktury (např. "Truhlářství Novák"). V pod-stránkách webu se v titulku zobrazuje titulek stránky i titulek projektu (např. "Naše služby - Truhlářství Novák"). Na stránkách konkrétních položek se pak navíc zobrazuje i název či titulek dané položky (např. "Dubový stůl - Nabízené produkty - Truhlářství Novák"). Implicitně není titulek vyplněn a je použit název stránky.
</p>

<h6>Popis</h6>
<p>Některé starší vyhledávače si všímaly popisu stránky zadaného autorem a pokud stránku vypisují v seznamu nalezených, připojují k titulku stránky i tento popis. Google vypisuje popisek tehdy, pokud obsahuje hledané slovo (nebo většinu hledaných slov). Implicitně není popis vyplněn a jeho hodnota se přebírá od nadřazené stránky.
</p>

<h6>Klíčová slova</h6>
<p>Aby vyhledávače snadněji pochopily, o čem se dané stránce píše, lze jim sdělit, jaká jsou klíčová slova textu. Nejdůležitější vyhledávače (např. Google a Seznam) však klíčová slova ignorují. Klíčová slova se oddělují čárkami, za čárkami mohou být mezery. Implicitně nejsou klíčová slova vyplněna a jeho hodnota se přebírá od nadřazené stránky.
</p>

<h6>Copyright</h6>
<p>Toto pole slouží k přidání informaci o copyrightu. Pro vyhledávače nemá žádný praktický význam. Implicitně není copyright vyplněn a jeho hodnota se přebírá od nadřazené stránky.
</p>

<h6>Indexování vyhledávači</h6>
<p>Pomocí tohoto přepínače lze pro danou stránku úplně zakázat indexování vyhledávači. Implicitně jeho hodnota toho pole přebírána od nadřazené stránky.
</p>

<h5>Nastavení přístupu ke stránce</h5>

<p>Pro každou stránku je možné nastavit přístupová práva. Právo na stránku se přiděluje vždy jen skupinám, nikoliv jednotlivým uživatelům. AnyShape CMS pro jednotlivé stránky rozlišuje tři úrovně přístupu:
</p>
<ul>
	<li><p><strong>Není k dispozici</strong> - stránka nebude uživatelům z dané skupiny vůbec přístupná a nebude v administračním systému vůbec vidět.</p></li>
	<li><p><strong>Povolit editaci</strong> - stránka bude uživatelům z dané skupiny přístupná jen pro editaci obsahu.</p></li>
	<li><p><strong>Povolit editaci i administraci</strong> - stránka bude uživatelům z dané skupiny přístupná jak pro editaci obsahu, tak i pro editaci jejích vlastností.</p></li>
</ul>

<p>
	Hromadné nastavení přístupových práv pro více stránek najednou je možné realizovat prostřednictvím komponenty "Správa uživatelů a skupin". 
</p>
