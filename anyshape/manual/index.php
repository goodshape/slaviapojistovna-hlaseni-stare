<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>

		<title>AnyShape - uživatelská příručka</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="keywords" content="" />
		<meta name="description" content="" />
		<meta name="robots" content="noindex,nofollow" />

		<link rel="shortcut icon" href='../client/favicon.ico' />		

		<link type="text/css" rel="stylesheet" href="default.css" media="screen" />

	</head>

	<body>

		<div id="body">

			<div id="header">
				<h1><span>AnyShape CMS</span></h1>
				<h2>Uživatelská příručka</h2>
			</div>

			<div id="index">
				<h3 id="obsah">1. Obsah</h3>

				<p>
					Tato verze příručky je platná k 1.7.2010 pro <strong>AnyShape CMS</strong> verze 2.0.
				</p>

				<? 
					$obsah = array(
						'Popis produktu' => 'popis_produktu',
						'Technické informace' => 'technicke_informace',
						'Začínáme' => 'zaciname',
						'Katalogové komponenty' => 'katalogove_komponenety',
						'Struktura projektu' => 'struktura_projektu',
						'Souborový manažer' => 'souborovy_manazer',
						'Správa uživatelů' => 'sprava_uzivatelu',
						'Modul Newsletter' => 'modul_newsletter',
						'Práce s WYSIWYG editorem' => 'wysiwyg_editor',
						'Příloha: Použitá terminologie' => 'pouzita_terminologie',
						'Příloha: Klávesové zkratky' => 'klavesove_zkratky'
					);
				?>
				<ol>
					<li><a href="#obsah">Obsah</a></li>
					<? foreach ($obsah as $kapitola => $id): ?>
					<li><a href="#<?=$id?>"><?=$kapitola?></a></li>
					<? endforeach ?>

				</ol>
					
			</div>

			<div id="contents">
					<? $i = 2; foreach ($obsah as $kapitola => $id): ?>
						<h3 id="<?=$id?>"><?=$i . '. ' . $kapitola?></h3> <? include $id.'.php' ?>
					<? $i++; endforeach ?>
			</div>

			<div id="footer">
				<strong>AnyShape CMS 2.0</strong> | &copy; 2010 <a class="goodshape" href="http://www.goodshape.cz/">GoodShape</a>
			</div>

		</div>

	</body>
</html>
