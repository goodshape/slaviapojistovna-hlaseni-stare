<p>
Součástí základní instalace AnyShape je modul <strong>Newsletter</strong>, který slouží pro rozesílání e-mailových zpravodajů či informačních bulletinů. Samotný modul se skládá ze tří komponent, jejichž funkce je následující:
</p>

<ul>
	<li><strong>Správa newsletterů</strong> - základní komponenta, která slouží k vytváření, rozesílání a evidenci jednotlivých newsletterů. Newsletterem zde rozumíme zprávu, která byla nebo bude hromadně rozeslána určité skupině příjemců. Odeslání každého newsletteru je realizováno vygenerováním fronty e-mailů (pro každého adresáta jeden), která je následně serverem na pozadí rozesílána.</li>
	<li><strong>Správa odběratelů</strong> - přehled a správa odběratelů newsletteru. Odběretelem newsletetru zde rozumíme osobu, která se dobrovolně přihlásila k odběru zpráv z určité předem definované tématické skupiny.</li>
	<li><strong>Fronta e-mailů</strong> - komponenta obsahuje archiv e-mailových zpráv, které byly odeslány nebo čekají na odeslání. E-maily které dosud nebyly odeslány je možno stornovat.</li>
</ul>

<p class="image">
	<img src="img12.png" alt="Obr.12 Newsletter - způsob vytvoření emailové zprávy"/>
	Obr.11 Newsletter - způsob vytvoření emailové zprávy
</p>

<h4>Vytvoření newsletteru</h4>
<p>
	Nový newsletter vytvoříte pomocí komponenty <strong>Správa newsletterů</strong>. Každý newsletter musí mít vyplněn <strong>předmět</strong> a <strong>tělo zprávy</strong>. Pro úpravu těla zprávy slouží vestavěný textový wysiwyg editor. Je Vám tak umožněno v těle zprávy používat styly, tabulky či obrázky. V případě, že je webová stránka vícejazyčná, je nutné vybrat i <strong>jazyk</strong>, aby byl newsletter doručen jen těm uživatelům, kteří se přihlásili k jeho odběru pro daný jazyk. Vzhled newsletteru určuje tzv. <strong>šablona</strong>. Šablony jsou předem připravné grafické návrhy, do kterých je doplněn Vámi zadaný text. Dále je nutné určit <strong>odesílatele</strong> a <strong>skupinu adresátů</strong>.
</p>

<h4>Plán rozesílání</h4>
<p>
	Každý newsletter může nabývat těchto stavů:
</p>
<ul>
	<li><strong>Nový</strong> - newsletter je nově vytvořený a není zatím určen k odeslání.</li>
	<li><strong>Čeká na odeslání</strong> - newsletter má nastaveno odložené odeslání na konkrétní datum a čas, který zatím nenastal.</li>
	<li><strong>Probíhá odeslání</strong> - probíhá odesílání jednotlivých e-mailů, což může trvat i několik hodin v závislosti na počtu odběratelů.</li>
	<li><strong>Vše odesláno</strong> - newsletter byl kompletně rozeslán.</li>
</ul>
<p>
	Nově vytvořený newsletter může být uložen a uschován pro pozdější úpravu až do doby, než je naplánováno jeho odeslání. Jakmile je newsletter rozeslán, není vhodné ho již nadále upravovat, jelikož odeslání již není možné opakovat.
</p>

<h4>Statistiky</h4>

<p>
	Každý newsletter může obsahovat měřící parametry pro statistiky <a href="http://www.google.com/analytics/" target="_blank">Google Analytics</a>. Na základě techto měřících parametrů lze rozlišovat 
jednotlivé kampaně a měřit jejich účinnost. Pro správné započítání návštěvy jsou povinné pouze parametry <strong>utm_source</strong> (zdroj, implicitně <em>newsletter</em>) a <strong>utm_medium</strong> 
(médium, implicitně <em>email</em>). Dále doporučujeme vyplnit parametr <strong>utm_campaign</strong> (kampaň, pro rozlišení jednotlivých newsletterů) a <strong>utm_content</strong> (obsah).
</p>
 
 <p>
	Samotný modul Newsletter je také vybaven nástroji pro počítání statistických údajů. U každého rozeslaného newsletteru jsou evidovány následujcí údaje:
 </p>
 
 <ul>
	<li><strong>Počet odeslaných emailů</strong> - počet všech emailů, které byly na základě daného newsletteru vytvořeny a odeslány.</li>
	<li><strong>Počet zobrazení na serveru</strong> - počet uživatelů, kteří si newsletter přečetli na serveru, tj. využili funkce <em>"Nezobrazuje se vám obsah správně? Klikněte zde."</em>.</li>
	<li><strong>Počet zobrazení s obrázky</strong> - počet uživatelů, kteří si email nechali zobrazit i s obrázky.</li>
	<li><strong>Počet kliknutí na odkaz</strong> - počet kliknutí na odkaz v emailu, který vede na stránky.</li>
	<li><strong>Počet odhlášení</strong> - počet uživatelů, kteří si odhlásili odběr newsletterů, na základě daného newsletteru.</li>
 </ul>
 
 <p>
	Většina těchto údajů je k dispozici nejen pro jedlitové newslettery, ale i pro jednotlivé odběratele a jedotlivé odeslané e-maily.
 </p>
 