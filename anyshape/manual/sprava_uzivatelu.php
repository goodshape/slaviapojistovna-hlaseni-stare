<p>
	Správa uživatelských účtů pro administrační rozhraní je realizována prostřednictvím komponenty "Správa uživatelů a skupin". Každý uživatelský účet je určen svým unikátním uživatelským jménem. Pro snazší orientaci se uživatelům přiřazuje i jméno a příjmení, případně i textový komentář. Aby se mohl uživatel přihlásit do systému, je nutné nastavit jeho účet do stavu "aktivní". 
</p>

<p>
	V rámci řízení uživatelských přístupových práv se jednotliví uživatelé zařazují do skupin. Každá skupina má podrobně nastavena oprávnění na jednotlivé komponenty a stránky v projektu. Uživatel může být členem více skupin a v takovém případě se jednotlivá práva těchto skupin sčítají.
	Je tedy vhodné vytvořit několik skupin s dílčími oprávněními na specifické části systému. Vytvořené uživatele lze pak zařazovat do několika skupin zároveň dle potřeby a skombinovat tak různé úrovně oprávnění. Kombinování více skupin je vhodnější, než vytváření mnoha komplexních skupin s různými stupni oprávnění.
</p>
<p>
	Příklad:<br/>
	<strong>administrátor</strong> - přístup ke komponentě Správa uživatelů a skupin, Úprava struktury projektu<br/>
	<strong>redaktor</strong> - přístup ke komponentě Články, Blog a Souborový manažer<br/>
	<strong>editor</strong> - přístup ke komponentě Schvalování článků<br/>
	<strong>fotograf</strong> - přístup ke komponentě Fotogalerie a Souborový manažer<br/>
	
</p>

<p>
	V levé části komponenty se nachází seznam uživatelů a v pravé části se nachází seznam skupin. Každý uživatel je zobrazen jako uzel stromu a jeho pod-uzly tvoří skupiny, ve kterých je zařazen. Každá skupina je zobrazena také jako uzel stromu, kde pod-uzlu tvoří uživatelé v dané skupině zařazení. Zařazení uživatele do skupiny je realizováno pomocí metody drag &amp; drop. Je potřeba chytnout myší uživatele z levého panelu a přetáhnout na skupinu v pravém panelu. Lze to i opačným způsobem, tedy chytnout myší skupiny z pravého panelu a přetáhnout na uživatele v levém panelu. 
	Odstranění uživatele ze skupiny slouží klávesa &lt;DEL&gt;.
</p>

<p class="image">
	<img src="img10.png" alt="Obr.10 Správa uživatelů a skupin"/>
	Obr.10 Správa uživatelů a skupin
</p>

<h4>Nastavení práv pro skupinu</h4>
<p>
Komponenta pro nastavení práv pro skupinu se spustí dvojklikem na název skupiny v přehledu skupin. Práva se nastavují ve dvou nezávislých vrstvách.
</p>

<h5>Práva na operace</h5>

<p>Práva na operace se nastavují v levém panelu. Panel obsahuje strom modulů a jejich komponent. Komponenty mají dále definovány operace, které lze v dané komponentě vykonávat. Každá komponenta obsahuje základní operaci "povolit přístup", která rozhoduje o tom, zda budou mít uživatelé z dané skupiny vůbec právo tuto komponentu otevřít. Ostatní typy operací se liší dle typu komponenty (např. přidat stránku, přidat položku, vytvořit složku, smazat soubor, apod.).
</p>

<h5>Práva na stránky struktury</h5>

<p>Práva na stránky struktury se nastavují v pravém panelu. Panel obsahuje strukturu projektu. Pro každou stránku lze nastavit tři úrovně oprávnění:</p>
<ul>
	<li><p><strong>Není k dispozici</strong> - stránka nebude uživatelům z dané skupiny vůbec přístupná a nebude v administračním systému vůbec vidět.</p></li>
	<li><p><strong>Povolit editaci</strong> - stránka bude uživatelům z dané skupiny přístupná jen pro editaci obsahu.</p></li>
	<li><p><strong>Povolit editaci i administraci</strong> - stránka bude uživatelům z dané skupiny přístupná jak pro editaci obsahu, tak i pro editaci jejích vlastností.</p></li>
</ul>

<p>Pro hromadné nastavení lze použít kombinaci levé tlačítko myši + klávesa &lt;SHIFT&gt;. Tímto bude dané právo nastaveno pro vybranou stránku i všechny její podstránky. Nastavení stejné úrovně práv pro všechny stránky struktury lze tedy docílit kliknutím na kořenový uzel stromu spolu s přidržením klávesy &lt;SHIFT&gt;.
</p>

<p class="image">
	<img src="img11.png" alt="Obr.11 Správa uživatelů a skupin - nastavení práv pro skupinu"/>
	Obr.11 Správa uživatelů a skupin - nastavení práv pro skupinu
</p>
