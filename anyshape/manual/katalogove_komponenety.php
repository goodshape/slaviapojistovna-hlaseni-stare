<p>
Pojmem katalogová komponenta rozumíme dílčí součást systému, která spravuje ohraničenou skupinu záznamů projektu (zpravidla jednu databázovou tabulku), která má přímou souvislost s některou z komponent prezentovaných na frontendu projektu (např. komponenta Blog, Fotogalerie, Novinky, Hudební alba, Tiskové zprávy, Soubory ke stažení apod.) Obvykle se katalogová komponenta dělí na část přehledovou a část editační.
</p>

<h4>Přehledová část</h4>

<p>Většina přehledových komponent má jednotný design, který vychází ze základního modulu Katalog. Primárně se jedná o tabulku, kde jsou přehledně seřazeny všechny záznamy spravované danou komponentou. Ve spodní části komponenty se nachází ovládací a navigační panel. V případě, že komponenta spravuje záznamy s podporou vícejazyčných textů, je možné zobrazený jazyk přepínat pomocí záložek vpravo nahoře.
</p>

<p class="image">
	<img src="img03.png" alt="Obr.3 Přehledová část komponenty"/>
	Obr.3 Přehledová část komponenty
</p>

<h5>Navigace</h5>

<p>
Obvykle se na jednu stránku zobrazuje 30 záznamů. Pokud tabulka obsahuje více než aktuální počet zobrazených záznamů, slouží pro přechod na další stránky navigační šipky v pravé části navigačního panelu. Jejich význam je následující (bráno zleva doprava) : přechod na první stránku, přechod na předchozí stránku, přechod na další stránku a přechod na poslední stránku. Uprostřed se nachází textové políčko, které umožňuje rychlý přechod na zadanou stránku zadáním čísla stránky. Vedle políčka za lomítkem je zobrazen celkový počet všech stránek. Počet zobrazovaných položek na stránku lze změnit pomocí konfiguračního menu přístupného přes tlačítko Nastavení v pravé části navigačního panelu.
</p>

<h5>Řazení</h5>

<p>
Změna řazení záznamů v tabulce se provádí kliknutím na záhlaví sloupce tabulky, podle kterého má být tabulka seřazena. Sloupec, podle něhož je tabulka aktuálně seřazena, je označen malou šipkou. Směr šipky označuje směr řazení záznamů (šipka nahoru - vzestupné řazení, šipka dolů - sestupné řazení). Směr řazení se mění opakovaným kliknutím na záhlaví sloupce, podle kterého je již tabulka seřazena.
</p>

<h5>Vyhledávání</h5>

<p>
Pro vyhledávání v tabulce slouží tzv. filtry, které jsou přístupně přes záhlaví sloupce tabulky. Při najetí kurzoru myši nad záhlaví sloupce, se zobrazí v pravé části záhlaví sloupce ikona. Kliknutím na tuto ikonu je vyvolán dialog nastavení filtrování dle specifického sloupce.
</p>
<p>
Příklad: potřebujete najít záznamy, které obsahují v názvu slovo "produkt".<br/>
Postup: najeďte kurzorem myši nad záhlaví sloupce "Název". V pravé části záhlaví sloupce se objeví se malá ikona, na kterou klikněte. Objeví se dialog, do kterého vepište hledaný výraz "produkt" a stiskněte tlačítko "Použít". V tabulce se zobrazí jen ty záznamy, které obsahují slovo "produkt".
</p>
<p>
Pro číselné záznamy nebo kalendářní data lze filtrovat i záznamy ve specifickém rozsahu. Je tedy možné docílit např. zobrazení záznamů z určitého rozsahu nebo zobrazení záznamů vytvořených před specifickým datem apod. 
</p>

<h5>Akce a operace</h5>

<p>
V levé části navigačního panelu se nachází tlačítka pro specifické operace. Některé operace pro svou funkci mohou vyžadovat, aby byly z tabulky vybrány (označeny) záznamy, nad kterými se má daná operace provést. Výběr jednoho záznamu je možné uskutečnit pomocí myši nebo kurzorových kláves. Pro výběr více záznamů slouží kombinace myši nebo kurzorových kláves spolu se současně stisknutým tlačítkem &lt;SHIFT&gt; nebo &lt;CTRL&gt;.
</p>
<p>
Většina komponent obvykle disponuje základními dvěma tlačítky pro založení nového záznamu a pro smazání vybraných záznamů. Další tlačítka se mohou dle typu komponenty lišit, jelikož jsou implementována na základě požadavků klienta nebo projektu. Obvykle se můžeme setkat s akcemi typu Tisk, Export, Kopírování a jinými hromadnými akcemi.
</p>
<p>
<strong>Pro úpravu specifického záznamu z tabulky stačí provést dvojklik na daný záznam a do nového panelu se otevře editační komponenta.</strong>
</p>

<h5>Nastavení</h5>

<p>
Navigační lišta obsahuje úplně vpravo tlačítko pro změnu a nastavení zobrazení tabulky. Význam jednotlivých položek je následující:
</p>

<ul>
	<li><p>Počet položek na stránku - umožňuje změnit počet položek, které jsou zobrazeny v rámci jedné stránky.</p></li>
	<li><p>Proměnná výška sloupců - pokud některé záznamy obsahují víceřádkové textové položky nebo různě veliké obrázky, je možné pomocí tohoto nastavení upravit zobrazení těchto záznamů. Implicitně je tato volba vypnuta, tzv. všechny řádky tabulky jsou stejně vysoké.</p></li>
	<li><p>Zalamování řádků - pokud některé záznamy obsahují příliš dlouhé texty, které se nevejdou na šířku sloupce (dojde tedy k jejich ořezání), je možné pomocí tohoto nastavení tyto texty zalamovat. Texty se pak zobrazí na více řádcích. Je však nutné zároveň aktivovat volbu "Proměnná výška sloupců".</p></li>
	<li><p>Obnovit - tato operace provede opakované načtení dat ze serveru.</p></li>
</ul>

<h5>Strukturované tabulky</h5>

<p>V některých případech jsou data spravovaná komponentou určitým způsobem strukturovaná. Typicky se jedná o záznamy řazené do kategorií. Příkladem jsou články a jejich zařazení do rubrik či seznam produktů v eshopu a jejich zařazení do produktových kategorií. V takovém případě je součástí přehledové komponenty i panel se strukturou těchto kategorií či rubrik a zobrazuje se v pravé části komponenty. Volba položky ve struktuře pak slouží k rychlému přecházení mezi jednotlivými kategoriemi či rubrikami. Tato struktura je zároveň podmnožinou celé struktury projektu, takže jednotlivé kategorie bývají na frontendu projektu obvykle prezentovány jako jednotlivé stránky a bývají dostupné přes menu.
</p>

<p class="image">
	<img src="img04.png" alt="Obr.4 Přehledová část komponenty se strukturou"/>
	Obr.4 Přehledová část komponenty se strukturou
</p>

<h4>Editační část</h4>

<p>Editační část komponenty je vždy formulář s jednotlivými položkami editovaného záznamu. Formulář obsahuje typické formulářové prvky jako textová pole, zaškrtávací políčka, výběrová tlačítka, roletové výběry, obrázky apod. Jednotlivé částí formulářů mohou být pro přehlednost sdružovány do tematických bloků. Často bývá vedle jednotlivých polí zobrazena i kontextová nápověda nebo návod, jak správně jednotlivá pole vyplnit.
</p>

<p>
Panel s ovládacími prvky formuláře se nachází ve spodní části komponenty. Vždy obsahuje tlačítka pro uložení a zavření komponenty. V některých případech může obsahovat i další tlačítka, která jsou vždy doplněna dle potřeby a požadavků na konkrétní projekt. Jako příklad takové akce může být náhled záznamu před uložením, odeslání záznamu na email, automatické doplnění některých polí ve formuláři, apod.
</p>

<p class="image">
	<img src="img05.png" alt="Obr.5 Editační část komponenty"/>
	Obr.5 Editační část komponenty
</p>


<h4>Vícejazyčné komponenty</h4>

<p>
U každé komponenty existuje možnost používat vícejazyčné textové položky. Tato zajímavá vlastnost se hodí všude tam, kde se záznamy tabulky pro jednotlivé jazykové verze projektu liší jen v některých textových sloupcích, ale ostatní klíčové sloupce těchto záznamů přitom zůstávají ve všech jazykových mutacích neměnné. <strong>Pro přepínání jednotlivých jazykových verzí slouží záložky vpravo nahoře. </strong>
</p>
<p>
Tato vlastnost je společná pro přehledové i editační komponenty. Při vyplňování těchto vícejazyčných záznamů tedy nezapomeňte vyplnit všechny jazykové varianty textů. Obvykle bývají tato klíčová textová pole nastavena jako povinná. Pokud se tedy v takovém případě budete snažit uložit záznam bez vyplnění dodatečných jazykových verzí záznamu, budete na to upozorněni.
</p>
<p>
Jako příklad můžeme uvést vícejazyčný eshop, kde je každý produkt v databázi veden pod jedním katalogovým číslem společným pro všechny jazykové verze. Takovýto produkt má však několik názvů a několik popisků pro jednotlivé jazykové verze obchodu. Vložení nového produktu do katalogu lze v takovém případě provést najednou pro všechny jazykové verze obchodu.
</p>
<p>Dalším příkladem může být tabulka CD nosičů, kde je kód CD, název interpreta i název alba stejný pro všechny jazykové verze webu. Popis produktu a cena se však v jednotlivých jazykových verzích liší. I v tomto případě není potřeba vést dvě oddělené tabulky těchto záznamů, ale stačí jen jedna tabulka s vícejazyčným popisem produktu.
</p>

