<?php

	/**
	 * Funkce, jejichz vyuziti je jen v modulech backendu
	 */

	/**
	 * Zobrazi chybu v XML a ukonci skript
	 * @param	int	oznaceni chyby
	 * @param	string	dodatecne informace (volitelne)
	 * @author	Hugo
	 */
	function error($id = 'unknown_error', $info = false) {
		header('Content-type: text/xml');
		if (strlen($info) > 0) $info_msg = "info=\"$info\""; else $info_msg = '';
		echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?><error id=\"$id\" $info_msg/>";
		exit();
	}

	/**
	 * Zobrazi "uspech" v XML a ukonci skript
	 * @param	string	zprava
	 * @author	Hugo
	 */
	function success($msg = false) {
		header('Content-type: text/xml');
		echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?><success ".(($msg !== false)?("msg=\"$msg\""):'')."/>";
		exit();
	}

	/**
	 * Ukončí skript a vrátí zadané pole jako XML se správnou hlavičkou
	 * @param	array	pole k prevedeni
	 * @param	string	nazev korenoveho elementu
	 * @param	string	nic, je pouzito pri rekurzi
	 * @return	string	vysledne xml
	 * @author	Hugo (uloupeno na webu)
	 */
	function returnXML($data, $rootNodeName = 'data', $count = false) {
		header('Content-type: text/xml');
		echo array2XML($data, $rootNodeName, $count);
		exit();
	}

	/**
	 * Zakoduje nepovolene znaky v URL, ale zachova lomitka a mezery prevede na %20
	 * @param	string	url
	 * @return	string	url
	 * @author	Hugo
	 */
	function encodeURL($url) {
		return str_replace("+", "%20", str_replace("%2F", "/", urlencode($url)));
	}

	/**
	 * Vrati segment z URL
	 * @param	int		offset v URL (od nuly)
	 * @param	string	implicitni hodnota v pripade, ze element neni zadan
	 * @return	string	nalezeny element nebo false
	 * @author	Hugo
	 */
	function url($offset, $default = '') {
		return element($offset, explode('/', get('q')), $default);
	}

	/**
	 * Vrati retezec segmnetu z URL
	 * @param	int		offset v URL (od nuly)
	 * @param	int		pocet segmentu (0 - vsechny az do konce, zaporne
	 *					cislo znamena pocet segmentu od konce)
	 * @return	string	nalezeny element nebo prazdny retezec
	 * @author	Hugo
	 */
	function urls($offset = 1 , $limit = 0) {
		$url = (explode('/', get('q')));
		$n = count($url);
		if ($limit == 0) $limit = $n;
		else if ($limit < 0) $limit = $n + $limit;
		else $limit = $offset + $limit;
		if ($limit > $n) $limit = $n;
		$result = '';
		for ($i = $offset; $i < $limit; $i++) {
			$result .= element($i, $url);
			if ($i < $limit-1) $result .= '/';
		}
		return $result;
	}

	/**
	 * Převede nepovolene entity v XML
	 * @param	string	retezec k prevedeni
	 * @return	string	prevedeny retezec
	 * @author	Hugo
	 */
	function xmlConvert($str) {
		$str = str_replace(array("&","<",">","\"", "'", "-"), array("&amp;", "&lt;", "&gt;", "&quot;", "&#39;", "&#45;"), $str);
		return $str;
	}

	/**
	 * Převede pole do jednoducheho XML
	 * @param	array	pole k prevedeni
	 * @param	string	nazev korenoveho elementu
	 * @param	string	prida do korenoveho uzlu atribut "count"
	 * @return	string	vysledne xml
	 * @author	Hugo
	 */
	function array2XML($data, $rootNodeName = 'data', $count = false, $header = true) {
		if ($header) $buffer = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"; else $buffer = '';
		$buffer .= "<$rootNodeName";
		if ($count !== false) $buffer .= " count=\"$count\"";
		$buffer .= ">";
		$buffer .= _xmlwrite($data);
		$buffer .= "</$rootNodeName>";
		return $buffer;
	}

	/**
	 * Pomocna funkce pro array2XML
	 */
	function _xmlwrite($data) {
		$buffer = '';
		if (!empty($data)) {
			foreach ($data as $key => $value) {
				if (is_numeric($key)) $key = "node";
				if (is_array($value) || $value instanceof Traversable) {
					$buffer .= "<$key>";
					$buffer .= _xmlwrite($value);
					$buffer .= "</$key>";
					continue;
				}
				$buffer .= "<$key>".xmlConvert(asNull($value))."</$key>";
			}
		}
		return $buffer;
	}

	/**
	 * Pomocná funkce, která zakóduje hodnotu null do řetězce __NULL__ pro použití v XML.
	 * Nebo naopak, pokud je vstupem řetězec __NULL__, vrátí null. V ostatních případech
	 * ponechá hodnotu beze změny.
	 *
	 * @param 	 *	Vstupní proměnná
	 * @return	 *	Výstupní proměnná
	 *
	 */
	function asNull($value) {
		if ($value === null) return '__NULL__';
		if ($value === '__NULL__') return null;
		return $value;
	}

	/*
	 * Funkce kontroluje, zda-li se nekdo nepokousi o hack. Jinymi slov se kontroluje,
	 * jestli zadany soubor/adresar neobsahuje dve tecky (..) nebo .htaccess
	 *
	 * @param	string	nazev souboru (nemusi existovat)
	 * @return	boolean	soubor je ve spravnem podadresari
	 * @author	Hugo
	 */
	function isSafe($filename) {
		return  is_string($filename) && (strpos($filename, '../') === false && strpos($filename, '/..') === false && basename($filename) != '.htaccess');
	}

	/*
	 * Rekurzivni vytvoreni nove slozky s moznosti FTP hacku a zapisem do databaze souboru.
	 * Vytvorenemu adresari nastavi prava 0777.
	 * @param	string	path - cesta k souboru/slozce relativne ke koreni aplikace
	 * @param	bool	pokud je true, je vytvorena struktura ulozena do databaze
	 * @return	bool	pokud, se povedlo, vrati true
	 * @author	Hugo
	 */
	function mkPath($path, $root = NULL) {
		$path = rtrim($path, '/\\');

		global $config;
        if($root === NULL) {
            $root = $config->wwwDir;
        }

		// je potreba FTP hack?
		if ($config['ftp']['hack']) {

			$conn_id = ftp_connect($config['ftp']['host']);
			if ($conn_id === false) error('ftp_unable_to_connect');
			$login_result = @ftp_login($conn_id, $config['ftp']['username'], $config['ftp']['password']);
			if ($login_result === false) error('ftp_unable_to_login');
            $folder = '';

            $pathArray = explode('/', $path);
			foreach ($pathArray as $dir) {
				$folder .= "$dir/";
				if (@ftp_chdir($conn_id, $config['ftp']['path'].$folder) === FALSE) {
					if (!@ftp_mkdir($conn_id, $config['ftp']['path'].$folder)) error('ftp_unable_to_mkdir');
					@ftp_site($conn_id, "CHMOD 0777 ".$config['ftp']['path'].$folder);
				}
			}

		} else {
            $fullPath = $root .'/'.$path;
            if (!file_exists($fullPath)) {
                if (!@mkdir($fullPath, 0777, true)) error('access_denied');
            }
		}
		return true;
	}

	/**
	 * Vrati nazev tabulky i prefixem ':data:'. Pokud je pred nazvem uveden modifikator '%',
	 * vrati nazev s prefixem ':as:'. Z nazvu tabulky jsou odstraneny vsechny znaky mimo a-z, 1-9.
	 * Zaroven kontroluje, zda-li tabulka vubec existuje.
	 * @param	$table	Nazev tabulky
	 * @author	Hugo
	 */
	function tableName($table) {
		if (substr($table, 0, 1) == '%') $result = ':as:' . safeStr(substr($table, 1));
		else $result = ':data:' . safeStr($table);
		$result = dibi::getConnection()->translate($result);
		if (count(dibi::query("SHOW TABLES LIKE %s", $result)->fetchAll()) > 0) return $result;
		else error('table_not_found', $result);
	}

	/**
	 * Funkce projde POST data a pokud nektery element nema obsah, vyhodi error
	 * Casem se muze rozsirit o kompletni validaci obsahu elementu
	 * @author HonzaB
	 */
	function postValidation($post) {
		$errors = false;

		if( ! is_array($post)) error('server_param_error');

		foreach($post as $var)
			if (post($var, null) === null) $errors = true;

		if($errors) error('server_param_error');
	}

	/**
	 * Prevede XML data formulare z formatu, ktery je bezny pri pouziti AnyFormu,
	 * na pole hodnot, kde klicem je nazev sloupce.
	 */
	function anyFormData()
	{
		// data zobrazeného záznamu
		$data = xml(post('data'));
		$vars = array();
		foreach ($data as $panel)
			foreach ($panel as $record)
				foreach ($record as $source)
					foreach ($source as $item)
						$vars[$item->getName()] = (string) $item;
		return $vars;
	}

	/**
	 * Prelozi zadany text do zadaneho jazyka
	 */
	function t($key, $language_id)
	{
		$result = $key;
		global $dictionary;

		if (!isset($dictionary)) $dictionary = array();

		if (!isset($dictionary[$language_id]))
		{
			$dir = dirname(__FILE__) . '/../core/languages/';
			$files = directoryMap($dir, FALSE);
			foreach ($files as $file)
				if (!is_array($file))
				{
					$xml = @simplexml_load_file($dir . $file);
					if ($xml && (string) $xml['id'] == $language_id)
					{
						$dictionary[$language_id] = $xml;
						break;
					}
				}
		}

		if (isset($dictionary[$language_id]))
			foreach ($dictionary[$language_id]->group as $group)
				foreach ($group->item as $item)
					if ($key == (string) $item['key']) return (string) $item;

		return $result;
	}
