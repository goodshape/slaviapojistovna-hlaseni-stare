<?php

	/**
	 *	Zakladni funkce, jijichz vyuziti je siroke
	 */

	/**
	 * Vraci element v poli dle indexu.
	 * @param	$index - index
	 * @param	$array - pole
	 * @param	$default - co vratit, kdyz element neexistuje
	 * @return	nalezeny element nebo $default
	 * @author	Hugo
	 */
	function element($index, $array, $default = FALSE) {
		if ((is_array($array) || $array instanceof Traversable) && array_key_exists($index, $array)) return $array[$index]; else return $default;
	}

	/**
	 * Vraci element z GETu.
	 * @param	$index - index
	 * @param	$default - co vratit, kdyz element neexistuje
	 * @return	nalezeny element nebo $default
	 * @author	Hugo
	 */
	function get($index = '', $default = FALSE) {
		return cleanInput(element($index, $_GET, $default));
	}

	/**
	 * Vrati element z POSTu.
	 * @param	$index - index
	 * @param	$default - co vratit, kdyz element neexistuje
	 * @return	nalezeny element nebo $default
	 * @author	Hugo
	 */
	function post($index = '', $default = FALSE) {
		return cleanInput(element($index, $_POST, $default));
	}

	/**
	 * Vrati promnnout z COOKIE.
	 * @param	$index - index
	 * @param	$default - co vratit, kdyz element neexistuje
	 * @return	nalezeny element nebo $default
	 * @author	Hugo
	 */
	function cookie($index = '', $default = FALSE) {
		return cleanInput(element($index, $_COOKIE, $default));
	}

	/**
	 * Funkce procisti vstupni data. Neni ochrana pred "cross site scripting"!
	 * Resi jen nekonzistenci chovani pri ruznem nastaveni magic quotes na serveru.
	 * @param	 *	vstupni data
	 * @author	Hugo (inspirace v CodeIngniteru)
	 */
	function cleanInput($data) {
		if (is_array($data)) {
			$result = array();
			foreach ($data as $key => $value) $result[$key] = cleanInput($value);
			return $result;
		}
		if (get_magic_quotes_gpc()) $data = strip($data);
		return $data;
	}

	/**
	 * Stejna funkce jako stripslashes, ale v pripade, ze vstupem neni retezec, vrati puvodni hodnotu
	 * a nikoliv prazdny retezec.
	 * @param	string	vstupni retezec
	 * @return	mixed	vystupni retezec nebo puvodni hodnota
	 * @author	Hugo
	 */
	function strip($str) {
		if (is_string($str)) return stripslashes($str);
		else return $str;
	}

	/**
	 * Vrati element ze _SESSION. Pouziva nastaveny prefix, aby nedochazelo ke kolizim s frontendem.
	 * @param	$index - index
	 * @param	$default - co vratit, kdyz element neexistuje
	 * @return	nalezeny element nebo $default
	 * @author	Hugo
	 */
	function session($index = '', $default = FALSE) {
		global $config;
		if (!isset($_SESSION)) {
			if (get('session_id')) session_id(get('session_id'));
			session_start();
		}
		return element($config['site']['session_prefix'].$index, $_SESSION, $default);
	}

	/**
	 * Nastavi promennou do _SESSION.
	 * @param	int	index
	 * @param	$valoue - hodnota
	 * @author	Hugo
	 */
	function setSession($index, $value) {
		global $config;
		if (!isset($_SESSION)) {
			if (get('session_id')) session_id(get('session_id'));
			session_start();
		}
		$_SESSION[$config['site']['session_prefix'].$index] = $value;
	}

	/**
	 * Provede explode("|", $str), ale v pripade, ze $str je prazdny, vrati prazdne pole
	 * @param	$str
	 * @return	pole retezcu
	 * @author	Hugo
	 */
	function boom($str) {
		if (strlen($str) == 0) return array();
		return explode("|", $str);
	}

	/**
	 * Provede redirect na nove ULR
	 * @param	string	$url - retezec s URL
	 * @author	Hugo
	 */
	function redirect($url = FALSE) {
		if ($url === FALSE) $url = BASEURL;
		header("Location: ".str_replace("&amp;", "&", $url));
		exit();
	}

	/**
	 * Prevede retezec do bezpecne podoby pro pouziti v URL, tim ze odstrani diakritiku, velka pismena
	 * prevede na mala a vsechny znaky mimo 'a'-'z', '0'-'9' prevede na '-'
	 * @param	string	vstupni retezec
	 * @return	string	upraveny retezec
	 * @author	Hugo
	 */
	function forURL($str) {
		return trim(preg_replace('/-{2,}/', '-', preg_replace('/[^a-z0-9\.]/', '-', strtolower(nocyrillic(flat(trim($str)))))), '-');
	}

	/**
	 * Odstrani diakritiku (v UTF-8)
	 * @param	string	vstupni retezec
	 * @return	string	upraveny retezec
	 * @author	Hugo
	 */
	function flat($str) {
		mb_internal_encoding('UTF-8');
		$from = "áäàâãåčçďéěèêíìîïľĺňñóöőôòõøřŕšťúůüűùýÿžÁÄÀÂÃÅČÇĎÉĚÈÍÌÎÏĽĹŇÑÓÖŐÔÒÕŘŔŠŤÚÙŮÛÜŰÝŸŽ";
		$to   = "aaaaaaccdeeeeiiiillnnooooooorrstuuuuuyyzAAAAAACCDEEEIIIILLNNOOOOOORRSTUUUUUUYYZ";
		$xlat = array();
		for ($i = 0; $i < mb_strlen($from); $i++)
			$xlat[mb_substr($from, $i, 1)] = substr($to, $i, 1);
		return strtr($str, $xlat);
	}

	/**
	 * Převod azbuky na latinku podle GOST 16876-71
	 * @param string text v azbuce
	 * @return string text v latince
	 * @copyright Jakub Vrána, http://php.vrana.cz/
	 */
	function nocyrillic($str)
	{
		return strtr($str, array(
			'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'jo', 'ж' => 'zh', 'з' => 'z', 'и' => 'i', 'й' => 'jj', 'к' => 'k', 'л' => 'l', 'м' => 'm', 'н' => 'n', 'о' => 'o', 'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't', 'у' => 'u', 'ф' => 'f', 'х' => 'kh', 'ц' => 'c', 'ч' => 'ch', 'ш' => 'sh', 'щ' => 'shh', 'ъ' => '', 'ы' => 'y', 'ь' => '', 'э' => 'eh', 'ю' => 'ju', 'я' => 'ja',
			'А' => 'A', 'Б' => 'B', 'В' => 'V', 'Г' => 'G', 'Д' => 'D', 'Е' => 'E', 'Ё' => 'JO', 'Ж' => 'ZH', 'З' => 'Z', 'И' => 'I', 'Й' => 'JJ', 'К' => 'K', 'Л' => 'L', 'М' => 'M', 'Н' => 'N', 'О' => 'O', 'П' => 'P', 'Р' => 'R', 'С' => 'S', 'Т' => 'T', 'У' => 'U', 'Ф' => 'F', 'Х' => 'KH', 'Ц' => 'C', 'Ч' => 'CH', 'Ш' => 'SH', 'Щ' => 'SHH', 'Ъ' => '', 'Ы' => 'Y', 'Ь' => '', 'Э' => 'EH', 'Ю' => 'JU', 'Я' => 'JA',
		));
	}

	/**
	 * Prevede retezec do bezpecne podoby pro pouziti jako identifikator, tim ze vsechny znaky
	 * mimo 'a'-'z', '0'-'9', '_' a '|' prevede na '_' a znaky 'A'-'Z' prevede na 'a'-'z'.
	 * @param	string	vstupni retezec
	 * @param	string	dalsi povolene znaky (je treba je pripadne escapovat pro pouziti v regularnim vyrazu)
	 * @return	string	upraveny retezec
	 * @author	Hugo
	 */
	function safeStr($str, $allow = '') {
		return preg_replace("/[^a-z0-9_\|$allow]/", '_', strtolower($str));
	}

	/**
	 * Vrati adresarovou strukturu i se soubory (vse serazeno dle abecedy)
	 * @param	string	adresar, jehoz obsah ma byt vracen
	 * @param	boolean	prochazet i podadresare
	 * @return	array	adresarova struktura jako pole, slozky jsou jako pole (index je nazev), soubory jsou retezce (index obsahuje por.cislo)
	 * @author	Hugo (inspirace v CodeIngniteru)
	 */
	function directoryMap($dir, $recursive = TRUE)
	{
		if ($fp = @opendir($dir))
		{
			$file_data = array();
			$folder_data = array();
			while (FALSE !== ($file = readdir($fp)))
			{
				if (@is_dir($dir.$file) && substr($file, 0, 1) != '.')
				{
					$temp_array = array();
					if ($recursive) $temp_array = directoryMap($dir.$file."/");
					$folder_data[$file] = $temp_array;
				}
				elseif (substr($file, 0, 1) != ".")
				{
					$file_data[] = $file;
				}
			}
			ksort($folder_data);
			natcasesort($file_data);
			return $folder_data + $file_data;
		} return FALSE;
	}

	/**
	 * Overi platnost emailu
	 * @param	string	email
	 * @return	boolean	pokud je platny email, vrati TRUE
	 * @author	Hugo
	 */
	function isValidEmail($email) {
		if (ereg("^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@([_a-zA-Z0-9-]+\.)*[a-zA-Z0-9-]{2,200}\.[a-zA-Z]{2,6}$", $email)) return TRUE;
		return FALSE;
	}

	/**
	 * Funkce vrati obsah zadane sablony
	 * @param	string	nazev souboru v adresari templates (bez pripony .php)
	 * @param	array	pole promennych, ktere jsou budou dispozici uvnitr zablony
	 * @return	string	vyhodnocena sablona
	 * @author	Hugo
	 */
	function template($filename, $vars = NULL, $parse = TRUE)
	{
		global $config;
		$filename = $config['site']['template_path'] . "$filename.php";

		if (file_exists($filename))
		{
			if (!empty($vars)) extract($vars);
			ob_start();
			include($filename);
			$buffer = ob_get_contents();
			@ob_end_clean();

			if ($parse)
			{
				// doplneni promennych
				$variable = '[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*';
				$parse_pattern = "/\{($variable)\}/";
				preg_match_all($parse_pattern, $buffer, $result);
				foreach (element(1, $result) as $variable) if (isset($$variable)) $buffer = str_replace('{'.$variable.'}', $$variable, $buffer);
			}

			return $buffer;

		} else echo "Šablona '$filename' nenalezena.";
	}

	/**
	 * Funkce spusti zadany skript a vrati obsah promenne data (slouzi ke spousteni datovych sablon)
	 * @param	string	nazev souboru v adresari templates (bez pripony .php)
	 * @param	array	pole promennych, ktere jsou budou dispozici uvnitr zablony
	 * @return	string	obsah promenne data (musi ji naplnit volany skript)
	 * @author	Hugo
	 */
	function getData($filename, $vars = NULL) {
		global $config;
		if (file_exists($config['site']['template_path'] . "$filename.php")) {
			if (!empty($vars)) extract($vars);
			$data = array();
			include($config['site']['template_path'] . "$filename.php");
			return $data;
		} else echo "Šablona '$filename' nenalezena.";
	}

	/**
	 * Funkce nahraje jazykovou sablonu do globalniho pole $langData
	 * @param	string	nazev souboru v adresari templates (bez pripony .php a pripony s identifikatorem jazyka)
	 * @param	string	identifikator jazyk (cz, en, de, apod.)
	 * @param	bool	zobrazit chybovou hlasku, pokud sablona neexistuje?
	 * @author	Hugo
	 */
	function getLang($filename, $lang, $require = TRUE) {
		global $config;
		if (!empty($filename)) {
			if (file_exists($config['site']['template_path'] . "$filename.$lang.php")) {
				global $langData;
				include($config['site']['template_path'] . "$filename.$lang.php");
			} else if ($require) echo "Šablona '$filename.$lang' nenalezena.";
		}
	}

	/**
	 * Funkce prevede HTML kod na plain text s tim, ze zachova urcita formatovani a linky.
	 * Primarne urceno pro vytvoreni textove alternativy k HTML emailu.
	 * Funkce je stale ve stadiu *BETA*.
	 * @param	string	html text
	 * @return	string	plain text
	 * @author	Hugo
	 */
	function html2text($html) {
		// odstraneni hlavicky a dalsich veci mimo <body>
		$html = preg_replace('/.*<body.*?>/s', "", $html);
		$html = preg_replace('/<\/body>.*/s', "", $html);
		// nove radky nemaji vyznam
		$html = preg_replace('/(\n|\r)/', " ", $html);
		// nahrazeni odkazu
		$html = preg_replace('/<a.*?href="(.*?)".*?>(.*?)<\/a>/', "$2 [$1]\n", $html);
		// odradkovani za nekterymi elementy
		$html = str_replace(array("</div>", "</p>", "<br/>", "<br>", "<br/ >", "</h1>", "</h2>", "</h3>", "</h4>", "</h5>", "</h6>", "</table>", "</tr>"), "\n", $html);
		// nahrazeni bunek tabulky za tabulatory
		$html = str_replace(array("</td>", "</th>"), "\t\t", $html);
		// odstraneni HTML tagu
		$html = strip_tags($html);
		// odstraneni tvrdych mezer
		$html = str_replace('&nbsp;', ' ', $html);
		// prazdne a zdvojene radky pryc
		$html = preg_replace('/(\n(\t| )*\n){2,}/', "\n", $html);
		// zdvojene mezery nebo tabulatory pryc
		$html = preg_replace('/( |\t){2,}/', "$1", $html);
		// mezery nebo tabulatory na zacatku radku pryc
		$html = preg_replace('/\n( |\t)+/', "\n", $html);
		return trim($html);
	}

	/**
	 * Funkce pro usnadneni zasilani emailu pomoci tridy phpMailer. Umi pouzit pripadne nastaveni SMTP v config.php
	 * 
	 * @param	string	email odesilatele. pokud je zadano pole, pouzije se prvni element jako email a druhy element jako jmeno.
	 *					pokud je zadano NULL, pouzije se nastaveni z config.php
	 * @param	string	email prijemce. je mozno zadat vice emailu (jako pole nebo oddelit carkami)
	 * @param	string	predmet zpravy
	 * @param	string	telo zpravy (text nebo html)
	 * @param	boolean	pokud je TRUE, povazuje se parametr $body za html
	 * @param	boolean	pokud je TRUE, ulozi se email do databaze (do tabulky data_sent_emails)
	 * @param	array	pole cest k souborum, ktere maji byt prilozeny k emailu
	 * @return	boolean	TRUE pri uspechu, FALSE pri chybe
	 * @author	Hugo
	 */
	function sendMail($from, $to, $subject, $body, $html = FALSE, $save = TRUE, $attachments = FALSE)
	{
		//require_once(BASEPATH.'/anyshape/libraries/phpmailer/class.phpmailer.php');

		global $config;
		$mail = new PHPMailer();

		// konfigurace pro STMP
		if (isset($config['email']['smtp']) && $config['email']['smtp'] == TRUE)
		{
			$mail->IsSMTP();
			$mail->Host = $config['email']['smtp_host'];
			$mail->Port = $config['email']['smtp_port'];
			$mail->Username = $config['email']['smtp_username'];
			$mail->Password = $config['email']['smtp_password'];
			if ($config['email']['smtp_ssl']) $mail->SMTPSecure == 'ssl';
			else if ($config['email']['smtp_tls']) $mail->SMTPSecure == 'tls';
		}

		// odesilatel
		if (is_array($from))
		{
			$mail->From = element(0, $from);
			$mail->FromName = element(1, $from);
		}
		else if ($from === NULL)
		{
			$mail->From = $config['email']['from'];
			$mail->FromName = $config['email']['from_name'];
		}
		else
		{
			$mail->From = $from;
			$mail->FromName = $from;
		}

		// adresat
		if (is_array($to))
		{
			foreach ($to as $to_item)
			{
				$mail->AddAddress($to_item);
			}
		}
		else
		{
			$mail->AddAddress($to);
		}

		$mail->CharSet = 'UTF-8';

		// predmet a telo
		$mail->Subject = $subject;
		if ($html)
		{
			$mail->AltBody = html2text($body);
			$mail->MsgHTML($body);
		}
		else
		{
			$mail->Body = $body;
		}

		// prilohy
		if (is_array($attachments) && count($attachments) > 0)
		{
			$attachmentsText = "";
			foreach ($attachments as $attachment)
			{
				if ($mail->AddAttachment($attachment)) $attachmentsText .= "OK: ";
				else $attachmentsText .= "FAIL: ";
				$attachmentsText .= $attachment . "\n";
			}
		}
		else
		{
			$attachmentsText = NULL;
		}

		// logovani
		if ($save)
		{
			dibi::query("INSERT INTO [:data:sent_emails]", array(
				'from_name'		=> (string) $mail->FromName,
				'from_email'	=> (string) $mail->From,
				'to'			=> (string) is_array($to) ? implode(",", $to) : $to,
				'subject'		=> (string) $subject,
				'source'		=> (string) $body,
				'attachments'	=> $attachmentsText
			));
		}

		// odeslani
		return $mail->Send();
	}

	/**
	 * Zobrazi zadanou promennou ve FireBugu v detailu pozadavku v zalozce Server.
	 * Je potreba mit nainstalovany FirePHP plugin.
	 * Logovani je potreba mit povolene v config.php
	 */
	function firedump($variable, $name = 'debug') {
		global $config;
		if (isset($config['site']['debug']) && $config['site']['debug'] == TRUE) {
			require_once(BASEPATH.'/anyshape/libraries/firephp/FirePHP.class.php');
			$firephp = FirePHP::getInstance(TRUE);
			$firephp->dump($name, $variable);
		}
	}

	/**
	 * Umoznuje logovani do konzole FireBugu.
	 * Je potreba mit nainstalovany FirePHP plugin.
	 * Logovani je potreba mit povolene v config.php
	 */
	function trace($text) {
		global $config;
		if (isset($config['site']['debug']) && $config['site']['debug'] == TRUE) {
			require_once(BASEPATH.'/anyshape/libraries/firephp/FirePHP.class.php');
			$firephp = FirePHP::getInstance(TRUE);
			$firephp->log($text);
		}
	}

	/**
	 * Zformatuje cenu na dve desetinna mista a pripadne nulove halere zobrazi jako pomlcku
	 * @param	int	castka
	 * @return	string	zformatovana castka
	 * @author	Hugo
	 */
	function price($n) {
		return str_replace(" ", "&nbsp;", str_replace(",00", "", number_format($n, 2, ',', ' ')));
	}

	/**
	 * Funkce incializuje objekt typu SimpleXMLElement dle zadaneho retezce.
	 * Pri uspechu vrati vytvoreny objekt. Pri chybe v XML vrati NULL
	 * @param	string	XML retezec
	 * @author	Hugo
	 */
	function XML($xmlString) {
		try { $xml = @new SimpleXMLElement($xmlString); } catch (Exception $e) { return NULL; }
		return $xml;
	}

	/**
	 * Funkce prevede danou cestu a nazev obrazku na format,
	 * ktereremu rozumi skript na znensovani obrazku
	 */
	function resize($file, $width, $height, $fullURL = TRUE, $crop = FALSE) {
		global $config;
		if ($crop) $height .= 'c';
		$hash = substr(hash("sha256", $width . $height . $file . 'AnyShapeResizeKey'), 5, 4);
		$result = dirname($file) . '/' . $width . 'x' . $height . '-' . $hash . '-' . basename($file);
		if ($fullURL) $result = $config['site']['resized_dir'] . $result;
		return $result;
	}

	/**
	 * Vrati nazev uzlu v zadanem jazyce
	 * @param	int		id uzlu
	 * @param	string	identifikator jazyka
	 * @return	string	nazev uzlu v zadanem jazyce
	 * @author	Hugo
	 */
	function nodeName($id, $language) {
		return dibi::query(
			'SELECT name.text FROM [:data:structure] AS s ',
			'LEFT JOIN [:data:texts] AS name ON name.id = s.name AND name.language_id = %s', $language,
			'WHERE s.id = %i', $id
		)->fetchSingle();
	}

	/**
	 * Vrati zadany element z globalniho pole $langData
	 * @param	int	index (od nuly)
	 * @param	string	implicitni hodnota v pripade, ze element neni zadan
	 * @return	string	nalezeny element nebo FALSE
	 * @author	Hugo
	 */
	function l($element) {
		global $langData;
		return element($element, $langData, $element);
	}

	/**
	 * Funkce prida do url promennou (jako query string). Pokud jiz tam je, tak jen zmeni jeji hodnotu.
	 * Funkce vraci jen query string zacinajici otaznikem (bez hostu).
	 * Pokud neni zadana hodnota, je promenna z url odstranena.
	 * Pokud neni zadan ani klic, vrati funkce aktualni query string.
	 * @param	string	nazev promenne
	 * @param	string	nova hodnota promenne
	 * @param	string	url, ktere menime; pokud neni zadano, bere se aktualni
	 * @author	Hugo
	 */
	function query($key = NULL, $value = NULL, $url = NULL) {
		// je URL zadano
		if ($url == NULL) {
			// musim si ho zjistit sam
			$get = array();
			if (count($_GET) > 0) foreach ($_GET as $index => $item) $get[$index] = get($index);
			$get = flatArray($get);
		}
		else {
			// odstranim pripadny otaznik
			if ($url[0] == '?') $url = substr($url, 1);
			// rozparsuju do pole
			parse_str(str_replace('&amp;', '&', $url), $get);
		}
		// odstranim polozku predavanou mod-rewritem
		if (isset($get['q'])) unset($get['q']);
		// odstraneni hodnoty
		if ($value === NULL && isset($get[$key])) unset($get[$key]);
		// vlozeni hodnoty
		else if ($key !== NULL) $get[$key] = $value;
		// vytvoreni query stringu
		$url = http_build_query($get);
		if (strlen($url) == 0) return '';
		else return str_replace('&', '&amp;', "?$url");
	}

	/**
	 * Funkce prevede vicerozmerne pole na jednorozmerne podobne, jak jsou
	 * predavana pole v GET a POST pozadavcich.
	 *
	 * Napr. pole
	 *    array('a' => 1, 'b' => array('c' => 2, 'd' => 3))
	 * je prevedeno na
	 *    array('a' => 1, 'b[c]' => 2, 'b[d]' => 3)
	 *
	 * @param	array	Vstupni pole
	 * @return	array	Vystupni pole
	 * @author	Hugo
	 */
	function flatArray($array, $parentKey = NULL) {
		if (!is_array($array)) return $array;
		$result = array();
		foreach ($array as $key => $element) {
			if ($parentKey === NULL) $newKey = $key;
			else $newKey = $parentKey . "[$key]";
			if (is_array($element)) $result = array_merge($result, flatArray($element, $newKey));
			else $result[$newKey] = $element;
		}
		return $result;
	}

	/**
	 * Funkce vytahne ze zadaneho XML text v zadanem jazyce
	 * @author Hugo
	 */
	function translate($text, $language_id)
	{
		$text = XML($text);
		if ($text && count($text->children()) > 0)
			foreach ($text->children() as $node)
				if ($node->language_id == $language_id) return $node->text;
		return '';
	}

	/**
	 * Vytvoření zámku při přechodu do kritické sekce
	 * @param  string  Název zámku
	 * @author Hugo
	 */
	function lock($lockName = 'critical')
	{
		global $anyShapeLocks, $config;

		if (!is_array($anyShapeLocks)) $anyShapeLocks = array();

		$handle = fopen($config['site']['temp_path'] . "/$lockName.lock", "w+");

		if (!flock($handle, LOCK_EX)) die("Couldn't lock the file!");
		else $anyShapeLocks[$lockName] = $handle;
	}

	/**
	 * Zrušení zámku při přechodu do kritické sekce
	 * @param  string  Název zámku
	 * @author Hugo
	 */
	function unlock($lockName = 'critical')
	{
		global $anyShapeLocks;

		if (isset($anyShapeLocks[$lockName]))
		{
			$handle = $anyShapeLocks[$lockName];
			flock($handle, LOCK_UN);
			fclose($handle);
		}
		else die("Couldn't unlock the file!");
	}
