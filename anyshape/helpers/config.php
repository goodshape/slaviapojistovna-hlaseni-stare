<?php

/**
 * Class Config
 *
 * @property-read string $wwwDir
 */
class Config implements ArrayAccess
{

    /** @var \Nette\DI\Container|\SystemContainer */
    private $container;

    private $parameters = [];

    function __construct(\Nette\DI\Container $container)
    {
        $this->container = $container;
        $parameters = $container->getParameters();
        if (!isset($parameters['anyshape'])) {
            throw new \InvalidArgumentException("Anyshape config was not found.");
        }
        $this->parameters = $parameters['anyshape'];

        $baseUrl = $container->getByType('Nette\\Http\\Request')->getUrl()->getBaseUrl();

        if (empty($this->parameters['site']['server_path'])) {

            if (strrpos($baseUrl, "anyshape") !== FALSE) {
                $baseUrl = substr($baseUrl, 0, strrpos($baseUrl, "anyshape"));
            }
        }
        if (empty($this->parameters['newsletter']['server_path'])) {
            $this->parameters['newsletter']['server_path'] = $this->parameters['site']['server_path'];
        }
        $this->parameters['site']['server_path'] = $baseUrl;
    }

    function &__get($name)
    {
        $val = $this->container->parameters[$name];
        return $val;
    }


    /**
     * @return array
     */
    public function getParameters()
    {
        return $this->parameters;
    }

    public function getParameter($name)
    {
        return isset($this->parameters[$name]) ? $this->parameters[$name] : NULL;
    }

    /**
     * @return DibiConnection
     */
    public function getDatabase()
    {
        return $this->container->getByType('DibiConnection');
    }

    public function offsetExists($offset)
    {
        return isset($this->parameters[$offset]);
    }

    public function offsetGet($offset)
    {
        return $this->getParameter($offset);
    }


    public function offsetSet($offset, $value)
    {
        throw new \LogicException("Setting of config value is not supported.");
    }

    public function offsetUnset($offset)
    {
        unset($this->parameters[$offset]);
    }

    public function isLocal()
    {
        return $this->container->parameters['debugMode'];
    }

	/**
	 * @return \Nette\DI\Container|SystemContainer
	 */
	public function getContainer()
	{
		return $this->container;
	}
}


/**
 *    Funkce pouzite pri konfiguraci
 **/

/**
 * Vráti TRUE, pokud base url obsahuje řetězec ".local/"
 */
function isLocal()
{
    /** @var Config $config */
    global $config;

    return strpos(@$_SERVER['HTTP_HOST'], '.local') !== FALSE ||
    strpos(@$_SERVER['SERVER_ADDR'], '192.168.') !== FALSE ||
    strpos(@$_SERVER['SERVER_ADDR'], '10.0.') !== FALSE || $config->isLocal();
}

/**
 * Funkce detekuje kořenovou www adresu projektu (tzv. baseURL).
 * Jedná se o velmi primitivní algoritmus, který je fukční
 * jen za určitých podmínek.
 */
function detectServerPath()
{
    $result = "http://" . $_SERVER['HTTP_HOST'];
    $result .= str_replace(array("admin.php", "anyshape/wysiwyg/index.php", "anyshape/wysiwyg/urllist.php"), "", $_SERVER['REQUEST_URI']);

    return $result;
}

/**
 * Vrátí proměnnou z konfigurace
 * @param  [type] $name [description]
 * @return [type]
 */
function getConfigVariable($name)
{
    global $config;

    return $config->getParameter($name);
}

function getDibiConnection()
{
    global $config;

    return $config->getDatabase();

}
