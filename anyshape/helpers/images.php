<?php

	/**
	 *	Funkce pro manipulaci s obrazky (zatim jen s pomoci GD)
	**/

	/**
	* Vrati spravnou funci pro manipulaci s danym typem obrazku
	* @param	string		MIME typ obrazku
	* @param	function	vrati funkci pro vytvoreni obrazku
	* @param	function	vrati funkci pro ulozeni obrazku
	* @return	true, pokud je mozno dany obrazek zmensit
	* @author	Hugo
	*/
	function imageHandlers($type, &$imgcreate, &$imgsave)
	{
		if ($type == IMAGETYPE_JPEG)
		{
			$imgcreate = create_function('$f' ,'return imagecreatefromjpeg($f);');
			$imgsave = create_function('$from, $dest' ,'return imagejpeg($from, $dest);');
			return true;
		}
		else if ($type == IMAGETYPE_GIF)
		{
			$imgcreate = create_function('$f' ,'return imagecreatefromgif($f);');
			$imgsave = create_function('$from, $dest' ,'return imagegif($from, $dest);');
			return true;
		}
		else if ($type == IMAGETYPE_PNG)
		{
			$imgcreate = create_function('$f' ,'return imagecreatefrompng($f);');
			$imgsave = create_function('$from, $dest' ,'return imagepng($from, $dest);');
			return true;
		}
		else if ($type == IMAGETYPE_WBMP)
		{
			$imgcreate = create_function('$f' ,'return imagecreatefromwbmp($f);');
			global $config;
			$q = $config['images']['quality'];
			$imgsave = create_function('$from, $dest' ,'return imagewbmp($from, $dest, ' . $q . ');');
			return true;
		}
		
		return false;
	}

	/**
	* Zmensi zadany obrazek na danou velikost. Pokud je obrazek mensi, ponecha ho bez zmen a vrati false
	* Je zachovan pomer stran obrazku. Pokud je nektery z rozmeru zadan jako nulovy, je zachovan puvodni
	* rozmer obrazku. Umi zpracovavat i obrazky s pruhlednym pozadim.
	* 
	* @param	string	cesta ke zdrojovemu obrazku (relativne ke koreni aplikace)
	* @param	string	cesta k cilovemu obrazku (relativne ke koreni aplikace)
	* @param	int		pozadovana sirka v pixelech
	* @param	int		pozadovana vyska v pixelech
	* @param	bool	obrazek je orezan, tak aby zaplnil celou plochu pozadovaneho vyrezu
	* @return	0 - obrazek se podarilo zmensit
	* 			1 - obrazek neni treba zmensovat
	* 			2 - zdrojovy obrazek nebyl nalezen
	* 			3 - obrazek nema spravny format
	* 			4 - chyba pri vytvoreni obrazku v pameti
	* 			5 - chyba pri ulozeni obrazku
	* 			6 - soubor jiz existuje a nelze smazat
	* @author	Hugo
	*/
	function resizeImage($source, $destination, $wwwDir, $newWidth, $newHeight, $crop = FALSE)
	{
		// pokud neexistuje cislovy adresar, vytvorime ho
		if (!file_exists($wwwDir.'/'.dirname($destination))) {
            mkPath(dirname($destination), $wwwDir);
        }
        $destination = $wwwDir . '/' . $destination;

		// pokud nexistuje zdrojovy soubor, skoncime
		if (!file_exists($source) || !is_file($source)) return 2;
		
		// pokud cilovy soubor jiz existuje, musime ho smazat
		if (file_exists($destination)) if (!@unlink($destination)) return 6;

		// zjistime informace o obrazku
		list($width, $height, $type, $attr) = @getimagesize($source);

		// poznamenam si puvodni rozmery obrazku
		$originalWidth = $width;
		$originalHeight = $height;

		// pokud neni potreba obrazek zmenosvat, skoncime
		// if ($newWidth > $width && $newHeight > $height) return 1;

		// pokud je zadana nula, zachovame puvodni rozmer
		if ($newWidth == 0) $newWidth = $width;
		if ($newHeight == 0) $newHeight = $height;

		// je potreba zachovat pomer stran (a zaroven dat bacha, aby nam nevysel jeden rozmer jako nula)
		if ($crop)
		{
			if ($newWidth / $width < $newHeight / $height) $width = max(1, round($newWidth / ($newHeight / $height)));
			else $height = max(1, round($newHeight / ($newWidth / $width)));
		}
		else
		{
			if ($newWidth / $width < $newHeight / $height) $newHeight = max(1, round($height * ($newWidth / $width)));
			else $newWidth = max(1, round($width * ($newHeight / $height)));
		}

		// je mozne pouzit Imagick?
		if (class_exists('Imagick'))
		{
			$thumb = new Imagick();
			$thumb->readImage($source);
			global $config;
			$thumb->setCompressionQuality($config['images']['quality']);

			// pokud bude potreba rezat, orezu nejdrive puvodni obrazek
			if ($crop) 
			{
				// orezove okno nastavim tak, aby bylo nacentrovane
				$cropX = floor(($originalWidth - $width) / 2);
				$cropY = floor(($originalHeight - $height) / 2);
				
				if (!@$thumb->cropImage($width, $height, $cropX, $cropY)) return 3;
				if (!@$thumb->setImagePage(0, 0, 0, 0)) return 3;
			}

			// provedu resize
			if (!@$thumb->resizeImage($newWidth, $newHeight, Imagick::FILTER_TRIANGLE, 0.75)) return 3;

			// ulozim na disk
			if (!@$thumb->writeImage($destination)) return 5;
			@$thumb->clear();
			@$thumb->destroy(); 

			@chmod($destination, 0777);

			return 0;

		} else 
			// umime tento typ zmensit?
			if (imageHandlers($type, $imgCreateHandler, $imgSaveHandler))
			{
				// vytvorime v pameti bitmapu pro cilovy obrazek
				if (!($newImage = @imagecreatetruecolor($newWidth, $newHeight))) return 4;

				// pokud jde o PNG nebo GIF, je potreba nastavit pruhlednost
				if ($type == IMAGETYPE_GIF || $type == IMAGETYPE_PNG)
				{
					imagealphablending($newImage, false);
					imagesavealpha($newImage, true);
					$transparent = imagecolorallocatealpha($newImage, 255, 255, 255, 127);
					imagefilledrectangle($newImage, 0, 0, $newWidth, $newHeight, $transparent);
				}

				// nacteme do pameti zdrojovy obrazek
				if (!($sourceImage = $imgCreateHandler($source))) return 4;

				// pokud bude potreba rezat, orezu nejdrive puvodni obrazek
				$cropX = 0;
				$cropY = 0;
				if ($crop) 
				{
					// orezove okno nastavim tak, aby bylo nacentrovane
					$cropX = floor(($originalWidth - $width) / 2);
					$cropY = floor(($originalHeight - $height) / 2);
				}

				// provedeme resampling (zmenseni)
				imagecopyresampled($newImage, $sourceImage, 0, 0, $cropX, $cropY, 
					$newWidth, $newHeight, $width, $height);

				// ulozime na disk
				$result = $imgSaveHandler($newImage, $destination);
				
				@chmod($destination, 0777);

				// uvolnime pamet
				imagedestroy($newImage);
				imagedestroy($sourceImage);

				if ($result) return 0;
				else return 5;

			} else return 3;

	}

	/**
	* Na obrazek je je aplikovan vodoznak. 
	* 
	* @param	string	cesta ke zdrojovemu obrazku (relativne ke koreni aplikace)
	* @param	string	cesta k cilovemu obrazku (relativne ke koreni aplikace)
	* @param	int		pozadovana sirka v pixelech
	* @param	int		pozadovana vyska v pixelech
	* @return	0 - vodoznak byl aplikovan
	* 			1 - vodoznak se zadanym id nebyl nalezen
	* 			2 - zdrojovy soubory nebyl nalezen
	* 			3 - obrazek nema spravny format
	* 			4 - chyba pri vytvoreni obrazku v pameti
	* 			5 - chyba pri ulozeni obrazku
	* 			6 - cilovy soubor jiz existuje a nelze smazat
	* @author	Hugo
	*/
	function applyWatermark($source, $destination, $watermark, $position, $size, $hPadding, $vPadding)
	{
		// pokud neexistuje zadany watermark, skoncime
		if (!file_exists($watermark)) return 1;

		// pokud neexistuje cilovy adresar, vytvorime ho
		if (!file_exists(dirname($destination))) mkPath(dirname($destination));

		// pokud nexistuje zdrojovy soubor, skoncime
		if (!file_exists($source) || !is_file($source)) return 2;

		// pokud cislovy soubor jiz existuje, musime ho smazat
		if (file_exists($destination)) if (!@unlink($destination)) return 6;

		// zjistime informace o obrazku
		if ((list($width, $height, $type, $attr) = @getimagesize($source)) === FALSE) return 2;

		// zjistime informace o watermarku
		if ((list($watermarkWidth, $watermarkHeight, $watermarkType, $watermarkAttr) = @getimagesize($watermark)) === FALSE) return 1;

		// prepocitam velikost watermarku
		$newWidth = round($width * $size / 100);
		$newHeight = round($height * $size / 100);

		if ($newWidth / $watermarkWidth < $newHeight / $watermarkHeight) $newHeight = max(1, round($watermarkHeight * ($newWidth / $watermarkWidth)));
		else $newWidth = max(1, round($watermarkWidth * ($newHeight / $watermarkHeight)));

		// implicitne nastavime pozici na stred
		$x = round(($width - $newWidth) / 2);
		$y = round(($height - $newHeight) / 2);

		// procentualni padding prepocitame na pixely
		$hPadding = $hPadding / 100 * $width;
		$vPadding = $vPadding / 100 * $height;

		// pozmenime pozici dle potreby
		switch ($position)
		{
			case 1:	{ $x = $hPadding; $y = $vPadding; break; }
			case 2:	{ $y = $vPadding; break; }
			case 3:	{ $x = $width - $newWidth - $hPadding; $y = $vPadding; break; }
			case 4:	{ $x = $hPadding; break; }
			case 5:	{ $x = $width - $newWidth - $hPadding; break; }
			case 6:	{ $x = 0; $y = $height - $newHeight - $vPadding; break; }
			case 7:	{ $y = $height - $newHeight - $vPadding; break; }
			case 8:	{ $x = $width - $newWidth- $hPadding; $y = $height - $newHeight - $vPadding; break; }
		}


		// je mozne pouzit Imagick?
		if (class_exists('Imagick')) {

			try {
				$sourceImage = new Imagick();
				$sourceImage->readImage($source);
				$sourceImage->setCompressionQuality(90);
	
				$watermarkImage = new Imagick();
				$watermarkImage->readImage($watermark);
				$watermarkImage->setCompressionQuality(90);
			} catch (Exception $e) {
				return 3;
			}

			if (!@$watermarkImage->resizeImage($newWidth, $newHeight, Imagick::FILTER_TRIANGLE, 0.75)) return 3;		
			if (!@$sourceImage->compositeImage($watermarkImage, Imagick::COMPOSITE_OVER, $x, $y)) return 3;		
			if (!@$sourceImage->writeImage($destination)) return 5;
			@$watermarkImage->clear();
			@$watermarkImage->destroy(); 
			@$sourceImage->clear();
			@$sourceImage->destroy(); 

			@chmod($destination, 0777);

			return 0;

		}
		else
		{
			// umime tento typ nacist?
			if (imageHandlers($type, $imgCreateHandler, $imgSaveHandler) &&
				imageHandlers($watermarkType, $watermarkImgCreateHandler, $watermarkImgSaveHandler)) {

				// nacteme do pameti zdrojovy obrazek
				if (!($sourceImage = $imgCreateHandler($source))) return 4;

				imagealphablending($sourceImage, true);
				imagesavealpha($sourceImage, true);

				// nacteme do pameti zdrojovy obrazek
				if (!($watermarkImage = $watermarkImgCreateHandler($watermark))) return 4;

				// provedeme resampling (zmenseni)
				imagecopyresampled($sourceImage, $watermarkImage, $x, $y, 0, 0, $newWidth, $newHeight, $watermarkWidth, $watermarkHeight);

				// ulozime na disk
				$result = $imgSaveHandler($sourceImage, $destination);

				@chmod($destination, 0777);

				// uvolnime pamet
				imagedestroy($watermarkImage);
				imagedestroy($sourceImage);

				if ($result) return 0;
				else return 5;

			} return 3;
		}

	}
