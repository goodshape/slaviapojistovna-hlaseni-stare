<?php

	/**
	 *	Funkce pro frontend
	 **/

	function error($msg, $info = '') {
		echo "Error: $msg $info";
		exit();
	}

	/**
	* Vezme zadane URL (pokud neni zadne, zjisti si ho sam) a nahradi v nem zadany segment hodnotou $value
	* @param	string	hodnota, ktera ma byl vlozena do URL
	* @param	int		pozice segmentu v URL (od nuly)
	* @param	string	url, v kterem se ma provest nahrazeni (neni-li zadanou bere se aktalni)
	* @return	string	obsah promenne data (musi ji naplnit volany skript)
	* @author	Hugo
	*/
	function urlAdd($value, $segment, $url = null) {
		if ($url == null) $url = explode('/', get('q'));
		if ($segment < count($url)) $url[$segment] = $value;
		return implode('/', $url);
	}

	/**
	* Vrati element z URL. Funkce je upravena pro pouziti na frontEndu - bere v potaz globalni promennou "$URLOffset"
	* @param	int		index (od nuly); je-li zaporny, bere se od konce (-1 ... posledni element, -2 ... predposledni element) a URLOffset se nepocita
	* @param	string	implicitni hodnota v pripade, ze element neni zadan
	* @return	string	nalezeny element nebo false
	* @author	Hugo
	*/
	function url($index, $default = '') {
		global $URLOffset;
		$segments = explode('/', get('q'));
		if (element(count($segments)-1, $segments) == '') unset($segments[count($segments)-1]);
		if ($index < 0) return element(count($segments) + $index, $segments, $default);
		else return element($index+$URLOffset, $segments, $default);
	}

	/**
	* Vrati seznam id potomku daneho uzlu
	* @param	int		id uzlu
	* @param	bool	zahranout do vysledku i zadany rodic. uzel?
	* @param	int		maximalni hloubka zanoreni (0 = neomezene)
	* @return	array	seznam id potomku
	* @author	Hugo
	*/
	function getChildNodeIds($nodeId, $includeParent = true, $maxDepth = 0) {
		$ids = $includeParent ? array($nodeId) : array();
		$childs = dibi::query("SELECT id FROM [:data:structure] WHERE parent_id = %i", $nodeId)->fetchPairs();
		$ids = array_merge($ids, $childs);
		$depth = 1;
		while (!empty($childs) && ($maxDepth == 0 || $depth < $maxDepth)) {
			$childs = dibi::query("SELECT id FROM [:data:structure] WHERE parent_id IN (%sql)", implode(', ', $childs))->fetchPairs();
			$ids = array_merge($ids, $childs);
			$depth++;
		}
		return $ids;
	}

	/**
	* Funkce zobrazi sablonu s chybovou hlaskou a posle v hlavicce kod 404
	* @param	int		id layoutu
	* @author	Hugo
	*/
	function show404page() {
		header("HTTP/1.0 404 Not Found");
		echo template('404');
		exit();
	}

	/**
	* Funkce vraci true, pokud je zadany soubor typu obrazek (rozhoduje se dle pripony)
	* @param	string	jmeno souboru
	* @author	Hugo
	*/
	function isImage($filename) {
		$extension = pathinfo($filename, PATHINFO_EXTENSION);
		if ($extension == 'png' || $extension == 'jpg' || $extension == 'jpeg' || $extension == 'gif') return true;
		else return false;
	}

	/**
	* Funkce oreze retezec na zadanou delku.  Zachova cele slovo, takze vysledny retezec muze byt kratsi.
	* @param	string	orezavany retezec
	* @param	int		pocet znaku
	* @param	string	tri tecky nebo tak neco
	* @author	Uloupeno v CodeIngiteru, upravil Hugo
	*/
	function limit($str, $n = 500, $end = '&#8230;') {
		if (strlen($str) < $n) return $str;
		$str = preg_replace("/\s+/", ' ', str_replace(array("\r\n", "\r", "\n"), ' ', $str));
		if (strlen($str) <= $n)	return $str;
		$out = "";
		foreach (explode(' ', trim($str)) as $val) {
			if (strlen($out.$val.' ') >= $n) return trim($out).$end;
			$out .= $val.' ';
		}
	}

	/**
	* Ochrana pred XSS. Je potreba pouzit pro vsechny promenne, ktere jdou na vystup.
	* V tuto chvili je to jen zkratka pro htmlspecialchars(). Predpokladam, ze je to dostatecna ochrana.
	* @param	string	vstupni retezec
	* @param	string	filterovany retezec
	* @author	Hugo
	*/
	function xss($str) {
		return htmlspecialchars($str, ENT_QUOTES);
	}

	/**
	* Ochrana emailovych adres pred spamboty. Funkce nejdrive nahradi odkazy s 'mailto',
	* pote prevede vsechny emailove adresy v textu. Pouziva prevod na html entity (a => &#97;)
	* a samotny retezec rozdeli na tri casti, aby ho nebylo ve zdrojovem kodu najit pomoci beznych technik.
	* @param	string	vstupni retezec
	* @return	string	retezec, kde jsou vsechny emaily nebo mailto-linky prevedeny na zakodovany retezec
	* @author	Hugo
	*/
	function safeMail($str) {
		$pattern = "[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})";
		preg_match_all("/mailto:$pattern/", $str, $split);
		foreach ($split[0] as $email) {
			$emailPart = explode('@', $email);
			$str = str_replace($email, "javascript:location.href='" . encodeEmail($emailPart[0]) . "'+'@'+'" . encodeEmail($emailPart[1]) . "'", $str);
		}
		preg_match_all("/$pattern/", $str, $split);
		foreach ($split[0] as $email) {
			$emailPart = explode('@', $email);
			$str = str_replace($email, "<script type=\"text/javascript\">document.write('" . encodeEmail($emailPart[0]) . "'+'@'+'" . encodeEmail($emailPart[1]) . "');</script>", $str);
		}
		return $str;
	}

	/**
	* Prevede zadany text na na html entity (a => &#97;).
	* @param	string	vstupni retezec
	* @return	string	prevedeny retezec
	* @author	Hugo
	*/
	function encodeEmail($str) {
		$output = '';
		for ($i = 0; $i < strlen($str); $i++) $output .= '&#'.ord($str[$i]).';';
		return $output;
	}
