<?php
    require_once __DIR__ .'/../bootstrap.php';
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="cz">
<head>

	<title><?=$config['site']['title']?> <?=$config['site']['version']?></title>

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<link rel="stylesheet" type="text/css" href="history/history.css" />
	<link rel="shortcut icon" href='favicon.ico' />

	<style type="text/css">
		html, body { margin: 0px; background: #f0f0f0; text-align: center; width: 100%; height: 100%; overflow: hidden; }
		img { border: none; margin-top: 200px;}
		#AnyShape { width: 100%; height: 100%; position: absolute; z-index: 200; top: 0; left: 0; }
		#wysiwyg { position: absolute; z-index: 100; background-color: transparent; border: 0px; top: 120px; left: 0; }
	</style>

	<script type="text/javascript" src="libraries/swfobject.js"></script>
	<script type="text/javascript" src="libraries/swfmacmousewheel2.js"></script>
	<script type="text/javascript" src="history/history.js"></script>

	<script type="text/javascript">
		var flashvars = {};
		var params = {
			play: "true",
			loop: "false",
			quality: "high",
			bgcolor: "#f0f0f0",
			allowscriptaccess: "sameDomain"
		};
		var attributes = {
			id: "AnyShape",
			name: "AnyShape",
			align: "middle"
		};
		swfobject.embedSWF("AnyShape.swf", "AnyShape", "100%", "100%", "9.0.0", "expressInstall.swf", flashvars, params, attributes);
		if (swfobject.hasFlashPlayerVersion("9.0.0")) swfobject.addDomLoadEvent(anyShapeSetFocus);
		if (swfmacmousewheel) swfmacmousewheel.registerObject(attributes.id);

		var loggedOut = false;
		var focusCounter = 0;
		var unloadMsg = 'Opravdu chcete zavřít AnyShape? Vaše práce nebude uložena!';

		function init() {
			setTimeout(anyShapeSetFocus, 3000);
			setTimeout(anyShapeSetFocus, 1000);

			if (window.addEventListener) window.addEventListener("beforeunload", anyShapeUnload, false);
			else if (window.attachEvent) window.attachEvent ("onbeforeunload", anyShapeUnload);
			else window.onbeforeunload = anyShapeUnload;

			if (window.addEventListener) window.addEventListener("resize", resizeHandler, false);
			else if (window.attachEvent) window.attachEvent ("onresize", resizeHandler);
			else window.onresize = resizeHandler;
		}

		function resizeHandler(event) {
			var iframe = document.getElementById('iframe_wysiwyg');
			iframe.width = document.getElementById('AnyShape').clientWidth;
			iframe.height = document.body.clientHeight - 120;
		}

		function anyShapeSetFocus()	{
			var obj = document.getElementById('AnyShape');
			if (obj) obj.focus();
		}

		function anyShapeUnload(e) {
			if (!loggedOut) {
				var e = e || window.event;
				// For IE and Firefox
				if (e) e.returnValue = unloadMsg;
				// For Safari
				return unloadMsg;
			}
		}

		function showWysiwyg() {
			var anyshape = document.getElementById('AnyShape');
			var iframe = document.getElementById('iframe_wysiwyg');
			if (anyshape.clientHeight == 120) return;
			iframe.style.top = '120px';
			iframe.style.left = '0px';
			iframe.width = document.getElementById('AnyShape').clientWidth;
			iframe.height = document.getElementById('AnyShape').clientHeight - 120;
			anyshape.style.height = '120px';
			setTimeout('wysiwygSetFocus()', 200);
		}

		function wysiwygSetFocus() {
			var iframe = document.getElementById('iframe_wysiwyg');
			if (iframe && iframe.contentWindow && iframe.contentWindow.tinyMCE)
				iframe.contentWindow.tinyMCE.execCommand('mceFocus', false, 'iframe_wysiwyg');
		}

		function hideWysiwyg() {
			document.getElementById('AnyShape').style.height = '100%';
			document.getElementById('AnyShape').focus();
		}

		function makeLogout() {
			loggedOut = true;
			window.location.href = '<?=$config['site']['server_path']?>';
		}

		function setUnloadMsg(text) {
			unloadMsg = text;
		}

		function changeLanguage(lang) {
			if (!lang) lang = 'cz';
			document.getElementById('iframe_wysiwyg').src = '../wysiwyg/?lang=' + lang;
		}

		function showManual() {
			window.open("../manual/", "AnyShapeManual", "width=800,height=600,scrollbars=yes,menubar=no,resizable=yes,left=50,top=50");
		}

	</script>

</head>
<body onload="init()">

	<div id="AnyShape">
		<a href="http://www.adobe.com/go/getflashplayer">
			<img src="get_flash.gif" alt="Get Adobe Flash player" />
		</a>
	</div>

	<div id="wysiwyg">
		<iframe id="iframe_wysiwyg" name="wysiwyg" src="javascript:void(0)" frameborder="0"></iframe>
	</div>

</body>
</html>