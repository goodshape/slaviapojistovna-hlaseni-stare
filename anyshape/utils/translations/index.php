<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://opengraphprotocol.org/schema/" xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
	<title>AnyShape - translations</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
<?php

	/**
	 *  Seznam překladů v XML souboru a jejich použití ve zdrojových kódech
	 */

	require_once __DIR__ .'/../../bootstrap.php';
	define('SRCPATH', BASEPATH . '/../flex/');

	$files = directoryMap(SRCPATH);

	$translations = getTranslationsFromSource($files);

	$xmlCs = XML(file_get_contents(BASEPATH . "/anyshape/core/languages/default.xml"));
	$xmlEn = XML(file_get_contents(BASEPATH . "/anyshape/core/languages/english.xml"));

	echo "<div style='font-family: sans-serif;'>";
	echo "<h1>Seznam překladů v XML souboru a jejich použití ve zdrojových kódech</h1>";

	echo "<table border=1 cellpadding=5 style='float: left; width: 500px; border-collapse: collapse; font-size: 10px;'>";

	$dictCs = array();

	foreach ($xmlCs->group as $group)
	{
		echo '<tr><th colspan=3>' . $group['name'] . '</th></tr>';
		foreach ($group->item as $item)
		{
			$key = (string) $item['key'];

			$dictCs[] = $key;

			echo "<tr><td>" . $key . "</td>";
			if (isset($translations[$key]))
			{
				echo "<td>" . implode(", ", $translations[$key]) . "</td>";
			}
			else
			{
				echo "<td>-</td>";
			}
			echo "</tr>";
		}
	}

	$dictCsDups = array_diff_key($dictCs, array_unique($dictCs));
	$dictCs = array_unique($dictCs);

	echo "</table>";

	echo "<table border=1 cellpadding=5 style='float: left; width: 500px; border-collapse: collapse; font-size: 10px;'>";

	$dictEn = array();

	foreach ($xmlEn->group as $group)
	{
		echo '<tr><th colspan=3>' . $group['name'] . '</th></tr>';
		foreach ($group->item as $item)
		{
			$key = (string) $item['key'];

			$dictEn[] = $key;

			echo "<tr><td>" . $key . "</td>";
			if (isset($translations[$key]))
			{
				echo "<td>" . implode(", ", $translations[$key]) . "</td>";
			}
			else
			{
				echo "<td>-</td>";
			}
			echo "</tr>";
		}
	}

	$dictEnDups = array_diff_key($dictEn, array_unique($dictEn));
	$dictEn = array_unique($dictEn);

	echo "</table>";

	echo "<div style='clear: both;'></div><br/>";

	echo "<h1>Seznam řetězců, které nemají překlad v XML souboru</h1>";
	echo "<table border=1 cellpadding=5 style='float: left; width: 500px; border-collapse: collapse; font-size: 10px;'>";

	foreach ($translations as $key => $files)
	{
		if (!in_array($key, $dictCs))
			echo "<tr><td>$key</td><td>" . implode(", ", $translations[$key]) . "</td></tr>";
	}

	echo "</table>";

	echo "<table border=1 cellpadding=5 style='float: left; width: 500px; border-collapse: collapse; font-size: 10px;'>";

	foreach ($translations as $key => $files)
	{
		if (!in_array($key, $dictCs))
			echo "<tr><td>$key</td><td>" . implode(", ", $translations[$key]) . "</td></tr>";
	}

	echo "</table>";

	echo "<div style='clear: both;'></div><br/>";

	echo "<h1>Seznam řetězců, které jsou v XML souboru zdvojené</h1>";

	echo "<table border=1 cellpadding=5 style='float: left; width: 500px; border-collapse: collapse; font-size: 10px;'>";

	foreach ($dictCsDups as $item)
	{
		echo "<tr><td>$item</td></tr>";
	}

	echo "</table>";

	echo "<table border=1 cellpadding=5 style='float: left; width: 500px; border-collapse: collapse; font-size: 10px;'>";

	foreach ($dictEnDups as $item)
	{
		echo "<tr><td>$item</td></tr>";
	}

	echo "</table>";

	echo "<div style='clear: both;'></div><br/>";

	echo "<h1>Seznam řetězců, které si neodpovídají v různých jazicích</h1>";

	echo "<table border=1 cellpadding=5 style='float: left; width: 500px; border-collapse: collapse; font-size: 10px;'>";

	foreach ($dictCs as $key => $item)
	{
		if ($dictCs[$key] != $dictEn[$key])
			echo "<tr><td>" . $dictCs[$key] . " != " . $dictEn[$key] . "</td></tr>";
	}

	echo "</table>";

	/**
	 * Vrati seznam vsech prekladanych textu ve zdrojovych kodech
	 */
	function getTranslationsFromSource($files, $path = '')
	{
		$result = array();

		foreach ($files as $index => $file)
		{
			if (is_array($file)) $result = array_merge($result, getTranslationsFromSource($file, $path . $index . '/'));
			else
			{
				if (substr($file, -3) == '.as' || substr($file, -5) == '.mxml')
				{
					//echo $path . $file . "<br/>";
					$contents = file_get_contents(SRCPATH . $path . $file);
					preg_match_all("/Lang\.ins.*?\('(.*?)'\)/", $contents, $matches);
					if (count($matches[1]) > 0)
						foreach ($matches[1] as $match) $result[$match][] = $file;
				}
			}
		}

		return $result;
	}
?>
</body>
</html>