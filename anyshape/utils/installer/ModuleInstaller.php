<?php if (!defined('BASEPATH')) die('Access denied!');

abstract class ModuleInstaller implements IModuleInstaller
{
	/** Nazev modulu (interni) */
	protected $moduleName;

	/** Cesta k instalacnim souborum */
	protected $modulePath;

	/**
	 * Nainstaluje modul
	 */
	public function install()
	{
		// tabulky
		dibi::loadFile($this->modulePath . "tables.sql");

		// modul
		dibi::query("INSERT INTO [:as:modules]", array("name" => $this->moduleName));
		$moduleId = dibi::insertId();

		// komponenety
		foreach ($this->components as $name => $component)
		{
			dibi::query("INSERT INTO [:as:components]", array(
				"folder_id"		=> 0,
				"name"			=> $name,
				"alias"			=> "",
				"module_id"		=> $moduleId,
				"type"			=> $component[0],
				"dashboard"		=> $component[1],
				"order"			=> $component[2],
				"icon16x16"		=> $component[3],
				"icon32x32"		=> $component[4],
				"operations"	=> $component[5],
				"config"		=> ""
			));

			$this->components[$name][6] = dibi::insertId();
		}

		// nahrazeni relativnich id komponenet v konfiguraci
		foreach ($this->components as $name => &$component)
		{
			$this->currentComponenetId = $component[6];
			$config = file_get_contents($this->modulePath . $name . ".xml");
			$config = preg_replace_callback('/{\+(\d+)}/', array($this, 'configCallback'), $config);

			dibi::query("UPDATE [:as:components] SET config = %s WHERE id = %i", $config, $component[6]);
		}
	}

	/**
	 * Nahradi {+N} za id komponenety
	 */
	protected function configCallback($matches)
	{
		return ((int) $matches[1]) + $this->currentComponenetId;
	}

	/**
	 * Odinstaluje modul
	 */
	public function uninstall()
	{
		$moduleId = dibi::query("SELECT id FROM [:as:modules] WHERE name = %s", $this->moduleName)->fetchSingle();

		$componentIds = dibi::query("SELECT id FROM [:as:components] WHERE module_id = %i", $moduleId)->fetchPairs();

		if (count($componentIds) > 0)
		{
			dibi::query("DELETE FROM [:as:groups_components] WHERE component_id IN %in", $componentIds);
			dibi::query("UPDATE [:as:page_types] SET component_id = 0 WHERE component_id IN %in", $componentIds);
			dibi::query("DELETE FROM [:as:components] WHERE module_id = %i", $moduleId);
		}

		dibi::query("DELETE FROM [:as:modules] WHERE id = %i", $moduleId);

		dibi::query("ALTER TABLE [:as:components] AUTO_INCREMENT=%i", (int) dibi::query("(SELECT MAX(id) FROM [:as:components])")->fetchSingle() + 1);
		dibi::query("ALTER TABLE [:as:modules] AUTO_INCREMENT=%i", (int) dibi::query("(SELECT MAX(id) FROM [:as:modules])")->fetchSingle() + 1);
	}

	/**
	 * Zjisti, zda-li je modul nainstalovany
	 */
	public function isInstalled()
	{
		return $this->hasModule($this->moduleName);
	}

	/**
	 * Vrati TRUE pokud ma zadany modul zaznam v tabulce modulu
	 */
	protected function hasModule($moduleName)
	{
		return $this->hasTable('anyshape_modules') && dibi::query("SELECT id FROM [:as:modules] WHERE name = %s", $moduleName)->fetchSingle() > 0;
	}

	/**
	 * Vrati TRUE pokud zadana tabulka je v databazi
	 */
	protected function hasTable($tableName)
	{
		$tables = dibi::query("SHOW TABLES")->fetchPairs();
		return in_array($tableName, $tables);
	}

}