<?
/**
 * Nette Framework Requirements Checker template.
 *
 * @param array    $requirements
 * @param bool     $errors
 * @param bool     $warnings
 */

header('Content-Type: text/html; charset=utf-8');
header('Cache-Control: s-maxage=0, max-age=0, must-revalidate');
header('Expires: Mon, 23 Jan 1978 10:00:00 GMT');
?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta name="robots" content="noindex">

		<title>AnyShape - instalace systému</title>

		<style type="text/css">
			html { font: 13px/1.5 Verdana, sans-serif; border-top: 130px solid #F4F4F4; }
			body { border-top: 1px solid #E4DED5; margin: 0; background: white; color: #333; }
			#wrapper { max-width: 780px; margin: -140px auto 0; }
			h1 { font: 32px sans-serif; margin: 0; padding: 60px 0 0 0; background: url(../../manual/anyshape.png) right center no-repeat; color: #7A7772; height: 90px; text-shadow: 0 -1px 0 #FFF; }
			h2 { font-size: 2em; font-weight: normal;color: #3484D2; margin: .7em 0; }
			p { margin: 1.2em 0; }
			.result { margin: 1.5em 0; padding: 0 1em; border: 2px solid white; }
			.passed h2 { color: #1A7E1E; }
			.failed h2 { color: white; }
			table { padding: 0; margin: 20px 0 0 0; border-collapse: collapse; width: 100%; }
			table td, table th { text-align: left; padding: 10px; vertical-align: top; border-style: solid; border-width: 1px 0 0; border-color: inherit; background: white none no-repeat 12px 8px; background-color: inherit; }
			table th { font-size: 105%; font-weight: bold; padding-left: 50px; width: 540px;}
			td.actions { text-align: right; }
			.passed, .info { background-color: #E4F9E3; border-color: #C6E5C4; }
			.passed th { background-image: url('../assets/passed.gif'); }
			.info th { background-image: url('../assets/info.gif'); }
			.warning { background-color: #FEFAD4; border-color: #EEEE99; }
			.warning th { background-image: url('../assets/warning.gif'); }
			.failed { background-color: #F4D2D2; border-color: #D2B994; }
			div.failed { background-color: #CD1818; border-color: #CD1818; }
			.failed th { background-image: url('../assets/failed.gif'); 	}
			.description td {border-top: none !important; padding: 0 10px 10px 50px; color: #555; }
			.passed.description { display: none; }
			a { font-size: 10px; color: #606060; }
			.files { display: none; color: #808080; }

		</style>

	</head>

	<body>

		<div id="wrapper">

			<h1>Instalace systému</h1>

			<? if ($logged): ?>

			<table>
				<? foreach ($modules as $module): ?>

				<? if ($module['installed']): ?>

					<tr class="passed">
							<th><?=$module['name']?></th>
							<td class="actions">
								<? if ($module['id'] != 'CoreInstaller'): ?>
								<a href="?uninstall=<?=$module['id']?>">Odinstalovat</a>
								<? endif; ?>
							</td>
					</tr>

					<tr class="passed description">
						<td colspan="2"><?=$module['description']?></td>
					</tr>

				<? else: ?>

					<tr class="failed">
							<th><?=$module['name']?></th>
							<td class="actions">
								<? if ($modules[0]['installed'] || $module['id'] == 'CoreInstaller'): ?>
									<a href="?install=<?=$module['id']?>">Instalovat</a>
								<? endif; ?>
							</td>
					</tr>

					<tr class="failed description">
						<td colspan="2"><?=$module['description']?></td>
					</tr>

				<? endif; ?>

				<? endforeach; ?>
			</table>

			<? else: ?>

			<table>
				<form method="post" action="<?=BASEURL . 'anyshape/utils/installer/'?>">
					<tr class="passed">
						<td>
							<label for="passowrd">Zadej zakódované heslo roota</label>
						</td>
						<td>
							<input type="text" name="password" value="" />
						</td>
					</tr>
				</form>
			</table>

			<? endif; ?>

			<p><small>AnyShape verze <?= $config['site']['version']?></small></p>
		</div>

	</body>
</html>