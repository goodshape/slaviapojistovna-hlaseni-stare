SET NAMES utf8;
SET foreign_key_checks = 0;
SET time_zone = 'SYSTEM';
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

CREATE TABLE `data_newsletters_archive` (
  `id` int(11) NOT NULL auto_increment,
  `message` mediumtext NOT NULL,
  `subject` varchar(255) NOT NULL,
  `sender` int(11) NOT NULL,
  `language` char(2) NOT NULL,
  `template` varchar(255) NOT NULL,
  `generated_body` mediumtext NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `send_immediately` tinyint(4) NOT NULL,
  `send_on` datetime NOT NULL,
  `sending_begin` datetime default NULL,
  `sending_end` datetime default NULL,
  `num_sent` int(11) NOT NULL,
  `num_views` int(11) NOT NULL,
  `num_img_views` int(11) NOT NULL,
  `num_clicks` int(11) NOT NULL,
  `num_unsubscribed` int(11) NOT NULL,
  `utm_source` varchar(255) NOT NULL,
  `utm_medium` varchar(255) NOT NULL,
  `utm_campaign` varchar(255) NOT NULL,
  `utm_content` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `is_delayed` tinyint(4) NOT NULL,
  `delay_hours` tinyint(4) NOT NULL,
  `delay_volume` tinyint(4) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


CREATE TABLE `data_newsletters_attachments` (
  `id` int(11) NOT NULL auto_increment,
  `newsletter_id` int(11) NOT NULL,
  `file` varchar(255) collate utf8_czech_ci NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `newsletter_id` (`newsletter_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


CREATE TABLE `data_newsletters_cronjob_log` (
  `id` int(11) NOT NULL auto_increment,
  `script_start` datetime NOT NULL,
  `script_stop` datetime NOT NULL,
  `blocking_duration` float NOT NULL,
  `creating_duration` float NOT NULL,
  `sending_duration` float NOT NULL,
  `total_duration` float NOT NULL,
  `num_newsletters_used` int(11) NOT NULL,
  `num_emails_created` int(11) NOT NULL,
  `num_emails_sent` int(11) NOT NULL,
  `num_emails_failed` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;


CREATE TABLE `data_newsletters_email_queue` (
  `id` int(11) NOT NULL auto_increment,
  `newsletter_id` int(11) NOT NULL,
  `subscriber_id` int(11) NOT NULL,
  `to_email` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `send_on` datetime NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `newsletter_id_2` (`newsletter_id`,`to_email`),
  KEY `status` (`status`),
  KEY `send_on` (`send_on`),
  KEY `newsletter_id` (`newsletter_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


CREATE TABLE `data_newsletters_emails_sent` (
  `id` int(11) NOT NULL,
  `newsletter_id` int(11) NOT NULL,
  `subscriber_id` int(11) NOT NULL,
  `to_email` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `send_on` datetime NOT NULL,
  `created` datetime NOT NULL,
  `sent_on` datetime default NULL,
  `num_views` int(11) NOT NULL,
  `num_img_views` int(11) NOT NULL,
  `num_clicks` int(11) NOT NULL,
  `num_unsubscribed` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


CREATE TABLE `data_newsletters_groups` (
  `id` int(11) NOT NULL auto_increment,
  `name` int(11) NOT NULL,
  `description` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `public` tinyint(4) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


CREATE TABLE `data_newsletters_groups_archive` (
  `group_id` int(11) NOT NULL,
  `newsletter_id` int(11) NOT NULL,
  PRIMARY KEY  (`newsletter_id`,`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


CREATE TABLE `data_newsletters_groups_subscribers` (
  `group_id` int(11) NOT NULL,
  `subscriber_id` int(11) NOT NULL,
  PRIMARY KEY  (`group_id`,`subscriber_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


CREATE TABLE `data_newsletters_import_log` (
  `id` int(11) NOT NULL auto_increment,
  `import_data` text collate utf8_czech_ci NOT NULL,
  `language` char(2) collate utf8_czech_ci NOT NULL,
  `source` varchar(255) collate utf8_czech_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `num_imported` int(11) NOT NULL,
  `num_invalid` int(11) NOT NULL,
  `num_duplicates` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;


CREATE TABLE `data_newsletters_import_log_groups` (
  `id` int(11) NOT NULL auto_increment,
  `group_id` int(11) NOT NULL,
  `import_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;


CREATE TABLE `data_newsletters_mailing_list` (
  `id` int(11) NOT NULL auto_increment,
  `email` varchar(255) NOT NULL,
  `language_id` char(2) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `note` varchar(255) NOT NULL,
  `num_sent` int(11) NOT NULL,
  `num_views` int(11) NOT NULL,
  `num_img_views` int(11) NOT NULL,
  `num_clicks` int(11) NOT NULL,
  `created` datetime default NULL,
  `modified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `ip` varchar(15) NOT NULL,
  `source` varchar(100) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `email_unique` (`email`),
  KEY `email` (`email`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


CREATE TABLE `data_newsletters_senders` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) character set utf8 collate utf8_bin NOT NULL,
  `email` varchar(320) character set utf8 collate utf8_bin NOT NULL,
  `created` datetime default NULL,
  `modified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `data_newsletters_senders` (`name`, `email`, `modified`) VALUES
('GoodShape', 'info@goodshape.cz', NOW());


CREATE TABLE `data_newsletters_templates` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `filename` text NOT NULL,
  `created` datetime default NULL,
  `modified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `data_newsletters_templates` (`name`, `filename`, `modified`) VALUES
('Základní šablona - česky', '../app/templates/Newsletter/template_cs.php', NOW()),
('Basic template - english', '../app/templates/Newsletter/template_en.php', NOW());


CREATE TABLE `data_newsletters_unsubscribes` (
  `id` int(11) NOT NULL auto_increment,
  `email_id` int(11) NOT NULL,
  `newsletter_id` int(11) NOT NULL,
  `subscriber_id` int(11) NOT NULL,
  `reasons` text collate utf8_czech_ci NOT NULL,
  `timestamp` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;
