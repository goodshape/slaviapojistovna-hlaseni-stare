SET NAMES utf8;
SET foreign_key_checks = 0;
SET time_zone = 'SYSTEM';
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

CREATE TABLE `data_polls` (
  `id` int(11) NOT NULL auto_increment,
  `structure_id` int(11) NOT NULL,
  `question` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `status` tinyint(4) NOT NULL,
  `published_from` datetime NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `component_id` (`structure_id`),
  KEY `status` (`status`),
  KEY `modified` (`modified`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `data_polls_answers` (
  `id` int(11) NOT NULL auto_increment,
  `question_id` int(11) NOT NULL,
  `text` int(11) NOT NULL,
  `num_votes` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `question_id` (`question_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
