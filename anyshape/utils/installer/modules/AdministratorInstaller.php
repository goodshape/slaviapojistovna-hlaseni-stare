<?php if (!defined('BASEPATH')) die('Access denied!');

class AdministratorInstaller extends ModuleInstaller
{
	/** Nazev modulu (interni) */
	protected $moduleName = 'administrator';

	/** Cesta k instalacnim souborum */
	protected $modulePath = 'modules/Administrator/';

	/** Komponenety a jejich vlastnosti (type, dashboard, order, icon16x16, icon32x32, operations */
	protected $components = array(
		"StructureAdmin"	=> array(SYSTEM_COMPONENT, 2, 10, "CHART_ORGANISATION", "STRUCTURE", "allow_access|add_page|add_web|edit_page|remove_page|move_page|page_properties|seo_properties|page_access_control"),
		"LogList"			=> array(SYSTEM_COMPONENT, 2, 20, "TABLE", "MONITOR", "allow_access"),
		"FileManager"		=> array(SYSTEM_COMPONENT, 2, 30, "FOLDER_FILES", "FILE_MANAGER", "allow_access|create_folder|rename_folder|delete_folder|upload_file|modify_file|delete_file|modify_other_users_file|setup_watermarks|access_recycle_bin|restore_files|empty_recycle_bin"),
		"UserAdmin"			=> array(SYSTEM_COMPONENT, 2, 40, "USER", "USERS", "allow_access|add_user|edit_user|delete_user|add_group|edit_group|delete_group|add_user_to_group"),
		"SimplePageAdmin"	=> array(SYSTEM_COMPONENT, 0, 0, "HTML", "TEXT", "allow_access|edit_item")
	);

	/**
	 * Vrati nazev modulu
	 */
	public function getTitle()
	{
		return "Modul Administrátor";
	}

	/**
	 * Vrati popis modulu
	 */
	public function getDescription()
	{
		return "Komponenty pro správu struktury, uživatelů a souborů.";
	}

}