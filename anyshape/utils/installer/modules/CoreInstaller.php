<?php if (!defined('BASEPATH')) die('Access denied!');

class CoreInstaller extends ModuleInstaller
{
	/** Cesta k instalacnim souborum */
	protected $modulePath = 'modules/Core/';

	/**
	 * Vrati nazev modulu
	 */
	public function getTitle()
	{
		return "Jádro systému";
	}

	/**
	 * Vrati popis modulu
	 */
	public function getDescription()
	{
		return "Základní tabulky";
	}

	/**
	 * Nainstaluje modul
	 */
	public function install()
	{
		// tabulky
		dibi::loadFile($this->modulePath . "tables.sql");
	}

	/**
	 * Odinstaluje modul
	 */
	public function uninstall()
	{
		// nelze
	}

	/**
	 * Zjisti, zda-li je modul nainstalovany
	 */
	public function isInstalled()
	{
		return $this->hasTable('anyshape_modules');
	}

}