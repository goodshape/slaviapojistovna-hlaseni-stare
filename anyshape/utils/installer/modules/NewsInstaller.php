<?php if (!defined('BASEPATH')) die('Access denied!');

class NewsInstaller extends ModuleInstaller
{
	/** Nazev modulu (interni) */
	protected $moduleName = 'news';

	/** Cesta k instalacnim souborum */
	protected $modulePath = 'modules/News/';

	/** Komponenety a jejich vlastnosti (type, dashboard, order, icon16x16, icon32x32, operations */
	protected $components = array(
		"NewsList"			=> array(LISTING_COMPONENT, 2, 200, "LIGHTNING", "SPLASH_GREEN", LISTING_COMPONENT_OPERATIONS),
		"NewsEdit"			=> array(EDIT_COMPONENT, 0, 0, "LIGHTNING", "SPLASH_GREEN", EDIT_COMPONENT_OPERATIONS)
	);

	/**
	 * Vrati nazev modulu
	 */
	public function getTitle()
	{
		return "Modul Novinky";
	}

	/**
	 * Vrati popis modulu
	 */
	public function getDescription()
	{
		return "Správa novinek";
	}

	/**
	 * Odinstaluje modul
	 */
	public function uninstall()
	{
		// modul a komponenety
		parent::uninstall();

		// texty
		dibi::query("DELETE FROM [:data:texts] WHERE id IN (SELECT title FROM [:data:news])");
		dibi::query("DELETE FROM [:data:texts] WHERE id IN (SELECT text FROM [:data:news])");

		// tabulky
		dibi::query("DROP TABLE [:data:news]");
	}

}