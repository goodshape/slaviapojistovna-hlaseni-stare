<?php if (!defined('BASEPATH')) die('Access denied!');

class FaqInstaller extends ModuleInstaller
{
	/** Nazev modulu (interni) */
	protected $moduleName = 'faq';

	/** Cesta k instalacnim souborum */
	protected $modulePath = 'modules/Faq/';

	/** Komponenety a jejich vlastnosti (type, dashboard, order, icon16x16, icon32x32, operations */
	protected $components = array(
		"FaqList"			=> array(LISTING_COMPONENT, 2, 300, "FAQ", "HELP", LISTING_COMPONENT_OPERATIONS),
		"FaqEdit"			=> array(EDIT_COMPONENT, 0, 0, "FAQ", "HELP", EDIT_COMPONENT_OPERATIONS)
	);

	/**
	 * Vrati nazev modulu
	 */
	public function getTitle()
	{
		return "Modul FAQ";
	}

	/**
	 * Vrati popis modulu
	 */
	public function getDescription()
	{
		return "Správa často kladených dotazů";
	}

	/**
	 * Odinstaluje modul
	 */
	public function uninstall()
	{
		// modul a komponenety
		parent::uninstall();

		// texty
		dibi::query("DELETE FROM [:data:texts] WHERE id IN (SELECT question FROM [:data:faq])");
		dibi::query("DELETE FROM [:data:texts] WHERE id IN (SELECT answer FROM [:data:faq])");

		// tabulky
		dibi::query("DROP TABLE [:data:faq]");
	}

}