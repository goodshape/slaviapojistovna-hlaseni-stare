<?php if (!defined('BASEPATH')) die('Access denied!');

class NewsletterInstaller extends ModuleInstaller
{
	/** Nazev modulu (interni) */
	protected $moduleName = 'newsletter';

	/** Cesta k instalacnim souborum */
	protected $modulePath = 'modules/Newsletter/';

	/** Komponenety a jejich vlastnosti (type, dashboard, order, icon16x16, icon32x32, operations */
	protected $components = array(
		"Newsletters"				=> array(LISTING_COMPONENT, 2, 30, "EMAIL_EXPORT", "SEND", LISTING_COMPONENT_OPERATIONS),
		"NewslettersEdit"			=> array(EDIT_COMPONENT, 0, 0, "EMAIL_EXPORT", "SEND", EDIT_COMPONENT_OPERATIONS),
		"Subscribers"				=> array(LISTING_COMPONENT, 0, 0, "USER_GREEN", "USERS_BLACK", LISTING_COMPONENT_OPERATIONS),
		"SubscribersEdit"			=> array(EDIT_COMPONENT, 0, 0, "USER_RED", "USERS_BLACK", EDIT_COMPONENT_OPERATIONS),
		"MailingListGroups"			=> array(LISTING_COMPONENT, 0, 0, "GROUP", "USERS", LISTING_COMPONENT_OPERATIONS),
		"MailingListGroupsEdit"		=> array(EDIT_COMPONENT, 0, 0, "GROUP", "USERS", EDIT_COMPONENT_OPERATIONS),
		"NewsletterSenders"			=> array(LISTING_COMPONENT, 0, 0, "USER_COMMENT", "USERS_BLACK", LISTING_COMPONENT_OPERATIONS),
		"NewsletterSendersEdit"		=> array(EDIT_COMPONENT, 0, 0, "USER_COMMENT", "USERS_BLACK", EDIT_COMPONENT_OPERATIONS),
		"NewsletterTemplates"		=> array(LISTING_COMPONENT, 0, 0, "HTML", "RICH_DOCUMENT", LISTING_COMPONENT_OPERATIONS),
		"NewsletterTemplatesEdit"	=> array(EDIT_COMPONENT, 0, 0, "HTML", "RICH_DOCUMENT", EDIT_COMPONENT_OPERATIONS),
		"EmailQueue"				=> array(LISTING_COMPONENT, 0, 0, "EMAIL_EXPORT", "FORWARD", LISTING_COMPONENT_OPERATIONS),
		"EmailsSent"				=> array(LISTING_COMPONENT, 0, 0, "EMAIL_EXPORT", "FORWARD", LISTING_COMPONENT_OPERATIONS),
		"MailingListImport"			=> array(EDIT_COMPONENT, 0, 0, "DATABASE_GEAR", "BLANK", EDIT_COMPONENT_OPERATIONS),
		"UnsubscribeReasons"		=> array(LISTING_COMPONENT, 0, 0, "DOOR", "BLANK", LISTING_COMPONENT_OPERATIONS),
		"UnsubscribeReasonsDetail"	=> array(EDIT_COMPONENT, 0, 0, "DOOR", "BLANK", EDIT_COMPONENT_OPERATIONS)
	);

	/**
	 * Vrati nazev modulu
	 */
	public function getTitle()
	{
		return "Modul Newsletter";
	}

	/**
	 * Vrati popis modulu
	 */
	public function getDescription()
	{
		return "Hromadné rozesílání emailových zpráv";
	}

	public function install()
	{
		parent::install();

		$nameTextId = dibi::query("SELECT id FROM [:data:texts] ORDER BY id DESC LIMIT 1")->fetchSingle() * 1 + 1;
		dibi::query("INSERT INTO [:data:texts]", array('id' => $nameTextId, 'text' => "Základní skupina", 'language_id' => "cs"));

		$descTextId = dibi::query("SELECT id FROM [:data:texts] ORDER BY id DESC LIMIT 1")->fetchSingle() * 1 + 1;
		dibi::query("INSERT INTO [:data:texts]", array('id' => $descTextId, 'text' => "", 'language_id' => "cs"));

		dibi::query("INSERT INTO [:data:newsletters_groups]", array(
			"name"			=> $nameTextId,
			"description"	=> $descTextId,
			"modified%sql"	=> "NOW()",
			"public"		=> 1
		));
	}
	/**
	 * Odinstaluje modul
	 */
	public function uninstall()
	{
		// modul a komponenety
		parent::uninstall();

		// texty
		dibi::query("DELETE FROM [:data:texts] WHERE id IN (SELECT name FROM [:data:newsletters_groups])");
		dibi::query("DELETE FROM [:data:texts] WHERE id IN (SELECT description FROM [:data:newsletters_groups])");

		// tabulky
		dibi::query("DROP TABLE [:data:newsletters_archive]");
		dibi::query("DROP TABLE [:data:newsletters_attachments]");
		dibi::query("DROP TABLE [:data:newsletters_cronjob_log]");
		dibi::query("DROP TABLE [:data:newsletters_email_queue]");
		dibi::query("DROP TABLE [:data:newsletters_emails_sent]");
		dibi::query("DROP TABLE [:data:newsletters_groups]");
		dibi::query("DROP TABLE [:data:newsletters_groups_archive]");
		dibi::query("DROP TABLE [:data:newsletters_groups_subscribers]");
		dibi::query("DROP TABLE [:data:newsletters_import_log]");
		dibi::query("DROP TABLE [:data:newsletters_import_log_groups]");
		dibi::query("DROP TABLE [:data:newsletters_mailing_list]");
		dibi::query("DROP TABLE [:data:newsletters_senders]");
		dibi::query("DROP TABLE [:data:newsletters_templates]");
		dibi::query("DROP TABLE [:data:newsletters_unsubscribes]");
	}

}