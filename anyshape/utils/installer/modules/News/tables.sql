SET NAMES utf8;
SET foreign_key_checks = 0;
SET time_zone = 'SYSTEM';
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `data_news`;
CREATE TABLE `data_news` (
  `id` int(11) NOT NULL auto_increment,
  `structure_id` int(11) NOT NULL,
  `title` int(11) NOT NULL,
  `text` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `published_from` datetime default NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `display_date` datetime default NULL,
  `author` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `structure_id` (`structure_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
