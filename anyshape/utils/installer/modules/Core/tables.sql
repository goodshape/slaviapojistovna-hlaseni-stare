SET NAMES utf8;
SET foreign_key_checks = 0;
SET time_zone = 'SYSTEM';
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

CREATE TABLE `anyshape_components` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL default '',
  `alias` varchar(255) NOT NULL default '',
  `folder_id` int(11) NULL,
  `module_id` int(11) NULL,
  `type` tinyint(4) NOT NULL,
  `dashboard` tinyint(4) NOT NULL,
  `order` int(11) NOT NULL,
  `icon16x16` varchar(255) NOT NULL,
  `icon32x32` varchar(255) NOT NULL,
  `operations` varchar(255) NOT NULL,
  `config` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


CREATE TABLE `anyshape_groups` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


CREATE TABLE `anyshape_groups_components` (
  `group_id` int(11) NULL,
  `component_id` int(11) NULL,
  `operation` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`group_id`,`component_id`,`operation`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


CREATE TABLE `anyshape_groups_structure` (
  `group_id` int(11) NULL,
  `structure_id` int(11) NULL,
  `level` tinyint(4) NOT NULL,
  PRIMARY KEY  (`group_id`,`structure_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


CREATE TABLE `anyshape_languages` (
  `id` char(2) NOT NULL,
  `name` varchar(30) NOT NULL,
  `adverb` varchar(30) NOT NULL,
  `icon` char(2) NOT NULL,
  `active` tinyint(4) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `anyshape_languages` (`id`, `name`, `adverb`, `icon`, `active`) VALUES
('cs',	'Čeština',	'česky',	'CZ',	1),
('en',	'English',	'English',	'GB',	0);

CREATE TABLE `anyshape_log` (
  `id` int(11) NOT NULL auto_increment,
  `component_id` int(11) NULL,
  `action` int(11) NULL,
  `item_id` int(11) NULL,
  `item_name` text NULL,
  `user_id` int(11) NULL,
  `timestamp` timestamp NULL default CURRENT_TIMESTAMP,
  `ip` varchar(40) NOT NULL default '',
  `user_agent` varchar(255) NOT NULL default '',
  `diff` text NOT NULL,
  `restored` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


CREATE TABLE `anyshape_login_requests` (
  `id` int(11) NOT NULL auto_increment,
  `timestamp` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `ip` char(15) NOT NULL,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `timestamp` (`timestamp`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


CREATE TABLE `anyshape_modules` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


CREATE TABLE `anyshape_page_types` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL default '',
  `presenter` varchar(255) NOT NULL,
  `component_id` varchar(255) NULL,
  `icon` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `anyshape_page_types` (`id`, `name`, `presenter`, `component_id`, `icon`) VALUES
(1,	'Homepage',	'Homepage',	'',	'HOME');


CREATE TABLE `anyshape_users` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL DEFAULT "",
  `comment` text NOT NULL DEFAULT "",
  `status` tinyint(4) NOT NULL,
  `config` text NOT NULL DEFAULT "",
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


CREATE TABLE `anyshape_users_groups` (
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY  (`user_id`,`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


CREATE TABLE `anyshape_watermarks` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `image` varchar(255) character set ascii NOT NULL,
  `position` tinyint(4) NOT NULL,
  `size` tinyint(4) NOT NULL,
  `horizontal_padding` tinyint(4) NOT NULL,
  `vertical_padding` tinyint(4) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `anyshape_watermarks` (`name`, `image`, `position`, `size`, `horizontal_padding`, `vertical_padding`) VALUES
('AnyShape',	'null',	0,	80,	0,	0),
('Vodoznak',	'vodoznaky/goodshape.png',	1,	20,	5,	5);


CREATE TABLE `data_filesystem` (
  `filename` varchar(255) character set ascii NOT NULL default '',
  `path` varchar(255) character set ascii NOT NULL default '',
  `name` varchar(255) NOT NULL default '',
  `description` text NOT NULL,
  `tags` text NOT NULL,
  `type` tinyint(4) NOT NULL,
  `owner` int(11) NOT NULL,
  `private` tinyint(4) NOT NULL,
  `watermark` int(11) NOT NULL,
  `num_downloads` int(11) NOT NULL,
  `time` datetime NOT NULL,
  `size` bigint(20) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`filename`,`path`),
  KEY `name` (`name`),
  KEY `description` (`description`(333)),
  KEY `tags` (`tags`(64))
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `data_filesystem` (`filename`, `path`, `name`, `description`, `tags`, `type`, `owner`, `private`, `watermark`, `num_downloads`, `time`, `size`, `created`) VALUES
('vodoznaky',	'',	'vodoznaky',	'',	'',	1,	0,	0,	0,	0,	'2011-01-18 12:25:53',	4096, NOW()),
('anyshape.png',	'vodoznaky',	'anyshape.png',	'',	'',	0,	0,	0,	0,	0,	'2011-01-18 11:53:32',	70514, NOW()),
('goodshape.png',	'vodoznaky',	'goodshape.png',	'',	'',	0,	0,	0,	0,	0,	'2011-01-18 11:48:09',	34699, NOW());


CREATE TABLE `data_sent_emails` (
  `id` int(11) NOT NULL auto_increment,
  `from_name` varchar(255) NOT NULL,
  `from_email` varchar(255) NOT NULL,
  `to` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `source` text NOT NULL,
  `created` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


CREATE TABLE `data_structure` (
  `id` int(11) NOT NULL auto_increment,
  `parent_id` int(11) NULL,
  `page_type` int(11) NOT NULL,
  `name` int(11) NOT NULL,
  `title` int(11) default NULL,
  `h1` int(11) default NULL,
  `description` int(11) default NULL,
  `keywords` int(11) default NULL,
  `copyright` int(11) default NULL,
  `robots` tinyint(4) default NULL,
  `content` int(11) NOT NULL,
  `url` text NOT NULL,
  `status` tinyint(4) NOT NULL default '0',
  `child_limit` int(11) default NULL,
  `position` int(11) NOT NULL default '0',
  `languages` varchar(59) default NULL,
  `host` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `parent_id` (`parent_id`),
  KEY `languages` (`languages`),
  KEY `position` (`position`),
  KEY `status` (`status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `data_structure` (`parent_id`, `page_type`, `name`, `host`, `title`, `h1`, `description`, `keywords`, `copyright`, `robots`, `content`, `url`, `status`, `child_limit`, `position`, `languages`) VALUES
(NULL,	1,	1,	'',	NULL,	NULL,	2,	3,	4,	1,	0,	'',	1,	NULL,	-1,	'cs');



CREATE TABLE `data_texts` (
  `id` int(11) NOT NULL,
  `language_id` char(2) NULL,
  `text` text NOT NULL,
  PRIMARY KEY  (`id`,`language_id`),
  FULLTEXT KEY `text` (`text`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `data_texts` (`id`, `language_id`, `text`) VALUES
(1,	'cs',	'NovýWeb.cz'),
(2,	'cs',	''),
(3,	'cs',	''),
(4,	'cs',	'');


CREATE TABLE `data_url_lookup_table` (
  `website_id` int(11) NOT NULL,
  `structure_id` int(11) NOT NULL,
  `language_id` char(2) NOT NULL,
  `url_string` varchar(255) NOT NULL,
  PRIMARY KEY  (`website_id`,`language_id`,`url_string`),
  KEY `structure_id` (`structure_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `data_url_lookup_table` (`website_id`, `structure_id`, `language_id`, `url_string`) VALUES
(1,	1,	'cs',	'');

