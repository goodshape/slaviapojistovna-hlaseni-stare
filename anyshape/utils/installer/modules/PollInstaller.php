<?php if (!defined('BASEPATH')) die('Access denied!');

class PollInstaller extends ModuleInstaller
{
	/** Nazev modulu (interni) */
	protected $moduleName = 'poll';

	/** Cesta k instalacnim souborum */
	protected $modulePath = 'modules/Poll/';

	/** Komponenety a jejich vlastnosti (type, dashboard, order, icon16x16, icon32x32, operations */
	protected $components = array(
		"PollList"	=> array(LISTING_COMPONENT, 2, 400, "POLL", "SPREADSHEET", LISTING_COMPONENT_OPERATIONS),
		"PollEdit"	=> array(EDIT_COMPONENT, 0, 0, "POLL", "SPREADSHEET", EDIT_COMPONENT_OPERATIONS)
	);

	/**
	 * Vrati nazev modulu
	 */
	public function getTitle()
	{
		return "Modul Ankety";
	}

	/**
	 * Vrati popis modulu
	 */
	public function getDescription()
	{
		return "Správa anket";
	}

	/**
	 * Odinstaluje modul
	 */
	public function uninstall()
	{
		// modul a komponenety
		parent::uninstall();

		// texty
		dibi::query("DELETE FROM [:data:texts] WHERE id IN (SELECT question FROM [:data:polls])");
		dibi::query("DELETE FROM [:data:texts] WHERE id IN (SELECT text FROM [:data:polls_answers])");

		// tabulky
		dibi::query("DROP TABLE [:data:polls]");
		dibi::query("DROP TABLE [:data:polls_answers]");
	}

}