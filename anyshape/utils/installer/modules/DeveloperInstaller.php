<?php if (!defined('BASEPATH')) die('Access denied!');

class DeveloperInstaller extends ModuleInstaller
{
	/** Nazev modulu **/
	protected $moduleName = 'developer';

	/** Cesta k instalacnim souborum */
	protected $modulePath = 'modules/Developer/';

	/** Komponenety a jejich vlastnosti (type, dashboard, order, icon16x16, icon32x32, operations */
	protected $components = array(
		"ModuleAdmin"		=> array(SYSTEM_COMPONENT, 2, 1, "MODULE", "MODULE", "allow_access|administrate_components"),
		"PageTypeAdmin"		=> array(SYSTEM_COMPONENT, 2, 2, "PAGE_EDIT", "PAGE_ADMIN", "allow_access"),
		"PageTypeAdminEdit"	=> array(SYSTEM_COMPONENT, 0, 0, "PAGE_EDIT", "PAGE_ADMIN", "")
	);

	/**
	 * Vrati nazev modulu
	 */
	public function getTitle()
	{
		return "Modul Vývojař";
	}

	/**
	 * Vrati popis modulu
	 */
	public function getDescription()
	{
		return "Komponenty pro vývojaře (správa modulů a správa typů stránek)";
	}

}