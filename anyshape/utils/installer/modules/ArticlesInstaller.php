<?php if (!defined('BASEPATH')) die('Access denied!');

class ArticlesInstaller extends ModuleInstaller
{
	/** Nazev modulu (interni) */
	protected $moduleName = 'articles';

	/** Cesta k instalacnim souborum */
	protected $modulePath = 'modules/Articles/';

	/** Komponenety a jejich vlastnosti (type, dashboard, order, icon16x16, icon32x32, operations */
	protected $components = array(
		"ArticleList"			=> array(LISTING_COMPONENT, 2, 100, "ARTICLE", "EDIT", LISTING_COMPONENT_OPERATIONS),
		"ArticleEdit"			=> array(EDIT_COMPONENT, 0, 0, "ARTICLE", "EDIT", EDIT_COMPONENT_OPERATIONS)
	);

	/**
	 * Vrati nazev modulu
	 */
	public function getTitle()
	{
		return "Modul Články";
	}

	/**
	 * Vrati popis modulu
	 */
	public function getDescription()
	{
		return "Správa článků";
	}

	/**
	 * Odinstaluje modul
	 */
	public function uninstall()
	{
		// modul a komponenety
		parent::uninstall();

		// texty
		dibi::query("DELETE FROM [:data:texts] WHERE id IN (SELECT title FROM [:data:articles])");
		dibi::query("DELETE FROM [:data:texts] WHERE id IN (SELECT perex FROM [:data:articles])");
		dibi::query("DELETE FROM [:data:texts] WHERE id IN (SELECT text FROM [:data:articles])");
		dibi::query("DELETE FROM [:data:texts] WHERE id IN (SELECT tags FROM [:data:articles])");

		// tabulky
		dibi::query("DROP TABLE [:data:articles]");
	}

}