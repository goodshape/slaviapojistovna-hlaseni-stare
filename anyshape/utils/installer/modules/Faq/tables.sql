DROP TABLE IF EXISTS `data_faq`;
CREATE TABLE `data_faq` (
  `id` int(11) NOT NULL auto_increment,
  `structure_id` int(11) NOT NULL,
  `question` int(11) NOT NULL,
  `answer` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `answered` datetime default NULL,
  `status` tinyint(4) NOT NULL,
  `questioner_name` varchar(255) NOT NULL,
  `questioner_email` varchar(255) NOT NULL,
  `author` varchar(255) NOT NULL,
  `email_sent` tinyint(4) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
