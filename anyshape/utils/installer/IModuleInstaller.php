<?php if (!defined('BASEPATH')) die('Access denied!');

interface IModuleInstaller
{

	/**
	 * Vrati nazev modulu
	 */
	public function getTitle();

	/**
	 * Vrati popis modulu
	 */
	public function getDescription();

	/**
	 * Nainstaluje modul
	 */
	public function install();

	/**
	 * Odinstaluje modul
	 */
	public function uninstall();

	/**
	 * Zjisti, zda-li je modul nainstalovany
	 */
	public function isInstalled();

}