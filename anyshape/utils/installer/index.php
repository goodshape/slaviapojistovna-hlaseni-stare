<?php

$config = require_once __DIR__ .'/../../bootstrap.php';

//	define('BASEURL', $config['site']['server_path']);

	/* Typy komponent */
	define('SYSTEM_COMPONENT', 0);
	define('LISTING_COMPONENT', 1);
	define('EDIT_COMPONENT', 2);
	define('CHART_COMPONENT', 3);
	define('FOLDER_COMPONENT', 4);

	/* Operace dle typu komponenty */
	define('SYSTEM_COMPONENT_OPERATIONS', 'allow_access');
	define('LISTING_COMPONENT_OPERATIONS', 'allow_access|add_item|edit_item|remove_item');
	define('EDIT_COMPONENT_OPERATIONS', 'allow_access|edit_item');

	/**
	 * Kontrola slozky ze sablonou a obrazjy
	 */
	define('TEMPLATE_FILE', dirname(__FILE__) . '/template.php');
	if (!is_readable(TEMPLATE_FILE)) die("Není možné načíst šablonu.");

	/**
	 * Pripojeni k databazi
	 */
	try
	{
		dibi::setConnection($config->getDatabase());
	}
	catch (DibiException $e)
	{
		die("Není možné se připojit k databázi.");
	}

	/**
	 * Kontrola prihlaseni
	 */
	$logged = session('root_logged') === TRUE;

	if (post('password') == $config['site']['root_password'])
	{
		setSession("root_logged", TRUE);
		redirect(BASEURL . 'anyshape/utils/installer/');
	}

	/**
	 * Vytvorim pole modulu
	 */
	$modules = array("CoreInstaller", "DeveloperInstaller", "AdministratorInstaller", "NewsletterInstaller",
		"ArticlesInstaller", "NewsInstaller", "FaqInstaller", "PollInstaller");

	foreach ($modules as &$module)
	{
//		require dirname(__FILE__) . "/modules/$module.php";

		$installer = new $module();

		if ($logged && get('install') == $module)
		{
			$installer->install();
			redirect(BASEURL . 'anyshape/utils/installer/');
		}

		if ($logged && get('uninstall') == $module)
		{
			$installer->uninstall();
			redirect(BASEURL . 'anyshape/utils/installer/');
		}

		$module = array(
			'id'			=> $module,
			'name'			=> $installer->getTitle(),
			'description'	=> $installer->getDescription(),
			'installed'		=> $installer->isInstalled()
		);
	}

	unset($module);

	/**
	 * Zobrazim sablonu
	 */
	require TEMPLATE_FILE;
