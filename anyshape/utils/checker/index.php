<?php

	require_once __DIR__ .'/../../bootstrap.php';

	@set_time_limit(0);

	/**
	 * Kontrola pripravenosti serveru pro beh AnyShape
	 */

	/**
	 * Kontrola PHP konfigurace
	 */
	foreach (array('function_exists', 'version_compare', 'extension_loaded', 'ini_get') as $function)
	{
		if (!function_exists($function)) {
			die("Nepodporovaná funkce '$function' je nutná pro fungování tohoto skriptu.");
		}
	}

	/**
	 * Kontrola slozky ze sablonou a obrazjy
	 */
	define('TEMPLATE_FILE', dirname(__FILE__) . '/template.php');
	if (!is_readable(TEMPLATE_FILE)) die("Není možné načíst šablonu.");

	$tests[] = array(
		'title' => 'Web server',
		'message' => $_SERVER['SERVER_SOFTWARE'],
	);

	$tests[] = array(
		'title' => 'Verze PHP',
		'required' => TRUE,
		'passed' => version_compare(PHP_VERSION, '5.4.0', '>='),
		'message' => PHP_VERSION,
		'description' => 'Verze PHP je zastaralá. Je nutná verze 5.2.0 nebo vyšší.',
	);

	$tests['sm'] = array(
		'title' => 'Safe mód',
		'required' => FALSE,
		'passed' => version_compare(PHP_VERSION, '5.3.0', '>=') || !ini_get('safe_mode'),
		'message' => "Neaktivní",
		'description' => 'Server běží v safe módu. Je nutné správně nastavit přístup na FTP.',
	);

	if (!$tests['sm']['passed'])
	{
		$result = checkFtp($config['ftp']);

		$tests[] = array(
			'title' => 'FTP hack',
			'required' => !$tests['sm']['passed'],
			'passed' => $result[0],
			'message' => $result[0] ? "Je funkční" : "Není funkční",
			'description' => $result[1],
		);
	}
	else
	{
		$tests[] = array(
			'title' => 'FTP hack',
			'required' => TRUE,
			'passed' => TRUE,
			'message' => 'Není potřeba',
			'description' => 'Není potřeba'
		);
	}

	$tests[] = array(
		'title' => 'Omezení paměti na skript',
		'message' => ini_get('memory_limit'),
	);

	$tests['hr'] = array(
		'title' => 'Mod_rewrite',
		'required' => TRUE,
		'passed' => in_array("mod_rewrite", apache_get_modules()),
		'description' => 'Mod_rewrite pravděpodobně není aktivní. '
	);

	$tests[] = array(
		'title' => 'Funkce ini_set()',
		'required' => FALSE,
		'passed' => function_exists('ini_set'),
		'description' => 'Funkce <code>ini_set()</code> není aktivní. Některé části systému AnyShape nemusí fungovat správně.',
	);

	$tests[] = array(
		'title' => 'Funkce flock()',
		'required' => TRUE,
		'passed' => flock(fopen(__FILE__, 'r'), LOCK_SH),
		'description' => 'Funkce <code>flock()</code> není podporována. Odesílání newsletterů může způsobovat potíže.',
	);

	$tests[] = array(
		'title' => 'Funkce set_time_limit()',
		'required' => FALSE,
		'passed' => function_exists('set_time_limit'),
		'description' => 'Funkce <code>set_time_limit()</code> není aktivní. Některé části systému AnyShape nemusí fungovat správně.',
	);

	$tests[] = array(
		'title' => 'Funkce register_shutdown_function()',
		'required' => FALSE,
		'passed' => function_exists('register_shutdown_function'),
		'description' => 'Funkce <code>register_shutdown_function()</code> není aktivní. Některé části systému AnyShape nemusí fungovat správně.',
	);

	$tests[] = array(
		'title' => 'Funkce error_reporting()',
		'required' => FALSE,
		'passed' => function_exists('error_reporting'),
		'description' => 'Funkce <code>error_reporting()</code> není aktivní. Některé části systému AnyShape nemusí fungovat správně.',
	);

	$tests[] = array(
		'title' => 'Rozšíření SimpleXML',
		'required' => TRUE,
		'passed' => class_exists('SimpleXMLElement'),
		'description' => 'Rozšíření SimpleXML je vyžadováno.',
	);

	$tests[] = array(
		'title' => 'Rozšíření DOMDocument',
		'required' => TRUE,
		'passed' => class_exists('DOMDocument'),
		'description' => 'Rozšíření DOMDocument je vyžadováno.',
	);

	$tests[] = array(
		'title' => 'Rozšíření PCRE',
		'required' => TRUE,
		'passed' => extension_loaded('pcre') && @preg_match('/pcre/u', 'pcre'),
		'message' => 'Aktivní a funguje správně',
		'errorMessage' => 'Neaktivní nebo bez podporu UTF-8',
		'description' => 'Rozšíření PCRE je vyžadováno a musí podporovat UTF-8.',
	);

	$tests[] = array(
		'title' => 'Rozšíření ICONV',
		'required' => TRUE,
		'passed' => extension_loaded('iconv') && (ICONV_IMPL !== 'unknown') && @iconv('UTF-16', 'UTF-8//IGNORE', iconv('UTF-8', 'UTF-16//IGNORE', 'test')) === 'test',
		'message' => 'Aktivní a funguje správně',
		'errorMessage' => 'Neaktivní nebo nefunguje správně',
		'description' => 'Rozšíření ICONV je vyžadováno a musí fungovat správně.',
	);

	$tests[] = array(
		'title' => 'Rozšíření Multibyte String',
		'required' => FALSE,
		'passed' => extension_loaded('mbstring'),
		'description' => 'Rozšíření Multibyte String není aktivní. Některé komponenty nemusí pracovat správně.',
	);

	$tests[] = array(
		'title' => 'Rozšíření GD',
		'required' => FALSE,
		'passed' => extension_loaded('gd'),
		'description' => 'Rozšíření GD není aktivní. Nebude možné aplikovat zmenšování obrázků nebo vodoznaky.',
	);

	$tests[] = array(
		'title' => 'Rozšíření ImageMagick',
		'required' => FALSE,
		'passed' => class_exists('Imagick'),
		'description' => 'Rozšíření ImageMagick není aktivní. Pro zmenšování obrázků nebo vodoznaky bude použito rozšíření GD.',
	);

	$tests[] = array(
		'title' => 'Proměnná SERVER_NAME',
		'required' => TRUE,
		'passed' => isset($_SERVER['SERVER_NAME']),
		'description' => 'Proměnná SERVER_NAME není dostupná.',
	);

	$tests[] = array(
		'title' => 'Proměnná SCRIPT_NAME',
		'required' => TRUE,
		'passed' => isset($_SERVER['SCRIPT_NAME']),
		'description' => 'Proměnná SCRIPT_NAME není dostupná.',
	);

	$tests[] = array(
		'title' => 'Administrační API',
		'required' => TRUE,
		'passed' => ((string) @file_get_contents($config['site']['server_path'] . 'admin/core/getLicence/')) === $config['site']['license_key'],
		'description' => 'Administrační API není dostupné. AnyShape není možné uvést do provozu.',
	);

	$result = checkPermissions($config['site']['files_path']);

	$tests[] = array(
		'title' => 'Možnost zápisu do "www/files"',
		'required' => TRUE,
		'passed' => $result !== FALSE && count($result) == 0,
		'description' => 'Adresář "files" nebo některé z vnořených adresářů/souborů nemají dostatečná oprávnění pro čtení a zápis.',
		'files' => $result
	);

	$result = checkPermissions($config['site']['data_path']);

	$tests[] = array(
		'title' => 'Možnost zápisu do "www/data"',
		'required' => TRUE,
		'passed' => $result !== FALSE && count($result) == 0,
		'description' => 'Adresář "data" nebo některé z vnořených adresářů/souborů nemají dostatečná oprávnění pro čtení a zápis.',
		'files' => $result
	);

	$result = checkAccess($config['site']['server_path']);

	$tests[] = array(
		'title' => 'Zamezení neoprávněnému přístupu',
		'required' => TRUE,
		'passed' => $result !== FALSE && count($result) == 0,
		'description' => 'Obsah některých chráněných adresářů je přístupný přes www server.',
		'files' => $result
	);

	$tests[] = array(
		'title' => 'Soubor .htaccess',
		'passed' => file_exists(BASEPATH . "/.htaccess"),
		'message' => 'Přítomen',
		'errorMessage' => 'Není k dispozici',
		'description' => "Pro správnou funkci je potřeba správně nakonfigurovaný .htaccess."
	);

	$tests[] = array(
		'title' => 'Soubor data/resized/.htaccess',
		'passed' => file_exists($config['site']['data_path'] . "/resized/.htaccess"),
		'message' => 'Přítomen',
		'errorMessage' => 'Není k dispozici',
		'description' => "Pro správnou funkci je potřeba správně nakonfigurovaný .htaccess."
	);

	if (isLocal())
	{
		$result = getRewriteBase(BASEPATH);

		$tests[] = array(
			'title' => 'RewriteBase pro root',
			'passed' => $result === getSubDir(),
			'message' => "Souhlasí",
			'errorMessage' => "Chyba",
			'description' => $result === FALSE  ? "Projekt je umístěn na vývojovém serveru a htaccess neobsahuje direktivu <code>RewriteBase /</code>. Je možné, že nebude fungovat přepisovaní URL." :
				$result !== getSubDir() ? 'Direktiva <code>"RewriteBase ' . $result . '"</code> neodpovídá umístění projektu (<code>' . getSubDir() . '</code>).' : ''
		);

		$result = getRewriteBase($config['site']['data_path']."/resized/");

		$tests[] = array(
			'title' => 'RewriteBase pro data/resized/',
			'passed' => $result === getSubDir() . "data/resized/",
			'message' => "Souhlasí",
			'errorMessage' => "Chyba",
			'description' => $result === FALSE  ? "Projekt je umístěn na vývojovém serveru a htaccess neobsahuje direktivu <code>RewriteBase " . getSubDir() . "data/resized/" . "</code>. Je možné, že nebudou fungovat operace s obrázky." :
				($result !== getSubDir() . "data/resized/" ? 'Direktiva <code>"RewriteBase ' . $result . '"</code> neodpovídá umístění projektu (<code>' . getSubDir() . "data/resized/" . '</code>).' : '')
		);

	}
	else
	{
		$result = getRewriteBase(BASEPATH);

		$tests[] = array(
			'title' => 'RewriteBase pro root',
			'passed' => $result === FALSE || $result === getSubDir(),
			'message' => $result === FALSE ? 'Není zadáno a není potřeba' : "Souhlasí",
			'errorMessage' => "Chyba",
			'description' => 'Direktiva <code>RewriteBase ' . $result . '</code> neodpovídá umístění projektu (<code>' . getSubDir() . '</code>). Pravděpodobně je možné direktivu úplně odstranit.'
		);

		$result = getRewriteBase($config['site']['data_path']."/resized/");

		$tests[] = array(
			'title' => 'RewriteBase pro data/resized/',
			'passed' => $result === FALSE || $result === getSubDir() . "data/resized/",
			'message' => $result === FALSE ? 'Není zadáno a není potřeba' : "Souhlasí",
			'errorMessage' => "Chyba",
			'description' => 'Direktiva <code>RewriteBase ' . $result . '</code> neodpovídá umístění projektu (<code>' . getSubDir() . "data/resized/" . '</code>). Pravděpodobně je možné direktivu úplně odstranit.'
		);

	}

	$result = checkSitemapsConfig();

	$tests[] = array(
		'title' => 'Korektní konfigurace Sitemaps',
		'required' => TRUE,
		'passed' => count($result) == 0,
		'message' => "V pořádku",
		'errorMessage' => "Chyba",
		'description' => 'Pro správnou funkci utility Sitemaps je potřeba ji správně <a href="../sitemaps/">nakonfigurovat</a>.',
		'items' => $result
	);

	$result = checkPermissions(BASEPATH . "/anyshape/utils/sitemaps/data/");

	$tests[] = array(
		'title' => 'Možnost zápisu do "sitemaps/data"',
		'required' => TRUE,
		'passed' => $result !== FALSE && count($result) == 0,
		'description' => 'Utilita Sitemaps nebude schopná zapsat do adresáře "sitemaps/data".',
		'files' => $result
	);

	$result = checkPermissions(BASEPATH . "/anyshape/utils/sitemaps/files/");

	$tests[] = array(
		'title' => 'Možnost zápisu do "sitemaps/files"',
		'required' => TRUE,
		'passed' => $result !== FALSE && count($result) == 0,
		'description' => 'Utilita Sitemaps nebude schopná zapsat do adresáře "sitemaps/files".',
		'files' => $result
	);

	paint($tests);

	function checkSitemapsConfig()
	{
		global $config;
		$result  = array();
		$configContents = @file_get_contents(BASEPATH . "/anyshape/utils/sitemaps/data/generator.conf");

		if (!$configContents || strlen($configContents) == 0)
		{
			$result[] = "Konfigurační soubor " . "<code>anyshape/utils/sitemaps/data/generator.conf</code>" . " nebyl nalezen.";
		}
		else
		{
			$configXML = XML($configContents);
			if (!$configXML)
			{
				$result[] = "Konfigurační soubor " . "<code>anyshape/utils/sitemaps/data/generator.conf</code>" . " není ve správném formátu.";
			}
			else
			{
				foreach ($configXML->option as $option)
				{
					switch ($option['name'])
					{
						case "xs_initurl":
							if ($option != $config['site']['server_path'])
								$result[] = "Není správně nastavena direktiva <strong>Starting URL</strong> na <code>" . $config['site']['server_path'] . "</code>";
							break;
						case "xs_smname":
							if ($option != BASEPATH . "/anyshape/utils/sitemaps/files/sitemap.xml")
								$result[] = "Není správně nastavena direktiva <strong>Save sitemap to</strong> na <code>" . BASEPATH . "/anyshape/utils/sitemaps/files/sitemap.xml</code>";
							break;
						case "xs_smurl":
							if ($option != $config['site']['server_path'] . "sitemap.xml")
								$result[] = "Není správně nastavena direktiva <strong>Your Sitemap URL</strong> na <code>" . $config['site']['server_path'] . "sitemap.xml</code>";
							break;
						case "xs_utf8":
							if ($option != "1")
								$result[] = "Není správně aktivní direktiva <strong>UTF8 charset</strong>.";
							break;
					}
				}
			}
		}

		return $result;
	}

	/**
	 * Zkontroluje, zda-li všechny souboru v zadaném adresáři jsou dostupné pro zápis
	 *
	 * @param string $folder Adresář
	 */
	function checkPermissions($dir)
	{
		$result = array();
        $dir = rtrim($dir, '\\/');

		if ($fp = @opendir($dir))
		{
			while (FALSE !== ($file = readdir($fp)))
			{
				if ($file != ".." && $file != ".")
				{
					if (!is_readable($dir . $file) || !is_writable($dir . $file)) $result[] = $dir . '/' . $file;

					if (is_dir($dir . $file))
					{
						$result = array_merge($result, checkPermissions($dir . $file . "/"));
					}
				}
			}
		} else return FALSE;

		return $result;
	}

	/**
	 * Zkontroluje funkcnost zemezeni pristupu do urcitych adresaru
	 * @param string $baseURL
	 */
	function checkAccess($baseURL)
	{
		global $config;

		$result = array();

		$dirs = array($config['site']['scripts_dir'], $config['site']['recycle_bin_dir'], "data/libraries/", $config['site']['temp_dir'], "app/");

		foreach ($dirs as $dir)
		{
			$f = $baseURL . $dir . "index.htm";
			@file_get_contents($f);
			$h = @$http_response_header[0];
            $match = NULL;
            preg_match("/([0-9]){3,3}/", $h, $match);

			if ($match[0] < 400) $result[] = "<strong>$f</strong> ($h)";
		}

		return $result;
	}

	/**
	 * Připojí se na zadané FTP a zkontroluje, jestli se tam nachází adresář "anyshape".
	 */
	function checkFtp($config)
	{
		if (!$config['hack']) return array(FALSE, "FTP hack není povolen. Ověř konfiguraci systému AnyShape.");

		$conn_id = ftp_connect($config['host']);
		if ($conn_id === false) return array(FALSE, "Serveru odmítl spojení nebo nereaguje.");

		$login_result = @ftp_login($conn_id, $config['username'], $config['password']);
		if ($login_result === false) return array(FALSE, "Nepodařilo se přihlášení k serveru.");

		if (@ftp_chdir($conn_id, $config['path'] . "anyshape/") === FALSE)
			return array(FALSE, 'Na serveru nebyl nalezen adresář "anyshape".');

		return array(TRUE, "Adresáře na serveru budou vytvářeny pomocí FTP hacku.");
	}

	/**
	 * Vrati nastaveni RewriteBase pro zadanou cestu.
	 *
	 * @param string $path
	 */
	function getRewriteBase($path = '')
	{
        $path = rtrim($path, '/\\');
		$content = @file_get_contents("$path/.htaccess");
		$pattern = "/RewriteBase ([^\s]+)/";
		preg_match($pattern, $content, $matches);
		if (isset($matches[1])) return $matches[1];
		return FALSE;
	}

	/**
	 * Vrati adresar, ve kterem je umisten projekt, vzhledem ke koreni webu (s lomitkem na konci).
	 */
	function getSubdir()
	{
		return str_replace("anyshape/utils/checker/index.php", "", @$_SERVER['SCRIPT_NAME']);
	}

	/**
	 * Paints checker.
	 * @param  array
	 * @return void
	 */
	function paint($requirements)
	{
		$errors = $warnings = FALSE;

		foreach ($requirements as $id => $requirement)
		{
			$requirements[$id] = $requirement = (object) $requirement;
			if (isset($requirement->passed) && !$requirement->passed) {
				if (isset($requirement->required) && $requirement->required) {
					$errors = TRUE;
				} else {
					$warnings = TRUE;
				}
			}
		}

		global $config;
		require TEMPLATE_FILE;
	}


	/**
	 * Gets the boolean value of a configuration option.
	 * @param  string  configuration option name
	 * @return bool
	 */
	function iniFlag($var)
	{
		$status = strtolower(ini_get($var));
		return $status === 'on' || $status === 'true' || $status === 'yes' || (int) $status;
	}
