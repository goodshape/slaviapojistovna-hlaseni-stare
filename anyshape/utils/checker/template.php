<?
/**
 * Nette Framework Requirements Checker template.
 *
 * @param array    $requirements
 * @param bool     $errors
 * @param bool     $warnings
 */

header('Content-Type: text/html; charset=utf-8');
header('Cache-Control: s-maxage=0, max-age=0, must-revalidate');
header('Expires: Mon, 23 Jan 1978 10:00:00 GMT');
?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta name="robots" content="noindex">

		<title>AnyShape - kontrola nastavení prostředí</title>

		<style type="text/css">
			html { font: 13px/1.5 Verdana, sans-serif; border-top: 130px solid #F4F4F4; }
			body { border-top: 1px solid #E4DED5; margin: 0; background: white; color: #333; }
			#wrapper { max-width: 780px; margin: -140px auto 0; }
			h1 { font: 32px sans-serif; margin: 0; padding: 60px 0 0 0; background: url(../../manual/anyshape.png) right center no-repeat; color: #7A7772; height: 90px; text-shadow: 0 -1px 0 #FFF; }
			h2 { font-size: 2em; font-weight: normal;color: #3484D2; margin: .7em 0; }
			p { margin: 1.2em 0; }
			a { color: #606060; }
			.result { margin: 1.5em 0; padding: 0 1em; border: 2px solid white; }
			.passed h2 { color: #1A7E1E; }
			.failed h2 { color: white; }
			table { padding: 0; margin: 20px 0 0 0; border-collapse: collapse; width: 100%; }
			table td, table th { text-align: left; padding: 10px; vertical-align: top; border-style: solid; border-width: 1px 0 0; border-color: inherit; background: white none no-repeat 12px 8px; background-color: inherit; }
			table th { font-size: 105%; font-weight: bold; padding-left: 50px; width: 300px;}
			.passed, .info { background-color: #E4F9E3; border-color: #C6E5C4; }
			.passed th { background-image: url('../assets/passed.gif'); }
			.info th { background-image: url('../assets/info.gif'); }
			.warning { background-color: #FEFAD4; border-color: #EEEE99; }
			.warning th { background-image: url('../assets/warning.gif'); }
			.failed { background-color: #F4D2D2; border-color: #D2B994; }
			div.failed { background-color: #CD1818; border-color: #CD1818; }
			.failed th { background-image: url('../assets/failed.gif'); 	}
			.description td {border-top: none !important; padding: 0 10px 10px 50px; color: #555; }
			.passed.description { display: none; }
			a.filesLink { font-size: 10px; color: #606060; }
			.files { display: none; color: #808080; }
			code { background: #DDD; }

		</style>

	</head>

	<body>
		<div id="wrapper">

			<h1>Kontrola nastavení prostředí</h1>

			<table>

			<? foreach ($requirements as $id => $requirement):?>

				<? $class = isset($requirement->passed) ? ($requirement->passed ? 'passed' : (isset($requirement->required) && $requirement->required ? 'failed' : 'warning')) : 'info' ?>

				<tr id="res<? echo $id ?>" class="<? echo $class ?>">

					<th><? echo htmlSpecialChars($requirement->title) ?></th>

					<? if (empty($requirement->passed) && isset($requirement->errorMessage)): ?>
						<td><? echo htmlSpecialChars($requirement->errorMessage) ?></td>
					<? elseif (isset($requirement->message)): ?>
						<td><? echo htmlSpecialChars($requirement->message) ?></td>
					<? elseif (isset($requirement->passed)): ?>
						<td><? echo $requirement->passed ? 'Aktivní' : 'Neaktivní' ?></td>
					<? else: ?>
						<td>Not tested</td>
					<? endif ?>
				</tr>

				<? if (isset($requirement->description)): ?>

				<tr id="desc<? echo $id ?>" class="<? echo $class ?> description">

					<td colspan="2"><? echo $requirement->description ?>

						<? if (isset($requirement->files) && count($requirement->files) > 0): ?>
							<a href="#" class="filesLink" onclick="document.getElementById('files<?=$id?>').style.display = 'block'; return false;">Zobrazit soubory</a>
							<div id="files<?=$id?>" class="files">
								<small>
								<? foreach ($requirement->files as $file): ?>
									<?=$file?><br/>
								<? endforeach; ?>
								</small>
							</div>
						<? endif; ?>

						<? if (isset($requirement->items) && count($requirement->items) > 0): ?>
							<a href="#" class="filesLink" onclick="document.getElementById('files<?=$id?>').style.display = 'block'; return false;">Zobrazit položky</a>
							<div id="files<?=$id?>" class="files">
								<small>
								<? foreach ($requirement->items as $item): ?>
									<?=$item?><br/>
								<? endforeach; ?>
								</small>
							</div>
						<? endif; ?>

					</td>

				</tr>

				<? endif ?>

			<? endforeach ?>
			</table>

			<p><small>AnyShape verze <?= $config['site']['version']?></small></p>
		</div>
	</body>
</html>