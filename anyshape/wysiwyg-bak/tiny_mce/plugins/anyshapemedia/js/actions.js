/* Functions for the advlink plugin popup */

tinyMCEPopup.requireLangPack();

function init() {

	tab = 1;
	document.getElementById('hrefbrowsercontainer').innerHTML = getBrowserHTML('hrefbrowser','href','video','advlink');
	document.getElementById('srcbrowsercontainer').innerHTML = getBrowserHTML('srcbrowser','src','image','advlink');

	var ed = tinyMCEPopup.editor;
	var f = document.forms[0];

	fe = ed.selection.getNode();
	if (/mceItemFlash/.test(ed.dom.getAttrib(fe, 'class'))) {

		f.insert.value = ed.getLang('update', 'Insert', true);
		
		var pl = tinyMCEPopup.editor.plugins.media._parse(fe.title);
		var flashVars = parseQuery(pl.FlashVars);

		if (/youtube/.test(pl.src)) {

			f.code.value = '<object width="'+fe.width+'" height="'+fe.height+'">' +
			'<param name="movie" value="http://www.youtube.com/v/GncHbWMIJws&hl=cs&fs=1&"></param>' +
			'<param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param>' +
			'<embed src="'+fe.src+'" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="'+fe.width+'" height="'+fe.height+'"></embed>' +
			'</object>';
			tab = 2;
			mcTabs.displayTab('youtube_tab','youtube_panel');

		} else {

			if (flashVars) {
				f.href.value = flashVars.flv;
				f.src.value = flashVars.startimage;
				f.width.value = fe.width;
				f.height.value = fe.height;
				f.autoplay.checked = flashVars.autoplay == 1;
				f.volume.selectedIndex = flashVars.volume / 25;
				f.showbar1.checked = flashVars.showplayer == 'autohide';
				f.showbar2.checked = flashVars.showplayer == 'always';
				f.showbar3.checked = flashVars.showplayer == 'never';
				f.showstop.checked = flashVars.showstop == 1;
				f.showvolume.checked = flashVars.showvolume == 1;
				f.showfullscreen.checked = flashVars.showfullscreen == 1;
				f.showtime.checked = flashVars.showtime == 1;
			}

		}

	}

}

function parseQuery(query) {
	if (!query) return null;
	var queryArray = query.split('&');
	var result = [];
	for (var i = 0; i < queryArray.length; i++) {
		var item = queryArray[i].split('=');
		if (item && item.length == 2) result[item[0]] = item[1];
	}
	return result;
}

function insertAction() {

	var inst = tinyMCEPopup.editor;
	var ed = tinyMCEPopup.editor;
	var elm = inst.dom.getParent(elm, "DIV");

	tinyMCEPopup.execCommand("mceBeginUndoLevel");

	if (elm == null) {
		// novy objekt

		if (tab == 1) {
			var width = document.getElementById('width').value;
			var height = document.getElementById('height').value;
			var flv = document.getElementById('href').value;
			var img = document.getElementById('src').value;
			var baseURL = tinyMCE.activeEditor.documentBaseURI.getURI();
			var params = '';
			params += document.getElementById('autoplay').checked ? '&autoplay=1' : '';
			params += '&volume=' + document.getElementById('volume').options[document.getElementById('volume').selectedIndex].value;
			if (document.getElementById('showbar3').checked) {
				params += '&showplayer=never';
			} else {
				if (document.getElementById('showbar2').checked) params += '&showplayer=always';
				else params += '&showplayer=autohide';
				params += document.getElementById('showstop').checked ? '&showstop=1' : '';
				params += document.getElementById('showvolume').checked ? '&showvolume=1' : '';
				params += document.getElementById('showfullscreen').checked ? '&showfullscreen=1' : '';
				params += document.getElementById('showtime').checked ? '&showtime=1' : '';
				
			}
			object = '<object type="application/x-shockwave-flash" data="'+baseURL+'flash/player_flv_maxi.swf" width="'+width+'" height="'+height+'">' +
				'<param name="movie" value="'+baseURL+'flash/player_flv_maxi.swf" />' +
				'<param name="allowFullScreen" value="true" />' +
				'<param name="FlashVars" value="flv='+flv+'&startimage='+img+'&margin=0'+params+'" />' +
				'</object>';
		} else {
			object = document.getElementById('code').value;
		}

		ed.execCommand('mceInsertContent', false, object);

	} else {
		// update stavajiciho objektu
	}

	tinyMCEPopup.execCommand("mceEndUndoLevel");
	tinyMCEPopup.close();
}

function barHandler() {
	var flag = document.getElementById('showbar3').checked;
	document.getElementById('showstop').disabled = flag;
	document.getElementById('showvolume').disabled = flag;
	document.getElementById('showfullscreen').disabled = flag;
	document.getElementById('showtime').disabled = flag;
}

tinyMCEPopup.onInit.add(init);
