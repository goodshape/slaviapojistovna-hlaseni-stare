/**
 * $Id: editor_plugin_src.js 1037 2009-09-02 16:41:15Z spocke $
 *
 * @author Hugo
 * @copyright Copyright � 2009 GoodShape, All rights reserved.
 */

(function() {
	tinymce.create('tinymce.plugins.AnyShapeMediaPlugin', {

		init : function(ed, url) {
			ed.addCommand('mceAnyShapeMedia', function() {
				ed.windowManager.open({
					file : url + '/dialog.htm',
					width : 470,
					height : 380,
					inline : 1
				}, {
					plugin_url : url
				});
			});

			ed.addButton('anyshapemedia', {
				title : 'media.desc',
				cmd : 'mceAnyShapeMedia',
				image : url + '/img/film.png'
			});

		},

		getInfo : function() {
			return {
				longname : 'AnyShapeMedia plugin',
				author : 'Hugo / GoodShape',
				authorurl : 'http://www.goodshape.cz/',
				infourl : 'http://www.goodshape.cz/',
				version : "1.0"
			};
		}
	});

	tinymce.PluginManager.add('anyshapemedia', tinymce.plugins.AnyShapeMediaPlugin);
})();
