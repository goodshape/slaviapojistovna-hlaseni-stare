tinyMCE.addI18n('cs.anyshapemedia_dlg',{
title:"Vlo\u017Eit/editovat video soubory",
settings:"Nastaven\u00ED",
flash_video:"Flash video (FLV)",
youtube_video:"Youtube video",

flv_file:"Video soubor",
img_file:"\u00DAvodn\u00ED obr\u00E1zek",
dimensions:"Rozm\u011Bry",
autoplay:"P\u0159ehr\u00E1t automaticky",
volume:"Hlasitost",
bar:"Ovl\u00E1dac\u00ED li\u0161ta",
show_bar:"Ovl\u00E1dac\u00ED li\u0161tu",
always:"Zobrazit",
never:"Nezobrazit",
autohide:"Automaticky skr\u00FDt",
show_stop:"Zobrazit tla\u010D\u00EDtko stop",
show_volume:"Zobrazit ovl\u00E1d\u00E1n\u00ED hlasitosti",
show_fullscreen:"Umo\u017Enit pu\u0159ehr\u00E1v\u00E1n\u00ED na celou obrazovku",
show_time:"Zobrazit \u010Das",

code:"K\u00F3d videa",
put_code_here:"Zde vlo\u017Ete vygenerovan\u00FD k\u00F3d videa..."

});