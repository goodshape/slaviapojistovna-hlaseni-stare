/**
 * $Id: editor_plugin_src.js 1037 2009-09-02 16:41:15Z spocke $
 *
 * @author Hugo
 * @copyright Copyright � 2009 GoodShape, All rights reserved.
 */

(function() {

	var css_ids, body_id;
	// tinymce.PluginManager.requireLangPack('multicss');

	tinymce.create('tinymce.plugins.MultiCSSPlugin', {
	
		init : function(ed, url) {
			css_ids = ed.settings.css_ids;
			body_id = ed.settings.body_id;
		},

		createControl: function(n, cm) {
			switch (n) {
				case 'multicss':
					if (css_ids) {

						var ids = css_ids.split(',');

						if (ids && ids.length > 0) {
						
							var mlb = cm.createListBox('multicss', {
								title: tinyMCE.activeEditor.getLang('multicss.title', 'Layout'),
								onselect : function(v) {
									var ed = tinyMCE.activeEditor;
									if (v && v.length > 0)
										ed.contentDocument.getElementsByTagName('body')[0].id = v;
									else
										ed.contentDocument.getElementsByTagName('body')[0].id = body_id;
								}
							});

							for (var i = 0; i < ids.length; i++)
							{
								var item = ids[i].split('=');
								mlb.add(item[0], item[1]);
							}

							// Return the new listbox instance
							return mlb;

						}
					} 
					
					return null;

			}

			return null;
		},

		getInfo : function() {
			return {
				longname : 'MultiCSS plugin',
				author : 'Hugo / GoodShape',
				authorurl : 'http://www.goodshape.cz/',
				infourl : 'http://www.goodshape.cz/',
				version : "1.0"
			};
		}
	});

	tinymce.PluginManager.add('multicss', tinymce.plugins.MultiCSSPlugin);
})();
