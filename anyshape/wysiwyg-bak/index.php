<?php
require_once __DIR__ .'/../bootstrap.php';
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		
		<title>wysiwiyg</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8"/>

		<style type="text/css">
			v\:* {behavior:url(#default#VML);}
			* { padding: 0; margin: 0; }
			body { width: 100%; height: 100%; overflow: hidden; }
			#iframe_wysiwyg { width: 100%; height: 100%; }
		</style>

		<script type="text/javascript" src="./tiny_mce/tiny_mce.js"></script>
		<script type="text/javascript" src="./tiny_mce/tiny_mce_gzip.js"></script>

		<script type="text/javascript">
			tinyMCE_GZ.init({
				plugins : "table,advimage,advlink,inlinepopups,contextmenu,paste,advimagescale,media,anyshapemedia,multicss",
				themes : 'advanced',
				languages : 'en,cs',
				disk_cache : true,
				debug : false
			});
		</script>

		<script type="text/javascript">

			//<![CDATA[

				tinyMCE.init({
					// General options
					mode: "textareas",
					theme: "advanced",
					plugins: "table,advimage,advlink,inlinepopups,contextmenu,paste,advimagescale,media,anyshapemedia,multicss",
					width: "100%",
					height: "100%",

					// Theme options
					theme_advanced_buttons1: "undo,redo,|,bold,italic,underline,strikethrough,|,sub,sup,|,justifyleft,justifycenter,justifyright,justifyfull,|,bullist,numlist,table,|,styleselect,formatselect,|,link,unlink,anchor,image,anyshapemedia,code",
					theme_advanced_buttons2: "",
					theme_advanced_buttons3: "",
					theme_advanced_toolbar_location: "top",
					theme_advanced_toolbar_align: "left",
					theme_advanced_statusbar_location: "bottom",
					theme_advanced_resizing: false,

					// Image resize plugin
					cleanup_on_startup: false,
					advimagescale_max_width: 400,
					advimagescale_max_height: 400,
					advimagescale_min_width: 16,
					advimagescale_min_height: 16,

					// Povolene elementy
					theme_advanced_blockformats : "p,h2,h3,h4",
					valid_elements :
						"@[id|class|title|style]," +
						"a[rel|rev|name|href|target|title|style]," +
						"-strong/b,-em/i,-strike,-u," +
						"-sub,-sup," +
						"#p[align|style]," +
						"+span[style]," +
						"-ol[type|compact]," +
						"-ul[type|compact]," +
						"-li," +
						"br,img[src|alt=|style|title|hspace|vspace|width|height|align]," +
						"-h2,-h3,-h4,-h5,-h6," +
						"+table[class|style|width|border|cellspacing|cellpadding|bgcolor|align],+tbody,+thead,+tfoot," +
						"+tr[class|style|bgcolor]," +
						"+td[class|style|width|height|colspan|rowspan|align|valign|bgcolor]," +
						"+th[class|style|width|height|colspan|rowspan|align|valign|bgcolor]," +
						"+object[*],+embed[*],+param[*]",
					verify_css_classes: true,
					inline_styles: false,
					// imlicitini id layoutu
					body_id: 'default',

					// styly pro editor (vsechny v jednom souboru rozlisene pomoci id)
					content_css: '<?=$config['site']['server_path']?>webtemp/css/mce.css',

					// nazvy a id ruznych rozvrzeni v ramci css (ve fromatu Nazev=id,Nazev2=id2,...)
					css_ids: 'Hlavní styl=default,Alternativní styl=alternative',
					
					// jednotlive styly, ktere budou uzivatelum k dispozici v nabidce editoru ve skupinach dle id layoutu
					style_formats : [
						{title: 'Šedá', inline: 'span', classes: 'c-shade'},
						{title: 'Oranžová', inline: 'span', classes: 'c-brick'},
						{title: 'VELKÁ', inline: 'span', classes: 'upper'},
						{title: 'malé písmo', inline: 'span', classes: 'small'},

					],

					init_instance_callback: 'wysiwygInit',
					entity_encoding: 'raw',
					language: '<?= get('lang', 'cz') == 'cz' ? 'cs' : 'en' ?>',
					file_browser_callback: 'anyshapeFileBrowser',
					relative_urls: false,
					remove_script_host: false,
					document_base_url: '<?=$config['site']['server_path']?>',
					external_link_list_url: '<?=$config['site']['server_path']?>anyshape/wysiwyg/urllist.php',
					verify_html : false,
					paste_auto_cleanup_on_paste: true,
					extended_valid_elements :"*[*]"

				});

				wysiwygInit = function(editor) {
					// event handlery na klavesy (ESC)
					tinymce.dom.Event.add(tinyMCE.activeEditor.getDoc(), "keypress", keyHandler); 
					tinymce.dom.Event.add(parent.window, "resize", resizeHandler);
				}

				function anyshapeFileBrowser(field_name, url, type, win) {
					var anyShape = parent.document.getElementById('AnyShape');
					browserWin = win;
					if (type == "image" || type == "file" || type == "video") anyShape.showFileManager(type, url);
					last_field_name = field_name;
				}
				
				function insertFile(src, name, description) {
					var hrefInput = browserWin.document.getElementById(last_field_name);
					var altInput = browserWin.document.getElementById('alt');
					var titleInput = browserWin.document.getElementById('title');
					if (hrefInput) hrefInput.value = src;
					if (altInput && description) altInput.value = description;
					if (titleInput && name) titleInput.value = name;
				}

				function keyHandler(event) {
					var anyShape = parent.document.getElementById('AnyShape');
					if (event.keyCode == 27 && anyShape) anyShape.closePanel();
				}

				function setContent(text) {
					tinyMCE.activeEditor.setContent(unescape(text));
					resizeHandler();
				}

				function getContent() {
					return escape(tinyMCE.activeEditor.getContent());
				}

				function resizeHandler(event) {
					var height = parent.document.body.clientHeight;
					document.getElementById('iframe_wysiwyg_tbl').style.width = '100%';
					document.getElementById('iframe_wysiwyg_tbl').style.height = (height - 171) + "px";
					document.getElementById('iframe_wysiwyg_ifr').style.height = (height - 171) + "px";
				}
			//]]>
		</script>
	</head>

	<body>
		<textarea id="iframe_wysiwyg" name="wysiwyg" rows="15" cols="80"></textarea>
	</body>

</html>