<?php
	require_once __DIR__ .'/../bootstrap.php';

	dibi::setConnection($config->getDatabase());

	session('language_id', 'cz');
	
	function walkTree($parent_id = 0, $depth = 0)
	{
		$lang = session('language_id', 'cz');

		$branch = dibi::query(
			'SELECT s.id, name.text AS name, url.url_string FROM [:data:structure] AS s '.
			'LEFT JOIN [:data:texts] AS name ON s.name = name.id AND name.language_id = %s', $lang,
			'LEFT JOIN [:data:url_lookup_table] AS url ON url.structure_id = s.id AND url.language_id = %s', $lang,
			'WHERE [parent_id] = %i', $parent_id, 
			'ORDER BY [position] ASC'
		)->fetchAssoc('id');

		$str = '';

		foreach ($branch as $id => $subbranch)
		{
			$padding = str_repeat("&nbsp;&nbsp;&nbsp;", $depth);
			$name = htmlspecialchars(element('name', $subbranch, ''));
			$segment = element('url_string', $subbranch, '');
			$url = BASEURL . $lang . '/'. $segment;
			if ($segment != '') $url .= "/";

			$str .= '["'. $padding . $name . '", "' . $url . "\"], \n";
			$str .= walkTree($id, $depth+1);
		}

		return $str;
	}

	header('Content-type: text/javascript; charset=utf-8');
	echo "var tinyMCELinkList = new Array(\n";
	echo substr(walkTree(), 0, -3);
	echo ");";
